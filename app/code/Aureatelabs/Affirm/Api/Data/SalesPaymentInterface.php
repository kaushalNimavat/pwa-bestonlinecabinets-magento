<?php
namespace Aureatelabs\Affirm\Api\Data;

interface SalesPaymentInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ORDER_ID          = 'order_id';
    const ADDITIONAL_INFO   = 'additional_info';
    const METHOD            = 'method';
    
    /**
     * @param $orderId
     * @return mixed
     */
    public function setOrderId($orderId);

    /**
     * @return mixed
     */
    public function getOrderId();

    /**
     * @param $additionalInfo
     * @return mixed
     */
    public function setAdditionalInfo($additionalInfo);

    /**
     * @return mixed
     */
    public function getAdditionalInfo();

    /**
     * @param $method
     * @return mixed
     */
    public function setMethod($method);

    /**
     * @return mixed
     */
    public function getMethod();
}
