<?php
namespace Aureatelabs\Affirm\Api\Data;

interface SalesPaymentResponseInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const MESSAGE       = 'message';
    
    /**
     * @param $message
     * @return mixed
     */
    public function setMessage($message);

    /**
     * @return mixed
     */
    public function getMessage();
}
