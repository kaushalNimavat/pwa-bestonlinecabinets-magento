<?php
namespace Aureatelabs\Affirm\Api;

use Aureatelabs\Affirm\Api\Data\SalesPaymentInterface;

interface UpdateSalesPaymentInterface
{
    /**
     * @param SalesPaymentInterface $paymentInfo
     * @return \Aureatelabs\Affirm\Api\Data\SalesPaymentResponseInterface
     */
    public function updateSalesPaymentInfo(SalesPaymentInterface $paymentInfo);
}
