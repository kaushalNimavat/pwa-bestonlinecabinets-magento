<?php
namespace Aureatelabs\Affirm\Model;

use Aureatelabs\Affirm\Api\Data\SalesPaymentInterface;
use Magento\Framework\Model\AbstractModel;

class SalesPayment extends AbstractModel implements SalesPaymentInterface
{
    /**
     * @param $orderId
     * @return SalesPayment|mixed
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * @param $additionalInfo
     * @return SalesPayment|mixed
     */
    public function setAdditionalInfo($additionalInfo)
    {
        return $this->setData(self::ADDITIONAL_INFO, $additionalInfo);
    }

    /**
     * @return mixed
     */
    public function getAdditionalInfo()
    {
        return $this->getData(self::ADDITIONAL_INFO);
    }

    /**
     * @param $method
     * @return SalesPayment|mixed
     */
    public function setMethod($method)
    {
        return $this->setData(self::METHOD, $method);
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->getData(self::METHOD);
    }
}
