<?php
namespace Aureatelabs\Affirm\Model;

use Aureatelabs\Affirm\Api\Data\SalesPaymentResponseInterface;
use Magento\Framework\Model\AbstractModel;

class SalesPaymentResponse extends AbstractModel implements SalesPaymentResponseInterface
{
    /**
     * @param $message
     * @return SalesPaymentResponse|mixed
     */
    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }
}
