<?php
namespace Aureatelabs\Affirm\Model;

use Aureatelabs\Affirm\Api\Data\SalesPaymentInterface;
use Aureatelabs\Affirm\Api\UpdateSalesPaymentInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Api\InvoiceOrderInterface;
use Aureatelabs\Affirm\Api\Data\SalesPaymentResponseInterface;

class UpdateSalesPayment implements UpdateSalesPaymentInterface
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var InvoiceOrderInterface
     */
    protected $invoiceOrder;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @var SalesPaymentResponseInterface
     */
    protected $paymentResponseInterface;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * UpdateSalesPayment constructor.
     * @param OrderRepository $orderRepository
     * @param InvoiceOrderInterface $invoiceOrder
     * @param Json $json
     * @param SalesPaymentResponseInterface $paymentResponseInterface
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        OrderRepository $orderRepository,
        InvoiceOrderInterface $invoiceOrder,
        Json $json,
        SalesPaymentResponseInterface $paymentResponseInterface,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->orderRepository = $orderRepository;
        $this->invoiceOrder = $invoiceOrder;
        $this->json = $json;
        $this->resource = $resource;
        $this->paymentResponseInterface = $paymentResponseInterface;
    }

    /**
     * @param SalesPaymentInterface $paymentInfo
     * @return SalesPaymentResponseInterface
     */
    public function updateSalesPaymentInfo(SalesPaymentInterface $paymentInfo)
    {   
        $orderId = $paymentInfo->getOrderId();
        $additionalInfo = $paymentInfo->getAdditionalInfo();
        $method = $paymentInfo->getMethod();
    
        try {
            $order = $this->orderRepository->get($orderId);
            if (!empty($additionalInfo)) {
                try {
                    $connection = $this->resource->getConnection();
                    $tableName = $connection->getTableName('sales_order_payment');

                    $query = "SELECT * FROM `" . $tableName . "` WHERE parent_id = $orderId ";
                    $result = $connection->fetchRow($query);
                    
                    if(isset($result['additional_information'])&&isset($additionalInfo['checkout_token']) && isset($additionalInfo['charge_id'])){

                        $resultArray = $this->json->unserialize($result['additional_information']);
                        
                        foreach($resultArray as $k=>$v){

                            if($k == 'checkout_token'){
                                $resultArray[$k] = $additionalInfo['checkout_token'];
                            }
                            if($k == 'charge_id'){
                                $resultArray[$k] = $additionalInfo['charge_id'];
                            }
                        }

                        if(!isset($resultArray['checkout_token'])){
                            $resultArray['checkout_token'] = $additionalInfo['checkout_token'];
                        }

                        if(!isset($resultArray['charge_id'])){
                            $resultArray['charge_id'] = $additionalInfo['charge_id'];
                        }

                        $payload = [
                            'additional_information' => $this->json->serialize($resultArray),
                            'last_trans_id' => $additionalInfo['charge_id']
                        ];
                        
                        $connection->update($tableName, $payload, 'parent_id = ' . $orderId);
                    }

                } catch (\Exception $e) {
                    $this->paymentResponseInterface->setMessage($e->getMessage());
                    return $this->paymentResponseInterface;
                }
            }
            /*
            // Update status to processing if not
            if ($order->getStatus() !== 'processing') {
                $order->setStatus('processing')->setState('processing');
                $this->orderRepository->save($order);
            } */
            $this->paymentResponseInterface->setMessage('success');
        } catch (\Exception $e) {
            $this->paymentResponseInterface->setMessage($e->getMessage());
        }
        return $this->paymentResponseInterface;
    }
}
