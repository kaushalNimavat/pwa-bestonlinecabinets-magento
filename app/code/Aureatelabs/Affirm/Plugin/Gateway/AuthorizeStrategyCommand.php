<?php
namespace Aureatelabs\Affirm\Plugin\Gateway;
use Magento\Payment\Gateway\Command;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;

class AuthorizeStrategyCommand extends \Astound\Affirm\Gateway\Command\AuthorizeStrategyCommand
{
    
    /**
     * Pre authorize
     */
    const PRE_AUTHORIZE = 'pre_authorize';

    /**
     * Order authorize
     */
    const ORDER_AUTHORIZE = 'order_authorize';

    /**
     * Command pool
     *
     * @var Command\CommandPoolInterface
     */
    private $commandPool;

    /**
     * Constructor
     *
     * @param Command\CommandPoolInterface $commandPool
     */
    public function __construct(
        Command\CommandPoolInterface $commandPool
    ) {
        parent::__construct($commandPool);
    }

    /**
     * Executes command basing on business object
     *
     * @param array $commandSubject
     * @return null|Command\ResultInterface
     * @throws LocalizedException
     */
    public function execute(array $commandSubject)
    {
        return null;
    }
}
