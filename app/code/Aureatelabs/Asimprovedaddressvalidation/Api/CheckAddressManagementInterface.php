<?php
/**
 * Copyright © Aureatelabs All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\Asimprovedaddressvalidation\Api;

interface CheckAddressManagementInterface
{

    /**
     * POST for CheckAddress api
     * @param string $param
     * @return string
     */
    public function postCheckAddress($param);
}

