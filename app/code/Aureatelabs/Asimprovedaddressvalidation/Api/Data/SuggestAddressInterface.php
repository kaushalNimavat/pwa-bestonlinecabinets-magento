<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Asimprovedaddressvalidation
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Asimprovedaddressvalidation\Api\Data;

interface SuggestAddressInterface {

    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getRegionCode();

    /**
     * Set response code.
     *
     * @param string $code
     * @return $this
     */
    public function setRegionCode($code);

    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getRegion();

    /**
     * Set response code.
     *
     * @param string $region
     * @return $this
     */
    public function setRegion($region);
    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getCountry();

    /**
     * Set response code.
     *
     * @param string $country
     * @return $this
     */
    public function setCountry($country);
    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getZip();

    /**
     * Set response code.
     *
     * @param string $zip
     * @return $this
     */
    public function setZip($zip);
    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getStreet0();

    /**
     * Set response code.
     *
     * @param string $street0
     * @return $this
     */
    public function setStreet0($street0);
    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getStreet1();

    /**
     * Set response code.
     *
     * @param string $getStreet1
     * @return $this
     */
    public function setStreet1($street1);
    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getCity();

    /**
     * Set response code.
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city);

}