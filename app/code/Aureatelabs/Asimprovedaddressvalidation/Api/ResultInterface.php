<?php
/**
 * Copyright © Aureatelabs All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aureatelabs\Asimprovedaddressvalidation\Api;

/**
 * Result interface.
 * @api
 */
interface ResultInterface
{
    /**
     * Get response code.
     *
     * @return integer/null
     */
    public function getCode();

    /**
     * Set response code.
     *
     * @param integer $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get response message.
     *
     * @return string|null
     */
    public function getMessage();

    /**
     * Set response message.
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get result data.
     *
     * @return string|null
     */
    public function getResult();

    /**
     * Set result data.
     *
     * @param string|array|null $result
     * @return $this
     */
    public function setResult($result);
}
