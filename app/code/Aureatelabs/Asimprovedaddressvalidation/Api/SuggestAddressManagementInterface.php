<?php
/**
 * Copyright © Aureatelabs All rights reserved.
 * See COPYING.txt for license details.
 */
// declare(strict_types=1);

namespace Aureatelabs\Asimprovedaddressvalidation\Api;

use Aureatelabs\Asimprovedaddressvalidation\Api\Data\SuggestAddressInterface;
use Aureatelabs\Asimprovedaddressvalidation\Api\ResultInterface;

interface SuggestAddressManagementInterface
{
    /**
     * POST for SuggestAddress api
     * @param SuggestAddressInterface $address
     * @return ResultInterface
     */
    public function getSuggestAddress(SuggestAddressInterface $address): ResultInterface;
}

