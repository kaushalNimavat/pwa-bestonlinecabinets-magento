<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Asimprovedaddressvalidation
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2022 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Asimprovedaddressvalidation\Model\Api;

use Aureatelabs\Asimprovedaddressvalidation\Api\Data\SuggestAddressInterface;

/**
 * Class SuggestAddress
 * @package Aureatelabs\Asimprovedaddressvalidation\Model\Api
 */
class SuggestAddress implements SuggestAddressInterface
{
    /**
     * @var string
     */
    private $regionCode;
    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $street0;

    /**
     * @var string
     */
    private $street1;

    /**
     * @var string
     */
    private $city;

   
    /**
     * @return string
     */
    public function getRegionCode()
    {
        return $this->regionCode;
    }

    /**
     * @param string $regionCode
     * @return $this|SuggestAddressInterface
     */
    public function setRegionCode($regionCode)
    {
        $this->regionCode = $regionCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return $this|SuggestAddressInterface
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }
   /**
     * Get response code.
     *
     * @return string/null
     */
    public function getCountry(){
      return $this->country;
    }

    /**
     * Set response code.
     *
     * @param string $country
     * @return $this
     */
    public function setCountry($country){
      $this->country = $country;
      return $this;
    }
    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getZip(){
      return $this->zip;
    }

    /**
     * Set response code.
     *
     * @param string $zip
     * @return $this
     */
    public function setZip($zip){
      $this->zip = $zip;
      return $this;
    }
    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getStreet0(){
      return $this->street0;
    }

    /**
     * Set response code.
     *
     * @param string $street0
     * @return $this
     */
    public function setStreet0($street0){
      $this->street0 = $street0;
      return $this;
    }
    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getStreet1(){
      return $this->street1;
    }

    /**
     * Set response code.
     *
     * @param string $street1
     * @return $this
     */
    public function setStreet1($street1){
      $this->street1 = $street1;
      return $this;
    }
    /**
     * Get response code.
     *
     * @return string/null
     */
    public function getCity(){
      return $this->city;
    }

    /**
     * Set response code.
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city){
      $this->city = $city;
      return $this;
    }
}
