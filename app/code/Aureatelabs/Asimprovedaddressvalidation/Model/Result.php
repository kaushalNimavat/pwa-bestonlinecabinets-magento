<?php
/**
 * Copyright © Aureatelabs All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aureatelabs\Asimprovedaddressvalidation\Model;

/**
 * Class Result
 * @package Aureatelabs\Asimprovedaddressvalidation\Model
 */
class Result implements \Aureatelabs\Asimprovedaddressvalidation\Api\ResultInterface
{
    /**
     * @var integer
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     * @var mixed
     */
    private $result;

    /**
     * Get response code.
     * @return integer/null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set response code.
     * @param integer $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get response message.
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set response message.
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get result data.
     * @return string|null
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set result data.
     * @param string|array|null $result
     * @return $this
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }
}
