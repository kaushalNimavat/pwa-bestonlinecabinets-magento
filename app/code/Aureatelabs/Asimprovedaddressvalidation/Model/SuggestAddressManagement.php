<?php
/**
 * Copyright © Aureatelabs All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\Asimprovedaddressvalidation\Model;

use Magento\Framework\Controller\ResultFactory as ResultFactory;
use Aureatelabs\Asimprovedaddressvalidation\Api\Data\SuggestAddressInterface;
use Aureatelabs\Asimprovedaddressvalidation\Api\SuggestAddressManagementInterface;
use Aureatelabs\Asimprovedaddressvalidation\Api\ResultInterface;

/**
 * Class SuggestAddressManagement
 */
class SuggestAddressManagement implements SuggestAddressManagementInterface
{
    /**
     * @var ResultInterface
     */
    protected $result;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * SuggestAddressManagement constructor
     * 
     * @param SubscriberInterface $subscriber
     * @param ResultFactory $resultFactory
     * @param ResultInterface $result
     */
    public function __construct(
        \Azaleasoft\Asimprovedaddressvalidation\Helper\Data $helper,
        ResultFactory $resultFactory,
        ResultInterface $result
    ) {
        $this->helper = $helper;
        $this->result = $result;
        $this->resultFactory = $resultFactory;
    }

    /**
     * {@inheritdoc}
     * @param SuggestAddressInterface $address
     * @return ResultInterface
     */
    public function getSuggestAddress(SuggestAddressInterface $address): ResultInterface
    {
        $this->result
            ->setCode(400)
            ->setMessage('Error');

        $responseData = [];
        $postData = [
            'region_code' => $address->getRegionCode(),
            'region' => $address->getRegion(),
            'country' => $address->getCountry(),
            'zip' => $address->getZip(),
            'street_0' => $address->getStreet0(),
            'street_1' => $address->getStreet1(),
            'city' => $address->getCity()
        ];

        try {
            $regionData = $this->helper->getRegionData($postData);
            $postData['region_code'] = isset($regionData['region_code']) ? $regionData['region_code'] : '';
            $postData['region'] = isset($regionData['region']) ? $regionData['region'] : $postData['region'];
            if ($postData['country'] == 'US') {
                $postData['zip'] = $this->helper->parseZipCode($postData['zip']);
            }
            $postData['fulladdress'] = $this->helper->getFullAddress($postData);
            $postData['type'] = 'O';
            $responseData[] = $postData;

            $rs = $this->helper->sendRequest($postData);
            if (!empty($rs)) {
                foreach ($rs as $row) {
                    $regionData = $this->helper->getRegionData($row);
                    $row['region_id'] = isset($regionData['region_id']) ? $regionData['region_id'] : '';
                    $row['region_code'] = isset($regionData['region_code']) ? $regionData['region_code'] : '';
                    $row['region'] = isset($regionData['region']) ? $regionData['region'] : '';
                    if ($row['country'] == 'US') {
                        $row['zip'] = $this->helper->parseZipCode($row['zip']);
                    }
                    $row['fulladdress'] = $this->helper->getFullAddress($row);
                    $row['type'] = 'S';
                    $responseData[] = $row;
                }
            }
            $this->result->setCode(200)->setMessage('Success');
        } catch (\Exception $e) {
            $this->result->setMessage($e->getMessage());
            return $e->getMessage();
        }

        $this->result->setResult($responseData);
        return $this->result;
    }
}

