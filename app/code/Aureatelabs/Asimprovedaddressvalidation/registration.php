<?php
/**
 * Copyright © Aureatelabs All rights reserved.
 * See COPYING.txt for license details.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Aureatelabs_Asimprovedaddressvalidation', __DIR__);

