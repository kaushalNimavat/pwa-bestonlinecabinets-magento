<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Banner\Model\Indexer\Action;

use Aureatelabs\Banner\Model\ImageUploader;
use Aureatelabs\Banner\Model\ResourceModel\Banner as BannerResource;
use Magento\Framework\App\AreaList;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;

/**
 * Class Banner
 * @package Aureatelabs\Banner\Model\Indexer\Action
 */
class Banner implements RebuildActionInterface
{
    /**
     * @var AreaList
     */
    private $areaList;

    /**
     * @var BannerResource
     */
    private $resourceModel;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @param BannerResource $bannerResource
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        BannerResource $bannerResource,
        ImageUploader $imageUploader
    ) {
        $this->resourceModel = $bannerResource;
        $this->imageUploader = $imageUploader;
    }

    /**
     * @param int $storeId
     * @param array $bannerIds
     *
     * @return \Traversable
     * @throws \Exception
     */
    public function rebuild(int $storeId = 1, array $bannerIds = []): \Traversable
    {
        $lastBannerId = 0;
        do {
            $banners = $this->resourceModel->loadBanners($storeId, $bannerIds, $lastBannerId);
            foreach ($banners as $banner) {
                $lastBannerId = (int)$banner['entity_id'];
                $banner['id'] = $lastBannerId;
                $banner['image'] = $this->imageUploader->getFileUrl($banner['image']);
                $banner['mobile_image'] = $this->imageUploader->getFileUrl($banner['mobile_image']);
                $banner['active'] = (bool)$banner['is_active'];
                $banner['sort_order'] = (int)$banner['sort_order'];

                unset($banner['is_active']);
                yield $lastBannerId => $banner;
            }
        } while (!empty($banners));
    }
}
