<?php

namespace Aureatelabs\Blog\Model\Indexer\Action;

use Aureatelabs\Blog\Model\ResourceModel\Blog as PostsResource;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Blog implements RebuildActionInterface
{
    /**
     * @var PostsResource
     */
    private $resourceModel;

    /**
     * @param PostsResource $postsResource
     */
    public function __construct(
        PostsResource $postsResource
    ) {
        $this->resourceModel = $postsResource;
    }

    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     */
    public function rebuild(int $storeId = 1, array $ids = []): \Traversable
    {
        $lastPostId = 0;
        $categories = $this->resourceModel->loadCategories();
        do {
            $posts = $this->resourceModel->loadPosts($storeId, $ids, $lastPostId);
            foreach ($posts as $post) {
                $lastPostId = (int) $post['post_id'];
                $post['id'] = $lastPostId;
                $post['categories'] = $categories[$lastPostId];
                yield $lastPostId => $post;
            }
        } while (!empty($posts));
    }
}
