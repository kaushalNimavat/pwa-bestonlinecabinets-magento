<?php

namespace Aureatelabs\Blog\Model\Indexer;

class BlogProcessor extends \Magento\Framework\Indexer\AbstractProcessor
{
    /**
     * Indexer ID
     */
    const INDEXER_ID = 'vsbridge_blog_indexer';
}
