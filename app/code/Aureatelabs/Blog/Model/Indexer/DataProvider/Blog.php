<?php

namespace Aureatelabs\Blog\Model\Indexer\DataProvider;

use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Magento\Catalog\Helper\Data;
use Magento\Framework\Exception\LocalizedException;

class Blog implements DataProviderInterface
{
    /**
     * @var string[]
     */
    private $parsedAttribute = ['short_content', 'full_content'];
    /**
     * @var Data
     */
    private $catalogHelper;

    /**
     * @param Data $catalogHelper
     */
    public function __construct(Data $catalogHelper)
    {
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * @param array $indexData
     * @param $storeId
     * @return array
     * @throws LocalizedException
     * @throws \Exception
     */
    public function addData(array $indexData, $storeId): array
    {
        $templateProcessor = $this->catalogHelper->getPageTemplateProcessor();
        foreach ($indexData as &$blogData) {
            foreach ($this->parsedAttribute as $attribute) {
                if (isset($blogData[$attribute])) {
                    $blogData[$attribute] = $templateProcessor->filter($blogData[$attribute]);
                }
            }
        }
        return $indexData;
    }
}
