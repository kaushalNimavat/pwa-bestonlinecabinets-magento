<?php

namespace Aureatelabs\Blog\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\EntityManager\MetadataPool;

class Blog
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var MetadataPool
     */
    private $metaDataPool;

    /**
     * @param MetadataPool $metadataPool
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        MetadataPool $metadataPool,
        ResourceConnection $resourceConnection
    ) {
        $this->resource = $resourceConnection;
        $this->metaDataPool = $metadataPool;
    }

    /**
     * @param int $storeId
     * @param array $postIds
     * @param int $fromId
     * @param int $limit
     * @return array
     */
    public function loadPosts(int $storeId = 1, array $postIds = [], int $fromId = 0, int $limit = 1000): array
    {
        $select = $this->getConnection()->select()->from(
            ['posts' => $this->resource->getTableName('amasty_blog_posts')]
        );
        if (!empty($postIds)) {
            $select->where('posts.post_id IN (?)', $postIds);
        }
        $select->where('posts.post_id > ?', $fromId)
            ->limit($limit)
            ->order('posts.post_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @return array
     */
    public function loadCategories(): array
    {
	    $categories = [];
        $select = $this->getConnection()->select()->from(
            [ 'pc' => $this->resource->getTableName('amasty_blog_posts_category')],
            ['pc.post_id', 'pc.category_id']
        );

        $select->join(
            ['c' => 'amasty_blog_categories_store'],
            'pc.category_id = c.category_id and c.status = 1',
            ['c.url_key', 'c.name']
        );

	    $records = $this->getConnection()->fetchAll($select);
        if (count($records)) {
            foreach ($records as $record) {
                $categories[$record['post_id']][] = $record;
            }
        }

        return $categories;
    }

    /**
     * @return AdapterInterface
     */
    private function getConnection(): AdapterInterface
    {
        return $this->resource->getConnection();
    }
}
