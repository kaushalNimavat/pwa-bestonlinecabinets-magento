<?php

namespace Aureatelabs\Blog\Plugin\Indexer\Blog\Save;

use Aureatelabs\Blog\Model\Indexer\BlogProcessor;
use Amasty\Blog\Model\Posts;

class UpdateBlog
{
    /**
     * @var BlogProcessor
     */
    private $blogProcessor;

    /**
     * @param BlogProcessor $blogProcessor
     */
    public function __construct(BlogProcessor $blogProcessor)
    {
        $this->blogProcessor = $blogProcessor;
    }

    /**
     * @param Posts $subject
     * @param Posts $result
     * @return Posts
     */
    public function afterAfterSave(Posts $subject, Posts $result): Posts
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->blogProcessor->reindexRow($result->getId());
        });

        return $result;
    }

    /**
     * @param Posts $subject
     * @param Posts $result
     * @return Posts
     */
    public function afterAfterDeleteCommit(Posts $subject, Posts $result): Posts
    {
        $this->blogProcessor->reindexRow($result->getId());

        return $result;
    }
}
