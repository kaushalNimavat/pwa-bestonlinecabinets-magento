<?php

namespace Aureatelabs\Braintree\Api\Data;

interface CardDataInterface
{
    const RESULT = 'result';

    /**
     * @param $token
     * @return mixed
     */
    public function setResult($token);

    /**
     * @return mixed
     */
    public function getResult();
}
