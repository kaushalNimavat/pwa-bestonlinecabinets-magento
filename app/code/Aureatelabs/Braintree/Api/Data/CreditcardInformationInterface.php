<?php

namespace Aureatelabs\Braintree\Api\Data;

interface CreditcardInformationInterface
{
    const PUBLIC_HASH = 'public_hash';
    const PAYMENT_METHOD_CODE = 'payment_method_code';
    const TYPE = 'type';
    const CREATED_AT = 'created_at';
    const EXPIRES_AT = 'expires_at';
    const GATEWAY_TOKEN = 'gateway_token';
    const DETAILS = 'details';
    const IS_ACTIVE = 'is_active';
    const IS_VISIBLE = 'is_visible';

    /**
     * @param $publicHash
     * @return mixed
     */
    public function setPublicHash($publicHash);

    /**
     * @return mixed
     */
    public function getPublicHash();

    /**
     * @param $paymentMethodCode
     * @return mixed
     */
    public function setPaymentMethodCode($paymentMethodCode);

    /**
     * @return mixed
     */
    public function getPaymentMethodCode();

    /**
     * @param $type
     * @return mixed
     */
    public function setType($type);

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @param $createdAt
     * @return mixed
     */
    public function setCreatedAt($createdAt);

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @param $expiresAt
     * @return mixed
     */
    public function setExpiresAt($expiresAt);

    /**
     * @return mixed
     */
    public function getExpiresAt();

    /**
     * @param $gatewayToken
     * @return mixed
     */
    public function setGatewayToken($gatewayToken);

    /**
     * @return mixed
     */
    public function getGatewayToken();

    /**
     * @param $details
     * @return mixed
     */
    public function setDetails($details);

    /**
     * @return mixed
     */
    public function getDetails();

    /**
     * @param $isActive
     * @return mixed
     */
    public function setIsActive($isActive);

    /**
     * @return mixed
     */
    public function getIsActive();

    /**
     * @param $isVisible
     * @return mixed
     */
    public function setIsVisible($isVisible);

    /**
     * @return mixed
     */
    public function getIsVisible();
}
