<?php

namespace Aureatelabs\Braintree\Api\Data;

interface PaymentDataInterface
{
    const CUSTOMER_ID = 'customer_id';
    const IS_VISIBLE = 'is_visible';
    const TOKEN_DETAILS = 'token_details';
    const GATEWAY_TOKEN = 'gateway_token';
    const ORDER_ID = 'order_id';

    /**
     * @param $customerId
     * @return mixed
     */
    public function setCustomerId($customerId);

    /**
     * @return mixed
     */
    public function getCustomerId();

    /**
     * @param $visible
     * @return mixed
     */
    public function setIsVisible($visible);

    /**
     * @return mixed
     */
    public function getIsVisible();

    /**
     * @param $details
     * @return mixed
     */
    public function setTokenDetails($details);

    /**
     * @return mixed
     */
    public function getTokenDetails();

    /**
     * @param $token
     * @return mixed
     */
    public function setToken($token);

    /**
     * @return mixed
     */
    public function getToken();

    /**
     * @param $orderId
     * @return mixed
     */
    public function setOrderId($orderId);

    /**
     * @return mixed
     */
    public function getOrderId();
}
