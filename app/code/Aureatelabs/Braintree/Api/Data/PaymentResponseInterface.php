<?php

namespace Aureatelabs\Braintree\Api\Data;

interface PaymentResponseInterface
{
    const CARD_INFO = 'card_info';

    /**
     * @param $cardInfo
     * @return \Aureatelabs\Braintree\Api\Data\CreditcardInformationInterface[]
     */
    public function setCreditCardInfo($cardInfo);

    /**
     * @return \Aureatelabs\Braintree\Api\Data\CreditcardInformationInterface[]
     */
    public function getCreditCardInfo();
}
