<?php

namespace Aureatelabs\Braintree\Api;

use Aureatelabs\Braintree\Api\Data\PaymentDataInterface as Payment;
use Aureatelabs\Braintree\Api\Data\CardDataInterface;
use Aureatelabs\Braintree\Api\Data\PaymentResponseInterface;

interface PaymentInterface
{
    /**
     * @param Payment $payment
     * @return array
     */
    public function setVaultPayment(Payment $payment);

    /**
     * @param $customerId
     * @return PaymentResponseInterface
     */
    public function getVaultPayment(int $customerId);

    /**
     * @param string $token
     * @return CardDataInterface
     */
    public function deleteVaultPayment(string $token);
}
