<?php

namespace Aureatelabs\Braintree\Model\Api;

use Aureatelabs\Braintree\Api\Data\CardDataInterface;
use Magento\Framework\Model\AbstractModel;

class CardData extends AbstractModel implements CardDataInterface
{
    /**
     * @inheritDoc
     */
    public function setResult($result)
    {
        return $this->setData(self::RESULT, $result);
    }

    /**
     * @inheritDoc
     */
    public function getResult()
    {
        return $this->getData(self::RESULT);
    }
}
