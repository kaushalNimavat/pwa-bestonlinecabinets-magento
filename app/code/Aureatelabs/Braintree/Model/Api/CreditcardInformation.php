<?php

namespace Aureatelabs\Braintree\Model\Api;

use Aureatelabs\Braintree\Api\Data\CreditcardInformationInterface;
use Magento\Framework\Model\AbstractModel;

class CreditcardInformation extends AbstractModel implements CreditcardInformationInterface
{
    /**
     * @inheritDoc
     */
    public function setPublicHash($publicHash)
    {
        return $this->setData(self::PUBLIC_HASH, $publicHash);
    }

    /**
     * @inheritDoc
     */
    public function getPublicHash()
    {
        return $this->getData(self::PUBLIC_HASH);
    }

    /**
     * @inheritDoc
     */
    public function setPaymentMethodCode($paymentMethodCode)
    {
        return $this->setData(self::PAYMENT_METHOD_CODE, $paymentMethodCode);
    }

    /**
     * @inheritDoc
     */
    public function getPaymentMethodCode()
    {
        return $this->getData(self::PAYMENT_METHOD_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setExpiresAt($expiresAt)
    {
        return $this->setData(self::EXPIRES_AT, $expiresAt);
    }

    /**
     * @inheritDoc
     */
    public function getExpiresAt()
    {
        return $this->getData(self::EXPIRES_AT);
    }

    /**
     * @inheritDoc
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * @inheritDoc
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setGatewayToken($gatewayToken)
    {
        return $this->setData(self::GATEWAY_TOKEN, $gatewayToken);
    }

    /**
     * @inheritDoc
     */
    public function getGatewayToken()
    {
        return $this->getData(self::GATEWAY_TOKEN);
    }

    /**
     * @inheritDoc
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * @inheritDoc
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * @inheritDoc
     */
    public function setDetails($details)
    {
        return $this->setData(self::DETAILS, $details);
    }

    /**
     * @inheritDoc
     */
    public function getDetails()
    {
        return $this->getData(self::DETAILS);
    }

    /**
     * @inheritDoc
     */
    public function setIsVisible($isVisible)
    {
        return $this->setData(self::IS_VISIBLE, $isVisible);
    }

    /**
     * @inheritDoc
     */
    public function getIsVisible()
    {
        return $this->getData(self::IS_VISIBLE);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }
}
