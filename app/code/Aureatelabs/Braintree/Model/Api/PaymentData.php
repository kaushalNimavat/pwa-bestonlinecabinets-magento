<?php

namespace Aureatelabs\Braintree\Model\Api;

use Aureatelabs\Braintree\Api\Data\PaymentDataInterface;
use Magento\Framework\Model\AbstractModel;

class PaymentData extends AbstractModel implements PaymentDataInterface
{
    /**
     * @inheritDoc
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setIsVisible($visible)
    {
        return $this->setData(self::IS_VISIBLE, $visible);
    }

    /**
     * @inheritDoc
     */
    public function getIsVisible()
    {
        return $this->getData(self::IS_VISIBLE);
    }

    /**
     * @param $details
     * @return PaymentData|mixed
     */
    public function setTokenDetails($details)
    {
        return $this->setData(self::TOKEN_DETAILS, $details);
    }

    /**
     * @return array|mixed|null
     */
    public function getTokenDetails()
    {
        return $this->getData(self::TOKEN_DETAILS);
    }

    /**
     * @param $token
     * @return PaymentData
     */
    public function setToken($token)
    {
        return $this->setData(self::GATEWAY_TOKEN, $token);
    }

    /**
     * @return array|mixed|null
     */
    public function getToken()
    {
        return $this->getData(self::GATEWAY_TOKEN);
    }

    /**
     * @param $orderId
     * @return PaymentData|mixed
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * @return array|mixed|null
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }
}
