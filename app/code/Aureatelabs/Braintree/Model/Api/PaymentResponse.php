<?php

namespace Aureatelabs\Braintree\Model\Api;

use Aureatelabs\Braintree\Api\Data\PaymentResponseInterface;
use Magento\Framework\Model\AbstractModel;

class PaymentResponse extends AbstractModel implements PaymentResponseInterface
{
    /**
     * @param $cardInfo
     * @return \Aureatelabs\Braintree\Api\Data\CreditcardInformationInterface[]|PaymentResponse
     */
    public function setCreditCardInfo($cardInfo)
    {
        return $this->setData(self::CARD_INFO, $cardInfo);
    }

    /**
     * @return array|\Aureatelabs\Braintree\Api\Data\CreditcardInformationInterface[]|mixed|null
     */
    public function getCreditCardInfo()
    {
        return $this->getData(self::CARD_INFO);
    }
}
