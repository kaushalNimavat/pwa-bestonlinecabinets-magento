<?php

namespace Aureatelabs\Braintree\Model;

use Aureatelabs\Braintree\Api\PaymentInterface;
use Aureatelabs\Braintree\Api\Data\PaymentDataInterface as PaymentData;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Vault\Model\PaymentTokenFactory;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use Magento\Vault\Model\PaymentTokenManagement;
use Magento\Vault\Api\PaymentTokenManagementInterface;
use Magento\Framework\App\ResourceConnection;
use Aureatelabs\Braintree\Api\Data\PaymentResponseInterface;
use Aureatelabs\Braintree\Api\Data\CardDataInterface;
use Aureatelabs\Braintree\Model\Api\CreditcardInformationFactory;
use DateTimeZone;
use DateTime;
use DateInterval;

class Payment implements PaymentInterface
{
    /**
     * @var PaymentTokenFactory
     */
    protected $paymentToken;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var SerializerInterface
     */
    private $json;

    /**
     * @var PaymentTokenManagement
     */
    protected $tokenManagement;

    /**
     * @var PaymentTokenManagementInterface
     */
    protected $paymentTokenManagement;

    /**
     * @var PaymentTokenRepositoryInterface
     */
    protected $paymentTokenRepository;

    /**
     * @var CreditcardInformationFactory
     */
    protected $creditcardInformation;

    /**
     * @var PaymentResponseInterface
     */
    protected $paymentResponse;

    /**
     * @var CardDataInterface
     */
    protected $cardResponse;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @param PaymentTokenFactory $paymentToken
     * @param SerializerInterface $json
     * @param PaymentTokenManagementInterface $paymentTokenManagement
     * @param PaymentTokenManagement $tokenManagement
     * @param PaymentTokenRepositoryInterface $paymentTokenRepository
     * @param PaymentResponseInterface $paymentResponse
     * @param CardDataInterface $cardResponse
     * @param CreditcardInformationFactory $creditcardInformation
     * @param ResourceConnection $resource
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
        PaymentTokenFactory $paymentToken,
        SerializerInterface $json,
        PaymentTokenManagementInterface $paymentTokenManagement,
        PaymentTokenManagement $tokenManagement,
        PaymentTokenRepositoryInterface $paymentTokenRepository,
        PaymentResponseInterface $paymentResponse,
        CardDataInterface $cardResponse,
        CreditcardInformationFactory $creditcardInformation,
        ResourceConnection $resource,
        EncryptorInterface $encryptor
    ) {
        $this->paymentToken = $paymentToken;
        $this->json = $json;
        $this->tokenManagement = $tokenManagement;
        $this->creditcardInformation = $creditcardInformation;
        $this->paymentTokenManagement = $paymentTokenManagement;
        $this->paymentTokenRepository = $paymentTokenRepository;
        $this->paymentResponse = $paymentResponse;
        $this->cardResponse = $cardResponse;
        $this->resource = $resource;
        $this->encryptor = $encryptor;
    }

    /**
     * @param PaymentData $paymentData
     * @return array
     * @throws \Exception
     */
    public function setVaultPayment(PaymentData $paymentData)
    {
        $vaultPaymentToken = $this->paymentToken->create(PaymentTokenFactory::TOKEN_TYPE_CREDIT_CARD);
        $vaultPaymentToken->setCustomerId($paymentData->getCustomerId());
        $vaultPaymentToken->setPaymentMethodCode('braintree');
        $vaultPaymentToken->setType('card');
        $storedCard = $paymentData->getTokenDetails();
        $vaultPaymentToken->setTokenDetails($this->json->serialize([
            'type' => $storedCard['cardType'],
            'maskedCC' => $storedCard['last4'],
            'expirationDate' => $storedCard['expirationDate']
        ]));
        $vaultPaymentToken->setIsActive(true);
        $vaultPaymentToken->setIsVisible($paymentData->getIsVisible());
        $vaultPaymentToken->setPublicHash(
            $this->encryptor->getHash(
                $paymentData->getCustomerId()
                . $vaultPaymentToken->getPaymentMethodCode()
                . $vaultPaymentToken->getType()
                . $vaultPaymentToken->getTokenDetails()
            )
        );

        $vaultPaymentToken->setExpiresAt($this->getExpirationDate($storedCard['expirationDate']));
        $vaultPaymentToken->setGatewayToken($paymentData->getToken());

        try {
            $paymentToken = $this->paymentTokenRepository->save($vaultPaymentToken);
            if ($paymentToken->getEntityId() !== null) {
                $this->paymentTokenManagement->addLinkToOrderPayment(
                    $paymentToken->getEntityId(),
                    $this->getpaymentId($paymentData->getOrderId())
                );
            }
            $result[] = [
                "status" => "success",
                "message" => "Card Stored Successfully"
            ];
        } catch (\Exception $exception) {
            throw new \Exception("Something went wrong while saving the data. ". $exception->getMessage());
        }

        return $result;
    }

    /**
     * @inheirtDoc
     */
    public function getVaultPayment(int $customerId)
    {
        $creditCardInfo = [];
        $availableTokens = $this->tokenManagement->getVisibleAvailableTokens($customerId);
        /** @var  CreditcardInformationFactory $paymentResponse */
        foreach ($availableTokens as $token) {
            $paymentResponse = $this->creditcardInformation->create();
            $paymentResponse->setPublicHash($token->getPublicHash());
            $paymentResponse->setPaymentMethodCode($token->getPaymentMethodCode());
            $paymentResponse->setType($token->getType());
            $paymentResponse->setCreatedAt($token->getCreatedAt());
            $paymentResponse->setExpiresAt($token->getExpiresAt());
            $paymentResponse->setGatewayToken($token->getGatewayToken());
            $paymentResponse->setDetails($this->json->unserialize($token->getTokenDetails()));
            $paymentResponse->setIsActive($token->getIsActive());
            $paymentResponse->setIsVisible($token->getIsVisible());
            $creditCardInfo[] = $paymentResponse;
        }

        $this->paymentResponse->setCreditCardInfo($creditCardInfo);

        return $this->paymentResponse;
    }

    /**
     * @param string $token
     * @return CardDataInterface
     * @throws \Exception
     */
    public function deleteVaultPayment(string $token)
    {
        $connection = $this->resource->getConnection();
        $tableName = $this->resource->getTableName('vault_payment_token');
        $data = ["is_active"=> false, "is_visible"=> false];
        $where = ['gateway_token = ?' => $token];

        try {
            $connection->update($tableName, $data, $where);
            $result = [
                "status" => "success",
                "message" => "Card Delete Successfully"
            ];
            $this->cardResponse->setResult($result);
        } catch (\Exception $exception) {
            throw new \Exception("Something went wrong while deleting the data. ". $exception->getMessage());
        }

        return $this->cardResponse;
    }

    /**
     * @param $date
     * @return string|void
     * @throws \Exception
     */
    private function getExpirationDate($date)
    {
        if ($date == null) {
            return ;
        }

        $expireDate = explode("/", $date);
        list($month, $year) = $expireDate;
        $expDate = new DateTime(
            $year
            . '-'
            . $month
            . '-'
            . '01'
            . ' '
            . '00:00:00',
            new DateTimeZone('UTC')
        );
        $expDate->add(new DateInterval('P1M'));
        return $expDate->format('Y-m-d 00:00:00');
    }

    /**
     * @param $orderId
     * @return mixed|void
     */
    public function getpaymentId($orderId)
    {
        if (!$orderId) {
            return;
        }
        $connection = $this->resource->getConnection();
        $tableName = $this->resource->getTableName('sales_order_payment');
        $sql = $connection->select()
            ->from($tableName, ['entity_id'])
            ->where('parent_id = ?', $orderId);

        $result = $connection->fetchRow($sql);

        return $result['entity_id'];
    }
}
