<?php

namespace Aureatelabs\CategoryAttributes\Model\Indexer;

class CategoryAttributesProcessor extends \Magento\Framework\Indexer\AbstractProcessor
{
    /**
     * Indexer ID
     */
    const INDEXER_ID = 'vsbridge_category_attribute_indexer';
}
