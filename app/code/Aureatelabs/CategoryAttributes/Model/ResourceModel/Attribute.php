<?php

namespace Aureatelabs\CategoryAttributes\Model\ResourceModel;

use Magento\Catalog\Model\ResourceModel\Category\Attribute\Collection;
use Magento\Catalog\Model\ResourceModel\Category\Attribute\CollectionFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Select;

class Attribute
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * ProductAttribute constructor.
     *
     * @param ResourceConnection $resource
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(ResourceConnection $resource, CollectionFactory $collectionFactory)
    {
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param array $attributeIds
     * @param int $fromId
     * @param int $limit
     *
     * @return array
     */
    public function getAttributes(array $attributeIds = [], int $fromId = 0, int $limit = 100): array
    {
        $select = $this->getAttributeCollectionSelect();
        $connection = $this->resource->getConnection();
        $sourceModelCondition = [$connection->quoteInto('source_model != ?', 'core/design_source_design')];
        $sourceModelCondition[] = 'source_model IS NULL';
        $select->where(sprintf('(%s)', implode(' OR ', $sourceModelCondition)));

        if (!empty($attributeIds)) {
            $select->where('main_table.attribute_id IN (?)', $attributeIds);
        }

        $select->where('main_table.attribute_id > ?', $fromId)
            ->limit($limit)
            ->order('main_table.attribute_id');

        return $connection->fetchAll($select);
    }

    /**
     * @return Select
     */
    private function getAttributeCollectionSelect(): Select
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        return $collection->getSelect();
    }
}
