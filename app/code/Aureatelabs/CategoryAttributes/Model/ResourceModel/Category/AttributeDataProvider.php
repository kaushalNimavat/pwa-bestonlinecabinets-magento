<?php

namespace Aureatelabs\CategoryAttributes\Model\ResourceModel\Category;

use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Category\LoadAttributes;
use Divante\VsbridgeIndexerCore\Api\ConvertValueInterface;
use Divante\VsbridgeIndexerCatalog\Index\Mapping\Product as ProductMapping;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\AbstractEavAttributes;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\Exception\LocalizedException;

class AttributeDataProvider extends AbstractEavAttributes
{
    /**
     * @var LoadAttributes
     */
    private $loadAttributes;

    /**
     * @param LoadAttributes $loadAttributes
     * @param ProductMapping $productMapping
     * @param ResourceConnection $resourceConnection
     * @param ConvertValueInterface $convertValue
     * @param MetadataPool $metadataPool
     * @param string $entityType
     */
    public function __construct(
        LoadAttributes $loadAttributes,
        ProductMapping $productMapping,
        ResourceConnection $resourceConnection,
        ConvertValueInterface $convertValue,
        MetadataPool $metadataPool,
        $entityType = \Magento\Catalog\Api\Data\CategoryInterface::class
    ) {
        $this->loadAttributes = $loadAttributes;
        parent::__construct($resourceConnection, $metadataPool, $convertValue, $productMapping, $entityType);
    }

    /**
     * @return Attribute[]
     */
    public function getAttributesById(): array
    {
        return $this->initAttributes();
    }

    /**
     * @param int $attributeId
     * @return Attribute
     * @throws LocalizedException
     */
    public function getAttributeById(int $attributeId): Attribute
    {
        return $this->loadAttributes->getAttributeById((int) $attributeId);
    }

    /**
     * @param string $attributeCode
     * @return Attribute
     * @throws LocalizedException
     */
    public function getAttributeByCode(string $attributeCode): Attribute
    {
        return $this->loadAttributes->getAttributeByCode($attributeCode);
    }

    /**
     * @return Attribute[]
     */
    public function initAttributes(): array
    {
        return $this->loadAttributes->execute();
    }
}
