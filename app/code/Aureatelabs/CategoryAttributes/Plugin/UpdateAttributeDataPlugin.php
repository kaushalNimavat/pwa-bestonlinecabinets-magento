<?php

namespace Aureatelabs\CategoryAttributes\Plugin;

use Aureatelabs\CategoryAttributes\Model\Indexer\CategoryAttributesProcessor;
use Divante\VsbridgeIndexerCatalog\Model\Indexer\CategoryProcessor;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;

class UpdateAttributeDataPlugin
{
    /**
     * @var CategoryAttributesProcessor
     */
    private $attributeProcessor;

    /**
     * @var CategoryProcessor
     */
    private $categoryProcessor;

    /**
     * UpdateAttributeData constructor.
     *
     * @param CategoryProcessor $categoryProcessor
     * @param CategoryAttributesProcessor $attributeProcessor
     */
    public function __construct(
        CategoryProcessor $categoryProcessor,
        CategoryAttributesProcessor $attributeProcessor
    ) {
        $this->categoryProcessor = $categoryProcessor;
        $this->attributeProcessor = $attributeProcessor;
    }

    /**
     * TODO check if we add new attribute, after adding new attribute send request to elastic to add new mapping
     * for field.
     * @param Attribute $attribute
     *
     * @return Attribute
     */
    public function afterAfterSave(Attribute $attribute): Attribute
    {
        $this->attributeProcessor->reindexRow($attribute->getId());

        return $attribute;
    }

    /**
     * After deleting attribute we should update all products
     * @param Attribute $attribute
     * @param Attribute $result
     *
     * @return Attribute
     */
    public function afterAfterDeleteCommit(Attribute $attribute, Attribute $result): Attribute
    {
        $this->attributeProcessor->reindexRow($attribute->getId());
        $this->categoryProcessor->markIndexerAsInvalid();

        return $result;
    }
}
