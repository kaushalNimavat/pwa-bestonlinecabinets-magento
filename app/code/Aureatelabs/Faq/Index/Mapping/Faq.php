<?php

namespace Aureatelabs\Faq\Index\Mapping;

use Divante\VsbridgeIndexerCore\Api\MappingInterface;
use Divante\VsbridgeIndexerCore\Api\Mapping\FieldInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;

class Faq implements MappingInterface
{
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string[]
     */
    private $textFields = [
        'title',
        'short_answer',
        'answer',
        'name',
        'email',
        'url_key',
        'meta_title',
        'meta_description',
        'asked_from_store'
    ];

    /**
     * @param EventManager $eventManager
     */
    public function __construct(EventManager $eventManager)
    {
        $this->eventManager = $eventManager;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array|mixed|null
     */
    public function getMappingProperties()
    {
        $properties = [
            'question_id' => ['type' => FieldInterface::TYPE_LONG],
            'visibility' => ['type' => FieldInterface::TYPE_INTEGER],
            'status' => ['type' => FieldInterface::TYPE_INTEGER],
            'position' => ['type' => FieldInterface::TYPE_INTEGER],
            'positive_rating' => ['type' => FieldInterface::TYPE_DOUBLE],
            'negative_rating' => ['type' => FieldInterface::TYPE_DOUBLE],
            'total_rating' => ['type' => FieldInterface::TYPE_DOUBLE],
            'created_at' => [
                'type' => FieldInterface::TYPE_DATE,
                'format' => FieldInterface::DATE_FORMAT,
            ],
            'updated_at' => [
                'type' => FieldInterface::TYPE_DATE,
                'format' => FieldInterface::DATE_FORMAT,
            ],
            'is_show_full_answer' => ['type' => FieldInterface::TYPE_BOOLEAN],
            'noindex' => ['type' => FieldInterface::TYPE_BOOLEAN],
            'nofollow' => ['type' => FieldInterface::TYPE_BOOLEAN],
            'exclude_sitemap' => ['type' => FieldInterface::TYPE_BOOLEAN],
            'visit_count' => ['type' => FieldInterface::TYPE_INTEGER],
            'avg_rating' => ['type' => FieldInterface::TYPE_DOUBLE],
            'avg_total' => ['type' => FieldInterface::TYPE_DOUBLE]
        ];

        foreach ($this->textFields as $field) {
            $properties[$field] = ['type' => FieldInterface::TYPE_TEXT];
        }

        $mappingObject = new \Magento\Framework\DataObject();
        $mappingObject->setData('properties', $properties);
        $this->eventManager->dispatch(
            'elasticsearch_faq_mapping_properties',
            ['mapping' => $mappingObject]
        );
        return $mappingObject->getData();
    }
}
