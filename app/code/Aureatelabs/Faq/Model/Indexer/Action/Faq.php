<?php

namespace Aureatelabs\Faq\Model\Indexer\Action;

use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;
use Aureatelabs\Faq\Model\ResourceModel\Faq as FaqResource;

class Faq implements RebuildActionInterface
{
    /**
     * @var FaqResource
     */
    private $resourceModel;

    /**
     * @param FaqResource $resourceModel
     */
    public function __construct(FaqResource $resourceModel)
    {
        $this->resourceModel = $resourceModel;
    }

    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     */
    public function rebuild(int $storeId, array $ids): \Traversable
    {
        $lastQuestionId = 0;
        do {
            $faqs = $this->resourceModel->loadFaqs($storeId, $ids, $lastQuestionId);

            foreach ($faqs as $faq) {
                $lastQuestionId = (int) $faq['question_id'];
                $faq['id'] = $lastQuestionId;
                yield $lastQuestionId => $faq;
            }
        } while (!empty($faqs));
    }
}
