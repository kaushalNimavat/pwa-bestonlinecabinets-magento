<?php

namespace Aureatelabs\Faq\Model\Indexer\DataProvider;

use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Magento\Catalog\Helper\Data;
use Magento\Framework\Exception\LocalizedException;
use Aureatelabs\Faq\Model\ResourceModel\Faq as FaqResource;

class Faq implements DataProviderInterface
{
    /**
     * @var Data
     */
    private $catalogHelper;

    /**
     * @var FaqResource
     */
    private $faqResource;

    /**
     * @var string[]
     */
    private $parsedAttribute = ['short_answer', 'answer'];

    /**
     * @var string[]
     */
    private $integerAttribute = [
        'question_id', 'status', 'visibility', 'position', 'positive_rating', 'negative_rating', 'total_rating', 'visit_count'
    ];

    /**
     * @var string[]
     */
    private $booleanAttribute = ['is_show_full_answer', 'noindex', 'nofollow', 'exclude_sitemap'];

    /**
     * @var array
     */
    private $faqCategories = [];

    /**
     * @var array
     */
    private $faqCategoryIds = [];

    /**
     * @var array
     */
    private $faqCategoryWithDetails = [];

    /**
     * @var array
     */
    private $faqProductIds = [];

    /**
     * @param FaqResource $faqResource
     * @param Data $catalogHelper
     */
    public function __construct(
        FaqResource $faqResource,
        Data $catalogHelper
    ) {
        $this->faqResource = $faqResource;
        $this->catalogHelper = $catalogHelper;
        $this->loadCategoryWithRequiredField();
        $this->loadCategoryIdsByFaq();
        $this->loadProductIdsByFaq();
    }

    /**
     * @param array $indexData
     * @param int $storeId
     * @return array
     * @throws LocalizedException
     * @throws \Exception
     */
    public function addData(array $indexData, $storeId): array
    {
        $templateProcessor = $this->catalogHelper->getPageTemplateProcessor();
        foreach ($indexData as &$faqData) {
            // Convert attribute to integer
            foreach ($this->integerAttribute as $intAttribute) {
                if (isset($faqData[$intAttribute])) {
                    $faqData[$intAttribute] = (int) $faqData[$intAttribute];
                }
            }

            // Convert attribute to boolean
            foreach ($this->booleanAttribute as $boolAttribute) {
                if (isset($faqData[$boolAttribute])) {
                    $faqData[$boolAttribute] = (bool) $faqData[$boolAttribute];
                }
            }

            // Parse CMS content
            foreach ($this->parsedAttribute as $attribute) {
                if (isset($faqData[$attribute])) {
                    $faqData[$attribute] = $templateProcessor->filter($faqData[$attribute]);
                }
            }

            $faqData['avg_rating'] = (float) $faqData['avg_rating'];
            $faqData['avg_total'] = (float) $faqData['avg_total'];

            if (isset($this->faqCategoryWithDetails[$faqData['question_id']])) {
                $faqData['category'] = $this->faqCategoryWithDetails[$faqData['question_id']];
            }
            if (isset($this->faqCategoryIds[$faqData['question_id']])) {
                $faqData['category_ids'] = $this->faqCategoryIds[$faqData['question_id']];
            }
            if (isset($this->faqProductIds[$faqData['question_id']])) {
                $faqData['product_ids'] = $this->faqProductIds[$faqData['question_id']];
            }
        }
        return $indexData;
    }

    /**
     * @return void
     */
    private function loadCategoryWithRequiredField(): void
    {
        if (empty($this->faqCategories)) {
            $faqCategories = $this->faqResource->loadFaqCategories();
            foreach ($faqCategories as $faqCategory) {
                $this->faqCategories[$faqCategory['category_id']] = [
                    'category_id' => (int) $faqCategory['category_id'],
                    'title' => $faqCategory['title'],
                    'url_key' => $faqCategory['url_key'],
                    'position' => (int) $faqCategory['position']
                ];
            }
        }
    }

    /**
     * @return void
     */
    private function loadCategoryIdsByFaq(): void
    {
        if (empty($this->faqCategoryIds)) {
            $faqCategories = $this->faqResource->loadFaqCategoriesRelations();
            foreach ($faqCategories as $faqCategory) {
                $this->faqCategoryIds[$faqCategory['question_id']][] = (int) $faqCategory['category_id'];
                if (isset($this->faqCategories[$faqCategory['category_id']])) {
                    $this->faqCategoryWithDetails[$faqCategory['question_id']][] = $this->faqCategories[$faqCategory['category_id']];
                }
            }
        }
    }

    /**
     * @return void
     */
    private function loadProductIdsByFaq(): void
    {
        if (empty($this->faqProductIds)) {
            $faqCategories = $this->faqResource->loadFaqProductsRelations();
            foreach ($faqCategories as $faqCategory) {
                $this->faqProductIds[$faqCategory['question_id']][] = (int) $faqCategory['product_id'];
            }
        }
    }
}
