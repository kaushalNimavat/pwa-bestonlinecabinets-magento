<?php

namespace Aureatelabs\Faq\Model\Indexer;

class FaqProcessor extends \Magento\Framework\Indexer\AbstractProcessor
{
    /**
     * Indexer ID
     */
    const INDEXER_ID = 'vsbridge_faq_indexer';
}
