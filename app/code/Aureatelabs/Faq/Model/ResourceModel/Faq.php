<?php

namespace Aureatelabs\Faq\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

class Faq
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * Faq constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resource = $resourceConnection;
    }

    /**
     * @param int $storeId
     * @param array $questionIds
     * @param int $fromId
     * @param int $limit
     * @return array
     */
    public function loadFaqs(int $storeId = 1, array $questionIds = [], int $fromId = 0, int $limit = 1000): array
    {
        $select = $this->getConnection()->select()->from(
            ['faq' => $this->getConnection()->getTableName('amasty_faq_question')]
        );

        if (!empty($questionIds)) {
            $select->where('faq.question_id IN (?)', $questionIds);
        }

        $select->where('faq.question_id > ?', $fromId)
            ->limit($limit)
            ->order('faq.question_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @param int $limit
     * @return array
     */
    public function loadFaqCategories(int $limit = 1000): array
    {
        $select = $this->getConnection()->select()->from(
            ['faq_category' => $this->getConnection()->getTableName('amasty_faq_category')]
        );
        $select->where('faq_category.status = ?', 1)
            ->limit($limit)
            ->order('faq_category.category_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @param int $limit
     * @return array
     */
    public function loadFaqCategoriesRelations(int $limit = 1000): array
    {
        $select = $this->getConnection()->select()->from(
            ['faq_category_relation' => $this->getConnection()->getTableName('amasty_faq_question_category')]
        )->limit($limit)->order('faq_category_relation.question_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @param int $limit
     * @return array
     */
    public function loadFaqProductsRelations(int $limit = 1000): array
    {
        $select = $this->getConnection()->select()->from(
            ['faq_products_relation' => $this->getConnection()->getTableName('amasty_faq_question_product')]
        )->limit($limit)->order('faq_products_relation.question_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @return AdapterInterface
     */
    private function getConnection(): AdapterInterface
    {
        return $this->resource->getConnection();
    }
}
