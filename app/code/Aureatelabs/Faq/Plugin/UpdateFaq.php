<?php

namespace Aureatelabs\Faq\Plugin;

use Aureatelabs\Faq\Model\Indexer\FaqProcessor;
use Amasty\Faq\Model\Question;

class UpdateFaq
{
    /**
     * @var FaqProcessor
     */
    private $faqProcessor;

    /**
     * @param FaqProcessor $faqProcessor
     */
    public function __construct(FaqProcessor $faqProcessor)
    {
        $this->faqProcessor = $faqProcessor;
    }

    /**
     * @param Question $subject
     * @param Question $result
     * @return Question
     */
    public function afterAfterSave(Question $subject, Question $result): Question
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->faqProcessor->reindexRow($result->getId());
        });

        return $result;
    }

    /**
     * @param Question $subject
     * @param Question $result
     * @return Question
     */
    public function afterAfterDeleteCommit(Question $subject, Question $result): Question
    {
        $this->faqProcessor->reindexRow($result->getId());
        return $result;
    }
}
