<?php

namespace Aureatelabs\FlatPanel\Model\Indexer\Action;

use Aureatelabs\FlatPanel\Model\ResourceModel\FlatPanel as ResourceModel;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;

class FlatPanel implements RebuildActionInterface
{

    /**
     * @var ResourceModel
     */
    private $resourceModel;

    /**
     * @param ResourceModel $resourceModel
     */
    public function __construct(
        ResourceModel $resourceModel
    ) {
        $this->resourceModel = $resourceModel;
    }

    /**
     * @param int $storeId
     * @param array $panelIds
     * @return \Traversable
     */
    public function rebuild(int $storeId = 1, array $panelIds = []): \Traversable
    {
        $lastPanelId = 0;
        do {
            $flatPanels = $this->resourceModel->getFlatPanels($storeId, $panelIds, $lastPanelId);
            foreach ($flatPanels as $panel) {
                $lastPanelId = (int)$panel['value_id'];
                $panel['sort_order'] = (int)$panel['sort_order'];
                yield $lastPanelId => $panel;
            }

        } while (!empty($flatPanels));
    }
}
