<?php

namespace Aureatelabs\FlatPanel\Model\Indexer\DataProvider;

use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\App\ResourceConnection;

class CatAttributeData implements DataProviderInterface
{
    /**
     * @var Json
     */
    private $json;

    /**
     * @var FilterProvider
     */
    private $templateFilter;

    /**
     * @var string
     */
    private $galleryAttribute = 'f_category_gallery';

    /**
     * @var string[]
     */
    private $categoryTabAttribute = [
        'f_category_tab_1',
        'f_category_tab_2',
        'f_category_tab_3',
        'f_category_tab_4',
        'category_top_content',
        'category_bottom_content',
    ];
    /**
     * @var ResourceConnection
     */
    private $resource;
    
    /**
     * @param FilterProvider $templateFilter
     * @param Json $json
     * @param ResourceConnection $resource
     */
    public function __construct(
        FilterProvider $templateFilter,
        Json $json,
        ResourceConnection $resource
    ) {
        $this->json = $json;
        $this->templateFilter = $templateFilter;
        $this->resource = $resource;
    }

    /**
     * @param array $indexData
     * @param int $storeId
     * @return array
     * @throws \Exception
     */
    public function addData(array $indexData, $storeId): array
    {
        $pageFilter = $this->templateFilter->getPageFilter();
        $categoryRequestPath = $this->getCategoryUrlPaths($indexData);
        foreach ($indexData as &$attributesData) {
            $indexData[$attributesData['id']]['children_data'] = $this->plotTree($attributesData['children_data'], $attributesData['id'], $storeId);
            foreach ($this->categoryTabAttribute as $categoryTabAttr) {
                if (isset($attributesData[$categoryTabAttr])) {
                    $attributesData[$categoryTabAttr] = $pageFilter->filter($attributesData[$categoryTabAttr]);
                }
            }
            if (isset($attributesData[$this->galleryAttribute]) && !empty($attributesData[$this->galleryAttribute])) {
                $attributesData['category_gallery'] = $this->prepareMediaGallery(
                    $attributesData[$this->galleryAttribute]
                );
                unset($attributesData[$this->galleryAttribute]);
            }
            $finalPrice = $this->calculateFinalPrice(
                ($attributesData['catprice'] ?? null), ($attributesData['spcatprice'] ?? null)
            );
            if (!empty($finalPrice)) {
                $attributesData['finalprice'] = $finalPrice;
            }
            if(array_key_exists($attributesData['id'], $categoryRequestPath)){
                $attributesData['url_path'] = $categoryRequestPath[$attributesData['id']]['request_path'];
            }
        }
        return $indexData;
    }
    function getCategoryUrlPaths($categoryList){
        if(is_array($categoryList) && !empty($categoryList)){
            $resource = $this->resource->getConnection();
            $tableName = $resource->getTableName('url_rewrite');

            $query = $resource->select()->from($tableName,['entity_id','request_path'])->where('entity_type = ?','category')->where('entity_id IN ('.implode(",",array_keys($categoryList)).')');
            $categoryRequestPath = $resource->fetchAssoc($query);
            return $categoryRequestPath;
        }
    }

    /**
     * @param null $originalPrice
     * @param null $specialPrice
     * @return mixed|null
     */
    private function calculateFinalPrice($originalPrice = null, $specialPrice = null)
    {
        $finalPrice = null;
        if (!empty($originalPrice) && !empty($specialPrice)) {
            $finalPrice = $originalPrice < $specialPrice ? $originalPrice : $specialPrice;
        } elseif (!empty($originalPrice)) {
            $finalPrice = $originalPrice;
        } elseif (!empty($specialPrice)) {
            $finalPrice = $specialPrice;
        }
        return $finalPrice;
    }

    /**
     * @param $categoryMediaGallery
     * @return array
     */
    private function prepareMediaGallery($categoryMediaGallery): array
    {
        $mediaImages = array_values($this->json->unserialize($categoryMediaGallery)['images']) ?? [];
        foreach ($mediaImages as &$mediaImage) {
            $mediaImage['position'] = (int) $mediaImage['position'];
            $mediaImage['disabled'] = (bool) $mediaImage['disabled'];
        }
        return $mediaImages;
    }

    /**
     * @param array $children
     *
     * @return array
     */
    private function groupChildrenById(array $children)
    {
        $sortChildrenById = [];

        foreach ($children as $cat) {
            if(is_array($cat) && array_key_exists('id', $cat)) {
                $sortChildrenById[$cat['id']] = $cat;
                $sortChildrenById[$cat['id']]['children_data'] = [];
            }
        }

        return $sortChildrenById;
    }

    /**
     * @param array $categories
     * @param int $rootId
     * @param int $storeId
     *
     * @return array
     */
    private function plotTree(array $categories, int $rootId, int $storeId)
    {
        $categoryTree = [];
        $categoryGroupedByKeys = $this->groupChildrenById($categories);
        $categoryRequestPath = $this->getCategoryUrlPaths($categoryGroupedByKeys);
        foreach ($categories as $categoryData) {
            if(is_array($categoryData) && array_key_exists('parent_id', $categoryData))
            {
                $categoryData['url_path'] = $categoryRequestPath[$categoryData['id']]['request_path'];
                $categoryData['children_data'] = $this->plotTree($categoryData['children_data'], $categoryData['id'], $storeId);
                $categoryTree[] = $categoryData;
            }
        }
        return empty($categoryTree) ? [] : $categoryTree;
    }
}
