<?php

namespace Aureatelabs\FlatPanel\Model\Indexer\DataProvider;

use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Aureatelabs\FlatPanel\Model\ResourceModel\FlatPanel as ResourceModel;

class FlatPanel implements DataProviderInterface
{

    /**
     * @var Json
     */
    private $json;

    /**
     * @var ResourceModel
     */
    private $resourceModel;

    /**
     * @param Json $json
     * @param ResourceModel $resourceModel
     */
    public function __construct(
        Json $json,
        ResourceModel $resourceModel
    ) {
        $this->json = $json;
        $this->resourceModel = $resourceModel;
    }

    /**
     * @param array $indexData
     * @param int $storeId
     * @return array
     */
    public function addData(array $indexData, $storeId): array
    {
        $panelIds = array_column($indexData, 'value_id');
        $assembly = $this->formatStructure($panelIds);
        foreach ($indexData as &$panelData) {
            if (!empty($assembly[$panelData['value_id']])) {
                $panelData['assembly'] = $assembly[$panelData['value_id']];
            }
            $panelData['name'] = $panelData['value'];
            $panelData['status'] = (bool)$panelData['flat_panel_status'];
            $panelData['flat_panel'] = ["id" => (int)$panelData['panel_id'], "name" => $panelData['panel_name']];
            if ($panelData['kitchen_image']) {
                $images = array_values($this->json->unserialize($panelData['kitchen_image'])['images']) ?? [];
                foreach ($images as &$image) {
                    $image['position'] = (int)$image['position'];
                    $image['disabled'] = (bool)$image['disabled'];
                }
                $panelData['kitchen_image'] = $images;
            }
            unset(
                $panelData['attribute_id'],
                $panelData['store_id'],
                $panelData['panel_name'],
                $panelData['value'],
                $panelData['value_id'],
                $panelData['panel_id'],
                $panelData['flat_panel_status']
            );
        }

        return $indexData;
    }

    /**
     * @param $panelIds
     * @return array
     */
    public function formatStructure($panelIds)
    {
        $assembly = [];
        $assemblyInstruction = $this->resourceModel->getAssemblyInstruction();
        foreach ($panelIds as $panelId) {
            foreach ($assemblyInstruction as $value) {
                $styles = explode(",", $value['style']);
                if (in_array($panelId, $styles)) {
                    $typeValue = $value['type'] == 1 ? "PDF instruction" : "Video instruction";
                    $assembly[$panelId][] = [
                        "status" => (bool)$value['assembly_status'],
                        "instruction_name" => $value['value'],
                        "youtube_url" => $value['video_url'],
                        "thumbnail" => $value['thumbnail'],
                        "pdf_file" => $value['psd_file'],
                        "type" => $typeValue,
                        "sort_order" => (int)$value['sort_order'],
                    ];
                }
            }
        }

        return $assembly;
    }
}
