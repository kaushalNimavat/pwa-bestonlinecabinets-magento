<?php

namespace Aureatelabs\FlatPanel\Model\Indexer;

class FlatPanelProcessor extends \Magento\Framework\Indexer\AbstractProcessor
{
    /**
     * Indexer ID
     */
    const INDEXER_ID = 'vsbridge_flatpanel_indexer';
}
