<?php

namespace Aureatelabs\FlatPanel\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;

class FlatPanel
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param int $storeId
     * @param array $panelIds
     * @param int $fromId
     * @param int $limit
     * @return array
     */
    public function getFlatPanels($storeId = 0, array $panelIds = [], $fromId = 0, $limit = 1000): array
    {
        $select = $this->getConnection()
            ->select()
            ->from(['flatpanel' => $this->resourceConnection->getTableName('flat_panel_style_entity_varchar')]);

        $select->join(
            ['link_table' => $this->resourceConnection->getTableName('flat_panel_style_entity')],
            'flatpanel.entity_id= link_table.entity_id',
            ['material_image', 'kitchen_image', 'flat_panel']
        );

        $select->join(
            ['link_table2' => $this->resourceConnection->getTableName('flat_panel_entity_varchar')],
            'link_table.flat_panel= link_table2.entity_id',
            ['link_table2.value_id as panel_id', 'link_table2.entity_id', 'link_table2.value as panel_name', 'link_table.sort_order']
        );

        $select->joinLeft(
            ['link_table3' => $this->resourceConnection->getTableName('flat_panel_style_entity_int')],
            'link_table.entity_id = link_table3.entity_id',
            ['link_table3.value as flat_panel_status']
        );

        $select->where('flatpanel.value_id > ?', $fromId)
            ->limit($limit)
            ->order('flatpanel.value_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getAssemblyInstruction()
    {
        $select = $this->getConnection()
            ->select()
            ->from(['assembly' => $this->resourceConnection->getTableName('assembly_instruction_entity')]);

        $select->joinLeft(
            ['link_table' => $this->resourceConnection->getTableName('assembly_instruction_entity_varchar')],
            'assembly.entity_id = link_table.entity_id',
            ['link_table.value']
        );

        $select->joinLeft(
            ['link_table2' => $this->resourceConnection->getTableName('assembly_instruction_entity_int')],
            'assembly.entity_id = link_table2.entity_id',
            ['link_table2.value as assembly_status']
        );

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private function getConnection()
    {
        return $this->resourceConnection->getConnection();
    }
}
