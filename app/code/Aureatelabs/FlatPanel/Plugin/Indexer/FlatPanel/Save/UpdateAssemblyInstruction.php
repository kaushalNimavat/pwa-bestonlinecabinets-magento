<?php
namespace Aureatelabs\FlatPanel\Plugin\Indexer\FlatPanel\Save;

use Aureatelabs\FlatPanel\Model\Indexer\FlatPanelProcessor;
use Codazon\FlatPanel\Model\AssemblyInstruction;

class UpdateAssemblyInstruction
{
    /**
     * @var $flatPanelProcessor
     */
    private $flatPanelProcessor;

    /**
     * Save constructor.
     *
     * @param FlatPanelProcessor $flatPanelProcessor
     */
    public function __construct(FlatPanelProcessor $flatPanelProcessor)
    {
        $this->flatPanelProcessor = $flatPanelProcessor;
    }

    /**
     * @param AssemblyInstruction $assembly
     * @param AssemblyInstruction $result
     * @return AssemblyInstruction
     */
    public function afterAfterSave(AssemblyInstruction $assembly, AssemblyInstruction $result)
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->flatPanelProcessor->reindexRow($result->getId());
        });

        return $result;
    }

    /**
     * @param AssemblyInstruction $assembly
     * @param AssemblyInstruction $result
     * @return AssemblyInstruction
     */
    public function afterAfterDeleteCommit(AssemblyInstruction $assembly, AssemblyInstruction $result)
    {
        $this->flatPanelProcessor->reindexRow($result->getId());

        return $result;
    }
}
