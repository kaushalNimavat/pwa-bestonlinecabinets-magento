<?php
namespace Aureatelabs\FlatPanel\Plugin\Indexer\FlatPanel\Save;

use Aureatelabs\FlatPanel\Model\Indexer\FlatPanelProcessor;
use Codazon\FlatPanel\Model\FlatPanel;

class UpdateFlatPanel
{
    /**
     * @var $flatPanelProcessor
     */
    private $flatPanelProcessor;

    /**
     * Save constructor.
     *
     * @param FlatPanelProcessor $flatPanelProcessor
     */
    public function __construct(FlatPanelProcessor $flatPanelProcessor)
    {
        $this->flatPanelProcessor = $flatPanelProcessor;
    }

    /**
     * @param FlatPanel $flatpanel
     * @param FlatPanel $result
     * @return FlatPanel
     */
    public function afterAfterSave(FlatPanel $flatpanel, FlatPanel $result)
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->flatPanelProcessor->reindexRow($result->getId());
        });

        return $result;
    }

    /**
     * @param FlatPanel $flatpanel
     * @param FlatPanel $result
     * @return FlatPanel
     */
    public function afterAfterDeleteCommit(FlatPanel $flatpanel, FlatPanel $result)
    {
        $this->flatPanelProcessor->reindexRow($result->getId());

        return $result;
    }
}
