<?php
namespace Aureatelabs\FlatPanel\Plugin\Indexer\FlatPanel\Save;

use Aureatelabs\FlatPanel\Model\Indexer\FlatPanelProcessor;
use Codazon\FlatPanel\Model\FlatPanelStyle;

class UpdateFlatPanelStyle
{
    /**
     * @var $flatPanelProcessor
     */
    private $flatPanelProcessor;

    /**
     * Save constructor.
     *
     * @param FlatPanelProcessor $flatPanelProcessor
     */
    public function __construct(FlatPanelProcessor $flatPanelProcessor)
    {
        $this->flatPanelProcessor = $flatPanelProcessor;
    }

    /**
     * @param FlatPanelStyle $flatpanelstyle
     * @param FlatPanelStyle $result
     * @return FlatPanelStyle
     */
    public function afterAfterSave(FlatPanelStyle $flatpanelstyle, FlatPanelStyle $result)
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->flatPanelProcessor->reindexRow($result->getId());
        });

        return $result;
    }

    /**
     * @param FlatPanelStyle $flatpanelstyle
     * @param FlatPanelStyle $result
     * @return FlatPanelStyle
     */
    public function afterAfterDeleteCommit(FlatPanelStyle $flatpanelstyle, FlatPanelStyle $result)
    {
        $this->flatPanelProcessor->reindexRow($result->getId());

        return $result;
    }
}
