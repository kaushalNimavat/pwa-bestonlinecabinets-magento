<?php

namespace Aureatelabs\FurnitureLayout\Index\Mapping;

use Divante\VsbridgeIndexerCore\Api\MappingInterface;
use Divante\VsbridgeIndexerCore\Api\Mapping\FieldInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;

class FurnitureLayout implements MappingInterface
{
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var string
     */
    private $type;

    /**
     * @param EventManager $eventManager
     */
    public function __construct(EventManager $eventManager)
    {
        $this->eventManager = $eventManager;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array|mixed|null
     */
    public function getMappingProperties()
    {
        $properties = [
            'entity_id' => ['type' => FieldInterface::TYPE_LONG],
            'name' => ['type' => FieldInterface::TYPE_TEXT],
            'structure' => $this->getStructureMapping(),
            'sale_end_date' => [
                'type' => FieldInterface::TYPE_DATE,
                'format' => FieldInterface::DATE_FORMAT,
            ],
            'active' => ['type' => FieldInterface::TYPE_BOOLEAN],
            'push_live' => ['type' => FieldInterface::TYPE_BOOLEAN]
        ];

        $mappingObject = new \Magento\Framework\DataObject();
        $mappingObject->setData('properties', $properties);
        $this->eventManager->dispatch(
            'elasticsearch_furniture_layout_mapping_properties',
            ['mapping' => $mappingObject]
        );
        return $mappingObject->getData();
    }

    /**
     * @return array[]
     */
    public function getStructureMapping(): array
    {
        return [
            'properties' => [
                'cat_id' => ['type' => FieldInterface::TYPE_INTEGER],
                'options' => [
                    'properties' => [
                        'group_title' => ['type' => FieldInterface::TYPE_TEXT],
                        'categories' => $this->getCategoryMapping(),
                        'promo_code' => ['type' => FieldInterface::TYPE_TEXT],
                        'discount_amount' => ['type' => FieldInterface::TYPE_TEXT],
                        'is_sale_on' => ['type' => FieldInterface::TYPE_BOOLEAN],
                        'start_date' => ['type' => FieldInterface::TYPE_TEXT],
                        'discount_type' => ['type' => FieldInterface::TYPE_TEXT],
                        'item_label' => ['type' => FieldInterface::TYPE_TEXT],
                    ]
                ],
                'subcats' => $this->getCategoryMapping(),
            ]
        ];
    }

    /**
     * @return \array[][]
     */
    public function getCategoryMapping(): array
    {
        return [
            'properties' => [
                'name' => ['type' => FieldInterface::TYPE_TEXT],
                'image' => ['type' => FieldInterface::TYPE_TEXT],
                'url_key' => ['type' => FieldInterface::TYPE_TEXT],
                'url_path' => ['type' => FieldInterface::TYPE_TEXT],
                'f_cat_sample_link' => ['type' => FieldInterface::TYPE_TEXT],
                'cat_base_image' => ['type' => FieldInterface::TYPE_TEXT],
                'cat_thumbnail' => ['type' => FieldInterface::TYPE_TEXT],
                'is_active' => ['type' => FieldInterface::TYPE_BOOLEAN],
                'catprice' => ['type' => FieldInterface::TYPE_DOUBLE],
                'spcatprice' => ['type' => FieldInterface::TYPE_DOUBLE],
            ]
        ];
    }
}
