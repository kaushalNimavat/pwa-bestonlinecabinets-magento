<?php

namespace Aureatelabs\FurnitureLayout\Model\Indexer\Action;

use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;
use Aureatelabs\FurnitureLayout\Model\ResourceModel\FurnitureLayout as FurnitureLayoutResource;
use Magento\Store\Model\StoreManagerInterface;

class FurnitureLayout implements RebuildActionInterface
{
    /**
     * @var FurnitureLayoutResource
     */
    private $resourceModel;

    /**
     * @param FurnitureLayoutResource $furnitureLayoutResource
     */
    public function __construct(
        FurnitureLayoutResource $furnitureLayoutResource
    ) {
        $this->resourceModel = $furnitureLayoutResource;
    }

    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     */
    public function rebuild(int $storeId, array $ids): \Traversable
    {
        $lastSalesId = 0;
        do {
            $currentSales = $this->resourceModel->loadCurrentSales($storeId, $ids, $lastSalesId);

            foreach ($currentSales as $currentSale) {
                $lastSalesId = (int) $currentSale['entity_id'];
                $currentSale['id'] = $lastSalesId;
                $currentSale['active'] = (bool)$currentSale['is_active'];
                $currentSale['push_live'] = (bool)$currentSale['push_live'];

                unset($currentSale['is_active']);
                yield $lastSalesId => $currentSale;
            }
        } while (!empty($currentSales));
    }
}
