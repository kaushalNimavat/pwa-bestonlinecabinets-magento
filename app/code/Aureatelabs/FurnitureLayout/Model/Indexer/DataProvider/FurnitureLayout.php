<?php

namespace Aureatelabs\FurnitureLayout\Model\Indexer\DataProvider;

use Divante\VsbridgeIndexerCatalog\Model\Attributes\CategoryAttributes;
use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Category\AttributeDataProvider;
use Divante\VsbridgeIndexerCatalog\Model\SystemConfig\CategoryConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class FurnitureLayout implements DataProviderInterface
{
    /**
     * @var Json
     */
    private $json;

    /**
     * @var AttributeDataProvider
     */
    private $attributeDataProvider;

    /**
     * @var CategoryAttributes
     */
    private $categoryAttributes;

    /**
     * @var CategoryConfigInterface
     */
    private $categoryConfig;

    /**
     * @var string[]
     */
    private $priorFields = [
        'group_title',
        'start_date',
        'item_label'
    ];

    /**
     * @var string[]
     */
    private $categoryGroup = [
        6066 => 'RTA',
        6067 => 'ASSEMBLED',
    ];

    /**
     * @var TimezoneInterface
     */
    private $timezoneInterface;

    /**
     * @var string[]
     */
    private $categoryFields = [
        'name',
        'image',
        'url_key',
        'url_path',
        'f_cat_sample_link',
        'cat_base_image',
        'cat_thumbnail',
        'is_active',
        'catprice',
        'spcatprice',
        'catgroup'
    ];

    /**
     * @param AttributeDataProvider $attributeDataProvider
     * @param CategoryAttributes $categoryAttributes
     * @param TimezoneInterface $timezoneInterface
     * @param CategoryConfigInterface $categoryConfig
     * @param Json $json
     */
    public function __construct(
        AttributeDataProvider $attributeDataProvider,
        CategoryAttributes $categoryAttributes,
        TimezoneInterface $timezoneInterface,
        CategoryConfigInterface $categoryConfig,
        Json $json
    ) {
        $this->attributeDataProvider = $attributeDataProvider;
        $this->categoryAttributes = $categoryAttributes;
        $this->timezoneInterface = $timezoneInterface;
        $this->categoryConfig = $categoryConfig;
        $this->json = $json;
    }

    /**
     * @param array $indexData
     * @param int $storeId
     * @return array
     * @throws \Exception
     */
    public function addData(array $indexData, $storeId): array
    {
        foreach ($indexData as &$saleData) {
            $groups = $this->json->unserialize($saleData['structure'])['groups'] ?? [];
            $groups = $this->formatStructure($groups, $storeId);
            $saleData['structure'] = $groups;
            $saleData['sale_end_date'] = $this->convertDate($saleData['sale_end_date']);
        }
        return $indexData;
    }

    /**
     * @param $structure
     * @param $storeId
     * @return array
     * @throws \Exception
     */
    private function formatStructure($structure, $storeId): array
    {
        foreach ($structure as &$data) {
            foreach ($this->priorFields as $field) {
                $data['options'][$field] = $this->getPriorValues($data['options'], $field);
            }
            $data['options']['is_sale_on'] = (bool) $data['options']['is_sale_on'];

            // Get category information from Ids
            $categories = $data['options']['categories'] ?? null;
            $categoryIds = explode(',', $categories) ?? [];
            $categoriesInfo = $this->getCategories($categoryIds, $storeId);
            $data['options']['categories'] = $categoriesInfo;

            // Get sub category information from Ids
            if (!empty($data['subcats'])) {
                $categoriesInfo = $this->getCategories($data['subcats'], $storeId);
                $data['subcats'] = $categoriesInfo;
            }
        }
        return $structure;
    }

    /**
     * @param $options
     * @param $field
     * @return null
     */
    private function getPriorValues($options, $field)
    {
        if (!empty($options[$field][1])) {
            return $options[$field][1];
        } elseif ($options[$field][0]) {
            return $options[$field][0];
        }
        return null;
    }

    /**
     * @param array $categoryIds
     * @param $storeId
     * @return array
     * @throws \Exception
     */
    private function getCategories(array $categoryIds, $storeId): array
    {
        $categories = [];
        if (!empty($categoryIds)) {
            try {
                $attributes = $this->attributeDataProvider->loadAttributesData(
                    $storeId,
                    $categoryIds,
                    $this->categoryAttributes->getRequiredAttributes($storeId)
                );
                foreach ($attributes as $categoryId => $attributeData) {
                    $categories[$categoryId] = $this->structuredCategoryData(
                        $categoryId, $attributeData, $storeId
                    )[$categoryId];
                }
                $categories = array_values($categories);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }
        return $categories;
    }

    /**
     * @param $categoryId
     * @param $attributeData
     * @param $storeId
     * @return array
     */
    private function structuredCategoryData($categoryId, $attributeData, $storeId): array
    {
        $categories[$categoryId]['id'] = $categoryId;
        foreach ($this->categoryFields as $categoryField) {
            $attrValue = $attributeData[$categoryField] ?? null;
            if ($categoryField === 'catgroup' && isset($attributeData[$categoryField])) {
                $attrValue = $this->categoryGroup[$attributeData[$categoryField]] ?? null;
            } elseif ($categoryField === 'url_path' && isset($attributeData[$categoryField])) {
                $attrValue = $attributeData[$categoryField] . $this->categoryConfig->getCategoryUrlSuffix($storeId) ?? null;
            }
            $categories[$categoryId][$categoryField] = $attrValue;
        }
        return $categories;
    }

    /**
     * @param $date
     * @return string|null
     * @throws \Exception
     */
    private function convertDate($date): ?string
    {
        if ($date) {
            return $this->timezoneInterface
                ->date(new \DateTime($date))
                ->format('Y-m-d H:i:s');
        }
        return null;
    }
}
