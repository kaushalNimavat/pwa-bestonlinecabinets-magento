<?php

namespace Aureatelabs\FurnitureLayout\Model\Indexer\DataProvider;

use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Divante\VsbridgeIndexerCatalog\Model\Attributes\CategoryAttributes;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Category\AttributeDataProvider;
use Divante\VsbridgeIndexerCatalog\Model\SystemConfig\CategoryConfigInterface;

class PairedCategory implements DataProviderInterface
{
    /**
     * @var \Aureatelabs\FurnitureLayout\Model\ResourceModel\PairedCategory
     */
    private $pairedCategory;

    /**
     * @var AttributeDataProvider
     */
    private $attributeDataProvider;

    /**
     * @var CategoryAttributes
     */
    private $categoryAttributes;

    /**
     * @var CategoryConfigInterface
     */
    private $categoryConfig;

    /**
     * @param AttributeDataProvider $attributeDataProvider
     * @param CategoryAttributes $categoryAttributes
     * @param CategoryConfigInterface $categoryConfig
     * @param \Aureatelabs\FurnitureLayout\Model\ResourceModel\PairedCategory $pairedCategory
     */
    public function __construct(
        AttributeDataProvider $attributeDataProvider,
        CategoryAttributes $categoryAttributes,
        CategoryConfigInterface $categoryConfig,
        \Aureatelabs\FurnitureLayout\Model\ResourceModel\PairedCategory $pairedCategory
    ) {
        $this->pairedCategory = $pairedCategory;
        $this->attributeDataProvider = $attributeDataProvider;
        $this->categoryAttributes = $categoryAttributes;
        $this->categoryConfig = $categoryConfig;
    }

    /**
     * @param array $indexData
     * @param int $storeId
     * @return array
     * @throws \Exception
     */
    public function addData(array $indexData, $storeId): array
    {
        $pairedCategories = $this->pairedCategory->loadPairedCategories();
        $categories = array_column($pairedCategories, 'group_2', 'group_1') ?? [];

        $categoriesPath = $this->getCategoryPaths($categories, $storeId);
        $categoryPathInformation = $this->setLinkedCategoryPath($categories, $categoriesPath);
        foreach ($categoryPathInformation as $categoryId => $categoryLink) {
            if (isset($indexData[$categoryId])) {
                $indexData[$categoryId]['linked_cat_url'] = $categoryLink;
            }
        }
        return $indexData;
    }

    /**
     * @param $categoryIds
     * @param $storeId
     * @return array
     * @throws \Exception
     */
    private function getCategoryPaths($categoryIds, $storeId): array
    {
        if ($categoryIds) {
            $totalCategories = array_merge($categoryIds, array_keys($categoryIds));
            $allCategoryIds = array_map('intval', $totalCategories);
            if (!empty($allCategoryIds)) {
                try {
                    $attributes = $this->attributeDataProvider->loadAttributesData(
                        $storeId,
                        $allCategoryIds,
                        $this->categoryAttributes->getRequiredAttributes($storeId)
                    );
                    foreach ($attributes as $categoryId => $attributeData) {
                        if ($attributeData['url_path']) {
                            $categories[$categoryId] = $attributeData['url_path']
                                . $this->categoryConfig->getCategoryUrlSuffix($storeId);
                        }
                    }
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
        }
        return $categories ?? [];
    }

    /**
     * @param $categories
     * @param $categoriesPath
     * @return array
     */
    private function setLinkedCategoryPath($categories, $categoriesPath): array
    {
        $reverseCategories = array_flip($categories);
        foreach ($categories as $group1 => $group2) {
            $categoryPathByIds[$group1] = $categoriesPath[$group2];
        }

        foreach ($reverseCategories as $group1 => $group2) {
            $categoryPathByIds[$group1] = $categoriesPath[$group2];
        }
        return $categoryPathByIds ?? [];
    }
}
