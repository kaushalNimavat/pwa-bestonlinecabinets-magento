<?php

namespace Aureatelabs\FurnitureLayout\Model\Indexer;

class FurnitureLayoutProcessor extends \Magento\Framework\Indexer\AbstractProcessor
{
    /**
     * Indexer ID
     */
    const INDEXER_ID = 'vsbridge_current_sale_indexer';
}
