<?php

namespace Aureatelabs\FurnitureLayout\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

class FurnitureLayout
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * Current Sales constructor.
     *
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resource = $resourceConnection;
    }

    /**
     * @param int $storeId
     * @param array $salesIds
     * @param int $fromId
     * @param int $limit
     *
     * @return array
     */
    public function loadCurrentSales(int $storeId = 1, array $salesIds = [], int $fromId = 0, int $limit = 1000): array
    {
        $select = $this->getConnection()->select()->from(
            ['current_sale' => $this->getConnection()->getTableName('currentsale')]
        );

        if (!empty($salesIds)) {
            $select->where('current_sale.entity_id IN (?)', $salesIds);
        }

        $select->where('is_active = ?', 1)
            ->where('current_sale.entity_id > ?', $fromId)
            ->limit($limit)
            ->order('current_sale.entity_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @return AdapterInterface
     */
    private function getConnection(): AdapterInterface
    {
        return $this->resource->getConnection();
    }
}
