<?php

namespace Aureatelabs\FurnitureLayout\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

class PairedCategory
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * Current Sales constructor.
     *
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resource = $resourceConnection;
    }

    /**
     * @return array
     */
    public function loadPairedCategories(): array
    {
        $select = $this->getConnection()->select()->from(
            ['paired_category' => $this->getConnection()->getTableName('fly_category_pair')]
        );
        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @return AdapterInterface
     */
    private function getConnection(): AdapterInterface
    {
        return $this->resource->getConnection();
    }
}
