<?php

namespace Aureatelabs\FurnitureLayout\Observer;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ResetPushLive implements ObserverInterface
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $model = $observer->getData('model');
        if ($model->getPushLive() === "1") {
            $this->resetPushLive();
        }
    }

    /**
     * @throws \Exception
     */
    private function resetPushLive()
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $connection->getTableName('currentsale');
        try {
            $connection->update($tableName, ['push_live' => 0], ['is_active = ?' => 1]);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
