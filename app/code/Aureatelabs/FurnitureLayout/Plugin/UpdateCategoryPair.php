<?php

namespace Aureatelabs\FurnitureLayout\Plugin;

use Codazon\FurnitureLayout\Model\CategoryPair;
use Divante\VsbridgeIndexerCatalog\Model\Indexer\CategoryProcessor;

class UpdateCategoryPair
{
    /**
     * @var CategoryProcessor
     */
    private $categoryProcessor;

    /**
     * @param CategoryProcessor $categoryProcessor
     */
    public function __construct(CategoryProcessor $categoryProcessor)
    {
        $this->categoryProcessor = $categoryProcessor;
    }

    /**
     * @param CategoryPair $subject
     * @param CategoryPair $result
     * @return CategoryPair
     */
    public function afterAfterSave(CategoryPair $subject, CategoryPair $result): CategoryPair
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->categoryProcessor->reindexList([$result->getData('group_1'), $result->getData('group_2')]);
        });

        return $result;
    }

    /**
     * @param CategoryPair $subject
     * @param CategoryPair $result
     * @return CategoryPair
     */
    public function afterAfterDeleteCommit(CategoryPair $subject, CategoryPair $result): CategoryPair
    {
        $this->categoryProcessor->reindexList([$result->getData('group_1'), $result->getData('group_2')]);
        return $result;
    }
}
