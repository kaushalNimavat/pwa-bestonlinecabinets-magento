<?php

namespace Aureatelabs\FurnitureLayout\Plugin;

use Codazon\FurnitureLayout\Model\Currentsale;
use Aureatelabs\FurnitureLayout\Model\Indexer\FurnitureLayoutProcessor;

class UpdateFurnitureLayout
{
    /**
     * @var FurnitureLayoutProcessor
     */
    private $furnitureLayoutProcessor;

    /**
     * @param FurnitureLayoutProcessor $furnitureLayoutProcessor
     */
    public function __construct(FurnitureLayoutProcessor $furnitureLayoutProcessor)
    {
        $this->furnitureLayoutProcessor = $furnitureLayoutProcessor;
    }

    /**
     * @param Currentsale $subject
     * @param Currentsale $result
     * @return Currentsale
     */
    public function afterAfterSave(Currentsale $subject, Currentsale $result): Currentsale
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->furnitureLayoutProcessor->reindexAll();
        });

        return $result;
    }

    /**
     * @param Currentsale $subject
     * @param Currentsale $result
     * @return Currentsale
     */
    public function afterAfterDeleteCommit(Currentsale $subject, Currentsale $result): Currentsale
    {
        $this->furnitureLayoutProcessor->reindexRow($result->getId());
        return $result;
    }
}
