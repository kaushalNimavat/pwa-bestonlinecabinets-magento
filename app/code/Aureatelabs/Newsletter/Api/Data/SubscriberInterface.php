<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Newsletter
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Newsletter\Api\Data;

/**
 * Interface SubscriberInterface
 * @api
 * @package Aureatelabs\Newsletter\Api\Data
 */
interface SubscriberInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const EMAIL   = 'email';
    const CONFIRM   = 'confirm';
    /**#@-*/

    /**
     * Get Email
     * @return string|null
     */
    public function getEmail();

    /**
     * Set Email
     * @param string $email
     * @return $this
     */
    public function setEmail($email);

    /**
     * Get Confirm
     * @return int|null
     */
    public function getConfirm();

    /**
     * Set Confirm
     * @param int $confirm
     * @return $this
     */
    public function setConfirm($confirm);
}
