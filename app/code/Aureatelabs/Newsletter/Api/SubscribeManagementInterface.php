<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Newsletter
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Newsletter\Api;

use Aureatelabs\Newsletter\Api\Data\SubscriberInterface;

/**
 * Interface SubscribeManagementInterface
 * @package Aureatelabs\Newsletter\Api
 */
interface SubscribeManagementInterface
{
    /**
     * POST for subscribe api
     * @param SubscriberInterface $subscriber
     * @return NewsletterResultInterface
     */
    public function postSubscribe(SubscriberInterface $subscriber): NewsletterResultInterface;
}
