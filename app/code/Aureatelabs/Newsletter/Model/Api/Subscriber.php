<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Newsletter
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Newsletter\Model\Api;

use Aureatelabs\Newsletter\Api\Data\SubscriberInterface;

/**
 * Class Subscriber
 * @package Aureatelabs\Newsletter\Model\Api
 */
class Subscriber implements SubscriberInterface
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $confirm;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this|SubscriberInterface
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int
     */
    public function getConfirm()
    {
        return $this->confirm;
    }

    /**
     * @param int $confirm
     * @return $this|SubscriberInterface
     */
    public function setConfirm($confirm)
    {
        $this->confirm = $confirm;
        return $this;
    }
}
