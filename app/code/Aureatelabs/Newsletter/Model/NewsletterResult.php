<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Newsletter
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Newsletter\Model;

/**
 * Class NewsletterResult
 * @package Aureatelabs\Newsletter\Model
 */
class NewsletterResult implements \Aureatelabs\Newsletter\Api\NewsletterResultInterface
{
    /**
     * @var integer
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     * @var mixed
     */
    private $result;

    /**
     * Get response code.
     * @return integer/null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set response code.
     * @param integer $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get response message.
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set response message.
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get result data.
     * @return string|null
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set result data.
     * @param string|null $result
     * @return $this
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }
}
