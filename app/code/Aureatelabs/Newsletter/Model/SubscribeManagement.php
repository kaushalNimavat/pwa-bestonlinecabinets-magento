<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Newsletter
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Newsletter\Model;

use Aureatelabs\Newsletter\Api\Data\SubscriberInterface;
use Aureatelabs\Newsletter\Api\NewsletterResultInterface;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Framework\Validator\EmailAddress as EmailValidator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Newsletter\Model\Subscriber;

/**
 * Class SubscribeManagement
 * @package Aureatelabs\Newsletter\Model
 */
class SubscribeManagement implements \Aureatelabs\Newsletter\Api\SubscribeManagementInterface
{
    /**
     * @var NewsletterResultInterface
     */
    private $_result;

    /**
     * Subscriber factory
     * @var SubscriberFactory
     */
    protected $_subscriberFactory;

    /**
     * @var EmailValidator
     */
    private $_emailValidator;

    /**
     * @param SubscriberFactory $subscriberFactory
     * @param NewsletterResultInterface $result
     * @param EmailValidator $emailValidator
     */
    public function __construct(
        SubscriberFactory $subscriberFactory,
        NewsletterResultInterface $result,
        EmailValidator $emailValidator
    ) {
        $this->_result = $result;
        $this->_subscriberFactory = $subscriberFactory;
        $this->_emailValidator = $emailValidator;
    }

    /**
     * Accept newsletter subscriber request.
     * @param SubscriberInterface $subscriber
     * @return NewsletterResultInterface
     */
    public function postSubscribe(SubscriberInterface $subscriber): NewsletterResultInterface
    {
        $this->_result->setCode(400)->setMessage('Error');
        try {

            if (!$subscriber->getConfirm()) {
                throw new \Exception('This is an invalid subscription confirmation.');
            }

            if (!$subscriber->getEmail() || !$this->_emailValidator->isValid($subscriber->getEmail())) {
                throw new \Exception('Please enter a valid email address.');
            }

            $model = $this->_subscriberFactory->create()->loadByEmail($subscriber->getEmail());
            if ($model->getId() && (int) $model->getSubscriberStatus() === Subscriber::STATUS_SUBSCRIBED) {
                throw new LocalizedException(
                    __('This email address is already subscribed.')
                );
            }

            $status = $this->_subscriberFactory->create()->subscribe($subscriber->getEmail());
            if ($status == \Magento\Newsletter\Model\Subscriber::STATUS_NOT_ACTIVE) {
                $result = __('The confirmation request has been sent.');
            } else {
                $result = __('Thank you for your subscription.');
            }

            $this->_result->setCode(200)->setMessage('Success');
        } catch (\Exception $e) {
            $result = $e->getMessage();
        }

        $this->_result->setResult($result);
        return $this->_result;
    }
}
