<?php
namespace Aureatelabs\Promotion\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;

abstract class Promotion extends Action
{
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var string
     */
    const ADMIN_RESOURCE = 'Aureatelabs_Promotion::aureatelabs';

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Aureatelabs'), __('Aureatelabs'))
            ->addBreadcrumb(__('Promotion'), __('Promotion'));
        return $resultPage;
    }
}
