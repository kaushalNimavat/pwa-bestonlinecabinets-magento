<?php
namespace Aureatelabs\Promotion\Controller\Adminhtml\Promotion;

use Aureatelabs\Promotion\Controller\Adminhtml\Promotion;
use Magento\Backend\Model\View\Result\Redirect;

class Delete extends Promotion
{
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Aureatelabs\Promotion\Model\Promotion::class);
                $model->load($id);

                $model->delete();

                $this->messageManager->addSuccessMessage(__('The promotion has been deleted.'));
                $this->_eventManager->dispatch('aureate_promotion_on_delete', [
                    'status' => 'success'
                ]);

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {

                $this->_eventManager->dispatch('aureate_promotion_on_delete', [
                    'status' => 'fail'
                ]);

                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }

        $this->messageManager->addErrorMessage(__('We can\'t find a promotion to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
