<?php

namespace Aureatelabs\Promotion\Model\Indexer\Action;

use Aureatelabs\Promotion\Model\ResourceModel\Promotion as ResourceModel;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;

class Promotion implements RebuildActionInterface
{

    /**
     * @var ResourceModel
     */
    private $resourceModel;

    /**
     * @param ResourceModel $resourceModel
     */
    public function __construct(
        ResourceModel $resourceModel
    ) {
        $this->resourceModel = $resourceModel;
    }

    /**
     * @param int $storeId
     * @param array $promotionIds
     * @return \Traversable
     */
    public function rebuild(int $storeId = 1, array $promotionIds = []): \Traversable
    {
        $lastPromotionId = 0;
        do {
            $promotions = $this->resourceModel->getPromotions($storeId, $promotionIds, $lastPromotionId);
            foreach ($promotions as $promotion) {
                $lastPromotionId = (int)$promotion['promotion_id'];
                $promotion['promotion_id'] = $lastPromotionId;
                yield $lastPromotionId => $promotion;
            }

        } while (!empty($promotions));
    }
}
