<?php

namespace Aureatelabs\Promotion\Model\Indexer\DataProvider;

use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Magento\Cms\Model\Template\FilterProvider;

class Promotion implements DataProviderInterface
{
    /**
     * @var FilterProvider
     */
    private $templateFilter;

    /**
     * @param FilterProvider $templateFilter
     */
    public function __construct(
        FilterProvider $templateFilter
    ) {
        $this->templateFilter = $templateFilter;
    }

    /**
     * @param array $indexData
     * @param int $storeId
     * @return array
     * @throws \Exception
     */
    public function addData(array $indexData, $storeId): array
    {
        foreach ($indexData as &$promotion) {
            $promotion['status'] = (bool)$promotion['status'];
            $promotion['promotion_content'] = $this->templateFilter->getPageFilter()->filter($promotion['promotion_content']);
        }

        return $indexData;
    }
}
