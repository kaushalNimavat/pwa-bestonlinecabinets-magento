<?php

namespace Aureatelabs\Promotion\Model\Indexer;

use Magento\Framework\Indexer\AbstractProcessor;

class PromotionProcessor extends AbstractProcessor
{
    /**
     * Indexer ID
     */
    const INDEXER_ID = 'vsbridge_promotion_indexer';
}
