<?php

namespace Aureatelabs\Promotion\Model;

use Magento\Framework\Model\AbstractModel;

class Promotion extends AbstractModel
{
    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Aureatelabs\Promotion\Model\ResourceModel\Promotion');
    }

    /**
     * @return $this|Promotion
     */
    public function beforeSave()
    {
        parent::beforeSave();
        if ($this->hasDataChanges()) {
            $this->setUpdatedAt(null);
        }

        return $this;
    }
}
