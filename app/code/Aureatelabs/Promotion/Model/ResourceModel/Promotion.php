<?php
namespace Aureatelabs\Promotion\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Promotion extends AbstractDb
{
    /**
     * @var string
     */
    protected $_idFieldName = 'promotion_id';

    /**
     * @param Context $context
     * @param null $resourcePrefix
     */
    public function __construct(
        Context $context,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
    }

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('aureatelabs_promotion', 'promotion_id');
    }

    /**
     * @param int $storeId
     * @param array $promotionIds
     * @param int $fromId
     * @return array
     */
    public function getPromotions($storeId = 0, array $promotionIds = [], $fromId = 0): array
    {
        $select = $this->getConnection()->select()->from(['promotion' => $this->getTable('aureatelabs_promotion')]);
        $select->where('promotion.promotion_id > ?', $fromId)
            ->order('promotion.promotion_id');

        return $this->getConnection()->fetchAll($select);
    }
}
