<?php
namespace Aureatelabs\Promotion\Model\ResourceModel\Promotion;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'promotion_id';

    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init(
            'Aureatelabs\Promotion\Model\Promotion',
            'Aureatelabs\Promotion\Model\ResourceModel\Promotion'
        );
    }
}
