<?php

namespace Aureatelabs\Promotion\Plugin\Indexer\Promotion\Save;

use Aureatelabs\Promotion\Model\Indexer\PromotionProcessor;
use Aureatelabs\Promotion\Model\Promotion as Promotion;

class UpdatePromotion
{
    /**
     * @var PromotionProcessor
     */
    private $promotionProcessor;

    /**
     * @param PromotionProcessor $promotionProcessor
     */
    public function __construct(PromotionProcessor $promotionProcessor)
    {
        $this->promotionProcessor = $promotionProcessor;
    }

    /**
     * @param Promotion $subject
     * @param Promotion $result
     * @return Promotion
     */
    public function afterAfterSave(Promotion $subject, Promotion $result): Promotion
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->promotionProcessor->reindexRow($result->getId());
        });

        return $result;
    }

    /**
     * @param Promotion $subject
     * @param Promotion $result
     * @return Promotion
     */
    public function afterAfterDeleteCommit(Promotion $subject, Promotion $result): Promotion
    {
        $this->promotionProcessor->reindexRow($result->getId());

        return $result;
    }
}
