<?php
namespace Aureatelabs\Promotion\Ui\Component\Listing\Column\Status;

use Magento\Framework\Data\OptionSourceInterface;

class Options implements OptionSourceInterface
{
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['label' => __('Disabled'), 'value' => self::STATUS_DISABLED],
            ['label' => __('Enabled'), 'value' => self::STATUS_ENABLED]
        ];
    }
}
