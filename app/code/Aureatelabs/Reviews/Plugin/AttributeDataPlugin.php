<?php

namespace Aureatelabs\Reviews\Plugin;

use Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\AttributeData;
use Magento\Framework\App\ResourceConnection;

class AttributeDataPlugin
{
    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @param ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->resource = $resource;
    }

    /**
     * @param AttributeData $subject
     * @param $result
     * @param $indexData
     * @param $storeId
     * @return mixed
     */
    public function afterAddData(
        AttributeData $subject,
        $result,
        $indexData,
        $storeId
    ) {
        $allReviewData = $this->getReviewData();
        foreach ($result as $idx => $product) {
            if ($product['id'] && isset($allReviewData[$product['id']])) {
                $result[$idx]["no_of_review"] = count($allReviewData[$product['id']]);
            }

            if (!empty($allReviewData[$product['id']])) {
                $ratingCount = array_sum(array_column($allReviewData[$product['id']], 'value'));
                $rating = $ratingCount / count($allReviewData[$product['id']]);
                $result[$idx]['rating'] = number_format((float)$rating, 2, '.', '');
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getReviewData()
    {
        $allReviewData = [];
        $connection = $this->resource->getConnection();
        $review =  $this->resource->getTableName('review');
        $reviewDetailTable =  $this->resource->getTableName('review_detail');
        $ratingTable =  $this->resource->getTableName('rating_option_vote');
        $review = $connection->select()
            ->from(['review' => $review], ['review_id', 'status_id', 'entity_pk_value','created_at'])
            ->joinInner(
                ['review_detail' => $reviewDetailTable],
                'review.review_id = review_detail.review_id',
                ['detail_id', 'detail', 'title','nickname']
            )->joinInner(
                ['rating_table' => $ratingTable],
                'review.review_id = rating_table.review_id',
                ['value']
            )->where('review.status_id = 1')->order(['rating_table.value DESC']);
        $reviewData = $connection->fetchAll($review);
        foreach ($reviewData as $reviewDetail) {
            $allReviewData[$reviewDetail['entity_pk_value']][] = $reviewDetail;
        }
        return $allReviewData;
    }
}
