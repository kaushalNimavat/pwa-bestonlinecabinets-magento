<?php

namespace Aureatelabs\SellRank\Cron;

use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory as BestSellerCollectionFactory;

class CalculateSellRank
{
    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var BestSellerCollectionFactory
     */
    private $bestSellerCollectionFactory;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @param CollectionFactory $productCollectionFactory
     * @param BestSellerCollectionFactory $collectionFactory
     * @param Action $productAction
     */
    public function __construct(
        CollectionFactory $productCollectionFactory,
        BestSellerCollectionFactory $bestSellerCollectionFactory,
        Action $productAction
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->bestSellerCollectionFactory = $bestSellerCollectionFactory;
        $this->productAction = $productAction;
    }

    /**
     * @throws \Exception
     */
    public function execute()
    {
        $allProductIds = $this->productCollectionFactory->create()
            ->addFieldToFilter('status', ['eq' => 1])
            ->getAllIds();

        $bestSellers = $this->bestSellerCollectionFactory->create()->setPeriod('month');
        $bestSellerProducts = [];
        foreach ($bestSellers as $product) {
            $bestSellerProducts[$product->getProductId()] = ($bestSellerProducts[$product->getProductId()] ?? 0) + (float)$product->getQtyOrdered();
        }
        $this->exposedDataToAttribute($allProductIds, $bestSellerProducts);
    }

    /**
     * @param $productsIds
     * @param $orderedItems
     * @throws \Exception
     */
    private function exposedDataToAttribute($productsIds, $orderedItems)
    {
        $valueWithZero = $valueWithNonZero = [];
        if (!empty($orderedItems)) {
            $allProducts = array_fill_keys($productsIds, 0);
            $valueWithZero = array_diff_key($allProducts, array_intersect_key($allProducts, $orderedItems));
            foreach ($orderedItems as $productId => $count) {
                if (in_array($productId, $productsIds)) {
                    $valueWithNonZero[$count][] = $productId;
                }
            }
        }
        if(!empty($valueWithZero)) {
            $this->updateAttributeData(array_keys($valueWithZero), 'sell_rank', 0);
        }
        if (!empty($valueWithNonZero)) {
            foreach ($valueWithNonZero as $orderItemCount => $orderProductIds) {
                $this->updateAttributeData($orderProductIds, 'sell_rank', (float) $orderItemCount);
            }
        }
    }

    /**
     * @param $productIds
     * @param $attributeCode
     * @param $attributeValue
     * @throws \Exception
     */
    private function updateAttributeData($productIds, $attributeCode, $attributeValue)
    {
        try {
            $this->productAction->updateAttributes($productIds, [$attributeCode => $attributeValue], 0);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
