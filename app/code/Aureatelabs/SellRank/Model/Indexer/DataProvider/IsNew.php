<?php
namespace Aureatelabs\SellRank\Model\Indexer\DataProvider;

use Aureatelabs\SellRank\Model\ResourceModel\CategoryPath;
use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class IsNew implements DataProviderInterface
{
    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var CategoryPath
     */
    private $categoryPath;

    /**
     * @param CollectionFactory $productCollectionFactory
     * @param CategoryPath $categoryPath
     */
    public function __construct(
        CollectionFactory $productCollectionFactory,
        CategoryPath $categoryPath
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryPath = $categoryPath;
    }

    /**
     * @param array $indexData
     * @param int $storeId
     * @return array
     */
    public function addData(array $indexData, $storeId): array
    {
        $productIds = $this->productCollectionFactory->create()
            ->addAttributeToFilter('news_to_date', ['gteq' => date('Y-m-d')])->getAllIds();
        $categoryPaths = $this->categoryPath->loadCategoryPath();
        $categoriesPath = array_column($categoryPaths, 'path', 'entity_id');
        foreach ($indexData as &$product) {
            $product['is_new'] = 0;
            if (in_array($product['id'], $productIds)) {
                $product['is_new'] = 1;
            }
            if (isset($product['category']) && !empty($product['category'])) {
                $product['category'] = $this->setCategoryPath($categoriesPath, $product['category']);
            }
        }
        return $indexData;
    }

    /**
     * @param $categoriesPath
     * @param $categoryArr
     * @return mixed
     */
    private function setCategoryPath($categoriesPath, $categoryArr)
    {
        if (empty($categoryArr)) {
            return $categoryArr;
        }
        foreach ($categoryArr as &$category) {
            if (isset($categoriesPath[$category['category_id']])) {
                $category['path'] = $categoriesPath[$category['category_id']];
            }
        }
        return $categoryArr;
    }
}
