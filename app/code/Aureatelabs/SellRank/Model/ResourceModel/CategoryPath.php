<?php

namespace Aureatelabs\SellRank\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

class CategoryPath
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * Current Sales constructor.
     *
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resource = $resourceConnection;
    }

    /**
     * @return array
     */
    public function loadCategoryPath(): array
    {
        $select = $this->getConnection()->select(['path', 'entity_id'])->from(
            ['category' => $this->getConnection()->getTableName('catalog_category_entity')]
        );

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @return AdapterInterface
     */
    private function getConnection(): AdapterInterface
    {
        return $this->resource->getConnection();
    }
}
