<?php

namespace Aureatelabs\SellRank\Setup\Patch\Data;

use Magento\Catalog\Model\Category;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class CreateCategoryContentAttribute implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * CreateNutritionTblAttribute constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @return CreateCategoryContentAttribute|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->removeAttribute(Category::ENTITY, 'category_top_content');
        if (!$eavSetup->getAttributeId(Category::ENTITY, 'category_top_content')) {
            $eavSetup->addAttribute(Category::ENTITY, 'category_top_content', [
                'type'     => 'text',
                'label'    => 'Category Top Content',
                'input'    => 'text',
                'visible'  => true,
                'default'  => '',
                'required' => false,
                'global'   => ScopedAttributeInterface::SCOPE_STORE,
                'group'    => 'Content',
            ]);
        }

        $eavSetup->removeAttribute(Category::ENTITY, 'category_bottom_content');
        if (!$eavSetup->getAttributeId(Category::ENTITY, 'category_bottom_content')) {
            $eavSetup->addAttribute(Category::ENTITY, 'category_bottom_content', [
                'type'     => 'text',
                'label'    => 'Category Bottom Content',
                'input'    => 'text',
                'visible'  => true,
                'default'  => '',
                'required' => false,
                'global'   => ScopedAttributeInterface::SCOPE_STORE,
                'group'    => 'Content',
            ]);
        }
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }
}
