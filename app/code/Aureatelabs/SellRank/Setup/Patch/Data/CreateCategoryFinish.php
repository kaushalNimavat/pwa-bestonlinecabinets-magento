<?php

namespace Aureatelabs\SellRank\Setup\Patch\Data;

use Magento\Catalog\Model\Category;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class CreateCategoryFinish implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * CreateNutritionTblAttribute constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @return CreateCategoryContentAttribute|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->removeAttribute(Category::ENTITY, 'category_finish');
        if (!$eavSetup->getAttributeId(Category::ENTITY, 'category_finish')) {
            $eavSetup->addAttribute(Category::ENTITY, 'category_finish', [
                'type'     => 'varchar',
                'label'    => 'Finish',
                'input'    => 'multiselect',
                'visible'  => true,
                'default'  => '',
                'required' => false,
                'user_defined' => true,
                'global'   => ScopedAttributeInterface::SCOPE_STORE,
                'group'    => 'Extra Attributes',
                'source'   => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
                'backend'   => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'option' => [
                    'values' => [
                        'White Kitchen Cabinets',
                        'Off White/Cream Kitchen Cabinets',
                        'Light Kitchen Cabinets',
                        'Medium Kitchen CabinetsGrey Kitchen Cabinets',
                        'Dar Kitchen Cabinets',
                    ],
                ]
            ]);
        }
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }
}
