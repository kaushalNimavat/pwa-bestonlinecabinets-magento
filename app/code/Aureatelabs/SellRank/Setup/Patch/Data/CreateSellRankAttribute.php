<?php

namespace Aureatelabs\SellRank\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class CreateSellRankAttribute implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * CreateNutritionTblAttribute constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function getAliases(): array
    {
        return [];
    }

    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->removeAttribute(Product::ENTITY, 'sell_rank');
        if (!$eavSetup->getAttributeId(Product::ENTITY, 'sell_rank')) {
            $eavSetup->addAttribute(Product::ENTITY, 'sell_rank', [
                'label' => 'Sell Rank',
                'input' => 'text',
                'type' => 'int',
                'required' => 0,
                'visible_on_front' => 1,
                'filterable' => 0,
                'searchable' => 0,
                'comparable' => 0,
                'user_defined' => 1,
                'is_configurable' => 0,
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'note' => '',
            ]);
        }
    }

    public static function getDependencies(): array
    {
        return [];
    }
}
