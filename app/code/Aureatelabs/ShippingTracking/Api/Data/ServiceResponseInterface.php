<?php

namespace Aureatelabs\ShippingTracking\Api\Data;

use Aureatelabs\ShippingTracking\Api\Data\TrackingInfoInterface;

interface ServiceResponseInterface
{
    const TRACKING_INFO = 'tracking_info';
    const STATUS = 'status';

    /**
     * @param $trackingInfo
     * @return \Aureatelabs\ShippingTracking\Api\Data\TrackingInfoInterface[]
     */
    public function setTrackingInformation($trackingInfo);

    /**
     * @return \Aureatelabs\ShippingTracking\Api\Data\TrackingInfoInterface[]
     */
    public function getTrackingInformation();

    /**
     * @param $status
     * @return string
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getStatus();
}
