<?php

namespace Aureatelabs\ShippingTracking\Api\Data;

interface TrackingInfoInterface
{
    const ORDER_ID = 'order_id';
    const ORDER_STATUS = 'order_status';
    const TRACKING_NUMBER = 'track_number';
    const DESCRIPTION = 'description';
    const TITLE = 'title';

    /**
     * @return string
     */
    public function getOrderId();

    /**
     * @param $orderId
     * @return string
     */
    public function setOrderId($orderId);

    /**
     * @return string
     */
    public function getOrderStatus();

    /**
     * @param $orderStatus
     * @return string
     */
    public function setOrderStatus($orderStatus);

    /**
     * @return string
     */
    public function getTrackingNumber();

    /**
     * @param $trackingNumber
     * @return string
     */
    public function setTrackingNumber($trackingNumber);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param $description
     * @return string
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param $title
     * @return string
     */
    public function setTitle($title);
}
