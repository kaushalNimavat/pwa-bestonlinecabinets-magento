<?php

namespace Aureatelabs\ShippingTracking\Api;

use Aureatelabs\ShippingTracking\Api\Data\ServiceResponseInterface;

interface ServiceInterface
{
    /**
     * @param string $orderId
     * @return ServiceResponseInterface
     */
    public function getTrackingInformation(string $orderId): ServiceResponseInterface;
}
