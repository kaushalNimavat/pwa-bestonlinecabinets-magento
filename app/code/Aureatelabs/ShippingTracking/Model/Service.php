<?php

namespace Aureatelabs\ShippingTracking\Model;

use Aureatelabs\ShippingTracking\Api\Data\TrackingInfoInterface;
use Aureatelabs\ShippingTracking\Api\ServiceInterface;
use Aureatelabs\ShippingTracking\Api\Data\ServiceResponseInterface;
use Aureatelabs\ShippingTracking\Api\Data\TrackingInfoInterfaceFactory;
use Magento\Sales\Api\Data\OrderInterface;

class Service implements ServiceInterface
{

    /**
     * @var OrderInterface
     */
    private $order;

    /**
     * @var ServiceResponseInterface
     */
    private $serviceResponse;

    /**
     * @var TrackingInfoInterface
     */
    private $trackingInfo;

    /**
     * @param ServiceResponseInterface $serviceResponse
     * @param TrackingInfoInterfaceFactory $trackingInfo
     * @param OrderInterface $order
     */
    public function __construct(
        ServiceResponseInterface $serviceResponse,
        TrackingInfoInterfaceFactory $trackingInfo,
        OrderInterface $order
    ) {
        $this->serviceResponse = $serviceResponse;
        $this->trackingInfo = $trackingInfo;
        $this->order = $order;
    }

    /**
     * @param string $orderId
     * @return ServiceResponseInterface
     */
    public function getTrackingInformation(string $orderId): ServiceResponseInterface
    {
        $order = $this->order->loadByIncrementId($orderId);
        $tracksCollection = $order->getTracksCollection();
        $trackInfo = [];
        if (!empty($tracksCollection->getItems())) {
            foreach ($tracksCollection->getItems() as $track) {
                /** @var TrackingInfoInterface $trackingInformation */
                $trackingInformation = $this->trackingInfo->create();
                $trackingInformation->setOrderId($orderId);
                $trackingInformation->setOrderStatus($order->getStatus());
                $trackingInformation->setTitle($track->getTitle());
                $trackingInformation->setDescription($track->getDescription() ?? null);
                $trackingInformation->setTrackingNumber($track->getTrackNumber());
                $trackInfo[] = $trackingInformation;
            }
            $this->serviceResponse->setTrackingInformation($trackInfo);
            $this->serviceResponse->setStatus('success');
        } else {
            $trackInfo = [
                "message" => __("There is no tracking information available")
            ];
            $this->serviceResponse->setTrackingInformation($trackInfo);
            $this->serviceResponse->setStatus('error');
        }
        return $this->serviceResponse;
    }
}
