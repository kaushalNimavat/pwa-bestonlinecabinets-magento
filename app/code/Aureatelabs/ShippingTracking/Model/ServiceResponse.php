<?php

namespace Aureatelabs\ShippingTracking\Model;

use Aureatelabs\ShippingTracking\Api\Data\ServiceResponseInterface;
use Magento\Framework\Model\AbstractModel;

class ServiceResponse extends AbstractModel implements ServiceResponseInterface
{
    /**
     * @param $trackingInfo
     * @return array|ServiceResponse
     */
    public function setTrackingInformation($trackingInfo)
    {
        return $this->setData(self::TRACKING_INFO, $trackingInfo);
    }

    /**
     * @param $status
     * @return ServiceResponse|string
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @return array|mixed|null
     */
    public function getTrackingInformation()
    {
        return $this->getData(self::TRACKING_INFO);
    }

    /**
     * @return array|mixed|string|null
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }
}
