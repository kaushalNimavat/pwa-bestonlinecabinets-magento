<?php

namespace Aureatelabs\ShippingTracking\Model;

use Aureatelabs\ShippingTracking\Api\Data\TrackingInfoInterface;
use Magento\Framework\Model\AbstractModel;

class TrackingInfo extends AbstractModel implements TrackingInfoInterface
{
    /**
     * @return array|mixed|string|null
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * @param $orderId
     * @return TrackingInfo|string
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * @return array|mixed|string|null
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @param $title
     * @return TrackingInfo|string
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @return array|mixed|string|null
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @param $description
     * @return TrackingInfo|string
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @return array|mixed|string|null
     */
    public function getTrackingNumber()
    {
        return $this->getData(self::TRACKING_NUMBER);
    }

    /**
     * @param $trackingNumber
     * @return TrackingInfo|string
     */
    public function setTrackingNumber($trackingNumber)
    {
        return $this->setData(self::TRACKING_NUMBER, $trackingNumber);
    }

    /**
     * @return array|mixed|string|null
     */
    public function getOrderStatus()
    {
        return $this->getData(self::ORDER_STATUS);
    }

    /**
     * @param $orderStatus
     * @return TrackingInfo|string
     */
    public function setOrderStatus($orderStatus)
    {
        return $this->setData(self::ORDER_STATUS, $orderStatus);
    }
}
