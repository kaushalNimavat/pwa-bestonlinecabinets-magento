<?php

namespace Aureatelabs\SocialSignup\Api;

/**
* @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*/
interface ResponseInterface
{
    const MESSAGE = 'message';
    const STATUS = 'status';
    const FACEBOOK = 'facebook';
    const GOOGLE = 'google';
    const TWITTER = 'twitter';
    const AMAZON = 'amazon';

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @param string $msg
     * @return $this
     */
    public function setMessage($msg);

    /**
     * @return bool
     */
    public function getStatus();

    /**
     * @param bool $msg
     * @return $this
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getFacebook();

    /**
     * @param string $facebook
     * @return $this
     */
    public function setFacebook($facebook);

    /**
     * @return string
     */
    public function getGoogle();

    /**
     * @param string $google
     * @return $this
     */
    public function setGoogle($google);

    /**
     * @return string
     */
    public function getTwitter();

    /**
     * @param string $twitter
     * @return $this
     */
    public function setTwitter($twitter);

    /**
     * @return string
     */
    public function getAmazon();

    /**
     * @param string $amazon
     * @return $this
     */
    public function setAmazon($amazon);
}
