<?php

namespace Aureatelabs\SocialSignup\Api;
use Magento\Framework\Exception\AuthenticationException;
use Aureatelabs\SocialSignup\Api\ResponseInterface;

/**
* @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*/
interface SocialSignupInterface
{
    /**
     * Social sign up
     * 
     * @param int $customerId
     * @param string $facebookId
     * @param string $googleId
     * @param string $amazonId
     * @param string $twitterId
     * @return ResponseInterface
     * @throws AuthenticationException
     */
    public function signup(int $customerId, $facebookId = null, $googleId = null, $amazonId = null, $twitterId = null);

    /**
     * Social sign up
     * 
     * @param string $facebookId
     * @param string $googleId
     * @param string $amazonId
     * @param string $twitterId
     * @return string
     * @throws AuthenticationException
     */
    public function signin($facebookId = null, $googleId = null, $amazonId = null, $twitterId = null);

    /**
     * New sign up
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $googleId
     * @param string $facebookId
     * @return string
     * @throws AuthenticationException
     */
    public function newsignup($firstname, $lastname, $email, $type, $typeId);

    /**
     * New sign up
     * @param string $email
     * @return ResponseInterface
     */
    public function list($email): ResponseInterface;

    /**
     * Remove social token
     * 
     * @param string $email
     * @param string $type
     * @return string
     */
    public function remove($email, $type);
}
