<?php

namespace Aureatelabs\SocialSignup\Model;

class Response extends \Magento\Framework\Model\AbstractExtensibleModel implements \Aureatelabs\SocialSignup\Api\ResponseInterface
{
    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }

    /**
     * @param string $msg
     * @return $this
     */
    public function setMessage($msg)
    {
        $this->setData(self::MESSAGE, $msg);
        return $this;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param string $msg
     * @return $this
     */
    public function setStatus($status)
    {
        $this->setData(self::STATUS, $status);
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebook()
    {
        return $this->getData(self::FACEBOOK);
    }

    /**
     * @param string $facebook
     * @return $this
     */
    public function setFacebook($facebook)
    {
        $this->setData(self::FACEBOOK, $facebook);
        return $this;
    }

    /**
     * @return string
     */
    public function getGoogle()
    {
        return $this->getData(self::GOOGLE);
    }

    /**
     * @param string $google
     * @return $this
     */
    public function setGoogle($google)
    {
        $this->setData(self::GOOGLE, $google);
        return $this;
    }

    /**
     * @return string
     */
    public function getTwitter()
    {
        return $this->getData(self::TWITTER);
    }

    /**
     * @param string $twitter
     * @return $this
     */
    public function setTwitter($twitter)
    {
        $this->setData(self::TWITTER, $twitter);
        return $this;
    }

    /**
     * @return string
     */
    public function getAmazon()
    {
        return $this->getData(self::AMAZON);
    }

    /**
     * @param string $amazon
     * @return $this
     */
    public function setAmazon($amazon)
    {
        $this->setData(self::AMAZON, $amazon);
        return $this;
    }
}