<?php

namespace Aureatelabs\SocialSignup\Model;

use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Filter\Input\MaliciousCode;
use Aureatelabs\SocialSignup\Api\ResponseInterface;

/**
* @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*/
class SocialSignup implements \Aureatelabs\SocialSignup\Api\SocialSignupInterface
{
    /**
     * Token Model
     *
     * @var TokenModelFactory
     */
    private $tokenModelFactory;

    /**
     * Customer model factory
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * Customer repository
     * @var \Magento\Customer\Model\ResourceModel\CustomerRepository
     */
    protected $_customerRepository;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    protected $_customerRes;

    /**
     * @var \Aureatelabs\SocialSignup\Api\ResponseFactory
     */
    protected $_responseFactory;

    /**
     * @var MaliciousCode
     */
    protected $maliciousCode;

    /**
     * @var ResponseInterface
     */
    protected $result;

    /**
     * SocialSignup constructor
     * 
     * @param TokenModelFactory $tokenModelFactory
     * @param TokenModelFactory $tokenModelFactory,
     * @param \Magento\Customer\Model\ResourceModel\Customer $customerRes,
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory,
     * @param \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
     * @param \Aureatelabs\SocialSignup\Model\ResponseFactory $responseFactory,
     * @param MaliciousCode $maliciousCode
     * @param ResponseInterface $result
     */
    public function __construct(
        TokenModelFactory $tokenModelFactory,
        \Magento\Customer\Model\ResourceModel\Customer $customerRes,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Aureatelabs\SocialSignup\Model\ResponseFactory $responseFactory,
        MaliciousCode $maliciousCode,
        ResponseInterface $result
    ) {
        $this->tokenModelFactory = $tokenModelFactory;
        $this->_customerFactory = $customerFactory;
        $this->_customerRepository = $customerRepository;
        $this->_customerRes = $customerRes;
        $this->_responseFactory = $responseFactory;
        $this->maliciousCode = $maliciousCode;
        $this->result = $result;
    }

    /**
     * @inheritdoc
     */
    public function signup(int $customerId, $facebookId = null, $googleId = null, $amazonId = null, $twitterId = null)
    {
        try {
            $this->_customerRepository->getById($customerId);
            if ($googleId) {
                $sql = "UPDATE `customer_entity` SET `google_id` = :google_id WHERE `entity_id` = :entity_id ;";
                $this->_customerRes->getConnection()->query($sql, [
                'google_id' => $googleId,
                'entity_id' => $customerId
            ]);
            } else if ($facebookId) {
                $sql = "UPDATE `customer_entity` SET `facebook_id` = :facebook_id WHERE `entity_id` = :entity_id ;";
                $this->_customerRes->getConnection()->query($sql, [
                    'facebook_id' => $facebookId,
                    'entity_id' => $customerId
                ]);
            } else if ($amazonId) {
                $sql = "UPDATE `customer_entity` SET `amazon_id` = :amazon_id WHERE `entity_id` = :entity_id ;";
                $this->_customerRes->getConnection()->query($sql, [
                    'amazon_id' => $amazonId,
                    'entity_id' => $customerId
                ]);
            } else if ($twitterId) {
                $sql = "UPDATE `customer_entity` SET `twitter_id` = :twitter_id WHERE `entity_id` = :entity_id ;";
                $this->_customerRes->getConnection()->query($sql, [
                    'twitter_id' => $twitterId,
                    'entity_id' => $customerId
                ]);
            }
            $response = $this->_responseFactory->create();
            $response->setMessage('saved data to customer')->setStatus(true);
            return $response;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        } catch (\Exception $e) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        }

        throw new AuthenticationException(
            __(
                'The account sign-in was incorrect or your account is disabled temporarily. '
                . 'Please wait and try again later.'
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function signin($facebookId = null, $googleId = null, $amazonId = null, $twitterId = null)
    {
        if (empty(trim($facebookId)) && empty(trim($googleId)) && empty(trim($amazonId)) && empty(trim($twitterId))) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        }
        $result = null;
        try {
            $sql = "SELECT `entity_id` AS `id` FROM `customer_entity`";
            $where = '';
            $bind = [];
            if ($facebookId) {
                $where .= " WHERE `facebook_id` = :facebook_id";
                $bind = [
                    'facebook_id' => $facebookId
                ];
            } elseif ($googleId) {
                $where .= " WHERE `google_id` = :google_id";
                $bind = [
                    'google_id' => $googleId
                ];
            }elseif ($amazonId) {
                $where .= " WHERE `amazon_id` = :amazon_id";
                $bind = [
                    'amazon_id' => $amazonId
                ];
            }elseif ($twitterId) {
                $where .= " WHERE `twitter_id` = :twitter_id";
                $bind = [
                    'twitter_id' => $twitterId
                ];
            }
            $id = $this->_customerRes->getConnection()->fetchOne($sql . $where, $bind);
            if ($id) {
                $token = $this->tokenModelFactory->create()->createCustomerToken($id)->getToken();
                return $token;
            } else {
                throw new AuthenticationException(
                    __(
                        'The account sign-in was incorrect or your account is disabled temporarily. '
                        . 'Please wait and try again later.'
                    )
                );
            }
        } catch (AuthenticationException $e) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        } catch (\Exception $e) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function newsignup($firstname, $lastname, $email, $type, $typeId)
    {
        if ($type != 'google' && $type != 'facebook' && $type != 'amazon' && $type != 'twitter') {
            throw new AuthenticationException(__('Invalid data request found.'));
        }

        $obj = \Magento\Framework\App\ObjectManager::getInstance();
        $store = $obj->create(\Magento\Store\Model\StoreManagerInterface::class);

        // Get Website ID
        $websiteId  = $store->getWebsite()->getWebsiteId();
        $customer = $this->_customerFactory->create();
        $customer->setWebsiteId($websiteId);

        // Preparing data for new customer
        $customer->setEmail($email);
        $customer->setFirstname($firstname);
        $customer->setLastname($lastname);
        $customer->setPassword("DEFAULTPASSWORD");

        try {
            // Save data
            $customer = $customer->save();
        } catch (AlreadyExistsException $e) {
            return $this->updateCustomer($type, $typeId, $email);
        }

        //$customer->sendNewAccountEmail();

        return $this->updateCustomer($type, $typeId, $email);
    }

    /**
     * Update Google Id/Facebook Id for the customer by email address
     */
    public function updateCustomer($type, $typeId, $email)
    {
        if ($type == 'google') {

            $sql = "UPDATE `customer_entity` SET `google_id` = :google_id WHERE `email` = :email ;";
            $this->_customerRes->getConnection()->query($sql, [
                'google_id' => $typeId,
                'email' => $email
            ]);

            return $this->signin(null, $typeId, null, null);
        } else if ($type == 'facebook') {

            $sql = "UPDATE `customer_entity` SET `facebook_id` = :facebook_id WHERE `email` = :email ;";
            $this->_customerRes->getConnection()->query($sql, [
                'facebook_id' => $typeId,
                'email' => $email
            ]);

            return $this->signin($typeId, null, null, null);
        } else if ($type == 'amazon') {

            $sql = "UPDATE `customer_entity` SET `amazon_id` = :amazon_id WHERE `email` = :email ;";
            $this->_customerRes->getConnection()->query($sql, [
                'amazon_id' => $typeId,
                'email' => $email
            ]);

            return $this->signin(null, null, $typeId, null);
        } else if ($type == 'twitter') {

            $sql = "UPDATE `customer_entity` SET `twitter_id` = :twitter_id WHERE `email` = :email ;";
            $this->_customerRes->getConnection()->query($sql, [
                'twitter_id' => $typeId,
                'email' => $email
            ]);

            return $this->signin(null, null, null, $typeId);
        }
    }

    public function list($email): ResponseInterface
    {
        try {
            $sql = "SELECT 
                        IF(`facebook_id` IS NULL, 0, 1) AS facebook,
                        IF(`google_id` IS NULL, 0, 1) AS google,
                        IF(`amazon_id` IS NULL, 0, 1) AS amazon,
                        IF(`twitter_id` IS NULL, 0, 1) AS twitter
                    FROM `customer_entity`";
            $where = " WHERE `email` = :email";
            $bind = ['email' => $email];

            $result = $this->_customerRes->getConnection()->fetchRow($sql . $where, $bind);
            if (!empty($result)) {
                $this->result->setFacebook($result['facebook']);
                $this->result->setGoogle($result['google']);
                $this->result->setAmazon($result['amazon']);
                $this->result->setTwitter($result['twitter']);

                return $this->result;
            }
        } catch (\Exception $e) {
            $result = [];
        }
        return $this->result;
    }

    /**
     * @inheritdoc
     */
    public function remove($email, $type)
    {
        try {
            $this->updateCustomer($type, null, $email);
        } catch (\Exception $e) {
            return '1';
        }
        return '1';
    }
}
