<?php

namespace Aureatelabs\Testimonial\Index\Mapping;

use Divante\VsbridgeIndexerCore\Api\MappingInterface;
use Divante\VsbridgeIndexerCore\Api\Mapping\FieldInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;

class Testimonial implements MappingInterface
{
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $textFields = [
        'name',
        'email',
        'image',
        'website',
        'company',
        'address',
        'testimonial',
        'stores',
        'video_url'
    ];

    /**
     * @param EventManager $eventManager
     */
    public function __construct(EventManager $eventManager)
    {
        $this->eventManager = $eventManager;
    }

    /**
     * @inheritdoc
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @inheritdoc
     */
    public function getMappingProperties()
    {
        $properties = [
            'testimonial_id' => ['type' => FieldInterface::TYPE_LONG],
            'status' => ['type' => FieldInterface::TYPE_INTEGER],
            'rating' => ['type' => FieldInterface::TYPE_INTEGER],
            'enabled_home' => ['type' => FieldInterface::TYPE_INTEGER],
            'enabled_widget' => ['type' => FieldInterface::TYPE_INTEGER]
        ];

        foreach ($this->textFields as $field) {
            $properties[$field] = ['type' => FieldInterface::TYPE_TEXT];
        }

        $mappingObject = new \Magento\Framework\DataObject();
        $mappingObject->setData('properties', $properties);

        $this->eventManager->dispatch(
            'elasticsearch_testimonial_mapping_properties',
            ['mapping' => $mappingObject]
        );

        return $mappingObject->getData();
    }
}
