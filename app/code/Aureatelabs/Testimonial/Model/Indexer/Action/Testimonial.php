<?php

namespace Aureatelabs\Testimonial\Model\Indexer\Action;

use Magebees\Testimonial\Model\ResourceModel\Testimonialcollection as TestimonialResource;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

class Testimonial implements RebuildActionInterface
{
    /**
     * @var TestimonialResource
     */
    private $resourceModel;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param TestimonialResource $testimonialResource
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        TestimonialResource $testimonialResource,
        StoreManagerInterface $storeManager
    ) {
        $this->resourceModel = $testimonialResource;
        $this->_storeManager = $storeManager;
    }

    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     * @throws NoSuchEntityException
     */
    public function rebuild(int $storeId = 1, array $ids = []): \Traversable
    {
        $lastTestimonialId = 0;
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        do {
            $testimonials = $this->resourceModel->loadTestimonials($storeId, $ids, $lastTestimonialId);

            foreach ($testimonials as $testimonial) {
                $image = null;
                if ($testimonial['image']) {
                    $image = $mediaUrl . 'testimonial/images' . $testimonial['image'];
                }
                $lastTestimonialId = (int) $testimonial['testimonial_id'];
                $testimonial['id'] = $lastTestimonialId;
                $testimonial['image'] = $image;
                yield $lastTestimonialId => $testimonial;
            }
        } while (!empty($testimonials));
    }
}
