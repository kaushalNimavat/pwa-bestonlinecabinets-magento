<?php

namespace Aureatelabs\Testimonial\Model\Indexer;

class TestimonialProcessor extends \Magento\Framework\Indexer\AbstractProcessor
{
    /**
     * Indexer ID
     */
    const INDEXER_ID = 'vsbridge_testimonial_indexer';
}
