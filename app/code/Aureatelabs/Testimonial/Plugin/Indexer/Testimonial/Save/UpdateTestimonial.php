<?php

namespace Aureatelabs\Testimonial\Plugin\Indexer\Testimonial\Save;

use Aureatelabs\Testimonial\Model\Indexer\TestimonialProcessor;
use Magebees\Testimonial\Model\Testimonialcollection as Testimonial;

class UpdateTestimonial
{
    /**
     * @var TestimonialProcessor
     */
    private $testimonialProcessor;

    /**
     * @param TestimonialProcessor $testimonialProcessor
     */
    public function __construct(TestimonialProcessor $testimonialProcessor)
    {
        $this->testimonialProcessor = $testimonialProcessor;
    }

    /**
     * @param Testimonial $subject
     * @param Testimonial $result
     * @return Testimonial
     */
    public function afterAfterSave(Testimonial $subject, Testimonial $result): Testimonial
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->testimonialProcessor->reindexRow($result->getId());
        });

        return $result;
    }

    /**
     * @param Testimonial $subject
     * @param Testimonial $result
     * @return Testimonial
     */
    public function afterAfterDeleteCommit(Testimonial $subject, Testimonial $result): Testimonial
    {
        $this->testimonialProcessor->reindexRow($result->getId());

        return $result;
    }
}
