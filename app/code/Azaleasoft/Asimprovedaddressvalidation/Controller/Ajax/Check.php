<?php
/**
 * @author Azaleasoft Team
 * @copyright Copyright (c) 2019 Azaleasoft (https://azaleasoft.com)
 * @package Azaleasoft_Asimprovedaddressvalidation
 */
namespace Azaleasoft\Asimprovedaddressvalidation\Controller\Ajax;

use Magento\Framework\Controller\ResultFactory;

class Check extends \Magento\Framework\App\Action\Action
{
    protected $helper;

    public function __construct(
        \Azaleasoft\Asimprovedaddressvalidation\Helper\Data $helper,
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        if ($this->getRequest()->isAjax() && $this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $responseData = $this->helper->getAutocompleteRegionData('code', $postData);
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($responseData);
            return $resultJson;
        }
    }
}
