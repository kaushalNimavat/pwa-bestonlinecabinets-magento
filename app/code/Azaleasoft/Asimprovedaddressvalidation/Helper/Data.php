<?php
/**
 * @author Azaleasoft Team
 * @copyright Copyright (c) 2019 Azaleasoft (https://azaleasoft.com)
 * @package Azaleasoft_Asimprovedaddressvalidation
 */
namespace Azaleasoft\Asimprovedaddressvalidation\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    private $configReader;
    private $soapClientFactory;
    protected $storeManager;
    protected $address;
    protected $streetLines;
    protected $region;
    protected $country;
    protected $urlBuilder;
    private $fedexServiceWsdl;

    const ASIMPROVEDADDRESSVALIDATION_GENERAL_ENABLE = 'azaleasoft_asimprovedaddressvalidation/general/enable';
    const ASIMPROVEDADDRESSVALIDATION_GENERAL_ALLOW_ORIGINAL_ADDRESS = 'azaleasoft_asimprovedaddressvalidation/general/allow_original_address';
    const ASIMPROVEDADDRESSVALIDATION_GENERAL_SERVICE_NAME = 'azaleasoft_asimprovedaddressvalidation/general/service_name';
    const ASIMPROVEDADDRESSVALIDATION_GENERAL_ENABLE_AUTOCOMPLETE = 'azaleasoft_asimprovedaddressvalidation/general/enable_autocomplete';
    const ASIMPROVEDADDRESSVALIDATION_GENERAL_ZIP_FORMAT = 'azaleasoft_asimprovedaddressvalidation/general/zip_format';

    const ASIMPROVEDADDRESSVALIDATION_UPS_MAXIMUM_LIST_SIZE = 'azaleasoft_asimprovedaddressvalidation/ups/maximum_list_size';
    const ASIMPROVEDADDRESSVALIDATION_UPS_USERNAME = 'azaleasoft_asimprovedaddressvalidation/ups/username';
    const ASIMPROVEDADDRESSVALIDATION_UPS_PASSWORD = 'azaleasoft_asimprovedaddressvalidation/ups/password';
    const ASIMPROVEDADDRESSVALIDATION_UPS_ACCESS_KEY = 'azaleasoft_asimprovedaddressvalidation/ups/access_key';
    const ASIMPROVEDADDRESSVALIDATION_UPS_URL_PRODUCTION = 'https://onlinetools.ups.com/rest/XAV';

    const ASIMPROVEDADDRESSVALIDATION_USPS_USERNAME = 'azaleasoft_asimprovedaddressvalidation/usps/username';
    const ASIMPROVEDADDRESSVALIDATION_USPS_PASSWORD = 'azaleasoft_asimprovedaddressvalidation/usps/password';
    const ASIMPROVEDADDRESSVALIDATION_USPS_URL_PRODUCTION = 'https://production.shippingapis.com/ShippingAPI.dll';

    const ASIMPROVEDADDRESSVALIDATION_FEDEX_KEY = 'azaleasoft_asimprovedaddressvalidation/fedex/key';
    const ASIMPROVEDADDRESSVALIDATION_FEDEX_PASSWORD = 'azaleasoft_asimprovedaddressvalidation/fedex/password';
    const ASIMPROVEDADDRESSVALIDATION_FEDEX_ACCOUNT_NUMBER = 'azaleasoft_asimprovedaddressvalidation/fedex/account_number';
    const ASIMPROVEDADDRESSVALIDATION_FEDEX_METER_NUMBER = 'azaleasoft_asimprovedaddressvalidation/fedex/meter_number';
    const ASIMPROVEDADDRESSVALIDATION_FEDEX_SANDBOX_MODE = 'azaleasoft_asimprovedaddressvalidation/fedex/sandbox_mode';
    const ASIMPROVEDADDRESSVALIDATION_FEDEX_URL_PRODUCTION = 'https://ws.fedex.com:443/web-services';
    const ASIMPROVEDADDRESSVALIDATION_FEDEX_URL_TEST = 'https://wsbeta.fedex.com:443/web-services';

    const ASIMPROVEDADDRESSVALIDATION_AUTOCOMPLETE_API_KEY = 'azaleasoft_asimprovedaddressvalidation/autocomplete/api_key';
    const ASIMPROVEDADDRESSVALIDATION_AUTOCOMPLETE_TRIGGER_FIELD = 'azaleasoft_asimprovedaddressvalidation/autocomplete/trigger_field';
    const ASIMPROVEDADDRESSVALIDATION_AUTOCOMPLETE_GEOLOCATION = 'azaleasoft_asimprovedaddressvalidation/autocomplete/geolocation';
    const ASIMPROVEDADDRESSVALIDATION_AUTOCOMPLETE_RESTRICTION = 'azaleasoft_asimprovedaddressvalidation/autocomplete/restriction';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Module\Dir\Reader $configReader,
        \Magento\Framework\Webapi\Soap\ClientFactory $soapClientFactory,
        \Magento\Customer\Helper\Address $address,
        \Magento\Directory\Model\Region $region,
        \Magento\Directory\Model\Country $country,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->configReader = $configReader;
        $this->soapClientFactory = $soapClientFactory;
        $this->storeManager = $storeManager;
        $this->address = $address;
        $this->streetLines = $this->address->getStreetLines();
        $this->region = $region;
        $this->country = $country;
        $this->urlBuilder = $context->getUrlBuilder();

        $wsdlBasePath = $this->configReader->getModuleDir('etc', 'Azaleasoft_Asimprovedaddressvalidation') . '/wsdl/';
        $this->fedexServiceWsdl = $wsdlBasePath . 'AddressValidationService_v4.wsdl';
    }

    public function isEnabled()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_GENERAL_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function allowOriginalAddress()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_GENERAL_ALLOW_ORIGINAL_ADDRESS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getServiceName()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_GENERAL_SERVICE_NAME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function enableAutocomplete()
    {
        return ($this->isEnabled() && $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_GENERAL_ENABLE_AUTOCOMPLETE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ));
    }

    public function getZipFormat()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_GENERAL_ZIP_FORMAT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getUpsMaximumListSize()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_UPS_MAXIMUM_LIST_SIZE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getUpsUsername()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_UPS_USERNAME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getUpsPassword()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_UPS_PASSWORD,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getUpsAccessKey()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_UPS_ACCESS_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getUspsUsername()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_USPS_USERNAME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getUspsPassword()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_USPS_PASSWORD,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getFedexKey()
    {
        return $this->scopeConfig->getValue(self::ASIMPROVEDADDRESSVALIDATION_FEDEX_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFedexPassword()
    {
        return $this->scopeConfig->getValue(self::ASIMPROVEDADDRESSVALIDATION_FEDEX_PASSWORD,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFedexAccountNumber()
    {
        return $this->scopeConfig->getValue(self::ASIMPROVEDADDRESSVALIDATION_FEDEX_ACCOUNT_NUMBER,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFedexMeterNumber()
    {
        return $this->scopeConfig->getValue(self::ASIMPROVEDADDRESSVALIDATION_FEDEX_METER_NUMBER,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFedexSandboxMode()
    {
        return $this->scopeConfig->getValue(self::ASIMPROVEDADDRESSVALIDATION_FEDEX_SANDBOX_MODE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getGoogleApiKey()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_AUTOCOMPLETE_API_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getTriggerField()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_AUTOCOMPLETE_TRIGGER_FIELD,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function enableGeolocation()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_AUTOCOMPLETE_GEOLOCATION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function enableRestriction()
    {
        return $this->scopeConfig->getValue(
            self::ASIMPROVEDADDRESSVALIDATION_AUTOCOMPLETE_RESTRICTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function sendRequest($params = [])
    {
        $result = [];
        $serviceName = $this->getServiceName();
        switch ($serviceName) {
            case 'ups':
                $response = $this->_sendUpsRequest($params);
                break;
            case 'usps':
                if ($params['country'] != 'US') {
                    return [];
                }
                $response = $this->_sendUspsRequest($params);
                break;
            case 'fedex':
                $response = $this->_sendFedexRequest($params);
                break;
            default:
                $response = [];
        }

        if (empty($response)) {
            return $result;
        }

        if ($serviceName == 'fedex') {
            $result = $this->_parseResponseData($response);
        } else {
            if (!empty($response) && !empty($response->getStatus()) && $response->getStatus() == 200) {
                $result = $this->_parseResponseData($response->getBody());
            } else {
                if (!empty($response) && !empty($response->getBody())) {
                    $this->_logger->error($response->getBody());
                } else {
                    $this->_logger->error('Unknown error for call 3rd-party API.');
                }
            }
        }

        return $result;
    }

    protected function _sendUpsRequest($params = [])
    {
        $addressLine = '';
        for ($i = 0; $i < $this->streetLines; $i++) {
            $addressLine .= $params['street_'.$i].' ';
        }
        $address = [
            'AddressLine'           => trim($addressLine),
            'PoliticalDivision2'    => $params['city'],
            'PoliticalDivision1'    => $params['region'],
            'PostcodePrimaryLow'    => strlen($params['zip']) >= 5 ? substr($params['zip'], 0, 5) : '',
            'CountryCode'           => $params['country']
        ];

        $request = [
            'UPSSecurity' => [
                'UsernameToken' => [
                    'Username' => $this->getUpsUsername(),
                    'Password' => $this->getUpsPassword()
                ],
                'ServiceAccessToken' => [
                    'AccessLicenseNumber' => $this->getUpsAccessKey()
                ]
            ],
            'XAVRequest' => [
                'Request' => [
                    'RequestOption' => '1'
                ],
                'MaximumListSize' => self::ASIMPROVEDADDRESSVALIDATION_UPS_MAXIMUM_LIST_SIZE,
                'AddressKeyFormat' => $address
            ]
        ];

        $client = new \Zend_Http_Client(self::ASIMPROVEDADDRESSVALIDATION_UPS_URL_PRODUCTION);
        $client->setMethod(\Zend_Http_Client::POST);
        $client->setRawData(json_encode($request), 'application/json');

        try {
            $response = $client->request();
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
            $response = null;
        }

        return $response;
    }

    protected function _sendUspsRequest($params = [])
    {
        $username = $this->getUspsUsername();
        if (empty($username)) {
            $this->_logger->error('User name of USPS is empty.');
            return null;
        }

        $xmlData = "<AddressValidateRequest USERID='$username'>".
            "<Address>".
            "<Address1>".(isset($params['street_1']) ? trim($params['street_1']) : '')."</Address1>".
            "<Address2>".(isset($params['street_0']) ? trim($params['street_0']) : '')."</Address2>".
            "<City>".$params['city']."</City>".
            "<State>".$params['region']."</State>".
            "<Zip5>".(strlen($params['zip']) >= 5 ? substr($params['zip'], 0, 5) : '')."</Zip5>".
            "<Zip4></Zip4>".
            "</Address>".
            "</AddressValidateRequest>";

        $client = new \Zend_Http_Client(self::ASIMPROVEDADDRESSVALIDATION_USPS_URL_PRODUCTION);
        $client->setMethod(\Zend_Http_Client::POST);
        $client->setParameterPost('API', 'Verify');
        $client->setParameterPost('XML', $xmlData);

        try {
            $response = $client->request('POST');
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
            $response = null;
        }

        return $response;
    }

    protected function _sendFedexRequest($params = array())
    {
        $key = $this->getFedexKey();
        $password = $this->getFedexPassword();
        $accountNumber = $this->getFedexAccountNumber();
        $meterNumber = $this->getFedexMeterNumber();
        if (empty($key) || empty($password) || empty($accountNumber) || empty($meterNumber)) {
            return null;
        }

        $soapClient = $this->soapClientFactory->create($this->fedexServiceWsdl, ['trace' => false]);
        if ($this->getFedexSandboxMode()) {
            $location = self::ASIMPROVEDADDRESSVALIDATION_FEDEX_URL_TEST . '/addressvalidation';
        } else {
            $location = self::ASIMPROVEDADDRESSVALIDATION_FEDEX_URL_PRODUCTION . '/addressvalidation';
        }
        $soapClient->__setLocation($location);
        $request = [
            'WebAuthenticationDetail' => [
                'UserCredential' => ['Key' => $key, 'Password' => $password],
            ],
            'ClientDetail' => ['AccountNumber' => $accountNumber, 'MeterNumber' => $meterNumber],
            'Version' => ['ServiceId' => 'aval', 'Major' => '4', 'Intermediate' => '0', 'Minor' => '0'],
            'AddressesToValidate' => [
                'Address' => [
                    'StreetLines' => $params['street_0'] . (isset($params['street_1']) ? ' '.$params['street_1'] : ''),
                    'City' => $params['city'],
                    'StateOrProvinceCode' => $params['region_code'],
                    'PostalCode' => $params['zip'],
                    'CountryCode' => $params['country'],
                    'Residential' => true
                ]
            ]
        ];

        try {
            $response = $soapClient->addressValidation($request);
//            $this->_logger->info(json_encode($response));
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
            $response = '';
        }

        return $response;
    }

    public function getFullAddress($params)
    {
        $fulladdress = '';
        $city       = isset($params['city']) ? $params['city'] : '';
        $region     = isset($params['region']) ? $params['region'] : '';
        $zip        = isset($params['zip']) ? $params['zip'] : '';
        $country    = isset($params['country']) ? $params['country'] : '';
        for ($i = 0; $i < $this->streetLines; $i++) {
            ${'street_'.$i} = isset($params['street_'.$i]) ? $params['street_'.$i] : '';
            if (!empty(${'street_'.$i})) {
                $fulladdress .= (empty($fulladdress) ? '' : ' ').${'street_'.$i};
            }
        }
        if (!empty($city)) {
            $fulladdress .= (empty($fulladdress) ? '' : ', ').$city;
        }
        if (!empty($region)) {
            $fulladdress .= (empty($region) ? '': ' ').$region;
        }
        if (!empty($zip)) {
            $fulladdress .= (empty($region) ? '' : ' ').$zip;
        }
        if (!empty($country)) {
            $fulladdress .= (empty($fulladdress) ? '' : ', ').$this->country->loadByCode($country)->getName();
        }

        return $fulladdress;
    }

    public function getRegionData($params)
    {
        $result = [];
        if (isset($params['region_id']) && !empty($params['region_id'])) {
            $region = $this->region->load($params['region_id']);
        } elseif (isset($params['region_code']) && isset($params['country'])) {
            $region = $this->region->loadByCode($params['region_code'], $params['country']);
        } else {
            $region = null;
        }
        if (!empty($region)) {
            $result['region_id']    = $region->getId();
            $result['region_code']  = $region->getCode();
            $result['region']       = $region->getName();
        }

        return $result;
    }

    public function getAutocompleteRegionData($type, $params)
    {
        $result = [];

        switch ($type) {
            case 'id':
                $region = $this->region->load($params['region_id']);
                break;
            case 'code':
                $region = $this->region->loadByCode($params['region_code'], $params['country']);
                break;
        }

        $result['region_id']   = $region->getId();
        $result['region_code'] = $region->getCode();
        $result['region']      = $region->getName();

        return $result;
    }

    protected function _parseResponseData($response)
    {
        switch ($this->getServiceName()) {
            case 'ups':
                return $this->_parseResponseDataFromUps($response);

            case 'usps':
                return $this->_parseResponseDataFromUsps($response);

            case 'fedex':
                return $this->_parseResponseDataFromFedex($response);

            default:
                return [];
        }
    }

    protected function _parseResponseDataFromUps($response)
    {
        $result = [];
        try {
            $data = json_decode($response, true);
            if (!empty($data['Fault'])) {
                return $result;
            }

            $status = $data['XAVResponse']['Response']['ResponseStatus']['Code'];
            if ($status == '1') {
                if (isset($data['XAVResponse']['NoCandidatesIndicator'])) {
                    return $result;
                }

                $candidate = $data['XAVResponse']['Candidate'];
                if (count($candidate) > 1) {
                    $maximumListSize = $this->getUpsMaximumListSize();
                    $index = 0;
                    foreach ($candidate as $row) {
                        $address = $row['AddressKeyFormat'];
                        if ($this->streetLines >= 2) {
                            $result[] = [
                                'street_0'   => is_array($address['AddressLine']) ? $address['AddressLine'][0] : $address['AddressLine'],
                                'street_1'   => is_array($address['AddressLine']) ? $address['AddressLine'][1] : '',
                                'city'       => $address['PoliticalDivision2'],
                                'region_code'     => $address['PoliticalDivision1'],
                                'zip'        => $address['PostcodePrimaryLow'].'-'.$address['PostcodeExtendedLow'],
                                'country'    => $address['CountryCode']
                            ];
                        } else {
                            $result[] = [
                                'street_0'   => is_array($address['AddressLine']) ? implode(' ', $address['AddressLine']) : $address['AddressLine'],
                                'city'      => $address['PoliticalDivision2'],
                                'region_code'    => $address['PoliticalDivision1'],
                                'zip'       => $address['PostcodePrimaryLow'].'-'.$address['PostcodeExtendedLow'],
                                'country'   => $address['CountryCode']
                            ];
                        }
                        $index += 1;
                        if ($maximumListSize <= $index) {
                            break;
                        }
                    }
                } else {
                    $address = $candidate['AddressKeyFormat'];
                    if ($this->streetLines >= 2) {
                        $result[] = [
                            'street_0'   => is_array($address['AddressLine']) ? $address['AddressLine'][0] : $address['AddressLine'],
                            'street_1'   => is_array($address['AddressLine']) ? $address['AddressLine'][1] : '',
                            'city'       => $address['PoliticalDivision2'],
                            'region_code'     => $address['PoliticalDivision1'],
                            'zip'        => $address['PostcodePrimaryLow'].'-'.$address['PostcodeExtendedLow'],
                            'country'    => $address['CountryCode']
                        ];
                    } else {
                        $result[] = [
                            'street_0'   => is_array($address['AddressLine']) ? implode(' ', $address['AddressLine']) : $address['AddressLine'],
                            'city'      => $address['PoliticalDivision2'],
                            'region_code'    => $address['PoliticalDivision1'],
                            'zip'       => $address['PostcodePrimaryLow'].'-'.$address['PostcodeExtendedLow'],
                            'country'   => $address['CountryCode']
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }

        return $result;
    }

    protected function _parseResponseDataFromUsps($response)
    {
        $result = [];
        try {
            $xmlObject = simplexml_load_string($response);
            $xmlArray = json_decode(json_encode((array)$xmlObject), true);
            if (isset($xmlArray['Description'])) {
                $result[] = [
                'error' => $xmlArray['Description']
                ];
                return $result;
            }

            $address = $xmlArray['Address'];
            // Lookup Zip Code
            if (isset($address['Error'])) {
                $result[] = [
                    'error' => isset($address['Error']['Description']) ? $address['Error']['Description'] : 'Invalid Address'
                ];
                return $result;
            }

            if ($this->streetLines >= 2) {
                $result[] = [
                    'street_0'  => isset($address['Address2']) ? $address['Address2'] : '',
                    'street_1'  => isset($address['Address1']) ? $address['Address1'] : '',
                    'city'      => isset($address['City']) ? $address['City'] : '',
                    'region_code'   => isset($address['State']) ? $address['State'] : '',
                    'zip'       => (isset($address['Zip5']) ? $address['Zip5'] : '').(isset($address['Zip4']) ? '-'.$address['Zip4'] : ''),
                    'country'   => 'US',
                    'message'   => isset($address['ReturnText']) ? $address['ReturnText'] : ''
                ];
            } else {
                $result[] = [
                    'street_0'  => (isset($adderss['Address2']) ? $address['Address2'] : '').(isset($adderss['Address1']) ? $address['Address1'] : ''),
                    'city'      => isset($address['City']) ? $address['City'] : '',
                    'region_code'   => isset($address['State']) ? $address['State'] : '',
                    'zip'       => (isset($address['Zip5']) ? $address['Zip5'] : '').(isset($address['Zip4']) ? '-'.$address['Zip4'] : ''),
                    'country'   => 'US',
                    'message'   => iset($address['ReturnText']) ? $address['ReturnText'] : ''
                ];
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }

        return $result;
    }

    protected function _parseResponseDataFromFedex($response)
    {
        $result = [];

        $data = json_decode(json_encode($response), true);
        if (isset($data['HighestSeverity']) && $data['HighestSeverity'] == 'SUCCESS') {
            $code = $data['Notifications']['Code'];
            if ($code == '0') {
                $addressResults = $data['AddressResults'];
                if (isset($addressResults['Classification']) && $addressResults['Classification'] != 'UNKNOWN') {
                    $effectiveAddress = $addressResults['EffectiveAddress'];
                    $result[] = [
                        'street_0' => is_array($effectiveAddress['StreetLines']) ? $effectiveAddress['StreetLines'][0] : $effectiveAddress['StreetLines'],
                        'street_1' => is_array($effectiveAddress['StreetLines']) ? $effectiveAddress['StreetLines'][1] : '',
                        'city' => $effectiveAddress['City'],
                        'region_code' => $effectiveAddress['StateOrProvinceCode'],
                        'zip' => $effectiveAddress['PostalCode'],
                        'country' => $effectiveAddress['CountryCode']
                    ];
                }
            }
        }

        return $result;
    }

    public function getAccountInitData()
    {
        $data = [
            'urlAddressValidation' => $this->urlBuilder->getUrl('asimprovedaddressvalidation/ajax/suggest', ['_secure' => true]),
            'allowOriginalAddress' => $this->allowOriginalAddress(),
            'streetLines'   => $this->streetLines
        ];

        return json_encode($data);
    }

    public function getAccountAutocompleteInitData()
    {
        $triggerField = $this->getTriggerField();
        if ($triggerField == 'street') {
            $triggerField = 'street_1';
        }
        $data = [
            'urlAddressAutocomplete' => $this->urlBuilder->getUrl('asimprovedaddressvalidation/ajax/check', ['_secure' => true]),
            'triggerField' => $triggerField,
            'geolocation' => (int)$this->enableGeolocation(),
            'restriction' => (int)$this->enableRestriction(),
            'streetLines'    => $this->streetLines,
            'zipFormat' => (int)$this->getZipFormat()
        ];

        return json_encode($data);
    }

    public function getCheckoutInitData()
    {
        $data = [
            'urlAddressValidation' => $this->urlBuilder->getUrl('asimprovedaddressvalidation/ajax/suggest', ['_secure' => true]),
            'allowOriginalAddress' => $this->allowOriginalAddress(),
            'streetLines'    => $this->streetLines
        ];

        return json_encode($data);
    }

    public function getCheckoutAutocompleteInitData()
    {
        $triggerField = $this->getTriggerField();
        if ($triggerField == 'street') {
            $triggerField = 'street[0]';
        } elseif ($triggerField == 'zip') {
            $triggerField = 'postcode';
        }
        $data = [
            'urlAddressAutocomplete' => $this->urlBuilder->getUrl('asimprovedaddressvalidation/ajax/check', ['_secure' => true]),
            'triggerField' => $triggerField,
            'geolocation' => (int)$this->enableGeolocation(),
            'restriction' => (int)$this->enableRestriction(),
            'streetLines'    => $this->streetLines,
            'zipFormat' => (int)$this->getZipFormat()
        ];

        return json_encode($data);
    }

    public function getMultishippingInitData()
    {
        $data = [
            'urlAddressValidation' => $this->urlBuilder->getUrl('asimprovedaddressvalidation/ajax/suggest', ['_secure' => true]),
            'allowOriginalAddress' => $this->allowOriginalAddress(),
            'streetLines'    => $this->streetLines
        ];

        return json_encode($data);
    }

    public function getMultishippingAutocompleteInitData()
    {
        $triggerField = $this->getTriggerField();
        if ($triggerField == 'street') {
            $triggerField = 'street_1';
        }
        $data = [
            'urlAddressAutocomplete' => $this->urlBuilder->getUrl('asimprovedaddressvalidation/ajax/check', ['_secure' => true]),
            'triggerField' => $triggerField,
            'geolocation' => (int)$this->enableGeolocation(),
            'restriction' => (int)$this->enableRestriction(),
            'streetLines'    => $this->streetLines,
            'zipFormat' => (int)$this->getZipFormat()
        ];

        return json_encode($data);
    }

    public function parseZipCode($zipCode)
    {
        if (!empty($zipCode) && strlen($zipCode) >= 5 && strlen($zipCode) <= 10) {
            if ($this->getZipFormat()) {
                $zipCode = substr($zipCode, 0, $this->getZipFormat());
            }
        }

        return $zipCode;
    }
}
