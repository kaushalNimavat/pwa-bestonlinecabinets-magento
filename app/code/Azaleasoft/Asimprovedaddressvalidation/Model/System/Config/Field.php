<?php
/**
 * @author Azaleasoft Team
 * @copyright Copyright (c) 2019 Azaleasoft (https://azaleasoft.com)
 * @package Azaleasoft_Asimprovedaddressvalidation
 */
namespace Azaleasoft\Asimprovedaddressvalidation\Model\System\Config;
 
class Field implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [
            ['value' => 'street', 'label' => __('Street')],
            ['value' => 'city', 'label' => __('City')],
            ['value' => 'region_id', 'label' => __('State')],
            ['value' => 'zip', 'label' => __('Zip Code')],
        ];
 
        return $options;
    }
}
