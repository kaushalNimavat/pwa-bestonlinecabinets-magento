<?php
/**
 * @author Azaleasoft Team
 * @copyright Copyright (c) 2019 Azaleasoft (https://azaleasoft.com)
 * @package Azaleasoft_Asimprovedaddressvalidation
 */
namespace Azaleasoft\Asimprovedaddressvalidation\Model\System\Config;
 
class Service implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [
            ['value' => 'ups', 'label' => __('UPS')],
            ['value' => 'usps', 'label' => __('USPS')],
            ['value' => 'fedex', 'label' => __('FedEx')]
        ];
 
        return $options;
    }
}
