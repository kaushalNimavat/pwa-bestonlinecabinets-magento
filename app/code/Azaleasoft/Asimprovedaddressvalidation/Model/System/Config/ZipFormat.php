<?php
/**
 * @author Azaleasoft Team
 * @copyright Copyright (c) 2019 Azaleasoft (https://azaleasoft.com)
 * @package Azaleasoft_Asimprovedaddressvalidation
 */
namespace Azaleasoft\Asimprovedaddressvalidation\Model\System\Config;
 
class ZipFormat implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [
            ['value' => 5, 'label' => __('5 digits')],
            ['value' => 10, 'label' => __('5+4 digits')]
        ];
 
        return $options;
    }
}