<?php
/**
 * @author Azaleasoft Team
 * @copyright Copyright (c) 2019 Azaleasoft (https://azaleasoft.com)
 * @package Azaleasoft_Asimprovedaddressvalidation
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Azaleasoft_Asimprovedaddressvalidation',
    __DIR__
);
