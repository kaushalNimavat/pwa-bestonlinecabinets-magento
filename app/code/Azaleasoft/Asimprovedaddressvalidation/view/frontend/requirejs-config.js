/**
 * @author Azaleasoft Team
 * @copyright Copyright (c) 2019 Azaleasoft (https://azaleasoft.com)
 * @package Azaleasoft_Asimprovedaddressvalidation
 */
var config = {
    map: {
        '*': {
        	asImprovedAddressValidationAccount: 'Azaleasoft_Asimprovedaddressvalidation/js/account',
        	asImprovedAddressValidationAccountAutocomplete: 'Azaleasoft_Asimprovedaddressvalidation/js/accountAutocomplete',
            asImprovedAdderssValidationCheckout: 'Azaleasoft_Asimprovedaddressvalidation/js/checkout',
            asImprovedAdderssValidationCheckoutAutocomplete: 'Azaleasoft_Asimprovedaddressvalidation/js/checkoutAutocomplete',
            asImprovedAddressValidationMultishipping: 'Azaleasoft_Asimprovedaddressvalidation/js/multishipping',
            asImprovedAddressValidationMultishippingAutocomplete: 'Azaleasoft_Asimprovedaddressvalidation/js/multishippingAutocomplete'
        }
    }
};
