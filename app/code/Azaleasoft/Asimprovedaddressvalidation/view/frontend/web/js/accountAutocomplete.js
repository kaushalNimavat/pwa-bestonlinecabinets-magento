/**
 * @author Azaleasoft Team
 * @copyright Copyright (c) 2019 Azaleasoft (https://azaleasoft.com)
 * @package Azaleasoft_Asimprovedaddressvalidation
 */
define([
    "jquery",
    "jquery/ui",
    "googleMapsPlaceLibrary"
], function($){
    "use strict";

return function (config) {

    var autocomplete;
    var address = {};
    var triggerId = config.triggerField;
    var restrictionIsEnabled = config.restriction;
    var streetLines = config.streetLines;
    var zipFormat = config.zipFormat;
    var fields = {
        street_number: ["short_name"],
        colloquial_area: ["short_name"],
        route: ["short_name"],
        locality: ["long_name"],
        sublocality_level_1: ["long_name"],
        postal_town: ["long_name"],
        administrative_area_level_1: ["long_name", "short_name"],
        country: ["short_name"],
        postal_code_prefix: ["short_name"],
        postal_code: ["short_name"],
        postal_code_suffix: ["short_name"]
    };

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById(triggerId)),
            {types: ["geocode"]}
        );
        if (restrictionIsEnabled == 1) {
            autocomplete.setComponentRestrictions({
                "country": [$("#country").val()]
            });
        }
        autocomplete.setFields(["address_components"]);
        autocomplete.addListener("place_changed", loadAddress);
    }

    function checkRegionId(params) {
        $.ajax({
            type: "post",
            url: config.urlAddressAutocomplete,
            data: params,
            cache: false,
            dataType: "json"
        }).done(function(data){
            if (data.region_id !== undefined) {
                $("#region_id").val(data.region_id);
            }
        });
    }

    function clearAddress() {
        $("#city").val("");
        $("#zip").val("");
        $("#region_id").val("").trigger("change");
        for (var i = 1; i <= streetLines; i++) {
            $("#street_" + i).val("");
        }
    }

    function updateAddress() {
        if (address["country"] != $("#country").val()) {
            $("#country").val(address["country"]).trigger("change");
        }
        $("#region_id").val("");
        $("#region").val(address["region"]);
        checkRegionId({
            region_code: address["region_code"],
            country: address["country"]
        });
        if (address["country"] == "US" && address["zip"] !== undefined && address["zip"].length >= 5 && address["zip"].length <= 10) {
            address["zip"] = address["zip"].substring(0, zipFormat);
        }
        $("#zip").val(address["zip"]);
        if (address["country"] === "UK" || address["country"] === "SE") {
            $("#city").val(address["town"]);
        } else {
            $("#city").val(address["city"]);
        }
        $("#street_1").val(address["street_1"]);
    }

    function parseAddress() {
        address = {};
        var place = autocomplete.getPlace();
        if(place === undefined || place === null || place.address_components === undefined) {
            return;
        }

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (fields[addressType]) {
                var names = fields[addressType];
                for (var index in names) {
                    var value = place.address_components[i][names[index]];
                    if (value === undefined) {
                        continue;
                    }
                    switch(addressType) {
                        case "street_number":
                            address["street_1"] = value;
                            break;
                                                case "colloquial_area":
                            address["street_1"] = value;
                            break;
                        case "route":
                            if (address["street_1"] === undefined) {
                                address["street_1"] = value;
                            } else {
                                address["street_1"] = address["street_1"] + ' ' + value;
                            }
                            break;
                        case "locality":
                            address["city"] = value;
                            break;
                        case "sublocality_level_1":
                            if (address["city"] === undefined) {
                                address["city"] = value;
                            }
                            break;
                        case "postal_town":
                            address["town"] = value;
                            break;
                        case "administrative_area_level_1":
                            if (names[index] == "short_name") {
                                address["region_code"] = value;
                            } else if (names[index] == 'long_name') {
                                address["region"] = value;
                            }
                            break;
                        case "country":
                            address['country'] = value;
                            break;
                        case "postal_code_prefix":
                            address["zip"] = value;
                            break;
                        case "postal_code":
                            if (address["zip"] === undefined) {
                                address["zip"] = value;
                            } else {
                                address["zip"] = address["zip"] + "-" + value;
                            }
                            break;
                        case "postal_code_suffix":
                            if (address["zip"] === undefined) {
                                address["zip"] = value;
                            } else {
                                address["zip"] = address["zip"] + "-" + value;
                            }
                            break;
                    }
                }
            }
        }
    }

    function loadAddress() {
        clearAddress();
        parseAddress();
        updateAddress();
    }

    $(document).on("focus", "#" + triggerId, function(event) {
        if (config.geolocation) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }
    });

    $(document).on("change", "#country", function(event) {
        initAutocomplete();
    });

    $(document).ready(function() {
        initAutocomplete();
    });
    
    $(document).ready(function() {
        $("#"+triggerId).keydown(function(e){
            if(e.which == 13 && $("#"+triggerId).length){
                return false;
            }
        });
    });
}
});
