/**
 * @author Azaleasoft Team
 * @copyright Copyright (c) 2019 Azaleasoft (https://azaleasoft.com)
 * @package Azaleasoft_Asimprovedaddressvalidation
 */
define([
    "jquery",
    "jquery/ui",
    "googleMapsPlaceLibrary"
], function($){
    "use strict";

return function (config) {

	var autocomplete;
	var triggerId = config.triggerField;
	var restrictionIsEnabled = config.restriction;
	var streetLines = config.streetLines;
	var zipFormat = config.zipFormat;
	var fields = {
		street_number: ["short_name"],
		route: ["short_name"],
		locality: ["long_name"],
		sublocality_level_1: ["long_name"],
		postal_town: ["long_name"],
		administrative_area_level_1: ["long_name", "short_name"],
		country: ["short_name"],
		postal_code_prefix: ["short_name"],
		postal_code: ["short_name"],
		postal_code_suffix: ["short_name"]
	};
	var formIds = {
		"#shipping": "#co-shipping-form ",
		"#payment": "#co-payment-form "
	};
	var formId = "#co-shipping-form ";

	function initAutocomplete(tmpTriggerId = null) {
		var currenthash = window.location.hash;
		if (currenthash !== undefined && currenthash !== null && currenthash.length > 1) {
			formId = formIds[currenthash];
		}
		var triggerIdElements = $(formId + "[name='" + triggerId + "']");
		for (var i=0; i < triggerIdElements.length; i++) {
			if (triggerIdElements[i].id == tmpTriggerId) {
				autocomplete = new google.maps.places.Autocomplete(
					/** @type {!HTMLInputElement} */(triggerIdElements[i]),
					{types: ["geocode"]}
				);
				if (restrictionIsEnabled == 1 && $(formId + "[name='country_id']").val() !== undefined) {
					autocomplete.setComponentRestrictions({
						"country": [$(formId + "[name='country_id']").val()]
					});
				}
				autocomplete.setFields(["address_components"]);
				autocomplete.addListener("place_changed", updateAddress);
			}
		}
	}

	function checkRegionId(params) {
		$.ajax({
			type: "post",
			url: config.urlAddressAutocomplete,
			data: params,
			cache: false,
			dataType: "json"
		}).done(function(data){
			if (data.region_id !== undefined) {
				$(formId + "[name='region_id']").val(data.region_id).trigger("change");
			}
		});
	}

	function clearAddress() {
		$(formId + "[name='city']").val("");
		$(formId + "[name='postcode']").val("");
		$(formId + "[name='region_id']").val("");
		for (var i = 0; i < streetLines; i++) {
			$(formId + "[name='street[" + i + "]']").val("");
		}
	}

	function loadAddress(address) {
		if (address["country"] != $("[name='country']").val()) {
			$(formId + "[name='country_id']").val(address["country"]).trigger("change");
		}
		$(formId + "[name='region_id']").val("");
		$(formId + "[name='region']").val(address["region"]).trigger("change");
		checkRegionId({
			region_code: address["region_code"],
			country: address["country"]
		});
		if (address["country"] == "US" && address["zip"] !== undefined && address["zip"].length >= 5 && address["zip"].length <= 10) {
            address["zip"] = address["zip"].substring(0, zipFormat);
        }
		$(formId + "[name='postcode']").val(address["zip"]).trigger("change");
		if (address["country"] == "UK" || address["country"] == "SE") {
			$(formId + "[name='city']").val(address["town"]).trigger("change");
		} else {
			$(formId + "[name='city']").val(address["city"]).trigger("change");
		}
		$(formId + "[name='street[0]']").val(address["street_1"]).trigger("change");
	}

	function updateAddress() {
		var place = autocomplete.getPlace();
		if(place === undefined || place === null || place.address_components === undefined) {
			return;
		}
		var address = {};
		for (var i = 0; i < place.address_components.length; i++) {
			var addressType = place.address_components[i].types[0];
			if (fields[addressType]) {
				var names = fields[addressType];
				for (var index in names) {
					var value = place.address_components[i][names[index]];
					if (value === undefined) {
						continue;
					}
					switch(addressType) {
						case "street_number":
							address["street_1"] = value;
							break;
						case "route":
							if (address["street_1"] === undefined) {
								address["street_1"] = value;
							} else {
								address["street_1"] = address["street_1"] + " " + value;
							}
							break;
						case "locality":
							address["city"] = value;
							break;
						case "sublocality_level_1":
							if (address["city"] === undefined) {
								address["city"] = value;
							}
							break;
						case "postal_town":
							address["town"] = value;
							break;
						case "administrative_area_level_1":
							if (names[index] == "short_name") {
								address["region_code"] = value;
							} else if (names[index] == "long_name") {
								address["region"] = value;
							}
							break;
						case "country":
							address["country"] = value;
							break;
						case "postal_code_prefix":
							address["zip"] = value;
							break;
						case "postal_code":
							if (address["zip"] === undefined) {
								address["zip"] = value;
							} else {
								address["zip"] = address["zip"] + "-" + value;
							}
							break;
						case "postal_code_suffix":
							if (address["zip"] === undefined) {
								address["zip"] = value;
							} else {
								address["zip"] = address["zip"] + "-" + value;
							}
							break;
					}
				}
			}
		}
		clearAddress();
		loadAddress(address);
	}

	$(document).on("focus", "[name='" + triggerId + "']", function(event) {
		initAutocomplete(this.id);
		if (config.geolocation) {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var geolocation = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					var circle = new google.maps.Circle({
						center: geolocation,
						radius: position.coords.accuracy
					});
					autocomplete.setBounds(circle.getBounds());
				});
			}
		}
	});

}
});