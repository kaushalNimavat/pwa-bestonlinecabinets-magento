<?php
declare(strict_types=1);
namespace Boc\CatalogAttribute\Plugin;

use Magento\Catalog\Model\Product\Media\Config;
use Magento\Catalog\Model\CategoryFactory;

class FilterProductMediaAttributes
{
    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * FilterProductMediaAttributes constructor.
     *
     * @param CategoryFactory $categoryFactory
     */
    public function __construct(CategoryFactory $categoryFactory)
    {
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * Filter attributes have frontend input type is media image
     *
     * @param Config $subject
     * @param $result
     * @return array
     */
    public function afterGetMediaAttributeCodes(Config $subject, $result)
    {
        $categoryModel = $this->categoryFactory->create();
        $attributes = $categoryModel->getAttributes();
        $excludedAttributes = [];
        foreach ( $attributes as $attribute) {
            if ($attribute->getFrontendInput() === 'media_image') {
                $excludedAttributes[] = $attribute->getAttributeCode();
            }
        }
        return array_diff($result, $excludedAttributes);
    }
}
