<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\AssemblyInstruction;

class AbstractAssemblyInstruction extends \Magento\Backend\App\Action
{
	protected $primary = 'entity_id';
    protected $modelClass = 'Codazon\FlatPanel\Model\AssemblyInstruction';
    
    public function execute()
    {
        /* TO DO */
    }
}

