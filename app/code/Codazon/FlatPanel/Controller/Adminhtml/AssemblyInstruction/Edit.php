<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\AssemblyInstruction;

use Magento\Backend\App\Action;

class Edit extends AbstractAssemblyInstruction
{
	/**
	* Core registry
	*
	* @var \Magento\Framework\Registry
	*/
	protected $_coreRegistry = null;
	/**
	 * @var \Magento\Framework\View\Result\PageFactory
	 */
	protected $resultPageFactory;
	
	public function __construct(
		Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Registry $registry
	) {
		$this->resultPageFactory = $resultPageFactory;
		$this->_coreRegistry = $registry;
		parent::__construct($context);
	}
    
	protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FlatPanel::assembly_instruction_edit');
    }
    
	protected function _initAction()
	{
		$resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Codazon_FlatPanel::header');
		return $resultPage;
	}
    
	public function execute()
	{
		$id = $this->getRequest()->getParam($this->primary);
		$model = $this->_objectManager->create($this->modelClass);
		
        if ($storeId = (int)$this->getRequest()->getParam('store')) {
            $model->setData('store_id', $storeId);
        }
		
        if ($id) {
			$model->load($id);
			if (!$model->getId()) {
				$this->messageManager->addError(__('This Assembly Instruction no longer exists.'));
				/** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
				$resultRedirect = $this->resultRedirectFactory->create();
				return $resultRedirect->setPath('*/*/');
			}
		}
	
		$data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}
	
		$this->_coreRegistry->register('flat_panel_assembly_instruction', $model);
	
		/** @var \Magento\Backend\Model\View\Result\Page $resultPage */
		$resultPage = $this->_initAction();
		$resultPage->addBreadcrumb(
			$id ? __('Edit Assembly Instruction') : __('New Assembly Instruction'),
			$id ? __('Edit Assembly Instruction') : __('New Assembly Instruction')
		);
		$resultPage->getConfig()->getTitle()->prepend(__('Assembly Instruction'));
		$resultPage->getConfig()->getTitle()
			->prepend($model->getId() ? $model->getData('name') : __('New Assembly Instruction'));
	
		return $resultPage;
	}
}

