<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\AssemblyInstruction;

use Magento\Backend\App\Action;

class Index extends AbstractAssemblyInstruction
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FlatPanel::assembly_instruction');
    }
    
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addBreadcrumb(__('Flat Panel'), __('Flat Panel'));
        $resultPage->addBreadcrumb(__('Assembly Instruction'), __('Assembly Instruction'));
        $resultPage->setActiveMenu('Codazon_FlatPanel::assembly_instruction');
        $resultPage->getConfig()->getTitle()->prepend(__('Assembly Instruction'));
        return $resultPage;
    }
}

