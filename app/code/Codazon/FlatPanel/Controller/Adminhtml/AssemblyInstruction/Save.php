<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\AssemblyInstruction;

use Magento\Backend\App\Action;

class Save extends AbstractAssemblyInstruction
{
    protected $eventName = 'flat_panel_assembly_instruction_prepare_save';
    protected $_updateMsg = 'You saved this Assembly Instruction.';
    
    protected $mediaDirectory;
    protected $fileStorageDb;
    protected $mediaAttributes;
    protected $mediaConfig;
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FlatPanel::assembly_instruction_save');
    }
    
    protected function _init()
    {
        $this->mediaConfig = $this->_objectManager->get(\Magento\Catalog\Model\Product\Media\Config::class);
        $this->filesystem = $this->_objectManager->get(\Magento\Framework\Filesystem::class);
        $this->mediaDirectory = $this->filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->fileStorageDb = $this->_objectManager->get(\Magento\MediaStorage\Helper\File\Storage\Database::class);
    }
       
    protected function filterData($model)
    {
        
       
        $baseTempPath = $this->mediaConfig->getBaseTmpMediaPath();
        
        $fileHelper = $this->_objectManager->create(\Codazon\FlatPanel\Helper\File::class);
        $request = $this->getRequest();
       
        /* File */
        $basePath = $model->getBaseFilePath();
        $fileIds = ['psd_file'];
        foreach ($fileIds as $fileId) {
            if ($file = $request->getParam($fileId)) {
                if (!empty($file[0])) {
                    $fileName = $file[0]['file'];
                    if (empty($file[0]['value_id'])) {
                        try {
                            $file[0]['status'] = 'new';
                            $fileName = $fileHelper->moveFileFromTmp($baseTempPath, $basePath, $file);
                        } catch (\Exception $e) {
                            $model->setData($fileId, $fileName);
                        }
                    }
                    $model->setData($fileId, $fileName);
                }
            } elseif (!empty($model->getData($fileId))) {
                $fileName = $model->getData($fileId);
                $this->mediaDirectory->delete(
                    str_replace('catalog/product', $basePath, $this->mediaConfig->getMediaPath($fileName))
                );
                $model->setData($fileId, false);
            }
        }
        /* Image */
        $basePath = $model->getBaseMediaPath();
        $fileIds = ['thumbnail'];
        foreach ($fileIds as $fileId) {
            if ($file = $request->getParam($fileId)) {
                if (!empty($file[0])) {
                    $fileName = $file[0]['file'];
                    if (empty($file[0]['value_id'])) {
                        try {
                            $file[0]['status'] = 'new';
                            $fileName = $fileHelper->moveFileFromTmp($baseTempPath, $basePath, $file);
                        } catch (\Exception $e) {
                            $model->setData($fileId, $fileName);
                        }
                    }
                    $model->setData($fileId, $fileName);
                }
            } elseif (!empty($model->getData($fileId))) {
                $fileName = $model->getData($fileId);
                $this->mediaDirectory->delete(
                    str_replace('catalog/product', $basePath, $this->mediaConfig->getMediaPath($fileName))
                );
                $model->setData($fileId, false);
            }
        }
        
        $arrayParams = ['style'];
        foreach ($arrayParams as $name) {
            if ($value = $request->getParam($name)) {
                $model->setData($name, implode(',', $value));
            }
        }
    }
    
    public function execute()
	{
        $this->_init();
        $request = $this->getRequest();
        $data = $request->getPostValue();
        
        
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create($this->modelClass);
            $id = $this->getRequest()->getParam($this->primary);
            if ($id) {
				$model->setStoreId((int)$request->getParam('store'))->load($id);
			} else {
                unset($data[$this->primary]);
            }
            if ($store = $request->getParam('store')) {
                $data['store_id'] = $store;
            } else {
                $data['store_id'] = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
            }
            
            
            if (isset($data['use_default']) && is_array($data['use_default'])) {
                foreach ($data['use_default'] as $attributeCode => $useDefault) {
                    if ($useDefault) {
                        $data[$attributeCode] = false;
                    }
                }
            }
            
            $model->addData($data);
            $this->filterData($model);            
            
            $this->_eventManager->dispatch(
				$this->eventName,
				['model' => $model, 'request' => $this->getRequest()]
			);
            
            try {
				$result = $model->save();
                $this->messageManager->addSuccess($this->_updateMsg);
                if ($request->getParam('back') == 'edit') {
                    $returnParams = [$this->primary => $model->getId(), '_current' => true, 'back' => false];
                    if ($store) {
                        $returnParams['store'] = $store;
                    }
					return $resultRedirect->setPath('*/*/edit', $returnParams);
				} elseif ($request->getParam('back') == 'new') {
                    return $resultRedirect->setPath('*/*/new', []);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
				$this->messageManager->addError($e->getMessage());
			} catch (\RuntimeException $e) {
				$this->messageManager->addError($e->getMessage());
			} catch (\Exception $e) {
				$this->messageManager->addException($e, $e->getMessage());
			}
            
            $this->_getSession()->setFormData($data);
			return $resultRedirect->setPath('*/*/edit', [$this->primary => $this->getRequest()->getParam($this->primary)]);
        }
    }   
}
