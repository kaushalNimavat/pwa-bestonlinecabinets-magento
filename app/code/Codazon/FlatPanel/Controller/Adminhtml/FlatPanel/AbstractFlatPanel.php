<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\FlatPanel;

class AbstractFlatPanel extends \Magento\Backend\App\Action
{
	protected $primary = 'entity_id';
    protected $modelClass = 'Codazon\FlatPanel\Model\FlatPanel';
    
    public function execute()
    {
        /* TO DO */
    }
}

