<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\FlatPanel;

use Magento\Backend\App\Action;
use \Magento\Store\Model\Store;

class MassDisable extends \Codazon\FlatPanel\Controller\Adminhtml\AbstractMassStatus
{
    protected $primary = 'entity_id';
    
    protected $modelClass = 'Codazon\FlatPanel\Model\FlatPanel';
    
    protected $_updateMsg = 'A total of %1 record(s) have been disabled.';
    
    protected $status = 0;
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FlatPanel::flat_panel_save');
    }
}

