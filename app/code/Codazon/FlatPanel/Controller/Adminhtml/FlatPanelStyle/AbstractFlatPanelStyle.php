<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\FlatPanelStyle;

class AbstractFlatPanelStyle extends \Magento\Backend\App\Action
{
	protected $primary = 'entity_id';
    protected $modelClass = 'Codazon\FlatPanel\Model\FlatPanelStyle';
    
    public function execute()
    {
        /* TO DO */
    }
}

