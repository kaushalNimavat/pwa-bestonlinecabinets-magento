<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\FlatPanelStyle;

use Magento\Backend\App\Action;

class Index extends AbstractFlatPanelStyle
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FlatPanel::flat_panel_style');
    }
    
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addBreadcrumb(__('Flat Panel'), __('Flat Panel'));
        $resultPage->addBreadcrumb(__('Flat Panel Style'), __('Flat Panel Style'));
        $resultPage->setActiveMenu('Codazon_FlatPanel::flat_panel_style');
        $resultPage->getConfig()->getTitle()->prepend(__('Flat Panel Style'));
        return $resultPage;
    }
}

