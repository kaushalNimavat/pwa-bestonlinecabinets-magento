<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\FlatPanelStyle;

use Magento\Backend\App\Action;
use \Magento\Store\Model\Store;

class MassEnable extends \Codazon\FlatPanel\Controller\Adminhtml\AbstractMassStatus
{
    protected $primary = 'entity_id';
    
    protected $modelClass = 'Codazon\FlatPanel\Model\FlatPanelStyle';
       
    protected $_updateMsg = 'A total of %1 record(s) have been enabled.';
    
    protected $status = 1;
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FlatPanel::flat_panel_style_save');
    }
}

