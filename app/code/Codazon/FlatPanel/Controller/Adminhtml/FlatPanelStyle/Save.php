<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Controller\Adminhtml\FlatPanelStyle;

use Magento\Backend\App\Action;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\MediaStorage\Service\ImageResize;
use Magento\Framework\App\State;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\EntityManager\Operation\ExtensionInterface;
use Magento\MediaStorage\Model\File\Uploader as FileUploader;

class Save extends AbstractFlatPanelStyle
{
    protected $eventName = 'flat_panel_flat_panel_style_prepare_save';
    protected $_updateMsg = 'You saved this Flat Panel Style.';
    
    protected $mediaDirectory;
    protected $fileStorageDb;
    protected $mediaAttributes;
    protected $mediaConfig;
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FlatPanel::flat_panel_style_save');
    }
    
    protected function _init()
    {
        $this->mediaConfig = $this->_objectManager->get(\Magento\Catalog\Model\Product\Media\Config::class);
        $this->filesystem = $this->_objectManager->get(\Magento\Framework\Filesystem::class);
        $this->mediaDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->fileStorageDb = $this->_objectManager->get(\Magento\MediaStorage\Helper\File\Storage\Database::class);
    }
    
    protected function filterData($model)
    {
        
        
        if ($gallery = $this->getRequest()->getParam('kitchen_image')) {
            $clearImages = [];
            $newImages = [];
            $existImages = [];
            $oldImages = [];
            if (!is_array($gallery)) {
                $gallery = json_decode($gallery, true);
            }
            
            $images = $gallery['images'];
            foreach ($images as $i => &$image) {
                $fi = $image['file'];
                if (!empty($image['removed'])) {
                    $clearImages[] = $image['file'];
                    unset($images[$i]);
                } elseif (empty($image['value_id'])) {
                    $newFile = $this->moveImageFromTmp($image['file']);
                    $image['value_id'] = uniqid();
                    $newImages[$image['file']] = $image;
                    $image['file'] = $newFile;
                }
                
                $oldImages[$fi] = $image['file'];
            }
           
            $this->processDeletedImages($model, $clearImages);
            $this->processNewAndExistingImages($model, $gallery['images']);
            $gallery['images'] = $images;
            $model->setData('kitchen_image', json_encode($gallery));
        }
        
        if ($metarialImage = $this->getRequest()->getParam('material_image')) {
            $material = $metarialImage[0];
            if (empty($material['value_id'])) {
                $material['file'] = $this->moveImageFromTmp($material['file']);
            }
            $model->setData('material_image', $material['file']);
        } elseif (!empty($model->getData('material_image'))) {
            $metarialImage = $model->getData('material_image');
            $this->processDeletedImages($model, [$metarialImage]);
            $model->setData('material_image', false);
        }
        
        return $this;
    }
    
    protected function processDeletedImages($model, array $images) {
        foreach ($images as $file) {
            //$file = $this->getFilenameFromTmp($file);
            $this->mediaDirectory->delete(
                $this->mediaConfig->getTmpMediaPath($file)
            );
            $this->mediaDirectory->delete(
                $this->mediaConfig->getMediaPath($file)
            );
        }
    }
    
    protected function processNewAndExistingImages($model, array &$images)
    {   
        foreach ($images as &$image) {
            if (empty($image['removed'])) {
                $data = $this->processNewImage($model, $image);
                $data['value_id'] = $image['value_id'];
                $data['label'] = isset($image['label']) ? $image['label'] : '';
                $data['position'] = isset($image['position']) ? (int)$image['position'] : 0;
                $data['disabled'] = isset($image['disabled']) ? (int)$image['disabled'] : 0;
                $data['store_id'] = (int)$model->getStoreId();
            }
        }
    }
    
    protected function processNewImage($model, array &$image)
    {
        $data = [];

        $data['value'] = $image['file'];
        $data['attribute_id'] = null;

        if (!empty($image['media_type'])) {
            $data['media_type'] = $image['media_type'];
        }

        $image['value_id'] = uniqid();

        return $data;
    }
    
    protected function copyImage($file)
    {
        try {
            $destinationFile = $this->getUniqueFileName($file);

            if (!$this->mediaDirectory->isFile($this->mediaConfig->getMediaPath($file))) {
                throw new \Exception();
            }

            if ($this->fileStorageDb->checkDbUsage()) {
                $this->fileStorageDb->copyFile(
                    $this->mediaDirectory->getAbsolutePath($this->mediaConfig->getMediaShortUrl($file)),
                    $this->mediaConfig->getMediaShortUrl($destinationFile)
                );
                $this->mediaDirectory->delete($this->mediaConfig->getMediaPath($destinationFile));
            } else {
                $this->mediaDirectory->copyFile(
                    $this->mediaConfig->getMediaPath($file),
                    $this->mediaConfig->getMediaPath($destinationFile)
                );
            }

            return str_replace('\\', '/', $destinationFile);
        } catch (\Exception $e) {
            $file = $this->mediaConfig->getMediaPath($file);
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We couldn\'t copy file %1. Please delete media with non-existing images and try again.', $file)
            );
        }
    }
    
    private function getSafeFilename($file)
    {
        $file = DIRECTORY_SEPARATOR . ltrim($file, DIRECTORY_SEPARATOR);

        return $this->mediaDirectory->getDriver()->getRealPathSafety($file);
    }
    
    protected function getUniqueFileName($file, $forTmp = false)
    {
        if ($this->fileStorageDb->checkDbUsage()) {
            $destFile = $this->fileStorageDb->getUniqueFilename(
                $this->mediaConfig->getBaseMediaUrlAddition(),
                $file
            );
        } else {
            $destinationFile = $forTmp
                ? $this->mediaDirectory->getAbsolutePath($this->mediaConfig->getTmpMediaPath($file))
                : $this->mediaDirectory->getAbsolutePath($this->mediaConfig->getMediaPath($file));
            $destFile = dirname($file) . '/' . FileUploader::getNewFileName($destinationFile);
        }
        
        return $destFile;
    }
    
    protected function moveImageFromTmp($file)
    {
        
        $file = $this->getFilenameFromTmp($this->getSafeFilename($file));
        $destinationFile = $this->getUniqueFileName($file);

        try {
            if ($this->fileStorageDb->checkDbUsage()) {
                $a = $this->fileStorageDb->renameFile(
                    $this->mediaConfig->getTmpMediaShortUrl($file),
                    $this->mediaConfig->getMediaShortUrl($destinationFile)
                );
                $this->mediaDirectory->delete($this->mediaConfig->getTmpMediaPath($file));
                $this->mediaDirectory->delete($this->mediaConfig->getMediaPath($destinationFile));
                 
            } else {
                $this->mediaDirectory->renameFile(
                    $this->mediaConfig->getTmpMediaPath($file),
                    $this->mediaConfig->getMediaPath($destinationFile)
                );
            }
        } catch (\Magento\Framework\Exception\FileSystemException $e) {
            
        }

        return str_replace('\\', '/', $destinationFile);
    }
    
    protected function getFilenameFromTmp($file)
    {
        return strrpos($file, '.tmp') == strlen($file) - 4 ? substr($file, 0, strlen($file) - 4) : $file;
    }
    
    public function execute()
	{
        $this->_init();
        $request = $this->getRequest();
        $data = $request->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            
            $model = $this->_objectManager->create($this->modelClass);
            $id = $this->getRequest()->getParam($this->primary);
           
            if ($id) {
				$model->setStoreId((int)$request->getParam('store'))->load($id);
			} else {
                unset($data[$this->primary]);
            }
            if ($store = $request->getParam('store')) {
                $data['store_id'] = $store;
            } else {
                $data['store_id'] = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
            }
            
            if (isset($data['use_default']) && is_array($data['use_default'])) {
                foreach ($data['use_default'] as $attributeCode => $useDefault) {
                    if ($useDefault) {
                        $data[$attributeCode] = false;
                    }
                }
            }
            
            $model->addData($data);          
            $this->filterData($model);
            
            $this->_eventManager->dispatch(
				$this->eventName,
				['model' => $model, 'request' => $this->getRequest()]
			);
            
            try {
				$result = $model->save();
                $this->messageManager->addSuccess($this->_updateMsg);
                if ($request->getParam('back') == 'edit') {
                    $returnParams = [$this->primary => $model->getId(), '_current' => true, 'back' => false];
                    if ($store) {
                        $returnParams['store'] = $store;
                    }
					return $resultRedirect->setPath('*/*/edit', $returnParams);
				} elseif ($request->getParam('back') == 'new') {
                    return $resultRedirect->setPath('*/*/new', []);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
				$this->messageManager->addError($e->getMessage());
			} catch (\RuntimeException $e) {
				$this->messageManager->addError($e->getMessage());
			} catch (\Exception $e) {
				$this->messageManager->addException($e, $e->getMessage());
			}
            
            $this->_getSession()->setFormData($data);
			return $resultRedirect->setPath('*/*/edit', [$this->primary => $this->getRequest()->getParam($this->primary)]);
        }
    }   
}
