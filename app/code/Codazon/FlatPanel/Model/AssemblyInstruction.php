<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Model;

class AssemblyInstruction extends \Codazon\FlatPanel\Model\AbstractModel
{

    const ENTITY = 'assembly_instruction';
    
    const BASE_FILE_PATH = 'codazon/flatpanel/instruction/file';
    
    const BASE_MEDIA_PATH = 'catalog/product';
    
    protected function _construct()
    {
        $this->_init('Codazon\FlatPanel\Model\ResourceModel\AssemblyInstruction');
    }
    
    public function getBaseFilePath()
    {
        return self::BASE_FILE_PATH;
    }
    
    public function getBaseMediaPath()
    {
        return self::BASE_MEDIA_PATH;
    }
}
