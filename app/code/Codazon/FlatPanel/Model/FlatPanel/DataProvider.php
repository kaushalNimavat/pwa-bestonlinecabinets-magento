<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Model\FlatPanel;

use Codazon\FlatPanel\Model\ResourceModel\FlatPanel\CollectionFactory as CollectionFactory;
use Magento\Framework\App\Request\Http as Request;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Eav\Model\Entity\Type;
use Magento\Ui\Component\Form\Field;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as EavAttribute;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Eav\Model\Config;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{   
    protected $collection;
    
    protected $dataPersistor;
    
    protected $request;
    
    protected $loadedData;
    
    protected $storeId;
    
    protected $_isEav = true;
    
    protected $_entityTypeCode = 'flat_panel';
    
    protected $requestScopeFieldName = 'store';
    
    protected $formElement = [
        'text' => 'input',
        'boolean' => 'checkbox',
    ];

	public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        Request $request,
        Config $eavConfig,
        \Magento\Framework\Registry $registry,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->request = $request;
        $this->registry = $registry;
        $this->storeId = $this->request->getParam('store', \Magento\Store\Model\Store::DEFAULT_STORE_ID);
        $this->eavConfig = $eavConfig;
        $this->entityType = $this->eavConfig->getEntityType($this->_entityTypeCode);
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->meta = $this->prepareMeta($this->meta);
    }
    
	public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        
        $item = $this->getCurrentItem();
        $data = $item->getData();
        if (empty($data)) {
            $data[$this->getPrimaryFieldName()] = null;
        }
        $data = $this->addUseDefaultSettings($item, $data);
        $data = $this->filterFields($data);
        $this->loadedData[$item->getId()] = $data;
        $data = $this->dataPersistor->get('flat_panel');
        if (!empty($data)) {
            $item = $this->collection->getNewEmptyItem();
            $item->setData($data);
            $this->loadedData[$item->getId()] = $item->getData();
            $this->dataPersistor->clear('flat_panel');
        }
        return $this->loadedData;
    }
    
    public function getCurrentItem()
    {
        $item = $this->registry->registry('flat_panel_flat_panel');
        if ($item) {
            return $item;
        }
        $requestId = $this->request->getParam($this->requestFieldName);
        $requestScope = $this->request->getParam($this->requestScopeFieldName, Store::DEFAULT_STORE_ID);
        if ($requestId) {
            $item = $this->collection->addFieldToFilter($this->primaryFieldName, $requestId)->getFirstItem();
            if (!$item->getId()) {
                throw NoSuchEntityException::singleField('id', $requestId);
            }
        }
        return $item;
    }
    
    protected function addUseDefaultSettings($item, $data)
    {
        $attributeCollection = $this->entityType->getAttributeCollection();
        foreach($attributeCollection as $attribute) {
            $code = $attribute->getAttributeCode();
            if ($item->getExistsStoreValueFlag($code) ||
                $item->getStoreId() === Store::DEFAULT_STORE_ID
            ) {
                $data['isUseDefault'][$code] = false;
            } else {
                $data['isUseDefault'][$code] = true;
            }
        }
        return $data;
    }
    
    protected function filterFields($data)
    {
        return $data;
    }
    
    public function prepareMeta($meta)
    {
        $meta = array_replace_recursive($meta, $this->prepareFieldsMeta(
            $this->getFieldsMap(),
            $this->getAttributesMeta($this->entityType)
        ));
        return $meta;
    }
    
    private function prepareFieldsMeta($fieldsMap, $fieldsMeta)
    {
        $result = [];
        foreach ($fieldsMap as $fieldSet => $fields) {
            foreach ($fields as $field) {
                if (isset($fieldsMeta[$field])) {
                    $result[$fieldSet]['children'][$field]['arguments']['data']['config'] = $fieldsMeta[$field];
                }
            }
        }
        return $result;
    }
    
    protected function getFieldsMap()
    {
        return [
            'general' => [
                'name',
                'is_active',
                'sort_order'
            ]
        ];
    }
    
    public function getAttributesMeta(Type $entityType)
    {
        $meta = [];
        $attributes = $entityType->getAttributeCollection();
        $isScopeStore = $this->request->getParam($this->requestScopeFieldName, Store::DEFAULT_STORE_ID) != Store::DEFAULT_STORE_ID;
        foreach ($attributes as $attribute) {
            $code = $attribute->getAttributeCode();           
            $meta[$code]['scopeLabel'] = $this->getScopeLabel($attribute);
            $meta[$code]['componentType'] = Field::NAME;
            
            if ($isScopeStore) {
                $meta[$code]['imports'] = [
                    'isUseDefault' => '${ $.provider }:data.isUseDefault.'.$code
                ];
                $meta[$code]['service'] = [
                    'template' => 'ui/form/element/helper/service'
                ];
            }
        }
        $result = [];
        foreach ($meta as $key => $item) {
            $result[$key] = $item;
        }
        $result = $this->getDefaultMetaData($result);
        return $result;
    }
    
    public function getScopeLabel($attribute)
    {
        $html = '';
        if (!$attribute || $this->storeManager->isSingleStoreMode()
            || $attribute->getFrontendInput() === AttributeInterface::FRONTEND_INPUT
        ) {
            return $html;
        }
        if ($attribute->isScopeGlobal()) {
            $html .= __('[GLOBAL]');
        } elseif ($attribute->isScopeWebsite()) {
            $html .= __('[WEBSITE]');
        } elseif ($attribute->isScopeStore()) {
            $html .= __('[STORE VIEW]');
        }

        return $html;
    }
    
    public function getDefaultMetaData($result)
    {
        return $result;
    }
}

