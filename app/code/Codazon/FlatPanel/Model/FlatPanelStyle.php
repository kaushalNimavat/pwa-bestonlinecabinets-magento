<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Model;

class FlatPanelStyle extends \Codazon\FlatPanel\Model\AbstractModel
{

    const ENTITY = 'flat_panel_style';
    
    protected function _construct()
    {
        $this->_init('Codazon\FlatPanel\Model\ResourceModel\FlatPanelStyle');
    }
}
