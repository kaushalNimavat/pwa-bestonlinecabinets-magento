<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Model\ResourceModel\AssemblyInstruction;


class Collection extends \Magento\Catalog\Model\ResourceModel\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Codazon\FlatPanel\Model\AssemblyInstruction', 'Codazon\FlatPanel\Model\ResourceModel\AssemblyInstruction');
    }
}
