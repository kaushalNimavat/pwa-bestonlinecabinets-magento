<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Model\Source;

class FlatPanelStyles implements \Magento\Framework\Data\OptionSourceInterface
{
    protected $collectionFactory;
	
    public function __construct(\Codazon\FlatPanel\Model\ResourceModel\FlatPanelStyle\CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }
    
	public function toOptionArray()
	{
		$options[] = ['label' => '', 'value' => ''];
		$collection = $this->collectionFactory->create();
        $collection->addAttributeToSelect(['name']);
		foreach ($collection as $item) {
			$options[] = [
				'label' => $item->getName(),
				'value' => $item->getId(),
			];
		}
		return $options;
	}
}
