<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Model\Source;

class InstructionTypes implements \Magento\Framework\Data\OptionSourceInterface
{
        
	public function toOptionArray()
	{
        $options = [];
		$options[] = ['label' => 'PDF instruction', 'value' => '1'];
        $options[] = ['label' => 'Video instruction', 'value' => '2'];
		return $options;
	}
}
