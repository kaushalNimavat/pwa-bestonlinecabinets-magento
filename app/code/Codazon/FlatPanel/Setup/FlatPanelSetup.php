<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Setup;

use Magento\Eav\Setup\EavSetup;

class FlatPanelSetup extends EavSetup
{
    
    public function getDefaultEntities()
    {
        $entities = array (
            'flat_panel' => array (
                'entity_model' => 'Codazon\FlatPanel\Model\ResourceModel\FlatPanel',
                'attribute_model' => 'Codazon\FlatPanel\Model\FlatPanelAttribute',
                'entity_attribute_collection' => 'Codazon\FlatPanel\Model\ResourceModel\FlatPanelAttribute\Collection',
                'table' => 'flat_panel_entity',
                'attributes' => array (
                    'name' => array (
                        'type' => 'varchar',
                        'label' => 'Name',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 5,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Flat Panel',
                    ),
                    'description' => array (
                        'type' => 'text',
                        'label' => 'Description',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 10,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Flat Panel',
                    ),
                    'is_active' => array (
                        'type' => 'int',
                        'label' => 'Is Active',
                        'input' => 'select',
                        'required' => true,
                        'sort_order' => 10,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Flat Panel',
                    ),
                ),
            ),
            'flat_panel_style' => array (
                'entity_model' => 'Codazon\FlatPanel\Model\ResourceModel\FlatPanelStyle',
                'attribute_model' => 'Codazon\FlatPanel\Model\FlatPanelStyleAttribute',
                'entity_attribute_collection' => 'Codazon\FlatPanel\Model\ResourceModel\FlatPanelStyleAttribute\Collection',
                'table' => 'flat_panel_style_entity',
                'attributes' => array (
                    'name' => array (
                        'type' => 'varchar',
                        'label' => 'Name',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 5,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Flat Panel Style',
                    ),
                    'is_active' => array (
                        'type' => 'int',
                        'label' => 'Is Active',
                        'input' => 'select',
                        'required' => true,
                        'sort_order' => 10,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Flat Panel Style',
                    ),
                ),
            ),
            'assembly_instruction' => array (
                'entity_model' => 'Codazon\FlatPanel\Model\ResourceModel\AssemblyInstruction',
                'attribute_model' => 'Codazon\FlatPanel\Model\AssemblyInstructionAttribute',
                'entity_attribute_collection' => 'Codazon\FlatPanel\Model\ResourceModel\AssemblyInstructionAttribute\Collection',
                'table' => 'assembly_instruction_entity',
                'attributes' => array (
                    'name' => array (
                        'type' => 'varchar',
                        'label' => 'Name',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 5,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Assembly Instruction',
                    ),
                    'description' => array (
                        'type' => 'text',
                        'label' => 'Description',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 10,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Assembly Instruction',
                    ),
                    'is_active' => array (
                        'type' => 'int',
                        'label' => 'Is Active',
                        'input' => 'select',
                        'required' => true,
                        'sort_order' => 10,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Assembly Instruction',
                    )
                ),
            ),
        );
        return $entities;
    }
}
