<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Ui\Component\Listing\Column;

class AssemblyInstructionActions extends \Codazon\FlatPanel\Ui\Component\Listing\Column\AbstractActions
{
	/** Url path */
	protected $_editUrl = 'flat_panel/assemblyinstruction/edit';
    /**
    * @var string
    */
	protected $_deleteUrl = 'flat_panel/assemblyinstruction/delete';
    /**
    * @var string
    */
    protected $_primary = 'entity_id';
}

