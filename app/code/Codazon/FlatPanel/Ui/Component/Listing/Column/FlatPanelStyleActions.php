<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FlatPanel\Ui\Component\Listing\Column;

class FlatPanelStyleActions extends \Codazon\FlatPanel\Ui\Component\Listing\Column\AbstractActions
{
	/** Url path */
	protected $_editUrl = 'flat_panel/flatpanelstyle/edit';
    /**
    * @var string
    */
	protected $_deleteUrl = 'flat_panel/flatpanelstyle/delete';
    /**
    * @var string
    */
    protected $_primary = 'entity_id';
}

