<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FlatPanel\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class MaterialImage extends \Magento\Ui\Component\Listing\Columns\Column
{
    const NAME = 'thumbnail';
    
    const ALT_FIELD = 'name';
    
    protected $objectManager;
    
    protected $urlBuilder;
    
    protected $mediaConfig;
    
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
		$this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$this->mediaConfig = $this->objectManager->get('Magento\Catalog\Model\Product\Media\Config');
        $this->imageHelper = $this->objectManager->get('Magento\Catalog\Helper\Image');
        $this->urlBuilder = $urlBuilder;
    }
    
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            $placeholder = $this->objectManager->get(\Magento\Framework\View\Asset\Repository::class)->getUrl('Magento_Catalog::images/product/placeholder/thumbnail.jpg');
            foreach ($dataSource['data']['items'] as & $item) {
                if (!empty($item['material_image'])) {
                    $imgUrl = $this->mediaConfig->getMediaUrl($item['material_image']);
                } else {
                    $imgUrl = $placeholder;
                }
                $item[$fieldName . '_src'] = $imgUrl;
                $item[$fieldName . '_alt'] = $item['name'];
                $item[$fieldName . '_link'] = $this->urlBuilder->getUrl('flat_panel/flatpanelstyle/edit', ['entity_id' => $item['entity_id']]);
                $item[$fieldName . '_orig_src'] = $imgUrl;
            }
        }
        return $dataSource;
    }
}