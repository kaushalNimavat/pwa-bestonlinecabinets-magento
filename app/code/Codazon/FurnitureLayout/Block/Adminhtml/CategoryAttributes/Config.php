<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Block\Adminhtml\CategoryAttributes;

class Config extends \Magento\Backend\Block\Template
{
    protected $_assetRepo;
    
    protected $_registry;
    
    protected $_objectManager;
    
    protected $_config;
    
    public function __construct(
		\Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
		array $data = [])
    {
		$this->_assetRepo = $context->getAssetRepository();
        $this->_registry = $registry;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context, $data);
    }
    
    
    
}