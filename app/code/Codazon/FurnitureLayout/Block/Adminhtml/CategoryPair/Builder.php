<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Block\Adminhtml\CategoryPair;

class Builder extends \Magento\Backend\Block\Template
{
    protected $_assetRepo;
    
    protected $_registry;
    
    protected $_objectManager;
    
    protected $_config;
    
    protected $_pairs;
    
    public function __construct(
		\Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
		array $data = [])
    {
		$this->_assetRepo = $context->getAssetRepository();
        $this->_registry = $registry;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context, $data);
    }
        
    public function getConfig()
    {
        if ($this->_config === null) {
            $this->_config['catChooserUrl'] = $this->getUrl('catalog/category_widget/chooser');
        }
        return $this->_config;
    }
    
    public function getCategoryData()
    {
        if ($this->_pairs === null) {
            $collection = $this->_objectManager->create(\Codazon\FurnitureLayout\Model\ResourceModel\CategoryPair\Collection::class);
            $collectionFactory = $this->_objectManager->get(\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory::class);
            $catIds = [];
            foreach ($collection as $item) {
                $catIds[$item->getData('group_1')] = $item->getData('group_1');
                $catIds[$item->getData('group_2')] = $item->getData('group_2');
            }
            $catCol = $collectionFactory->create()->addFieldToFilter('entity_id', ['in' => $catIds])
                ->addAttributeToSelect(['name']);
            $this->_pairs = [];
            foreach ($collection as $item) {
                $cat1Id = $item->getData('group_1');
                $cat2Id = $item->getData('group_2');
                if ($cat1Id && $cat2Id) {
                    $cat1 = $catCol->getItemById($cat1Id);
                    $cat2 = $catCol->getItemById($cat2Id);
                    $this->_pairs[] = [
                        'id'    => $item->getId(),
                        'cat1'  => [
                            'id'    => $cat1->getId(),
                            'name'  => $cat1->getName(),
                        ],
                        'cat2'  => [
                            'id'    => $cat2->getId(),
                            'name'  => $cat2->getName(),
                        ],
                    ];
                }
            }
        }
        return $this->_pairs;
    }
}