<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Block\Adminhtml\Currentsale;

class Builder extends \Magento\Backend\Block\Template
{
    protected $_assetRepo;
    
    protected $_registry;
    
    protected $_objectManager;
    
    protected $_config;
    
    public function __construct(
		\Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
		array $data = [])
    {
		$this->_assetRepo = $context->getAssetRepository();
        $this->_registry = $registry;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context, $data);
    }
    
    public function getMediaUrl($path = '')
    {
		return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$path;
	}
    
    public function getStoresArray()
    {
        $storeManagerDataList = $this->_storeManager->getStores();
        $stores = [
            [
                'name'          => 'Admin',
                'code'          => 'admin',
                'store_id'      => 0
            ]
        ];

        foreach ($storeManagerDataList as $key => $value) {
            $stores[] = ['name' => $value['name'], 'code' => $value['code'], 'store_id' => $key];
        }
        return $stores;
    }
    
    public function getConfig()
    {
        if ($this->_config === null) {
            $this->_config['catChooserUrl'] = $this->getUrl('catalog/category_widget/chooser');
            $this->_config['catchildrenUrl'] = $this->getUrl('furniturelayout/currentsale/catchildren');
            $this->_config['stores'] = $this->getStoresArray();
            $this->_config['groupOptionFields'] = $this->getGroupOptionFields();
            $this->_config['availableChildren'] = $this->getAvailableChildren();
            $this->_config['catIdsChooserUrl'] = $this->getUrl('catalog_rule/promo_widget/chooser', ['attribute' => 'category_ids']);
        }
        return $this->_config;
    }
    
    public function getAvailableChildren()
    {
        $categories = [];
        if ($model = $this->_registry->registry('furniturelayout_currentsale')) {
            $data = $model->getData('structure');
            if ($data) {
                $data = json_decode($data, true);
                $categoryRepository = $this->_objectManager->get('Magento\Catalog\Model\CategoryRepository');
                $collectionFactory = $this->_objectManager->get('Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
                foreach ($data['groups'] as $group) {
                    $groupId = $group['cat_id'];
                    if ($groupId) {
                        $categories[$groupId] = [];
                        $parentCat = false;
                        try {
                            $parentCat = $categoryRepository->get($groupId);
                            $children = explode(',', $parentCat->getChildren());
                        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                            $children = [];
                        }
                        if ($parentCat) {
                            $collection = $collectionFactory->create();
                            $collection->addFieldToFilter('entity_id', ['in' => $children])
                                ->addAttributeToSelect(['name']);
                            $categories[$groupId] = [
                                'cat_id'    => $groupId,
                                'cat_name'  => $parentCat->getName(),
                                'children'  => []
                            ];
                            if ($collection->count()) {
                                foreach ($collection as $subcat) {
                                    $subcatId = $subcat->getId();
                                    $categories[$groupId]['children'][$subcatId] = [
                                        'id'        => $subcatId,
                                        'name'      => $subcat->getName(),
                                        'active'    => in_array($subcatId, $group['subcats'])
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }
        return $categories;
    }
    
    protected function getGroupOptionFields()
    {
        return [
            'group_title' => [
                'label' => __('Title'),
                'type' => 'multi_text',
                'name' => 'group_title',
                'value' => [],
                'show_in_overview' => true
            ],
            /* 'categories_operator' => [
                'label' => __('Categories'),
                'type' => 'select',
                'name' => 'categories_operator',
                'value' => '',
                'options' => [
                    ['value' => '==', 'label' => __('is')],
                    ['value' => '!=', 'label' => __('is not')],
                ]
            ], */
            'categories' => [
                'label' => __('Categories'),
                'type' => 'category_checkbox_tree',
                'name' => 'categories',
                'value' => ''
            ],
            'promo_code' => [
                'label' => __('Promo code'),
                'type' => 'text',
                'name' => 'promo_code',
                'value' => '',
                'show_in_overview' => true
            ],            
            'discount_amount' => [
                'label' => __('Discount Amount'),
                'type' => 'text',
                'name' => 'discount_amount',
                'value' => ''
            ],
            'is_sale_on' => [
                'label' => __('Is sale on'),
                'type' => 'yes_no',
                'name' => 'is_sale_on',
                'value' => 1,
                'show_in_overview' => true
            ],
            'start_date' => [
                'label' => __('Start date'),
                'type' => 'multi_text',
                'name' => 'start_date',
                'value' => ''
            ],
            'discount_type' => [
                'label' => __('Discount type'),
                'type' => 'select',
                'name' => 'discount_type',
                'value' => '',
                'options' => [
                    ['value' => '1', 'label' => __('Ready To Assemble Finishes')],
                    ['value' => '2', 'label' => __('Pre-Assembled Finishes')],
                    ['value' => '3', 'label' => __('Clearance')]
                ]
            ],
            'item_label' => [
                'label' => __('Label on item'),
                'type' => 'multi_text',
                'name' => 'item_label',
                'value' => ''
            ]
        ];
    }
}