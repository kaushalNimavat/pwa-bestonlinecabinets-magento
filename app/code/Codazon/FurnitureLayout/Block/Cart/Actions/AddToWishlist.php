<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Block\Cart\Actions;

use Magento\Checkout\Block\Cart\Item\Renderer\Actions\Generic;
use Magento\Checkout\Helper\Cart;
use Magento\Framework\View\Element\Template;

class AddToWishlist extends Generic
{
    protected $_template = 'Codazon_FurnitureLayout::cart/actions/add-to-wishlist.phtml';
    
    public function __construct(
        Template\Context $context,
        \Magento\Wishlist\Helper\Data $wishlishHeler,
        Cart $cartHelper,
        array $data = []
    ) {
        $this->cartHelper = $cartHelper;
        $this->wishlishHeler = $wishlishHeler;
        parent::__construct($context, $data);
    }
    
    
    /**
     * Get item configure url
     *
     * @return string
     */
    public function getConfigureUrl()
    {
        return $this->wishlishHeler->getAddParams($this->getItem()->getProduct());
    }
}
