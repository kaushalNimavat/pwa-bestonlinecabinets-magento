<?php

/**
 * Copyright © 2017 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */
 
namespace Codazon\FurnitureLayout\Block\Category;

use Magento\Catalog\Model\Layer\Resolver;

class CategoryPair extends \Magento\Framework\View\Element\Template
{
    protected $coreRegistry;
    
    protected $helper;
    
    protected $currentCategory;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $helper->getCoreRegistry();
        $this->helper = $helper;
        $this->objectManager = $helper->getObjectManager();
    }
    
    public function getCurrentCategory()
    {
        if ($this->currentCategory === null) {
            $this->currentCategory = $this->coreRegistry->registry('current_category');
            if (!$this->currentCategory) {
                $this->currentCategory = false;
            }
        }
        return $this->currentCategory;
    }
    
    public function getCurrentPair()
    {
        if ($category = $this->getCurrentCategory()) {
            return $this->getCategoryPairByCatId($category->getId());
        }
        return false;
    }
    
    public function getCategoryPairByCatId($id)
    {
        $collection = $this->objectManager->create(\Codazon\FurnitureLayout\Model\ResourceModel\CategoryPair\Collection::class)
            ->addFieldToFilter(['group_1', 'group_2'], [$id, $id])->setPageSize(1);
        return $collection->getFirstItem();
    }
    
    public function getCategoryById($id)
    {
        return $this->objectManager->get(\Magento\Catalog\Model\CategoryRepository::class)->get($id);
    }
}