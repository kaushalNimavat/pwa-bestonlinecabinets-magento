<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Block\Category\Compare;

use Magento\Catalog\Model\Category;
use Magento\Customer\Model\Context;
use Magento\Framework\App\Action\Action;

/**
 * Catalog products compare block
 * @api
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class ListCompare extends \Magento\Framework\View\Element\Template
{
    /**
     * Product Compare items collection
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\Collection
     */
    protected $_items;

    /**
     * Compare Products comparable attributes cache
     *
     * @var array
     */
    protected $_attributes;

    /**
     * Flag which allow/disallow to use link for as low as price
     *
     * @var bool
     */
    protected $_useLinkForAsLowAs = false;

    /**
     * Customer id
     *
     * @var null|int
     */
    protected $_customerId = null;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * Customer visitor
     *
     * @var \Magento\Customer\Model\Visitor
     */
    protected $_customerVisitor;

    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;

    /**
     * Item collection factory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\CollectionFactory
     */
    protected $_itemCollectionFactory;

    /**
     * @var \Magento\Framework\Url\EncoderInterface
     */
    protected $urlEncoder;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;
    
    protected $_compareCategory;
    
    protected $objectManager;
    
    protected $categoryAttributes;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\CollectionFactory $itemCollectionFactory
     * @param Product\Visibility $catalogProductVisibility
     * @param \Magento\Customer\Model\Visitor $customerVisitor
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Codazon\FurnitureLayout\Model\ResourceModel\Category\Compare\Item\CollectionFactory $itemCollectionFactory,
        \Magento\Customer\Model\Visitor $customerVisitor,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Codazon\FurnitureLayout\Helper\Category\Compare $compareCategory,
        array $data = []
    ) {
        $this->urlEncoder = $urlEncoder;
        $this->_itemCollectionFactory = $itemCollectionFactory;
        $this->_customerVisitor = $customerVisitor;
        $this->httpContext = $httpContext;
        $this->currentCustomer = $currentCustomer;
        $this->_compareCategory = $compareCategory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct(
            $context,
            $data
        );
    }

    /* protected function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(
            __('Products Comparison List') . ' - ' . $this->pageConfig->getTitle()->getDefault()
        );
        return parent::_prepareLayout();
    } */

    /**
     * Retrieve Product Compare items collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\Collection
     */
    public function getItems($sortBy = 'catalog_compare_category_id', $sortDirection = 'asc')
    {
        if ($this->_items === null) {
            $this->_compareCategory->setAllowUsedFlat(false);

            $this->_items = $this->_itemCollectionFactory->create();
            $this->_items->useCategoryItem(true)->setStoreId($this->_storeManager->getStore()->getId());

            if ($this->httpContext->getValue(Context::CONTEXT_AUTH)) {
                $this->_items->setCustomerId($this->currentCustomer->getCustomerId());
            } elseif ($this->_customerId) {
                $this->_items->setCustomerId($this->_customerId);
            } else {
                $this->_items->setVisitorId($this->_customerVisitor->getId());
            }

            $this->_items->addAttributeToSelect(
                $this->_getComparableAttributes()
            )->loadComparableAttributes();
            $this->_items->setOrder($sortBy, $sortDirection);
        }

        return $this->_items;
    }
    
    public function getItemsArray()
    {
        $result = [];
        $items = $this->getItems();
        $imageHelper = $this->objectManager->get(\Magento\Catalog\Helper\Image::class);
        $width = (float)$this->_scopeConfig->getValue('codazon_furniture_layout/category_image/small_image_width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $height = (float)$this->_scopeConfig->getValue('codazon_furniture_layout/category_image/small_image_height', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        foreach ($items as $item) {
            $result[] = [
                'id'                => (int)$item->getData('catalog_compare_category_id'),
                'category'          => (int)$item->getId(),
                'cat_thumbnail'     => $imageHelper->init($item, 'category_page_grid')->setImageFile($item->getData('cat_thumbnail'))->resize($width, $height)->getUrl(),
                'name'              => $item->getName(),
                'url'               => $item->getUrl(),
            ];
        }
        return $result;
    }
    
    public function getMaxItems() {
        return (int)$this->_scopeConfig->getValue('codazon_furniture_layout/category_comparison/max_items', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getItemsWithComparableAttributesArray()
    {
        $result = [];
        $items = [];
        $itemObjects = $this->getItems();
        $imageHelper = $this->objectManager->get(\Magento\Catalog\Helper\Image::class);
        $width = (float)$this->_scopeConfig->getValue('codazon_furniture_layout/category_image/small_image_width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $height = (float)$this->_scopeConfig->getValue('codazon_furniture_layout/category_image/small_image_height', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        $imageRatio = $height/$width;
        $height = 170 * $imageRatio;
        $width = 170;
        
        $attributeObjects = $this->getAttributes();
        $attributes = [];
        foreach ($attributeObjects as $attribute) {
            $attributes[] = [
                'code'      => $attribute->getAttributeCode(),
                'label'     => $attribute->getStoreLabel(),
            ];
        }
        
        foreach ($itemObjects as $item) {
            $itemData = [
                'id'                => (int)$item->getData('catalog_compare_category_id'),
                'category'          => (int)$item->getId(),
                'cat_thumbnail'     => $imageHelper->init($item, 'category_page_grid')->setImageFile($item->getData('cat_thumbnail'))->resize($width, $height)->getUrl(),
                'name'              => $item->getName(),
                'url'               => $item->getUrl(),
            ];
            
            foreach ($attributeObjects as $attribute) {
                $itemData[$attribute->getAttributeCode()] = $this->getCategoryAttributeValue($item, $attribute);
            }
            $items[] = $itemData;
        }
        $result['items'] = $items;
        $result['comparableAttributes'] = $attributes;
        $result['imageRatio'] = 100 * $imageRatio;
        return $result;
    }
    
    
    protected function _getComparableAttributes()
    {
        if ($this->categoryAttributes === null) {
            $this->categoryAttributes = [];
            $categoryEntity = $this->objectManager->get(\Magento\Eav\Model\Entity\Type::class)->loadByCode(
                \Magento\Catalog\Model\Category::ENTITY
            );
            $collection = $categoryEntity->getAttributeCollection()
                ->addFieldToFilter('is_user_defined', 1)
                ->addFieldToFilter('is_comparable', 1);
            if ($collection->getSize()) {
                foreach ($collection as $attribute) {
                    $this->categoryAttributes[] = $attribute->getAttributeCode();
                }
            }
            $this->categoryAttributes = array_merge($this->categoryAttributes, ['name', 'cat_thumbnail']);
        }
        return $this->categoryAttributes;
    }
    
    /**
     * Retrieve Product Compare Attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        if ($this->_attributes === null) {
            $this->_attributes = $this->getItems()->getComparableAttributes();
        }

        return $this->_attributes;
    }

    /**
     * Retrieve Product Attribute Value
     *
     * @param Product $product
     * @param \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute
     * @return \Magento\Framework\Phrase|string
     */
    public function getCategoryAttributeValue($category, $attribute)
    {
        if (!$category->hasData($attribute->getAttributeCode())) {
            return __('N/A');
        }
        $priceHelper = $this->objectManager->get('Magento\Framework\Pricing\Helper\Data');
        
        if ($attribute->getSourceModel() || in_array(
            $attribute->getFrontendInput(),
            ['select', 'boolean', 'multiselect']
        )
        ) {
            $value = $attribute->getFrontend()->getValue($category);
        } else {
            if ($attribute->getFrontendInput() == 'price') {
                $value = $priceHelper->currency($category->getData($attribute->getAttributeCode()), true, false);
            } else {
                $value = $category->getData($attribute->getAttributeCode());
            }
        }
        return (string)$value == '' ? __('No') : $value;
    }

    /**
     * Check if any of the products has a value set for the attribute
     *
     * @param \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute
     * @return bool
     * @since 102.0.6
     */
    public function hasAttributeValueForCategory($attribute)
    {
        foreach ($this->getItems() as $item) {
            if ($item->hasData($attribute->getAttributeCode())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieve Print URL
     *
     * @return string
     */
    public function getPrintUrl()
    {
        return $this->getUrl('*/*/*', ['_current' => true, 'print' => 1]);
    }

    /**
     * Setter for customer id
     *
     * @param int $id
     * @return \Magento\Catalog\Block\Product\Compare\ListCompare
     */
    public function setCustomerId($id)
    {
        $this->_customerId = $id;
        return $this;
    }
    
    public function getWidgetConfig()
    {
        $width = (float)$this->_scopeConfig->getValue('codazon_furniture_layout/category_image/small_image_width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $height = (float)$this->_scopeConfig->getValue('codazon_furniture_layout/category_image/small_image_height', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return [
            'Codazon_FurnitureLayout/js/category-comparison' => [
                'addToCompareUrl'               => $this->getUrl('furniturelayout/category_compare/add'),
                'removeFromCompareUrl'          => $this->getUrl('furniturelayout/category_compare/remove'),
                'comparisonListUrl'             => $this->getUrl('furniturelayout/category_compare/ajaxlist'),
                'clearCompareUrl'               => $this->getUrl('furniturelayout/category_compare/clear'),
                'exportPDFUrl'                  => $this->getUrl('furniturelayout/category_compare/printpdf'),
                'loadItemsUrl'                  => $this->getUrl('furniturelayout/category_compare/items'),
                'itemsTemplate'                 => '#cat-compare-item-tmpl',
                'compTableTemplate'             => '#cat-comparison-table-tmpl',
                'overMsgTemplate'               => '#cat-comparison-max-items-over-tmpl',
                'pDFFileName'                   => 'comparison-table.pdf',
                'imageSize'                     => ['width' => $width, 'height' => $height],
                'maxItems'                      => $this->getMaxItems(),
                'overMsg'                       => __('You have added maximum number of items to compare.'),
            ]
        ];
    }
}
