<?php

/**
 * Copyright © 2017 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */
 
namespace Codazon\FurnitureLayout\Block\Category;

use Magento\Catalog\Model\Layer\Resolver;

class Gallery extends \Magento\Framework\View\Element\Template
{
    protected $helper;
    
    protected $context;
    
    protected $gallery;
    
    protected $imageHelper;
    
    protected $mediaConfig;
    
    protected $coreRegistry;
    
    protected $tabIndexes = [
		['label' => 'Highlights',    'id' => 3, 'can_print' => false],
        ['label' => 'Specs',        'id' => 1, 'can_print' => true],
        ['label' => 'About',        'id' => 2, 'can_print' => false],        
        ['label' => 'Assembly',     'id' => 4, 'can_print' => false]
    ];
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Catalog\Helper\Image $imageHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $helper->getCoreRegistry();
        $this->helper = $helper;
        $this->context = $context;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->mediaConfig = $mediaConfig;
        $this->imageHelper = $imageHelper;
    }
    
    public function getHelper()
    {
        return $this->helper;
    }
    
    public function getImageRatio()
    {
        if (!$this->hasData('image_ratio')) {
            $width = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/base_image_width');
            $height = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/base_image_height');
            if ($width) {
                $this->setData('image_ratio', $height/$width);
            }
            $this->setData('image_ratio', 1);
        }
        return $this->getData('image_ratio');
    }
    
    public function getGalleryImages()
    {
        if (!$this->hasData('gallery_images')) {
            $category = $this->helper->getCurrentCategory();
            $images = [];
            $this->imageHelper->constrainOnly(false);
            if ($galleryImages = $category->getData('f_category_gallery')) {
                $galleryImages = json_decode($galleryImages, true);
                if (!empty($galleryImages['images'])) {
                    $width = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/base_image_width');
                    $height = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/base_image_height');
                    $smallWidth = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/small_image_width');
                    $smallHeight = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/small_image_height');
                    
                    $panorama = $category->getData('panorama');
                    
                    foreach ($galleryImages['images'] as $image) {
                        $images[] = [
                            'base'      => $this->mediaConfig->getBaseMediaUrl() . $image['file'],
                            'large'     => $this->imageHelper
                                ->init(null, 'category_page_grid')
                                ->setImageFile($image['file'])->resize($width, $height)->getUrl(),
                            'small'     => $this->imageHelper
                                ->init(null, 'category_page_grid')
                                ->setImageFile($image['file'])->resize($smallWidth, $smallHeight)->getUrl(),
                            'is_panorama' => ($panorama == $image['file'])
                        ];
                    }
                }
            }
            $this->setData('gallery_images', $images);
        }
        return $this->getData('gallery_images');
    }
    
    public function getTabsHtml()
    {
        return $this->setTemplate('Codazon_FurnitureLayout::category/tabs.phtml')->toHtml();
    }
    
    public function getTabItems() {
        if (!$this->hasData('tab_items')) {
            $category = $this->helper->getCurrentCategory();
            $tabs = [];
            foreach ($this->tabIndexes as $tabIndex) {
                if ($content = $category->getData('f_category_tab_' . $tabIndex['id'])) {
                    $tabs[] = [
                        'label'     => __($tabIndex['label']),
                        'content'   => $this->helper->htmlFilter($content),
                        'can_print' => $tabIndex['can_print']
                    ];
                }
            }
            $this->setData('tab_items', $tabs);
        }
        return $this->getData('tab_items');
    }
    
    public function getIdentities()
    {
        return $this->helper->getCurrentCategory()->getIdentities();
    }
    
    public function getFlatPanelConfig()
    {
        if (!$this->hasData('flat_panel_config')) {
            $this->setData('flat_panel_config', $this->_getFlatPanelConfig());
        }
        return $this->getData('flat_panel_config');
    }
    
    protected function _getFlatPanelConfig()
    {
        $flatPanelConfig = [];
        $storeId = $this->helper->getStoreManager()->getStore()->getId();
        $flatPanelCollection = $this->objectManager->get(\Codazon\FlatPanel\Model\ResourceModel\FlatPanel\CollectionFactory::class)->create();
        $styleCreator = $this->objectManager->get(\Codazon\FlatPanel\Model\ResourceModel\FlatPanelStyle\CollectionFactory::class);
        $displayIds = $this->helper->getConfig('codazon_furniture_layout/flat_panel_gallery/display_flat_panels');
        
        if ($displayIds) {
            $flatPanelCollection->setStoreId($storeId)
                ->addFieldToSelect(['entity_id'])
                ->addAttributeToFilter('is_active', 1)
                ->addAttributeToSelect(['name'])
                ->addFieldToFilter('entity_id', ['in' => explode(',', $displayIds)])
                ->setOrder('sort_order', 'ASC')
                ->setOrder('entity_id', 'ASC');
            foreach ($flatPanelCollection->getItems() as $flatPanel) {
                $fpId = $flatPanel->getId();
                $config = [];
                $styleCollection = $styleCreator->create();
                $styleCollection->setStoreId($storeId)
                    ->addFieldToSelect(['entity_id'])
                    ->addAttributeToFilter('is_active', 1)
                    ->addAttributeToSelect(['name'])
                    ->addFieldToFilter('flat_panel', $fpId)
                    ->setOrder('sort_order', 'ASC')
                    ->setOrder('entity_id', 'ASC');
                $config['name'] = str_replace(["'",'"'],["&#39;", "&#34;"], $flatPanel->getData('name'));
                $config['styles'] = $this->_getConfigByFlatePanelStyleCollection($styleCollection);
                $flatPanelConfig[] = $config;
            }
        }
        return $flatPanelConfig;
    }
    
    public function getCatalogProductMediaUrl()
    {
        return $this->mediaConfig->getBaseMediaUrl();
    }
    
    protected function _getConfigByFlatePanelStyleCollection($collection)
    {
        $styleConfig = [];
        $width = 100;
        //$height = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/base_image_height');
        $height = null;
        foreach ($collection as $item) {
            $config = [];
            $config['name'] = str_replace(["'",'"'],["&#39;", "&#34;"], $item->getData('name'));
            if ($materialImage = $item->getData('material_image')) {
                $config['material_image'] = [
                    'base' => $materialImage
                ];
            }
            if ($item->getData('kitchen_image')) {
                $kitchenImage = json_decode($item->getData('kitchen_image'), true);
                $kitchenImage = $kitchenImage['images'];
                $config['kitchen_image'] = [];
                foreach ($kitchenImage as $image) {
                    $config['kitchen_image'][] = [
                        'base'   => $image['file'],
                        'small' => $this->imageHelper
                                ->init(null, 'category_page_grid')
                                ->keepFrame(false)
                                ->setImageFile($image['file'])->resize($width)->getUrl()
                    ];
                }
            }
            $styleConfig[] = $config;
        }
        return $styleConfig;
    }
}