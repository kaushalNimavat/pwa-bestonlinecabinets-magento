<?php
/**
 * Copyright © 2019 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Block\Category;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Codazon\FurnitureLayout\Block\Category\LandingPage\Toolbar;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Codazon\FurnitureLayout\Model\LandingPage\Toolbar as ToolbarModel;
use Codazon\FurnitureLayout\Helper\Category\CategoryList;

class LandingPage extends Template implements \Magento\Widget\Block\BlockInterface
{
    
    protected $defaultToolbarBlock = Toolbar::class;
    
    protected $toolbarBlockName = 'landing_page_toolbar';
    
	protected $_template = 'Codazon_FurnitureLayout::landing-page/container.phtml';
    
    protected $categoryCollectionFactory;
    
    protected $categoryRepository;
    
    protected $collectionFactory;
    
    protected $objectManager;
    
    protected $helper;
    
    protected $storeManager;
    
    protected $currentStoreId;
    
    protected $coreRegistry;
    
    protected $storeId;
    
    protected $repareToolbar;
    
    public function __construct(
		Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        CategoryRepositoryInterface $categoryRepository,
        array $data = []
	){
        parent::__construct($context, $data);
        $this->categoryRepository = $categoryRepository;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->helper = $helper;
        $this->storeManager = $helper->getStoreManager();
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->coreRegistry = $this->helper->getCoreRegistry();
        $this->storeId = $this->storeManager->getStore()->getId();
    }
    
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->coreRegistry->registry('landing_page_config')) {
            $currentSaleConfig = $this->addChild('landing_page_config', 'Magento\Framework\View\Element\Template');
            $currentSaleConfig->setTemplate('Codazon_FurnitureLayout::landing-page/config.phtml');
            
            //$this->addChild($this->toolbarBlockName, Toolbar::class);
            $toolbar = $this->getLayout()->createBlock(Toolbar::class, $this->toolbarBlockName);
            $toolbar->addChild('category_list_toolbar_pager', \Magento\Theme\Block\Html\Pager::class);
            $toolbar->addChild('category_list_toolbar_filter', \Codazon\FurnitureLayout\Block\Category\LandingPage\Filter::class);
            
            $this->helper->getCoreRegistry()->register('landing_page_config', $currentSaleConfig);
        }       
    }
   
    public function getToolbarHtml($enableFilter = true)
    {
        $this->getChildBlock($this->toolbarBlockName)->setEnableFilter($enableFilter);
        return $this->getChildHtml($this->toolbarBlockName);
    }
    
    public function getToolbarBlock()
    {
        $block = $this->getToolbarFromLayout();

        if (!$block) {
            $block = $this->getLayout()->createBlock($this->defaultToolbarBlock, uniqid(microtime()));
        }

        return $block;
    }
    
    protected function getToolbarFromLayout()
    {
        $blockName = $this->toolbarBlockName;//$this->getToolbarBlockName();

        $toolbarLayout = false;

        if ($blockName) {
            $toolbarLayout = $this->getLayout()->getBlock($blockName);
        }

        return $toolbarLayout;
    }
    
    protected function addToolbarBlock($collection)
    {
        $toolbarLayout = $this->getToolbarFromLayout();

        //if ($toolbarLayout) {
            $this->configureToolbar($toolbarLayout, $collection);
        //}
    }
    
    protected function configureToolbar(Toolbar $toolbar, Collection $collection)
    {
        // use sortable parameters
        $orders = $this->getAvailableOrders();
        if ($orders) {
            $toolbar->setAvailableOrders($orders);
        }
        $sort = $this->getSortBy();
        if ($sort) {
            $toolbar->setDefaultOrder($sort);
        }
        $dir = $this->getDefaultDirection();
        if ($dir) {
            $toolbar->setDefaultDirection($dir);
        }
        $modes = $this->getModes();
        if ($modes) {
            $toolbar->setModes($modes);
        }
        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);
        $this->setChild($this->toolbarBlockName, $toolbar);
    }

    
    protected function _beforeToHtml()
    {
        if ($this->repareToolbar === null) {
            $collection = $this->getCategoryCollection();
            $this->addToolbarBlock($collection);
            $this->repareToolbar = true;
        }
        return parent::_beforeToHtml();
    }
        
    public function getCategoryCollection()
    {
        if (!$this->hasData('children_category')) {
            $parent = $this->getCurrentCategory();
            $collection = $this->categoryCollectionFactory->create();
            $collection->addFieldToFilter('parent_id', $parent->getId())
                ->setStoreId($this->storeId)
                ->addAttributeToFilter('is_active', 1)
                ->addAttributeToSelect('*');
            $this->setData('children_category', $collection);
        }
        return $this->getData('children_category');
    }
    
    public function getCurrentCategory()
    {
        if (!$this->hasData('current_category')) {
            if ($categoryId = $this->getData('parent_id')) {
                $categoryId = (int)str_replace("category/", "", $categoryId);
                $category = $this->categoryRepository->get($categoryId, $this->storeId);
            } else {
                $category = $this->coreRegistry->registry('current_category') ? : new \Magento\Framework\DataObject();
            }
            $this->setData('current_category', $category);
        }
        return $this->getData('current_category');
    }
    
    public function getIdentities()
    {
        return $this->getCurrentCategory()->getIdentities();
    }
    
    public function getContentHtml()
    {
        return $this->setTemplate('Codazon_FurnitureLayout::landing-page/view.phtml')->toHtml();
    }
    
    protected function _getCategoryIds()
    {
        return $this->getData('category_ids');
    }
    
    public function getAjaxUrl()
    {
        return $this->getUrl('furniturelayout/category/landingajax', [
            '_query'        => $this->getRequest()->getQueryValue(),
        ]);
    }
    
    public function getAjaxParams()
    {
        return [
            'ajax_nav'      => 1,
            'parent_id'     => $this->getCurrentCategory()->getId()
        ];
    }
    
    public function getConfigParams()
    {
        return [
            'Codazon_FurnitureLayout/js/landing-page' => [
                'ajaxUrl'           => $this->getAjaxUrl(),
                'ajaxParams'        => $this->getAjaxParams(),
                'orderParam'        => ToolbarModel::ORDER_PARAM_NAME,
                'directionParam'    => ToolbarModel::DIRECTION_PARAM_NAME,
                'directionDefault'  => CategoryList::DEFAULT_SORT_DIRECTION,
                'currentUrl'        => $this->getUrl('', [
                    '_current'      => true,
                    '_use_rewrite'  => true,
                ])
            ]
        ];
    }
    
    public function getCategoryNotification()
    {
        return $this->getCurrentCategory()->getData('f_cat_notif') ? : $this->helper->getConfig('codazon_furniture_layout/general/cat_notif');
    }
    
    public function getSampleItemsLink()
    {
        return $this->getCurrentCategory()->getData('f_cat_sample_link') ? : $this->helper->getConfig('codazon_furniture_layout/general/cat_sample_link');
    }
    public function getSampleItemsLabel()
    {
        return $this->getCurrentCategory()->getData('f_cat_sample_label') ? : $this->helper->getConfig('codazon_furniture_layout/general/cat_sample_label');
    }
}