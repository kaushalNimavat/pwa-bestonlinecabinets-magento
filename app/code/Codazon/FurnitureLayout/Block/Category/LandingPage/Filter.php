<?php
/**
 * Copyright © 2019 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Block\Category\LandingPage;

use Magento\Eav\Model\Entity\Type as EntityType;

use Magento\Catalog\Model\Category;

use Codazon\FurnitureLayout\Model\LandingPage\Toolbar as ToolbarModel;

class Filter extends \Magento\Framework\View\Element\Template
{
    protected $helper;
    
    protected $categoryEntity;
    
    protected $categoryAttributes;
    
    protected $filterableAttributes;
    
    protected $usedForSortByAttributes;
     
    protected $_template = 'Codazon_FurnitureLayout::landing-page/filter.phtml';  
   
    protected $attributeUsedForSortByArray;
    
    protected $_collection;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        EntityType $entityType,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->categoryEntity = $entityType->loadByCode(
            \Magento\Catalog\Model\Category::ENTITY
        );
        $this->helper = $helper;
        $this->categoryAttribute = $this->categoryEntity->getAttributeCollection()->addFieldToFilter('is_user_defined', 1);
    }
    
    protected function getCategoryAttributes()
    {
        return $this->categoryAttributes;
    }
    
    public function setCollection($collection)
    {
        $this->_collection = $collection;
        $this->filterByAttributes($this->_collection);
        //$this->sortByAttribute($this->_collection);
        return $this;
    }
    
    protected function filterByAttributes($collection)
    {
        $attributes = $this->getFilterableAttributes();
        $request = $this->getRequest();
        foreach ($attributes as $attribute) {
            $code = $attribute->getAttributeCode();
            if ($value = $request->getParam($code)) {
                $value = explode(',', trim($value));
                $collection->addAttributeToFilter($code, ['in' => $value]);
            }
        }
        return $this;
    }

    protected function sortByAttribute($collection)
    {
        if ($order = $this->getRequest()->getParam(ToolbarModel::ORDER_PARAM_NAME)) {
            
        }
        return $this;
    }

    public function getCollection()
    {
        return $this->_collection;
    }
    
    public function getFilterableAttributes()
    {
        if ($this->filterableAttributes === null) {
            $this->filterableAttributes = (clone $this->categoryAttribute)->addFieldToFilter('is_filterable', 1);
        }
        return $this->filterableAttributes;
    }
    
    public function getUsedForSortByAttributes()
    {
        if ($this->usedForSortByAttributes === null) {
            $this->usedForSortByAttributes = (clone $this->categoryAttribute)->addFieldToFilter('used_for_sort_by', 1);
        }
        return $this->usedForSortByAttributes;
    }
    
    public function getAttributeUsedForSortByArray()
    {
        if ($this->attributeUsedForSortByArray === null) {
            $this->attributeUsedForSortByArray = [];
            $attributes = $this->getUsedForSortByAttributes();
            foreach ($attributes as $attribute) {
                $this->attributeUsedForSortByArray[$attribute->getAttributeCode()] = $attribute->getStoreLabel();
            }
        }
        return $this->attributeUsedForSortByArray;
    }
    
    public function isActiveFilter($code, $value)
    {
        if ($values = $this->getRequest()->getParam($code)) {
            $values = explode(',', trim($values));
            return in_array($value, $values);
        }
        return false;
    }
    
    public function isActiveOrder($code, $value)
    {
        $request = $this->getRequest();
        if ($sortby = $request->getParam(ToolbarModel::ORDER_PARAM_NAME)) {
            $cond1 = ($code == $sortby);
            $order = $request->getParam(ToolbarModel::DIRECTION_PARAM_NAME) ? : 'asc';
            $cond2 = ($value == $order);
            return $cond1 && $cond2;
        }
        return false;
    }
    
    public function getReplaceTextArray()
    {
        $replaceTextConfig = $this->helper->getConfig('codazon_furniture_layout/category_landing_page/sort_dir_rpl_text');
        $replaceTextArray = [];
        foreach (explode("\n", $replaceTextConfig) as $line) {
            if ($line) {
                $data = explode("|", $line);
                $code = $data[0];
                $replaceTextArray[$code] = [
                    'asc'       => $data[1],
                    'desc'      => $data[2],
                ];
            }
        }
        return $replaceTextArray;
    }
}