<?php

/**
 * Copyright © 2017 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */
 
namespace Codazon\FurnitureLayout\Block\Category;

use Magento\Catalog\Model\Layer\Resolver;

class FurnitureLayout extends \Magento\Catalog\Block\Product\AbstractProduct
{
    protected $helper;
    
    protected $context;
    
    protected $coreRegistry;
    
    protected $catalogLayer;
    
    protected $displaySubCategories;
    
    protected $requestMainSubCat;
    
    const CAT_PARAM = 'cattab';
    
    protected $productCollectionFactory;
    
    protected $catalogProductVisibility;
    
    protected $freshCollection;
    
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->context = $context;
        $this->coreRegistry = $registry;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }
    
    public function getFreshProductCollection()
    {
        if ($this->freshCollection === null) {
            $this->freshCollection = $this->productCollectionFactory->create();
            $this->freshCollection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
            $this->freshCollection = $this->_addProductAttributesAndPrices($this->freshCollection)->addStoreFilter();
        }
        return (clone $this->freshCollection);
    }
    
    public function getCatQueryString()
    {
        return self::CAT_PARAM;
    }
    
    
    public function getSubCategories()
    {
        if (!$this->hasData('sub_categories')) {
            $currentCat = $this->getCurrentCategory();
            $this->setData('sub_categories', $this->getChildrenByCategoryObject($currentCat));
        }
        return $this->getData('sub_categories');
    }
    
    public function getChildrenByCategoryObject($currentCat)
    {
        return $currentCat->getCollection()
                ->setStoreId($currentCat->getStoreId())
                ->addAttributeToSelect(['name', 'url_key'])
                ->addAttributeToSort('position', 'ASC')
                ->addAttributeToSort('updated_at', 'DESC')
                ->addAttributeToFilter('is_active', 1)
                ->addAttributeToFilter('parent_id', $currentCat->getId());
    }
    
    public function getCurrentCategory()
    {
        if (!$this->hasData('current_category')) {
            $this->setData('current_category', $this->coreRegistry->registry('current_category'));
        }
        return $this->getData('current_category');
    }
    
    public function getSubCategoriesData()
    {
        if (!$this->hasData('sub_categories_data')) {
            $subcategories = $this->getSubCategories();
            $rows = [];
            if (count($subcategories)) {
                foreach ($subcategories as $subcategory) {
                    $row = [
                        'id'    => $subcategory->getId(),
                        'url'   => $subcategory->getUrl(),
                        'name'  => $subcategory->getName(),
                    ];
                    if($subcategory->hasChildren()) {
                        $subSubCats = $this->getChildrenByCategoryObject($subcategory);
                        $row['children'] = [];
                        foreach ($subSubCats as $subSubCat)
                        $row['children'][] = [
                            'id'    => $subSubCat->getId(),
                            'url'   => $subSubCat->getUrl(),
                            'name'  => $subSubCat->getName(),
                        ];
                    }
                    $rows[] = $row;
                }
            }
            $this->setData('sub_categories_data', $rows);
        }
        return $this->getData('sub_categories_data');
    }
    
    public function getCurrentSubCat($id = null)
    {
        $subCats = $this->getSubCategories();
        $subcat = null;
        if (is_array($subCats)) {
            if (!$id) {
                foreach ($subCats as $item) {
                    $subcat = $item;
                    break;
                }
            } elseif (!empty($subCats[$id])) {
                $subcat = $subCats[$id];
            }
        } else {
            if (!$id) {
                $subcat = $this->getSubCategories()->getFirstItem();
            } else {
                $subcat = $this->getSubCategories()->getItemById($id);
            }
        }
        return $subcat;
    }
    
    public function prepareCollection()
    {
        
    }
    
    
    public function getDisplaySubCategories()
    {
        if ($this->displaySubCategories === null) {
            $currentSubCat = $this->getCurrentSubCat($this->getCurrentRequestMainSubCatId());
            $this->displaySubCategories = $currentSubCat->getChildrenCategories();
            if (!$this->displaySubCategories) {
                $this->displaySubCategories = [];
            }
        }
        return $this->displaySubCategories;
    }
    
    public function getCurrentRequestMainSubCatId()
    {
        return $this->getRequest()->getParam(self::CAT_PARAM) ? : null;
    }
    
    public function getCurrentRequestMainSubCat()
    {
        if ($this->requestMainSubCat === null) {
            $this->requestMainSubCat = $this->getCurrentSubCat($this->getCurrentRequestMainSubCatId());
        }
        return $this->requestMainSubCat;
    }
    

    public function getProductsByCategory($category, $limit = 15)
    {
        if ($category) {
            $collection = $this->getFreshProductCollection()
                ->addCategoryFilter($category)
                ->setOrder('position', 'asc')              
                ->load();
            return $collection;
        }
        return null;
    }
    
    public function getNotificationHtml()
    {
        return $this->setTemplate('Codazon_FurnitureLayout::category/notification.phtml')->toHtml();
    }
    
    public function enableSpecialNotification()
    {
        return $this->getCurrentCategory()->getData('f_cat_notif_enable');
    }
    
    public function getCategoryNotification()
    {
        return $this->getCurrentCategory()->getData('f_cat_notif') ? : $this->helper->getConfig('codazon_furniture_layout/general/cat_notif');
    }
    
    public function getSampleItemsLink()
    {
        return $this->getCurrentCategory()->getData('f_cat_sample_link') ? : $this->helper->getConfig('codazon_furniture_layout/general/cat_sample_link');
    }
    public function getSampleItemsLabel()
    {
        return $this->getCurrentCategory()->getData('f_cat_sample_label') ? : $this->helper->getConfig('codazon_furniture_layout/general/cat_sample_label');
    }
}