<?php
/**
 * Copyright © 2019 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Block\Widget;

use Magento\Framework\View\Element\Template;
use Codazon\FurnitureLayout\Model\CurrentsaleFactory as BrandFactory;

class AssemblyInstruction extends Template implements \Magento\Widget\Block\BlockInterface
{
	protected $_template = 'Codazon_FurnitureLayout::assembly-instruction/view.phtml';
    
    protected $flatPanelCollectionFactory;
    
    protected $styleCreator;
    
    protected $objectManager;
    
    protected $helper;
    
    protected $storeManager;
    
    protected $currentStoreId;
    
    protected $mediaConfig;
    
    protected $fileUrl;
    
    public function __construct(
		Template\Context $context,
		\Codazon\FlatPanel\Model\ResourceModel\FlatPanel\CollectionFactory $flatPanelCollectionFactory,
        \Codazon\FlatPanel\Model\ResourceModel\FlatPanelStyle\CollectionFactory $styleCreator,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        array $data = []
	){
        parent::__construct($context, $data);
        $this->flatPanelCollectionFactory = $flatPanelCollectionFactory;
        $this->styleCreator = $styleCreator;
        $this->helper = $helper;
        $this->storeManager = $helper->getStoreManager();
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->mediaConfig = $mediaConfig;
        $this->fileUrl = str_replace('catalog/product', \Codazon\FlatPanel\Model\AssemblyInstruction::BASE_FILE_PATH, $this->mediaConfig->getBaseMediaUrl());
    }
    
    public function getCatalogProductMediaUrl()
    {
        return $this->mediaConfig->getBaseMediaUrl();
    }
    
    public function getFileUrl()
    {
        return $this->fileUrl;
    }
    
    public function getCollectionsData()
    {
        if (!$this->hasData('collections_data')) {
            $this->setData('collections_data', $this->_getCollectionsData());
        }
        return $this->getData('collections_data');
    }
    
    protected function _getCollectionsData()
    {
        $flatPanelConfig = [];
        if ($flpColIds = $this->getData('collection_ids')) {
            $flpColIds = explode(',', $flpColIds);
            $storeId = $this->helper->getStoreManager()->getStore()->getId();
            $flatPanelCollection = $this->flatPanelCollectionFactory->create();
            $flatPanelCollection->setStoreId($storeId)
                ->addFieldToSelect(['entity_id'])
                ->addFieldToFilter('entity_id', ['in' => $flpColIds])
                ->addAttributeToFilter('is_active', 1)
                ->addAttributeToSelect(['name'])
                ->setOrder('sort_order', 'ASC')
                ->setOrder('entity_id', 'ASC');
            foreach ($flatPanelCollection->getItems() as $flatPanel) {
                $fpId = $flatPanel->getId();
                $config = [];
                $styleCollection = $this->styleCreator->create();
                $styleCollection->setStoreId($storeId)
                    ->addFieldToSelect(['entity_id'])
                    ->addAttributeToFilter('is_active', 1)
                    ->addAttributeToSelect(['name'])
                    ->addFieldToFilter('flat_panel', $fpId)
                    ->setOrder('sort_order', 'ASC');
                $config['name'] = str_replace(["'",'"'], ["&#39;", "&#34;"], $flatPanel->getData('name'));
                $config['styles'] = $this->_getConfigByFlatePanelStyleCollection($styleCollection);
                $flatPanelConfig[] = $config;
            }
        }
        return $flatPanelConfig;
    }
    
    protected function _getConfigByFlatePanelStyleCollection($collection)
    {
        $styleConfig = [];
        foreach ($collection as $item) {
            $config = [];
            $config['name'] = str_replace(["'",'"'],["&#39;", "&#34;"], $item->getData('name'));
            $config['id'] = $item->getId();
            if ($materialImage = $item->getData('material_image')) {
                $config['material_image'] = [
                    'base' => $materialImage
                ];
            }
            $styleConfig[] = $config;
        }
        return $styleConfig;
    }
    
    public function getAjaxInstructionUrl()
    {
        return $this->getUrl('furniturelayout/flatpanel/instruction');
    }
}