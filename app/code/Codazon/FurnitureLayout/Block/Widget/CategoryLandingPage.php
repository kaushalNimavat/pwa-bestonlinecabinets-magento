<?php
namespace Codazon\FurnitureLayout\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Codazon\FurnitureLayout\Block\Category\LandingPage\Toolbar;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Codazon\FurnitureLayout\Model\LandingPage\Toolbar as ToolbarModel;
use Codazon\FurnitureLayout\Helper\Category\CategoryList;

class CategoryLandingPage extends \Codazon\FurnitureLayout\Block\Category\LandingPage implements \Magento\Widget\Block\BlockInterface
{
    protected $_template = 'Codazon_FurnitureLayout::widget/category-landing-page.phtml';
    
    protected $categoryCollectionFactory;
    
    protected $categoryRepository;
        
    protected $objectManager;
    
    protected $helper;
    
    protected $storeManager;
    
    protected $storeId;
    
    protected $toolbarBlockName = 'landing_page_toolbar';
    
    protected $coreRegistry;
    
    protected $repareToolbar;
    
    public function __construct(
		Template\Context $context,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        array $data = []
	){
        parent::__construct($context, $categoryCollectionFactory, $helper, $categoryRepository, $data);
    }
    
    protected function _prepareLayout()
    {
        if (!$this->coreRegistry->registry('landing_page_config')) {
            $this->getData('parent_id') ? : $this->setData('parent_id', $this->_storeManager->getStore()->getRootCategoryId());
            $currentSaleConfig = $this->addChild('landing_page_config', 'Magento\Framework\View\Element\Template');
            $currentSaleConfig->setTemplate('Codazon_FurnitureLayout::landing-page/config.phtml');
            $toolbar = $this->getLayout()->createBlock(Toolbar::class, $this->toolbarBlockName);
            $toolbar->addChild('category_list_toolbar_pager', \Magento\Theme\Block\Html\Pager::class);
            $toolbar->addChild('category_list_toolbar_filter', \Codazon\FurnitureLayout\Block\Category\LandingPage\Filter::class);
            
            $compareBlock = $this->addChild('category_comparison', \Codazon\FurnitureLayout\Block\Category\Compare\ListCompare::class);
            $compareBlock->setTemplate('Codazon_FurnitureLayout::category/compare/footer-bar.phtml');
            $this->helper->getCoreRegistry()->register('landing_page_config', $currentSaleConfig);
        }
        //parent::_prepareLayout();
        return $this;
    }
    
    public function getCategoryCollection()
    {
        if (!$this->hasData('children_category')) {
            if ($this->getData('display_type') == '2') {
                $collection = $this->categoryCollectionFactory->create();
                $collection->addFieldToFilter('parent_id', (int)str_replace("category/", "", $this->getData('parent_id')))
                    ->setStoreId($this->storeId)
                    ->addAttributeToFilter('is_active', 1)
                    ->addAttributeToSelect('*');
                $this->setData('children_category', $collection);
            } else {
                $collection = $this->categoryCollectionFactory->create();
                $collection->addFieldToFilter('entity_id', ['in' => $this->_getCategoryIds()])
                    ->setStoreId($this->storeId)
                    ->addAttributeToFilter('is_active', 1)
                    ->addAttributeToSelect('*');
                $this->setData('children_category', $collection);
            }
        }
        return $this->getData('children_category');
    }
    
    public function getAjaxUrl()
    {
        return $this->getUrl('furniturelayout/category/widgetlandingajax', [
            '_query'        => $this->getRequest()->getQueryValue(),
        ]);
    }
    
    public function getAjaxParams()
    {
        return [
            'ajax_nav'      => 1,
            'display_type'  => $this->getData('display_type'),
            'parent_id'     => $this->getCurrentCategory()->getId(),
            'category_ids'  => $this->getData('category_ids'),
        ];
    }
}