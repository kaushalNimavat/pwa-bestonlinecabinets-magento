<?php
namespace Codazon\FurnitureLayout\Block\Widget;

use Magento\Framework\View\Element\Template;

class CurrentSale extends Template implements \Magento\Widget\Block\BlockInterface
{
	protected $_template = 'Codazon_FurnitureLayout::current-sale/view.phtml';
    
    protected $categoryCollectionFactory;
    
    protected $categoryRepository;
    
    protected $collectionFactory;
    
    protected $objectManager;
    
    protected $helper;
    
    protected $storeManager;
    
    protected $currentStoreId;
    
    public function __construct(
		Template\Context $context,
		\Codazon\FurnitureLayout\Model\ResourceModel\Currentsale\CollectionFactory $collectionFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        array $data = []
	){
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->categoryRepository = $categoryRepository;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->helper = $helper;
        $this->storeManager = $helper->getStoreManager();
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }
    
    public function getCurrentSaleModelById($id)
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('entity_id', $id)->addFieldToFilter('is_active', 1);
        return $collection->count() ? $collection->getFirstItem() : false;
    }
    
    public function getCurrenSale()
    {
        if (!$this->hasData('current_sale')) {
            $this->setData('current_sale', false);
            if ($id = $this->getData('item_id')) {
                $currentSale = $this->getCurrentSaleModelById($id);
                if ($currentSale) {
                    $data = $this->setData('current_sale', $currentSale);
                }
            }
        }
        return $this->getData('current_sale');
    }
    
    public function getCurrenSaleData()
    {
        if (!$this->hasData('current_sale_data')) {
            $groups = [];
            $categories = [];
            $usedIds = [];
            $currentSale = $this->getCurrenSale();
            $structure = $currentSale->getData('structure');
            
            if ($structure) {
                $structure = json_decode($structure, true);
                foreach ($structure['groups'] as $group) {
                    if (!empty($group['options']['categories'])) {
                        $subcats = explode(',', $group['options']['categories']);
                        $usedIds = array_merge($usedIds, $subcats);
                        $group['subcats'] = $subcats;
                        $groups[] = $group;
                    } else {
                        $groupId = $group['cat_id'];
                        if ($groupId) {
                            $usedIds = array_merge($usedIds, $group['subcats']);
                            $groups[] = $group;
                        }
                    }
                }
            }
            //$usedIds = array_unique($usedIds);
            $width = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/thumbnail_image_width');
            $height = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/thumbnail_image_height');
            if (count($usedIds)) {
                $collection = $this->categoryCollectionFactory->create();
                $collection->addFieldToFilter('entity_id', ['in' => $usedIds])
                    ->setStoreId($this->getCurrentStoreId())
                    ->addAttributeToFilter('is_active', 1)
                    ->addAttributeToSelect(['name', 'alias_name', 'spcatprice', 'cat_thumbnail', 'f_cat_sample_link', 'low_invetory', 'catgroup']);
                $imageHelper = $this->objectManager->get('\Magento\Catalog\Helper\Image');
                if ($collection->count()) {
                    foreach ($collection as $subcat) {
                        $subcatId = $subcat->getId();
                        $categories[$subcatId] = [
                            'id'        => $subcatId,
                            'name'      => str_replace("'", '&apos;', $subcat->getName()),
                            'url'       => $subcat->getUrl(),
                            'alias_name'=> $subcat->getData('alias_name'),
							'catgroup'  => $subcat->getData('catgroup'),
							'catprice'  => $subcat->getData('spcatprice'),
                            'f_cat_sample_link' => $subcat->getData('f_cat_sample_link'),
							'low_inventory' => $subcat->getData('low_invetory'),
                            'cat_thumbnail' => $imageHelper->init($subcat, 'category_page_grid')
                                ->setImageFile($subcat->getData('cat_thumbnail'))->resize($width, $height)->getUrl(),
                        ];
                    }
                }
            }
            $data = [
                'groups'                => $groups,
                'categories'            => $categories,
                'filter_options'        => $this->getFilterOptions(),
                'price_format'          => $this->helper->getPriceFormat(),
                'image_size'            => [
                    'width'             => $width,
                    'height'            => $height
                ]
            ];
            $this->setData('current_sale_data', $data);
        }
        return $this->getData('current_sale_data');
    }
    
    public function getFilterOptions()
    {
        return [
            ['value' => '1', 'label' => __('Ready To Assemble Finishes')],
            ['value' => '2', 'label' => __('Pre-Assembled Finishes')],
            ['value' => '3', 'label' => __('Clearance')]
        ];
    }
    
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->helper->getCoreRegistry()->registry('current_sale_config')) {
            $currentSaleConfig = $this->addChild('current_sale_config', 'Magento\Framework\View\Element\Template');
            $currentSaleConfig->setTemplate('Codazon_FurnitureLayout::current-sale/config.phtml')
                ->setSampleItemsLink($this->getSampleItemsLink());
            $this->helper->getCoreRegistry()->register('current_sale_config', $currentSaleConfig);
        }       
    }
    
    public function getCurrentStoreId()
    {
        if ($this->currentStoreId === null) {
            $this->currentStoreId = (int)$this->storeManager->getStore()->getId();
        }
        return $this->currentStoreId;
    }
    
    public function getSampleItemsLink()
    {
        return $this->helper->getConfig('codazon_furniture_layout/general/cat_sample_link');
    }
}