<?php

/**
 * Copyright © 2017 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */
 
namespace Codazon\FurnitureLayout\Block\Widget;

use Magento\Catalog\Model\Layer\Resolver;

class ImageGallery extends \Magento\Framework\View\Element\Template
{
    protected $helper;
    
    protected $context;
    
    protected $gallery;
    
    protected $imageHelper;
    
    protected $mediaConfig;
    
    protected $coreRegistry;
    
    protected $_template = 'Codazon_FurnitureLayout::widget/image-gallery.phtml';
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Catalog\Helper\Image $imageHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $helper->getCoreRegistry();
        $this->helper = $helper;
        $this->context = $context;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->mediaConfig = $mediaConfig;
        $this->imageHelper = $imageHelper;
    }
    
    public function getCurrentCategory()
    {
        if (!$this->hasData('current_category')) {
            $category = false;
            if ($id = $this->getData('category_id')) {
                $category = $this->objectManager->get(\Magento\Catalog\Api\CategoryRepositoryInterface::class)
                    ->get($id, $this->helper->getStoreManager()->getStore()->getId());
            }
            $this->setData('current_category', $category);
        }
        return $this->getData('current_category');
    }
    
    public function getGalleryImages()
    {
        if ($this->getData('use_custom_image')) {
            return $this->getCustomGalleryImages();
        } else {
            return $this->getCategoryGalleryImages();
        }
    }
    
    public function getMediaUrl($path = '')
    {
        return $this ->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA ) . $path;
    }
    
    public function getCustomGalleryImages()
    {
        if (!$this->hasData('custom_gallery_images')) {
            $images = [];
            if ($items = $this->getData('custom_items')) {
                $mediaUrl = $this->getMediaUrl();
                $imageHelper = $this->objectManager->get(\Codazon\ThemeLayoutPro\Helper\Image::class);
                $items = json_decode($items, true);
                $width = $this->getBaseWidth();
                $height = $this->getBaseHeight();
                $smallWidth = (float)($this->getData('small_width') ? : $this->helper->getConfig('codazon_furniture_layout/category_image/small_image_width'));
                $smallHeight = (float)($this->getData('small_height') ? : $this->helper->getConfig('codazon_furniture_layout/category_image/small_image_height'));
                if ($img = $this->getData('panorama_image')) {
                    $img = str_replace('{{media url="', '', $img);
                    $img = str_replace('"}}', '', $img);
                    $images[] = [
                        'base' => $mediaUrl . $img,
                        'large' => $mediaUrl . $img,
                        'small' => $mediaUrl . $img,
                        'is_panorama' => true,
                    ];
                }
                foreach ($items as $item) {
                    if ($item['image']) {
                        $img = str_replace('{{media url="', '', $item['image']);
                        $img = str_replace('"}}', '', $img);
                        $images[] = [
                            'base' => $mediaUrl . $img,
                            'large' => $imageHelper->init($img)->resize($width, $height)->__toString(),
                            'small' => $imageHelper->init($img)->resize($smallWidth, $smallHeight)->__toString(),
                            'is_panorama' => false,
                        ];
                    }
                }
            }
            $this->setData('custom_gallery_images', $images);
        }
        return $this->getData('custom_gallery_images');
    }
    
    public function getCategoryGalleryImages()
    {
        if (!$this->hasData('category_gallery_images')) {
            $category = $this->getCurrentCategory();
            $images = [];
            $this->imageHelper->constrainOnly(false);
            if ($category && ($galleryImages = $category->getData('f_category_gallery'))) {
                $galleryImages = json_decode($galleryImages, true);
                if (!empty($galleryImages['images'])) {
                    $width = $this->getBaseWidth();
                    $height = $this->getBaseHeight();
                    $smallWidth = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/small_image_width');
                    $smallHeight = (float)$this->helper->getConfig('codazon_furniture_layout/category_image/small_image_height');
                    
                    $panorama = $category->getData('panorama');
                    
                    foreach ($galleryImages['images'] as $image) {
                        $images[] = [
                            'base'      => $this->mediaConfig->getBaseMediaUrl() . $image['file'],
                            'large'     => $this->imageHelper
                                ->init(null, 'category_page_grid')
                                ->setImageFile($image['file'])->resize($width, $height)->getUrl(),
                            'small'     => $this->imageHelper
                                ->init(null, 'category_page_grid')
                                ->setImageFile($image['file'])->resize($smallWidth, $smallHeight)->getUrl(),
                            'is_panorama' => ($panorama == $image['file'])
                        ];
                    }
                }
            }
            $this->setData('category_gallery_images', $images);
        }
        return $this->getData('category_gallery_images');
    }
    
    public function getHelper()
    {
        return $this->helper;
    }
    
    public function getImageRatio()
    {
        if (!$this->hasData('image_ratio')) {
            $width = $this->getBaseWidth();
            $height = $this->getBaseHeight();
            if ($width) {
                $this->setData('image_ratio', $height/$width);
            }
            $this->setData('image_ratio', 1);
        }
        return $this->getData('image_ratio');
    }
    
    public function getBaseWidth()
    {
        return (float)($this->getData('width') ? : $this->helper->getConfig('codazon_furniture_layout/category_image/base_image_width'));
    }
    
    public function getBaseHeight()
    {
        return (float)($this->getData('height') ? : $this->helper->getConfig('codazon_furniture_layout/category_image/base_image_height'));
    }
    
    public function needToDisplayTemplateScript()
    {
        if (!$this->coreRegistry->registry('fly_widget_images_gallery')) {
            $this->coreRegistry->register('fly_widget_images_gallery', true);
            return true;
        }
        return false;
    }
}