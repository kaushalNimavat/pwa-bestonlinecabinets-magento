<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Adminhtml\CategoryAttributes;

use Magento\Backend\App\Action;

class Index extends AbstractCategoryAttributes
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FurnitureLayout::cdz_category_attributes');
    }
    
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->createActionPage();
        $resultPage->addContent(
            $resultPage->getLayout()->createBlock(\Codazon\FurnitureLayout\Block\Adminhtml\CategoryAttributes\Attribute::class)
        );
        return $resultPage;
    }
}

