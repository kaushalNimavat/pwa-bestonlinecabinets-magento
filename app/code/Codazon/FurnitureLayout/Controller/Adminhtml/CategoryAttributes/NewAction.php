<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Adminhtml\CategoryAttributes;

use Magento\Backend\App\Action;

class NewAction extends AbstractCategoryAttributes
{
	protected $resultForwardFactory;
	
    /**
     * Is the user allowed to view the menu grid.
     *
     * @return bool
     */
	protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FurnitureLayout::cdz_category_attributes_edit');
    }
    
	public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->_objectManager->get('\Magento\Backend\Model\View\Result\ForwardFactory')->create();
        return $resultForward->forward('edit');
    }
}

