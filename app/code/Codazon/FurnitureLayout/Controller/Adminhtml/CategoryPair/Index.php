<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Adminhtml\CategoryPair;

use Magento\Backend\App\Action;

class Index extends \Magento\Backend\App\Action
{
	/**
	* Core registry
	*
	* @var \Magento\Framework\Registry
	*/
	protected $_coreRegistry = null;
	/**
	 * @var \Magento\Framework\View\Result\PageFactory
	 */
	protected $resultPageFactory;
	
	public function __construct(
		Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Registry $registry
	) {
		$this->resultPageFactory = $resultPageFactory;
		$this->_coreRegistry = $registry;
		parent::__construct($context);
	}
    
	protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FurnitureLayout::currentsale_edit');
    }
    
	protected function _initAction()
	{
		$resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Codazon_FurnitureLayout::cdz_categorypair');
		return $resultPage;
	}
    
	public function execute()
	{
		$resultPage = $this->_initAction();
		$resultPage->addBreadcrumb(
			__('Category Pair'),
			__('Category Pair')
		);
		$resultPage->getConfig()->getTitle()->prepend(__('Category Pair'));	
		return $resultPage;
	}
}

