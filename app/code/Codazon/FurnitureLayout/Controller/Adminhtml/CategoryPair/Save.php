<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Adminhtml\CategoryPair;

use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
	/**
	* Core registry
	*
	* @var \Magento\Framework\Registry
	*/
	protected $_coreRegistry = null;
	/**
	 * @var \Magento\Framework\View\Result\PageFactory
	 */
	protected $resultPageFactory;
	
	public function __construct(
		Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Registry $registry
	) {
		$this->resultPageFactory = $resultPageFactory;
		$this->_coreRegistry = $registry;
		parent::__construct($context);
	}
    
	protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FurnitureLayout::save');
    }

	public function execute()
	{
        $request = $this->getRequest();
        if ($data = $request->getParam('cat_data')) {
            try {
                $data = json_decode($data, true);
                $existedData = [];
                $existedId = [];
                $newData = [];
                foreach ($data as $itemData) {
                    if ($itemData['pair_id'] == 0) {
                        $newData[] = $itemData;
                    } else {
                        $existedData[] = $itemData;
                        $existedId[] = $itemData['pair_id'];
                    }
                }
                $collection = $this->_objectManager->create(\Codazon\FurnitureLayout\Model\ResourceModel\CategoryPair\Collection::class);
                $collection->addFieldToFilter('pair_id', ['nin' => $existedId]);
                $connection = $collection->getConnection();
                if (count($existedId)) {
                    $connection->delete($collection->getMainTable(), 'pair_id NOT IN(' . implode(',', $existedId) . ')');
                } else {
                    $connection->delete($collection->getMainTable(), '1');
                }
                foreach ($newData as $itemData) {
                    $model = $this->_objectManager->create(\Codazon\FurnitureLayout\Model\CategoryPair::class);
                    unset($itemData['pair_id']);
                    if ($itemData['group_1'] && $itemData['group_2']) {
                        $model->addData($itemData)->save();
                    }
                }
                foreach ($existedData as $itemData) {
                    $model = $this->_objectManager->create(\Codazon\FurnitureLayout\Model\CategoryPair::class)->load($itemData['pair_id']);
                    $model->addData($itemData)->save();
                }
                
                $this->messageManager->addSuccess('Updated successfully.');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
				$this->messageManager->addError($e->getMessage());
			} catch (\RuntimeException $e) {
				$this->messageManager->addError($e->getMessage());
			} catch (\Exception $e) {
				$this->messageManager->addException($e, $e->getMessage());
			}
            
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }
        
	}
}
