<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Adminhtml\Currentsale;

use Magento\Backend\App\Action;

class Catchildren extends AbstractCurrentsale
{
    protected $categoryRepository;
    
    protected $collectionFactory;
    
    public function __construct(
		Action\Context $context,
		\Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory
	) {
        parent::__construct($context);
        $this->categoryRepository = $categoryRepository;
        $this->collectionFactory = $collectionFactory;
	}
    
    public function execute()
    {
        $id = $this->getRequest()->getParam('cat_id');
        $result = [
            'categories' => []
        ];
        if ($id) {
            $category = $this->categoryRepository->get($id);
            $children = explode(',', $category->getChildren());
            $collection = $this->collectionFactory->create();
            $collection->addFieldToFilter('entity_id', ['in' => $children])
                ->addAttributeToSelect(['name']);
            
            if ($collection->count()) {
                foreach ($collection as $subcat) {
                    $catData = [
                        'id'        => $subcat->getId(),
                        'name'      => $subcat->getName(),
                        'active'    => true //Default is active
                    ];
                    $result['categories'][$subcat->getId()] = $catData;
                }
            }
        }
        return $this->getResponse()->representJson(json_encode($result));
    }
}