<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Adminhtml\Currentsale;

use Magento\Backend\App\Action;

class Index extends AbstractCurrentsale
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Codazon_FurnitureLayout::currentsale');
    }
    
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addBreadcrumb(__('Product Attributes Manager'), __('Product Attributes Manager'));
        $resultPage->addBreadcrumb(__('Current Sale'), __('Current Sale'));
        $resultPage->setActiveMenu('Codazon_FurnitureLayout::currentsale');
        $resultPage->getConfig()->getTitle()->prepend(__('Current Sale'));
        return $resultPage;
    }
}

