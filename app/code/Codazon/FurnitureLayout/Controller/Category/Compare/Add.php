<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Controller\Category\Compare;

use Magento\Framework\Exception\NoSuchEntityException;

class Add extends \Codazon\FurnitureLayout\Controller\Category\Compare
{
    /**
     * Add item to compare list
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $postResult = ['success' => false, 'message' => __('We can\'t add the item to comparison list right now.')];
        $isAjax = $this->isAjax();
        
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            if (!$isAjax) {
                return $resultRedirect->setRefererUrl();
            }
        }

        $categoryId = (int)$this->getRequest()->getParam('category');
        if ($categoryId && ($this->_customerVisitor->getId() || $this->_customerSession->isLoggedIn())) {
            $storeId = $this->_storeManager->getStore()->getId();
            try {
                $category = $this->categoryRepository->get($categoryId, $storeId);
            } catch (NoSuchEntityException $e) {
                $category = null;
            }

            if ($category) {
                $this->_catalogCategoryCompareList->addCategory($category);
                $categoryName = $this->_objectManager->get(
                    \Magento\Framework\Escaper::class
                )->escapeHtml($category->getName());
                
                if ($isAjax) {
                    $postResult['success'] = true;
                    $postResult['message'] = __('You added %1 to your category compare list', '<strong>' . $categoryName .'</strong>');
                } else {
                    $this->messageManager->addSuccessMessage(
                        __('You added %1 to your category compare list', $categoryName)
                    );
                }
                $this->_eventManager->dispatch('catalog_category_compare_add_category', ['category' => $category]);
            }

            $this->_objectManager->get(\Codazon\FurnitureLayout\Helper\Category\Compare::class)->calculate();
        }
        
        if ($isAjax) {
            return $this->returnResult($postResult);
        } else {
            return $resultRedirect->setRefererOrBaseUrl();
        }
    }
}
