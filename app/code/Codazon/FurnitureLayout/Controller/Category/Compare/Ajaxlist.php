<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Controller\Category\Compare;

use Magento\Framework\Exception\NoSuchEntityException;

class Ajaxlist extends \Codazon\FurnitureLayout\Controller\Category\Compare
{
    protected $returnShortList = false;
      
    public function execute()
    {
        return $this->returnResult([]);
    }
}
