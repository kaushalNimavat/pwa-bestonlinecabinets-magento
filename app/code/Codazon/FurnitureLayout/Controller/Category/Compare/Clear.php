<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Controller\Category\Compare;

use Magento\Framework\Controller\ResultFactory;

class Clear extends \Codazon\FurnitureLayout\Controller\Category\Compare
{
    /**
     * Remove all items from comparison list
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $postResult = ['success' => false, 'message' => __('We can\'t add the item to comparison list right now.')];
        $isAjax = $this->isAjax();
        
        $items = $this->_itemCollectionFactory->create();

        if ($this->_customerSession->isLoggedIn()) {
            $items->setCustomerId($this->_customerSession->getCustomerId());
        } elseif ($this->_customerId) {
            $items->setCustomerId($this->_customerId);
        } else {
            $items->setVisitorId($this->_customerVisitor->getId());
        }

        try {
            $items->clear();
            if ($isAjax) {
                $postResult['success'] = true;
                $postResult['message'] = __('You cleared the comparison list.');
            } else {
                $this->messageManager->addSuccessMessage(__('You cleared the comparison list.'));
            }
            $this->_objectManager->get(\Magento\Catalog\Helper\Product\Compare::class)->calculate();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($isAjax) {
                $postResult['message'] = $e->getMessage();
            } else {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        } catch (\Exception $e) {
            if ($isAjax) {
                $postResult['message'] = __('Something went wrong  clearing the comparison list.');
            } else {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong  clearing the comparison list.'));
            }
        }

        if ($isAjax) {
            return $this->returnResult($postResult);
        } else {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setRefererOrBaseUrl();
        }
    }
}
