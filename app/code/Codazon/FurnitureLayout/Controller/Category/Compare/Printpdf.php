<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Controller\Category\Compare;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\View\Result\PageFactory;

/**
 * Catalog compare controller
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Printpdf extends \Magento\Framework\App\Action\Action
{
    protected $helper;
    
    protected $fileFactory;
    
    protected $y;
    
    protected $rowHeight = 18;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Codazon\FurnitureLayout\Helper\Data $helper
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->fileFactory = $fileFactory;
    }
    
    protected function getComparisonData()
    {
        return $this->_objectManager->get(\Codazon\FurnitureLayout\Block\Category\Compare\ListCompare::class)->getItemsWithComparableAttributesArray();
    }
    
    protected function getTextWidth($text, \Zend_Pdf_Resource_Font $font, $fontSize)
    {
        $text = iconv('', 'UTF-16BE', $text);
        $chars = array();
        for ($i = 0; $i < strlen($text); $i++) {
            $chars[] = (ord($text[$i++]) << 8) | ord($text[$i]);
        }
        $glyphs = $font->glyphNumbersForCharacters($chars);
        $widths = $font->widthsForGlyphs($glyphs);
        return (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
    }
    
    protected function drawText($text, $x, $y, $page, $width, $lineHeight, $maxLines = 1, $hyperlink = false, $itemWidth = false) {
        $textChunk = wordwrap($text, $width, "\n");
        $line = 0;
        $y1 = $y + 15;
        $maxTextWidth = 0;
        $font = $page->getFont();
        $fontSize = $page->getFontSize();
        $measureText = ($hyperlink && (!$itemWidth));
        foreach(explode("\n", $textChunk) as $textLine) {
            $line++;
            if ($text !== '') {
                $page->drawText(strip_tags(ltrim($textLine)), $x, $y, 'UTF-8');
                $y -= $lineHeight;
                if ($measureText) {
                    $textWidth = $this->getTextWidth($textLine, $font, $fontSize);
                    if ($textWidth > $maxTextWidth) {
                        $maxTextWidth = $textWidth;
                    }
                }
            }
        }

        if ($line > $maxLines) {
            $maxLines = $line;
        }
        if ($itemWidth) {
            $maxTextWidth = $itemWidth;
        }
        if ($hyperlink) {
            $target = \Zend_Pdf_Action_URI::create($hyperlink);
            $x1 = $x - 5;
            $y2 = $y1 - $maxLines*$this->rowHeight - 5;
            $annotation = \Zend_Pdf_Annotation_Link::create($x1, $y1, $x1 + $maxTextWidth, $y2, $target);
            $page->attachAnnotation($annotation);
            //$page->drawRectangle($x1, $y1, $x1 + $maxTextWidth, $y2, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        }
        return $maxLines;
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $data = [];
        if (!empty($params['data'])) {
            $data = json_decode($params['data'], true);
        } else {
            $data = $this->getComparisonData();
        }
        if (!empty($data['items'])) {
            $pdf = new \Zend_Pdf();
            $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
            $page = $pdf->pages[0];
            $style = new \Zend_Pdf_Style();
            $style->setLineColor(new \Zend_Pdf_Color_Html('#999999'));
            $fontNormal = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES);
            $fontBold =  \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES_BOLD);
            $strongColor = new \Zend_Pdf_Color_HTML('#000077');
            $normalColor = new \Zend_Pdf_Color_HTML('#000000');
            
            $style->setLineWidth(1);
            $style->setFont($fontNormal, 11);
            $page->setStyle($style);
            
            $width = $page->getWidth();
            $height = $page->getHeight();
            $x = 30;
            $pageTopalign = $height;        //default PDF page height
            $this->y = $height - 50;       //print table row from page top – 100px
            //Draw table header row's
            
            $rowHeight = $this->rowHeight;
            $count = count($data['items']);
            
            $attributes = array_merge([
                ['label' => __('Name'), 'code' => 'name'],
                //['label' => __('Link'), 'code' => 'url'],
            ], $data['comparableAttributes']);
            
            if ($count) {
                $labelWidth = 100;
                $itemWidth = ($width - 2*$x - $labelWidth)/$count;
                $itemMaxlength = 4*18/$count;            
                $maxLines = 1;
                $y1 = $this->y;
                
                foreach ($attributes as $i => $attr) {
                    $style->setFillColor($normalColor);
                    $style->setFont($fontBold, 11);
                    $page->setStyle($style);
                    $maxLines = 1;
                    $maxLines = $this->drawText($attr['label'], $x + 5, $y1 - 15, $page, 15, $rowHeight, $maxLines);
                    if ($attr['code'] == 'name') {
                        $style->setFillColor($strongColor);
                    }
                    foreach ($data['items'] as $j => $item) {
                        if ($i == 0) {
                            $style->setFont($fontBold, 11);
                        } else {
                            $style->setFont($fontNormal, 11);
                        }
                        $page->setStyle($style);
                        if ($attr['code'] == 'name') {
                            $maxLines = $this->drawText($item[$attr['code']],  $x + $labelWidth + $j*$itemWidth + 5, $y1 - 15, $page, $itemMaxlength, $rowHeight, $maxLines, $item['url'], $itemWidth);
                        } else {
                            $maxLines = $this->drawText($item[$attr['code']],  $x + $labelWidth + $j*$itemWidth + 5, $y1 - 15, $page, $itemMaxlength, $rowHeight, $maxLines);
                        }
                    }
                    
                    $y2 = $y1 - $maxLines*$rowHeight - 5;
                    $page->drawRectangle($x, $y1, $width - $x, $y2, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
                    $y1 = $y2;
                }
            }
            foreach ($data['items'] as $j => $item) {
                $page->drawLine($x + $labelWidth + $j*$itemWidth, $this->y, $x + $labelWidth + $j*$itemWidth, $y2);
            }
            
            
     
            $fileName = empty($params['file_name']) ? 'download.pdf' : $params['file_name'];
            
            
            if ($this->getRequest()->getParam('only_view')) {
                /* header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=$fileName");
                echo $pdf->render(); die(); */
                $this->getResponse()->setHeader('Content-type', 'application/pdf');
                $this->getResponse()->setHeader('Content-Disposition', "inline; filename=$fileName");
                $this->getResponse()->setBody($pdf->render());
                return false;
            }
     
            $this->fileFactory->create(
               $fileName,
               $pdf->render(),
               \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR, // this pdf will be saved in var directory with the name example.pdf
               'application/pdf'
            );
        }
    }

}
