<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Controller\Category\Compare;

use Magento\Framework\Exception\NoSuchEntityException;

class Remove extends \Codazon\FurnitureLayout\Controller\Category\Compare
{
    /**
     * Remove item from compare list
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $postResult = ['success' => false, 'message' => __('We can\'t remove the item from comparison list right now.')];
        $isAjax = $this->isAjax();
        
        $categoryId = (int)$this->getRequest()->getParam('category');
        if ($categoryId) {
            $storeId = $this->_storeManager->getStore()->getId();
            try {
                $category = $this->categoryRepository->get($categoryId, $storeId);
            } catch (NoSuchEntityException $e) {
                $category = null;
            }

            if ($category) {
                /** @var $item \Magento\Catalog\Model\Product\Compare\Item */
                $item = $this->_compareItemFactory->create();
                if ($this->_customerSession->isLoggedIn()) {
                    $item->setCustomerId($this->_customerSession->getCustomerId());
                } elseif ($this->_customerId) {
                    $item->setCustomerId($this->_customerId);
                } else {
                    $item->addVisitorId($this->_customerVisitor->getId());
                }

                $item->loadByCategory($category);
                /** @var $helper \Magento\Catalog\Helper\Product\Compare */
                $helper = $this->_objectManager->get(\Codazon\FurnitureLayout\Helper\Category\Compare::class);
                if ($item->getId()) {
                    $item->delete();
                    $categoryName = $this->_objectManager->get(\Magento\Framework\Escaper::class)
                        ->escapeHtml($category->getName());
                    if ($isAjax) {
                        $postResult['success'] = true;
                        $postResult['message'] = __('You removed %1 to your category compare list', '<strong>' . $categoryName .'</strong>');
                    } else {
                        $this->messageManager->addSuccessMessage(
                            __('You removed category %1 from the comparison list.', $categoryName)
                        );
                    }
                    $this->_eventManager->dispatch(
                        'catalog_category_compare_remove_category',
                        ['category' => $item]
                    );
                    $helper->calculate();
                }
            }
        }

        if ($isAjax) {
            return $this->returnResult($postResult);
        } else {
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setRefererOrBaseUrl();
        }
    }
}
