<?php
/**
* Copyright © 2018 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Category;

use Magento\Framework\View\Result\PageFactory;
use Codazon\FurnitureLayout\Model\LookbookCategoryFactory;


class Landingajax extends \Magento\Framework\App\Action\Action
{

    protected $helper;
    
    protected $layoutFactory;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        \Magento\Framework\View\Result\LayoutFactory $layoutFactory
    ) {
        parent::__construct($context);
        $this->layoutFactory = $layoutFactory;
        $this->helper = $helper;
    }
    
    public function execute()
    {
        $parentId = $this->getRequest()->getParam('parent_id');
        $result = [];
        if ($parentId) {
            $layout = $this->layoutFactory->create();
            $landingBlock = $layout->getLayout()->getBlock('category_list')->setData('parent_id', $parentId);
            $result['categoryList'] = $landingBlock->toHtml();
            $result['paramData'] = $this->getRequest()->getQueryValue();
        }
        return $this->getResponse()->representJson(json_encode($result));
    }
}