<?php
/**
* Copyright © 2018 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Category;

use Magento\Framework\View\Result\PageFactory;
use Codazon\FurnitureLayout\Model\LookbookCategoryFactory;


class Widgetlandingajax extends \Magento\Framework\App\Action\Action
{

    protected $helper;
    
    protected $layoutFactory;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        \Magento\Framework\View\Result\LayoutFactory $layoutFactory
    ) {
        parent::__construct($context);
        $this->layoutFactory = $layoutFactory;
        $this->helper = $helper;
    }
    
    public function execute()
    {
        $request = $this->getRequest();
        $categoryIds = $request->getParam('category_ids');
        $parentId = $request->getParam('parent_id');
        $result = [];
        if ($categoryIds || $parentId) {
            $layout = $this->layoutFactory->create();
            $landingBlock = $layout->getLayout()
                ->getBlock('category_list')
                ->setData('display_type', $request->getParam('display_type', 1))
                ->setData('category_ids', $categoryIds)
                ->setData('parent_id', $parentId);
            $result['categoryList'] = $landingBlock->toHtml();
            $result['paramData'] = $this->getRequest()->getQueryValue();
        }
        return $this->getResponse()->representJson(json_encode($result));
    }
}