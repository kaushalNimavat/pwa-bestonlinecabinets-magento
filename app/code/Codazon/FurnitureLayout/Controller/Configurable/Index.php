<?php
/**
* Copyright © 2018 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Configurable;

use Magento\Framework\View\Result\PageFactory;
use Codazon\FurnitureLayout\Model\LookbookCategoryFactory;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $coreRegistry;
    
    protected $storeManager;
    
    protected $catalogProductVisibility;
    
    protected $catalogConfig;
    
    protected $helper;
    
    protected $resultJsonFactory;
    
    protected $storeId;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Codazon\FurnitureLayout\Helper\Data $helper,
        \Magento\Framework\View\Result\LayoutFactory $layoutFactory
    ) {
        parent::__construct($context);
        $this->layoutFactory = $layoutFactory;
        $this->helper = $helper;
    }
    
    public function execute()
    {
        $result = [];
        $layout = $this->layoutFactory->create();
        
        if ($productId = $this->getRequest()->getParam('id')) {
            $product = $this->_objectManager->get('\Magento\Catalog\Api\ProductRepositoryInterface')->getById($productId);            
            $this->helper->getCoreRegistry()->register('product', $product);
            $this->helper->getCoreRegistry()->register('current_product', $product);
        }
        return $layout;
    }
}