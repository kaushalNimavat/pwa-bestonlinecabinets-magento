<?php
/**
* Copyright © 2018 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Controller\Flatpanel;

use Magento\Framework\View\Result\PageFactory;

class Instruction extends \Magento\Framework\App\Action\Action
{
    protected $coreRegistry;
    
    protected $storeManager;
    
    protected $catalogProductVisibility;
    
    protected $catalogConfig;
    
    protected $helper;
    
    protected $resultJsonFactory;
    
    protected $storeId;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Codazon\FurnitureLayout\Helper\Data $helper
    ) {
        parent::__construct($context);
        $this->helper = $helper;
    }
    
    public function execute()
    {
        $styleId = $this->getRequest()->getParam('style');
        if ($styleId) {
            $collection = $this->_objectManager->get(\Codazon\FlatPanel\Model\ResourceModel\AssemblyInstruction\CollectionFactory::class)->create();
            $storeId = $this->_objectManager->get(\Magento\Store\Model\StoreManagerInterface::class)->getStore()->getId();
            $collection->setStoreId($storeId);
            $collection->addFieldToFilter(
                'style', ['finset' => [$styleId]]
            )
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToSelect(['name']);
            $instructions = [];
            $count = $collection->count();
            foreach ($collection as $item) {
                $data = [];
                $data['name'] = $item->getData('name');
                $data['psd_file'] = $item->getData('psd_file');
                $data['video_url'] = $item->getData('video_url');
                $data['thumbnail'] = $item->getData('thumbnail');
                $data['type'] = $item->getData('type');
                $instructions[] = $data;
            }
            return $this->getResponse()->representJson(
                $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode([
                    'itemsCount'    => $count,
                    'items'         => $instructions,
                    'message'       => $count ? '' : __('There are no matching instructions')
                ])
            );
        }
    }
}