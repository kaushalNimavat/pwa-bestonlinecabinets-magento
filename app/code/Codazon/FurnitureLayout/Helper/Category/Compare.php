<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Helper\Category;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\Compare\Item\Collection;

/**
 * Catalog Product Compare Helper
 *
 * @api
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Compare extends \Magento\Framework\Url\Helper\Data
{
    /**
     * Product Compare Items Collection
     *
     * @var Collection
     */
    protected $_itemCollection;

    /**
     * Product Comapare Items Collection has items flag
     *
     * @var bool
     */
    protected $_hasItems;

    /**
     * Allow used Flat catalog product for product compare items collection
     *
     * @var bool
     */
    protected $_allowUsedFlat = true;

    /**
     * Customer id
     *
     * @var null|int
     */
    protected $_customerId = null;

    /**
     * Catalog session
     *
     * @var \Magento\Catalog\Model\Session
     */
    protected $_catalogSession;

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * Customer visitor
     *
     * @var \Magento\Customer\Model\Visitor
     */
    protected $_customerVisitor;

    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;

    /**
     * Product compare item collection factory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\CollectionFactory
     */
    protected $_itemCollectionFactory;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $_formKey;

    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    protected $_wishlistHelper;

    /**
     * @var \Magento\Framework\Data\Helper\PostHelper
     */
    protected $postHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\CollectionFactory $itemCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Customer\Model\Visitor $customerVisitor
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Catalog\Model\Session $catalogSession
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Wishlist\Helper\Data $wishlistHelper
     * @param \Magento\Framework\Data\Helper\PostHelper $postHelper
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Codazon\FurnitureLayout\Model\ResourceModel\Category\Compare\Item\CollectionFactory $itemCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Customer\Model\Visitor $customerVisitor,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Wishlist\Helper\Data $wishlistHelper,
        \Magento\Framework\Data\Helper\PostHelper $postHelper
    ) {
        $this->_itemCollectionFactory = $itemCollectionFactory;
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->_customerVisitor = $customerVisitor;
        $this->_customerSession = $customerSession;
        $this->_catalogSession = $catalogSession;
        $this->_formKey = $formKey;
        $this->_wishlistHelper = $wishlistHelper;
        $this->postHelper = $postHelper;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Retrieve compare list url
     *
     * @return string
     */
    public function getListUrl()
    {
        $itemIds = [];
        foreach ($this->getItemCollection() as $item) {
            $itemIds[] = $item->getId();
        }

        $params = [
            'items' => implode(',', $itemIds),
            \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl()
        ];

        return $this->_getUrl('furniturelayout/category_compare', $params);
    }

    /**
     * Get parameters used for build add product to compare list urls
     *
     * @param Product $product
     * @return string
     */
    public function getPostDataParams($category)
    {
        $params = ['category' => $category->getId()];
        $requestingPageUrl = $this->_getRequest()->getParam('requesting_page_url');

        if (!empty($requestingPageUrl)) {
            $encodedUrl = $this->urlEncoder->encode($requestingPageUrl);
            $params[\Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED] = $encodedUrl;
        }

        return $this->postHelper->getPostData($this->getAddUrl(), $params);
    }

    /**
     * Retrieve url for adding product to compare list
     *
     * @return string
     */
    public function getAddUrl()
    {
        return $this->_getUrl('furniturelayout/category_compare/add');
    }
    
    /**
     * Retrieve remove item from compare list url
     *
     * @return string
     */    
    public function getRemoveUrl()
    {
        return $this->_getUrl('catalog/product_compare/remove');
    }
    
    /**
     * Retrieve clear compare list url
     *
     * @return string
     */
    public function getClearListUrl()
    {
        return $this->_getUrl('catalog/product_compare/clear');
    }
    
    /**
     * Get parameters to remove products from compare list
     *
     * @param Product $product
     * @return string
     */
    public function getPostDataRemove($category)
    {
        $data = [
            \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED => '',
            'category' => $category->getId(),
            'confirmation' => true,
            'confirmationMessage' => __('Are you sure you want to remove this item from your Compare Categories list?')
        ];
        return $this->postHelper->getPostData($this->getRemoveUrl(), $data);
    }

    /**
     * Get parameters to clear compare list
     *
     * @return string
     */
    public function getPostDataClearList()
    {
        $params = [
            \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED => '',
            'confirmation' => true,
            'confirmationMessage' => __('Are you sure you want to remove all items from your Compare Categories list?'),
        ];
        return $this->postHelper->getPostData($this->getClearListUrl(), $params);
    }

    /**
     * Retrieve compare list items collection
     *
     * @return Collection
     */
    public function getItemCollection()
    {
        if (!$this->_itemCollection) {
            // cannot be placed in constructor because of the cyclic dependency which cannot be fixed with proxy class
            // collection uses this helper in constructor when calling isEnabledFlat() method
            $this->_itemCollection = $this->_itemCollectionFactory->create();
            $this->_itemCollection->useCategoryItem(true)->setStoreId($this->_storeManager->getStore()->getId());

            if ($this->_customerSession->isLoggedIn()) {
                $this->_itemCollection->setCustomerId($this->_customerSession->getCustomerId());
            } elseif ($this->_customerId) {
                $this->_itemCollection->setCustomerId($this->_customerId);
            } else {
                $this->_itemCollection->setVisitorId($this->_customerVisitor->getId());
            }

            //$this->_itemCollection->setVisibility($this->_catalogProductVisibility->getVisibleInSiteIds());

            /* Price data is added to consider item stock status using price index */
            //$this->_itemCollection->addPriceData();

            $this->_itemCollection->addAttributeToSelect('name')->load();

            /* update compare items count */
            $this->_catalogSession->setCatalogCompareCategoriesCount(count($this->_itemCollection));
        }

        return $this->_itemCollection;
    }

    /**
     * Calculate cache product compare collection
     *
     * @param bool $logout
     * @return $this
     */
    public function calculate($logout = false)
    {
        /** @var $collection Collection */
        $collection = $this->_itemCollectionFactory->create()
            ->useCategoryItem(true);
        
        if (!$logout && $this->_customerSession->isLoggedIn()) {
            $collection->setCustomerId($this->_customerSession->getCustomerId());
        } elseif ($this->_customerId) {
            $collection->setCustomerId($this->_customerId);
        } else {
            $collection->setVisitorId($this->_customerVisitor->getId());
        }

        /* Price data is added to consider item stock status using price index */
        /* $collection->addPriceData()->setVisibility($this->_catalogProductVisibility->getVisibleInSiteIds()); */

        $count = $collection->getSize();
        $this->_catalogSession->setCatalogCompareCategoriesCount($count);

        return $this;
    }

    /**
     * Retrieve count of items in compare list
     *
     * @return int
     */
    public function getItemCount()
    {
        
        if (!$this->_catalogSession->hasCatalogCompareCategoriesCount()) {
            $this->calculate();
        }

        return $this->_catalogSession->getCatalogCompareCategoriesCount();
    }

    /**
     * Check has items
     *
     * @return bool
     */
    public function hasItems()
    {
        return $this->getItemCount() > 0;
    }

    /**
     * Set is allow used flat (for collection)
     *
     * @param bool $flag
     * @return $this
     */
    public function setAllowUsedFlat($flag)
    {
        $this->_allowUsedFlat = (bool)$flag;
        return $this;
    }

    /**
     * Retrieve is allow used flat (for collection)
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getAllowUsedFlat()
    {
        return $this->_allowUsedFlat;
    }

    /**
     * Setter for customer id
     *
     * @param int $id
     * @return $this
     */
    public function setCustomerId($id)
    {
        $this->_customerId = $id;
        return $this;
    }
}
