<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Helper\Category;

class CustomContent extends \Codazon\FurnitureLayout\Helper\Data
{
    public function getCustomHtmlByCategory($category)
    {
        $blockId = $category->getData('custom_content_block');
        $html = '';
        if ($blockId) {
            $block = $this->getObjectManager()->get(\Magento\Cms\Model\BlockFactory::class)->create();
            $storeId = $this->getStoreManager()->getStore()->getId();
            $block->setStoreId($storeId)->load($blockId);
            $html = $this->htmlFilter($block->getContent());
        }
        return $html;
    }
    public function getCustomJsFilesByCategory($category)
    {
        $jsStr = $category->getData('custom_content_js_files');
        $files = explode(',', $jsStr);
        $include = [];
        foreach ($files as $file) {
            if (stripos($file, '.js') !== false) {
                $include[] = '"'. str_replace('{{media}}', $this->getMediaUrl(), $file) . '"';
            } else {
                $include[] = '"'. $file . '"';
            }
        }
        $html = '<script>require(['.implode(',', $include).']);</script>';
        return $html;
    }
    
    public function getCustomCssFilesByCategory($category, $block)
    {
        $jsStr = $category->getData('custom_content_css_files');
        $files = explode(',', $jsStr);
        $include = [];
        foreach ($files as $file) {
            if (stripos($file, '::') !== false) {
                $include[] = '<link  rel="stylesheet" type="text/css"  media="all" href="'. $block->getViewFileUrl($file) . '" />';
            } else {
                $include[] = '<link  rel="stylesheet" type="text/css"  media="all" href="'. str_replace('{{media}}', $this->getMediaUrl(), $file) . '" />';
            }
        }
        $html = implode(',', $include);
        return $html;
    }
    
    public function getMediaUrl($path = '') {
        return $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $path;
   }
}