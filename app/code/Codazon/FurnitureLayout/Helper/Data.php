<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Layout merge factory
     *
     * @var \Magento\Framework\View\Layout\ProcessorFactory
     */
    protected $layoutProcessorFactory;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $assetRepo;

    protected $objectManager;
    
    protected $coreRegistry;
    
    protected $currentCategory = null;
    
    protected $pageConfig;

    protected $pageLayout;
    
    protected $localeFormat;
    
    protected $storeManager;
    
    protected $_blockFilter;
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\View\Page\Config $pageConfig,
        \Magento\Framework\View\Layout\ProcessorFactory $layoutProcessorFactory,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        
        $this->layoutProcessorFactory = $layoutProcessorFactory;
        $this->assetRepo = $assetRepo;
        $this->coreRegistry = $registry;
        $this->pageConfig = $pageConfig;
        $this->pageLayout = $pageConfig->getPageLayout();
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->localeFormat = $localeFormat;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }
    
    public function getStoreManager()
    {
        return $this->storeManager;
    }
    
    public function getConfig($path)
    {
        return $this->scopeConfig->getValue($path, 'store');
    }
    
    public function getCoreRegistry()
    {
        return $this->coreRegistry;
    }
    
    public function getPageLayout()
    {
        return $this->pageLayout;
    }
    
    public function getPageConfig()
    {
        return $this->pageConfig;
    }
    
    public function getObjectManager()
    {
        return $this->objectManager;
    }
    
    public function getCurrentCategory()
    {
        if ($this->currentCategory === null) {
            $this->currentCategory = $this->coreRegistry->registry('current_category');
            if (!$this->currentCategory) {
                $this->currentCategory = false;
            }
        }
        return $this->currentCategory;
    }
    
    public function getPriceFormat()
	{
		return $this->localeFormat->getPriceFormat();
	}
    
    public function getBlockFilter()
    {
        if ($this->_blockFilter === null) {
            $this->_blockFilter = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Cms\Model\Template\FilterProvider')
            ->getBlockFilter();
        }
        return $this->_blockFilter;
    }
    
    public function htmlFilter($content)
    {
        return $this->getBlockFilter()->filter($content);
    }
}