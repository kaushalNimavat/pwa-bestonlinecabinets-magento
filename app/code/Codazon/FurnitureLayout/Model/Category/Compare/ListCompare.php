<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Codazon\FurnitureLayout\Model\Category\Compare;

use Magento\Catalog\Model\ResourceModel\Product\Compare\Item\Collection;

/**
 * Product Compare List Model
 *
 * @api
 * @SuppressWarnings(PHPMD.LongVariable)
 * @since 100.0.2
 */
class ListCompare extends \Magento\Framework\DataObject
{
    /**
     * Customer visitor
     *
     * @var \Magento\Customer\Model\Visitor
     */
    protected $_customerVisitor;

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * Catalog product compare item
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\Compare\Item
     */
    protected $_catalogCategoryCompareItem;

    /**
     * Item collection factory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\CollectionFactory
     */
    protected $_itemCollectionFactory;

    /**
     * Compare item factory
     *
     * @var \Magento\Catalog\Model\Product\Compare\ItemFactory
     */
    protected $_compareItemFactory;

    /**
     * Constructor
     *
     * @param \Magento\Catalog\Model\Product\Compare\ItemFactory $compareItemFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\Compare\Item\CollectionFactory $itemCollectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\Compare\Item $catalogCategoryCompareItem
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\Visitor $customerVisitor
     * @param array $data
     */
    public function __construct(
        \Codazon\FurnitureLayout\Model\Category\Compare\ItemFactory $compareItemFactory,
        \Codazon\FurnitureLayout\Model\ResourceModel\Category\Compare\Item\CollectionFactory $itemCollectionFactory,
        \Codazon\FurnitureLayout\Model\ResourceModel\Category\Compare\Item $catalogCategoryCompareItem,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Visitor $customerVisitor,
        array $data = []
    ) {
        $this->_compareItemFactory = $compareItemFactory;
        $this->_itemCollectionFactory = $itemCollectionFactory;
        $this->_catalogCategoryCompareItem = $catalogCategoryCompareItem;
        $this->_customerSession = $customerSession;
        $this->_customerVisitor = $customerVisitor;
        parent::__construct($data);
    }

    /**
     * Add product to Compare List
     *
     * @param int|\Magento\Catalog\Model\Product $product
     * @return $this
     */
    public function addCategory($category)
    {
        /* @var $item \Magento\Catalog\Model\Product\Compare\Item */
        $item = $this->_compareItemFactory->create();
        $this->_addVisitorToItem($item);
        $item->loadByCategory($category);

        if (!$item->getId()) {
            $item->addCategoryData($category);
            $item->save();
        }

        return $this;
    }

    /**
     * Add products to compare list
     *
     * @param string[] $productIds
     * @return $this
     */
    public function addCategories($categoryIds)
    {
        if (is_array($categoryIds)) {
            foreach ($categoryIds as $categoryId) {
                $this->addCategory($categoryId);
            }
        }
        return $this;
    }

    /**
     * Retrieve Compare Items Collection
     *
     * @return Collection
     */
    public function getItemCollection()
    {
        return $this->_itemCollectionFactory->create();
    }

    /**
     * Remove category from compare list
     *
     * @param int|\Magento\Catalog\Model\Category $category
     * @return $this
     */
    public function removeCategory($category)
    {
        $item = $this->_compareItemFactory->create();
        $this->_addVisitorToItem($item);
        $item->loadByCategory($category);

        if ($item->getId()) {
            $item->delete();
        }

        return $this;
    }

    /**
     * Add visitor and customer data to compare item
     *
     * @param \Magento\Catalog\Model\Product\Compare\Item $item
     * @return $this
     */
    protected function _addVisitorToItem($item)
    {
        $item->addVisitorId($this->_customerVisitor->getId());
        if ($this->_customerSession->isLoggedIn()) {
            $item->setCustomerId($this->_customerSession->getCustomerId());
        }

        return $this;
    }

    /**
     * Check has compare items by visitor/customer
     *
     * @param int $customerId
     * @param int $visitorId
     * @return bool
     */
    public function hasItems($customerId, $visitorId)
    {
        return (bool)$this->_catalogCategoryCompareItem->getCount($customerId, $visitorId);
    }
}
