<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Model;

class CategoryAttributes extends \Codazon\FurnitureLayout\Model\AbstractModel
{

    const ENTITY = 'cdz_category_attributes';
    
    protected function _construct()
    {
        $this->_init('Codazon\FurnitureLayout\Model\ResourceModel\CategoryAttributes');
    }
}
