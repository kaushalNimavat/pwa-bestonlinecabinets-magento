<?php
/**
* Copyright © 2020 Nicolas. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Model;

class CategoryPair extends \Magento\Framework\Model\AbstractModel
{    
    protected function _construct()
    {
        $this->_init('Codazon\FurnitureLayout\Model\ResourceModel\CategoryPair');
    }
}
