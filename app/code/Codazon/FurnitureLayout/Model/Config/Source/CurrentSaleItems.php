<?php
/**
 * Copyright © 2017 Codazon, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Model\Config\Source;

class CurrentSaleItems implements \Magento\Framework\Option\ArrayInterface
{
    protected $_currentSaleItems;
    
	public function toOptionArray()
    {
        if ($this->_currentSaleItems === null) {
            $this->_currentSaleItems = [];
            $collection = \Magento\Framework\App\ObjectManager::getInstance()
                ->create('Codazon\FurnitureLayout\Model\ResourceModel\Currentsale\Collection');
            foreach ($collection->getItems() as $item) {
                $this->_currentSaleItems[] = [
                    'value' => $item->getId(),
                    'label' => $item->getName()
                ];
            }
        }
        return $this->_currentSaleItems;
    }	
	
}