<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Model;

class Currentsale extends \Codazon\FurnitureLayout\Model\AbstractModel
{

    const ENTITY = 'currentsale';
    
    protected function _construct()
    {
        $this->_init('Codazon\FurnitureLayout\Model\ResourceModel\Currentsale');
    }
}
