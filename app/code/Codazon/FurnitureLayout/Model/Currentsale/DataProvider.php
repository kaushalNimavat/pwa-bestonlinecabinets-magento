<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Model\Currentsale;

use Codazon\FurnitureLayout\Model\ResourceModel\Currentsale\CollectionFactory as CollectionFactory;
use Magento\Framework\App\Request\Http as Request;
use Magento\Framework\App\Request\DataPersistorInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{   
    protected $collection;
    
    protected $dataPersistor;
    
    protected $request;
    
    protected $loadedData;
    
    protected $storeId;
    
    protected $_isEav = false;

	public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        Request $request,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->request = $request;
        $this->storeId = $this->request->getParam('store', \Magento\Store\Model\Store::DEFAULT_STORE_ID);
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
    
	public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        if ($this->_isEav) {
            $this->collection->setStoreId($this->storeId)->addAttributeToSelect(['name']);
        }
        $items = $this->collection->getItems();
        foreach ($items as $item) {
            $this->loadedData[$item->getId()] = $item->getData();
        }
        $data = $this->dataPersistor->get('currentsale');
        if (!empty($data)) {
            $item = $this->collection->getNewEmptyItem();
            $item->setData($data);
            $this->loadedData[$item->getId()] = $item->getData();
            $this->dataPersistor->clear('currentsale');
        }
        return $this->loadedData;
    }
}

