<?php
/**
* Copyright © 2020 Nicolas. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Model\ResourceModel;

class CategoryPair extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
	{
		$this->_init('fly_category_pair', 'pair_id');
	}
}
