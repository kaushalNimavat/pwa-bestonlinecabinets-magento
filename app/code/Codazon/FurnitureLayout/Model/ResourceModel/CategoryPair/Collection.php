<?php
/**
* Copyright © 2020 Nicolas. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Model\ResourceModel\CategoryPair;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Codazon\FurnitureLayout\Model\CategoryPair', 'Codazon\FurnitureLayout\Model\ResourceModel\CategoryPair');
    }
}
