<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Model\ResourceModel\Currentsale;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct() {
        $this->_init('Codazon\FurnitureLayout\Model\Currentsale', 'Codazon\FurnitureLayout\Model\ResourceModel\Currentsale');
    }
}
