<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Observer\Adminhtml;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\MediaStorage\Service\ImageResize;
use Magento\Framework\App\State;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\EntityManager\Operation\ExtensionInterface;
use Magento\MediaStorage\Model\File\Uploader as FileUploader;

class CategorySave implements ObserverInterface
{
    protected $helper;

    protected $imageResize;

    protected $state;

    protected $jsonHelper;

    protected $mediaDirectory;

    protected $fileStorageDb;

    protected $mediaAttributes;

    public function __construct(
        \Codazon\FurnitureLayout\Helper\Data $helper,
        ImageResize $imageResize,
        State $state,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageDb
    ) {
        $this->helper = $helper;
        $this->imageResize = $imageResize;
        $this->state = $state;
        $this->jsonHelper = $jsonHelper;
        $this->mediaConfig = $mediaConfig;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->fileStorageDb = $fileStorageDb;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getData('request');
        $category = $observer->getData('category');

        $clearImages = [];
        $newImages = [];
        $existImages = [];
        $oldImages = [];

        if ($fCategoryGallery = $request->getParam('f_category_gallery')) {
            if (!is_array($fCategoryGallery)) {
                $fCategoryGallery = json_decode($fCategoryGallery, true);
            }

            $images = $fCategoryGallery['images'];
            foreach ($images as $i => &$image) {
                $fi = $image['file'];
                if (!empty($image['removed'])) {
                    $clearImages[] = $image['file'];
                    unset($images[$i]);
                } elseif (empty($image['value_id'])) {
                    $newFile = $this->moveImageFromTmp($image['file']);
                    $image['value_id'] = uniqid();
                    $image['new_file'] = $newFile;
                    $newImages[$image['file']] = $image;
                    $image['file'] = $newFile;
                }
                $oldImages[$fi] = $image['file'];
            }

            $this->processDeletedImages($category, $clearImages);
            $this->processNewAndExistingImages($category, $fCategoryGallery['images']);

            $fCategoryGallery['images'] = $images;
            $category->setData('f_category_gallery', json_encode($fCategoryGallery));
        }

        if ($catParams = $request->getParam('category')) {
            $mediaAttributes = $this->getMediaAttributes($category);
            foreach ($mediaAttributes as $attribute) {
                $code = $attribute->getAttributeCode();
                $value = false;
                if (!empty($catParams[$code]) && isset($oldImages[$catParams[$code]])) {
                    $value = $oldImages[$catParams[$code]];
                }
                if (in_array($value, $clearImages)) {
                    $value = false;
                }
                $category->setData($code, $value);
            }
        }

    }

    public function getMediaAttributes($model)
    {
        if ($this->mediaAttributes === null) {
            $this->mediaAttributes = [];
            foreach ($model->getAttributes() as $attribute) {
                if ($attribute->getFrontend()->getInputType() == 'media_image') {
                    $this->mediaAttributes[$attribute->getAttributeCode()] = $attribute;
                }
            }
        }
        return $this->mediaAttributes;
    }


    protected function processDeletedImages($category, array $images) {
        foreach ($images as $file) {
            //$file = $this->getFilenameFromTmp($file);
            $this->mediaDirectory->delete(
                $this->mediaConfig->getTmpMediaPath($file)
            );
            $this->mediaDirectory->delete(
                $this->mediaConfig->getMediaPath($file)
            );
        }
    }

    protected function processNewAndExistingImages($category, array &$images)
    {
        foreach ($images as &$image) {
            if (empty($image['removed'])) {
                $data = $this->processNewImage($category, $image);
                $data['value_id'] = $image['value_id'];
                $data['label'] = isset($image['label']) ? $image['label'] : '';
                $data['position'] = isset($image['position']) ? (int)$image['position'] : 0;
                $data['disabled'] = isset($image['disabled']) ? (int)$image['disabled'] : 0;
                $data['store_id'] = (int)$category->getStoreId();
            }
        }
    }

    protected function processNewImage($category, array &$image)
    {
        $data = [];
        $data['value'] = $image['file'];
        $data['attribute_id'] = null;
        if (!empty($image['media_type'])) {
            $data['media_type'] = $image['media_type'];
        }
        $image['value_id'] = uniqid();
        return $data;
    }

    protected function copyImage($file)
    {
        try {
            $destinationFile = $this->getUniqueFileName($file);
            if (!$this->mediaDirectory->isFile($this->mediaConfig->getMediaPath($file))) {
                throw new \Exception();
            }
            if ($this->fileStorageDb->checkDbUsage()) {
                $this->fileStorageDb->copyFile(
                    $this->mediaDirectory->getAbsolutePath($this->mediaConfig->getMediaShortUrl($file)),
                    $this->mediaConfig->getMediaShortUrl($destinationFile)
                );
                $this->mediaDirectory->delete($this->mediaConfig->getMediaPath($destinationFile));
            } else {
                $this->mediaDirectory->copyFile(
                    $this->mediaConfig->getMediaPath($file),
                    $this->mediaConfig->getMediaPath($destinationFile)
                );
            }
            return str_replace('\\', '/', $destinationFile);
        } catch (\Exception $e) {
            $file = $this->mediaConfig->getMediaPath($file);
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We couldn\'t copy file %1. Please delete media with non-existing images and try again.', $file)
            );
        }
    }

    private function getSafeFilename($file)
    {
        $file = DIRECTORY_SEPARATOR . ltrim($file, DIRECTORY_SEPARATOR);
        return $this->mediaDirectory->getDriver()->getRealPathSafety($file);
    }

    protected function getUniqueFileName($file, $forTmp = false)
    {
        if ($this->fileStorageDb->checkDbUsage()) {
            $destFile = $this->fileStorageDb->getUniqueFilename(
                $this->mediaConfig->getBaseMediaUrlAddition(),
                $file
            );
        } else {
            $destinationFile = $forTmp
                ? $this->mediaDirectory->getAbsolutePath($this->mediaConfig->getTmpMediaPath($file))
                : $this->mediaDirectory->getAbsolutePath($this->mediaConfig->getMediaPath($file));
            $destFile = dirname($file) . '/' . FileUploader::getNewFileName($destinationFile);
        }

        return $destFile;
    }

    protected function moveImageFromTmp($file)
    {
        $file = $this->getFilenameFromTmp($this->getSafeFilename($file));
        $destinationFile = $this->getUniqueFileName($file);

        try {
            if ($this->fileStorageDb->checkDbUsage()) {
                $this->fileStorageDb->renameFile(
                    $this->mediaConfig->getTmpMediaShortUrl($file),
                    $this->mediaConfig->getMediaShortUrl($destinationFile)
                );
                $this->mediaDirectory->delete($this->mediaConfig->getTmpMediaPath($file));
                $this->mediaDirectory->delete($this->mediaConfig->getMediaPath($destinationFile));
            } else {
                $this->mediaDirectory->renameFile(
                    $this->mediaConfig->getTmpMediaPath($file),
                    $this->mediaConfig->getMediaPath($destinationFile)
                );
            }
        } catch (\Magento\Framework\Exception\FileSystemException $e) {
        }

        return str_replace('\\', '/', $destinationFile);
    }

    protected function getFilenameFromTmp($file)
    {
        return strrpos($file, '.tmp') == strlen($file) - 4 ? substr($file, 0, strlen($file) - 4) : $file;
    }
}
