<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Observer\Adminhtml;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class ContentBuilderItemTypes implements ObserverInterface
{
    protected $helper;
    
    public function __construct(
        \Codazon\FurnitureLayout\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $builder = $observer->getData('builder');
        $itemTypes = $builder->getData('item_types');
        $yesno = $builder->getYesNoOptions();
        $itemTypes['category_images_slider'] = [
            'name'      => 'category_images_slider',
            'title'     => __('Category Images Gallery'),
            'disable_children' => true,
            'fields'    => [
                ['type' => 'hidden', 'name' => 'handle_class', 'value'    => 'Codazon\FurnitureLayout\Block\Widget\ImageGallery'],
                ['type' => 'text', 'name' => 'width', 'label' => __('Width'), 'value' => 800],
                ['type' => 'text', 'name' => 'height', 'label' => __('Height'), 'value' => 533],
                ['type' => 'text', 'name' => 'small_width', 'label' => __('Thumb Width'), 'value' => 100], 
                ['type' => 'text', 'name' => 'small_height', 'label' => __('Thumb Height'), 'value' => 67],
                ['type' => 'select', 'name' => 'use_custom_image', 'label' => __('Use custom Image'), 'values' => $yesno],
                ['type' => 'custom_field',
                    'template_id' => 'fly-category-id',
                    'label' => __('Category Id'),
                    'name' => 'category_id',
                    'desc' => __('Applied when "Use custom Image" is "No". Category images will be displayed instead of custom images.'),
                    'data' => [
                        'name' => 'category_id'
                    ]
                ],
                ['type' => 'multitext', 'name' => 'custom_items', 'label' => __('Custom Images'), 'full_field' => true,
                    'sub_fields' => [
                        ['type' => 'image', 'name' => 'image', 'label' => __('Image')],
                    ],
                    'desc' => __('Applied when "Use custom Image" is "Yes".')
                ],
                ['type' => 'image', 'name' => 'panorama_image', 'label' => __('Panorama Image'), 'desc' => __('Applied when "Use custom Image" is "Yes".')],
            ]
        ];
        $builder->setData('item_types', $itemTypes);
    }
}
