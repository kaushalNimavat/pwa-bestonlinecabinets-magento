<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Theme\Model\Theme;
use Magento\Framework\App\Filesystem\DirectoryList;


/**
 * Theme Observer model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class LayoutLoadBefore implements ObserverInterface
{
    protected $helper;
    
    public function __construct(
        \Codazon\FurnitureLayout\Helper\Data $helper,
        \Magento\Framework\Registry $registry
    ) {
        $this->helper = $helper;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $pageLayout = $this->helper->getPageLayout();
        $catageory = $this->helper->getCoreRegistry()->registry('current_category');
        if ($catageory && $catageory->getData('custom_content_block')) {
            $observer->getLayout()->getUpdate()->addPageHandles(['catalog_category_view_custom_content']);
        } elseif ($pageLayout == 'f-layout-1') {
            $observer->getLayout()->getUpdate()->addPageHandles(['catalog_category_view_f_layout_1']);
        } elseif ($pageLayout == 'f-layout-2') {
            $observer->getLayout()->getUpdate()->addPageHandles(['catalog_category_view_f_layout_2']);
        }
    }
}
