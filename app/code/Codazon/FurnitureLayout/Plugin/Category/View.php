<?php
/**
 * Copyright © 2017 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */


namespace Codazon\FurnitureLayout\Plugin\Category;

class View
{
    protected $helper;

    const AJAX_PARAM = 'ajax_cat_tab';

    /**
     * @var \Magento\Framework\DataObject\Factory
     */
    private $dataObjectFactory;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $eventManager;

    public function __construct(
        \Codazon\FurnitureLayout\Helper\Data $helper,
        \Magento\Framework\DataObject\Factory $dataObjectFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager
    ) {
        $this->helper = $helper;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->eventManager = $eventManager;
    }

    public function afterExecute(\Magento\Catalog\Controller\Category\View $controller, $page)
    {
        $catageory = $this->helper->getCoreRegistry()->registry('current_category');
        if ($customBlock = $catageory->getData('custom_content_block')) {
            $layout = $this->helper->getObjectManager()->get('Magento\Framework\View\Layout');
            $this->helper->getPageConfig()->addBodyClass('page-layout-1column');
            if ($listBlock = $layout->getBlock('category.products.list')) {
                $listBlock->setTemplate('Codazon_FurnitureLayout::category/custom-content.phtml')->setData(
                    'category', $catageory
                );
            }
        } else {
            $layout = $this->helper->getObjectManager()->get('Magento\Framework\View\Layout'); //$page->getLayout();
            $request = $controller->getRequest();
            $isAjax = $request->getParam(self::AJAX_PARAM);
            $pageLayout = $this->helper->getPageLayout();

            if ($pageLayout == 'f-layout-1' || $pageLayout == 'f-layout-2') {
                $listBlock = $layout->getBlock('category.products.list');
                //$swatchesBlock = $layout->getBlock('category.product.type.details.renderers.configurable');
                if ($listBlock) {
                    $listBlock->setTemplate('Codazon_FurnitureLayout::product/list.phtml');
                }
                //if ($swatchesBlock) {
                    $layout->unsetElement('category.product.type.details.renderers.configurable');
                //}
                if (!$isAjax) {
                    $ajaxCartConfig = $layout->getBlock('codazon_shopping_cart_config');
                    if ($ajaxCartConfig) {
                        $ajaxCartConfig->setTemplate('Codazon_FurnitureLayout::category/ajax-cart-config.phtml');
                    }
                    if ($furnitureBlock = $layout->getBlock('furniture_layout_config')) {
                        $furnitureBlock->setChild('category_products', 'category.products');
                    }
                }
                if ($isAjax) {
                    $result = [];
                    if ($block = $layout->getBlock('category.products.list')) {
                        $blockHtml = $block->toHtml();

                        /* Compatibility with Amasty_PageSpeedOptimizer */
                        $data = $this->dataObjectFactory->create(
                            [
                                'page' => $blockHtml,
                                // \Amasty\PageSpeedOptimizer\Model\Output\LazyLoadProcessor::PAGE_CONFIG keys
                                'pageType' => 'catalog_category_view'
                            ]
                        );
                        //if Amasty PageSpeedOptimizer is not installed output page will be the same
                        $this->eventManager->dispatch('amoptimizer_process_ajax_page', ['data' => $data]);
                        $blockHtml = $data->getData('page');
                        /* end */

                        $result['category_products'] = $blockHtml;
                    }
                    $json = \Magento\Framework\App\ObjectManager::getInstance()->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
                    $json->setData($result);
                    return $json;
                }
            } elseif ($pageLayout == 'category_landing_page') {
                $this->helper->getPageConfig()->addBodyClass('page-layout-1column');
                $layout->unsetElement('category.image');
                $layout->unsetElement('category.description');
                $landing = $layout->createBlock(\Codazon\FurnitureLayout\Block\Category\LandingPage::class, 'category_landing');
                $layout->setChild('content', 'category_landing', 'category_landing');
                $layout->createBlock(\Codazon\FurnitureLayout\Block\Category\Compare\ListCompare::class, 'category_comparison')
                    ->setTemplate('Codazon_FurnitureLayout::category/compare/footer-bar.phtml');
                $layout->setChild('content', 'category_comparison', 'category_comparison');
                $layout->setChild('content', 'category.cms', 'category.cms');
            }
        }
        return $page;
    }
}
