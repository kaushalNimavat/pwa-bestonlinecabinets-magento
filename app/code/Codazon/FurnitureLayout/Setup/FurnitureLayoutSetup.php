<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Setup;

use Magento\Eav\Setup\EavSetup;

class FurnitureLayoutSetup extends EavSetup
{
    
    public function getDefaultEntities()
    {
        $entities = array (
        );
        return $entities;
    }
}
