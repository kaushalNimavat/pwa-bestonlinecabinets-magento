<?php
/**
 * Copyright © 2019 Codazon, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Setup;

use Magento\Framework\Setup\{
    ModuleContextInterface,
    ModuleDataSetupInterface,
    InstallDataInterface
};

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class InstallData implements InstallDataInterface {
    
    private $eavSetupFactory;
    
    protected $objectManager;

    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory   ;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }
    
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        
        $this->addSpecialCategoryAttributes($eavSetup);
        $this->addCurrentSaleAttributes($eavSetup);
        
        $setup->endSetup();
    }
    
    protected function addCurrentSaleAttributes($eavSetup)
    {
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'cat_base_image', [
            'type'     => 'varchar',
            'label'    => 'Category Base Image',
            'input'    => 'media_image',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Current Sale',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'cat_thumbnail', [
            'type'     => 'varchar',
            'label'    => 'Category Thumbnail',
            'input'    => 'media_image',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Current Sale',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'alias_name', [
            'type'     => 'varchar',
            'label'    => 'Alias Name',
            'input'    => 'text',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Extra Attributes',
            'user_defined'   => true
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'catprice', [
            'type'     => 'decimal',
            'label'    => 'Category Price',
            'input'    => 'price',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Extra Attributes',
            'used_for_sort_by'  => true,
            'user_defined'   => true
        ]);
    }
    
    protected function addSpecialCategoryAttributes($eavSetup)
    {
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'f_cat_sample_link', [
            'type'     => 'varchar',
            'label'    => 'Category Sample Items Link',
            'input'    => 'text',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'f_cat_sample_label', [
            'type'     => 'varchar',
            'label'    => 'Category Sample Items Link Label',
            'input'    => 'text',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'f_category_gallery', [
            'type'     => 'text',
            'label'    => 'Category Gallery Images',
            'input'    => 'text',
            'visible'  => true,
            'default'  => '0',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Content',
        ]);
        
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'f_cat_notif_enable', [
            'type'     => 'int',
            'label'    => 'Enable Category Notification',
            'input'    => 'boolean',
            'visible'  => true,
            'default'  => '1',
            'required' => false,
            'source'   => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'f_cat_notif', [
            'type'     => 'text',
            'label'    => 'Category Special Notification',
            'input'    => 'text',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'f_category_tab_1', [
            'type'     => 'text',
            'label'    => 'Category Tab 1',
            'input'    => 'text',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'f_category_tab_2', [
            'type'     => 'text',
            'label'    => 'Category Tab 2',
            'input'    => 'text',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'f_category_tab_3', [
            'type'     => 'text',
            'label'    => 'Category Tab 3',
            'input'    => 'text',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'f_category_tab_4', [
            'type'     => 'text',
            'label'    => 'Category Tab 4',
            'input'    => 'text',
            'visible'  => true,
            'default'  => '',
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Content',
        ]);
    }
}