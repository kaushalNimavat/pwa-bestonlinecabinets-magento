<?php
/**
 * Copyright © 2017 Codazon, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Codazon\FurnitureLayout\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;

use Magento\Eav\Model\Entity\Attribute;
use Magento\Eav\Model\Entity\AttributeFactory;
use Magento\Eav\Model\AttributeManagement;
use Magento\Eav\Model\Entity\Attribute\Group;
use Magento\Eav\Model\Entity\Attribute\GroupFactory;
use Magento\Eav\Model\Entity\Attribute\Set;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Eav\Model\Entity\TypeFactory;

class UpgradeData implements UpgradeDataInterface {
    
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        AttributeFactory $attributeFactory,
        SetFactory $attributeSetFactory,
        GroupFactory $attributeGroupFactory,
        TypeFactory $typeFactory,
        AttributeManagement $attributeManagement
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $this->attributeFactory = $attributeFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->eavTypeFactory = $typeFactory;
        $this->attributeGroupFactory = $attributeGroupFactory;
        $this->attributeManagement = $attributeManagement;
    }
    
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $this->addCustomCategoryContentAttributes($eavSetup);
        $setup->endSetup();
    }
    
    protected function addCustomCategoryContentAttributes($eavSetup)
    {
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'custom_content_block', [
            'type'     => 'int',
            'label'    => 'Custom Block',
            'input'    => 'select',
            'visible'  => true,
            //'default'  => '1',
            'required' => false,
            //'source'   => 'Codazon\FurnitureLayout\Model\Config\Source\CmsBlocks',
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Custom Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'custom_content_js_files', [
            'type'     => 'varchar',
            'label'    => 'Custom Js Files',
            'input'    => 'text',
            'visible'  => true,
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Custom Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'custom_content_css_files', [
            'type'     => 'varchar',
            'label'    => 'Custom CSS Files',
            'input'    => 'text',
            'visible'  => true,
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Custom Content',
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'custom_content_inline_script', [
            'type'     => 'text',
            'label'    => 'Custom Inline Script',
            'input'    => 'textarea',
            'visible'  => true,
            'required' => false,
            'global'   => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group'    => 'Custom Content',
        ]);
    }
}