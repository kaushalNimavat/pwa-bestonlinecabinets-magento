<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Ui\Component\Form;

use Magento\Framework\View\Element\UiComponent\ContextInterface;  
use Magento\Framework\View\Element\UiComponentInterface;  
use Magento\Ui\Component\Form\FieldFactory;  
use Magento\Ui\Component\Form\Fieldset as BaseFieldset;
use Magento\Eav\Model\Entity\Type;
use Magento\Ui\Component\Form\Field;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\EavValidationRules;
use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as EavAttribute;
use Magento\Eav\Api\Data\AttributeInterface;

class CategoryFieldset extends BaseFieldset
{
    protected $fieldFactory;
    
    protected $objectManager;
    
    protected $request;

    protected $storeManager;
    
    protected $metaProperties = [
        'dataType' => 'frontend_input',
        'visible' => 'is_visible',
        'required' => 'is_required',
        'label' => 'frontend_label',
        'sortOrder' => 'sort_order',
        'notice' => 'note',
        'default' => 'default_value',
        'size' => 'multiline_count',
    ];
    
    protected $filterFields = [];

    protected $formElement = [
        'text'      => 'input',
        'boolean'   => 'checkbox',
        'price'     => 'input'
    ];
    
    protected $eavValidationRules;
    
    public function __construct(
        ContextInterface $context,
        array $components = [],
        array $data = [],
        EavValidationRules $eavValidationRules,
        \Magento\Framework\App\RequestInterface $request,
        FieldFactory $fieldFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context, $components, $data);
        $this->fieldFactory = $fieldFactory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->request = $request;
        $this->storeManager = $storeManager;
        $this->eavValidationRules = $eavValidationRules;
    }
    
    public function getDefaultMetaData($result)
    {
        $result['parent']['default'] = (int)$this->request->getParam('parent');
        $result['use_config.available_sort_by']['default'] = true;
        $result['use_config.default_sort_by']['default'] = true;
        $result['use_config.filter_price_range']['default'] = true;

        return $result;
    }

    public function getScopeLabel(EavAttribute $attribute)
    {
        $html = '';
        if (!$attribute || $this->storeManager->isSingleStoreMode()
            || $attribute->getFrontendInput() === AttributeInterface::FRONTEND_INPUT
        ) {
            return $html;
        }
        if ($attribute->isScopeGlobal()) {
            $html .= __('[GLOBAL]');
        } elseif ($attribute->isScopeWebsite()) {
            $html .= __('[WEBSITE]');
        } elseif ($attribute->isScopeStore()) {
            $html .= __('[STORE VIEW]');
        }

        return $html;
    }
    
    public function getAttributesMeta()
    {
        $entityType = $this->objectManager->create(
            \Magento\Eav\Model\Entity\Type::class
        )->loadByCode(
            \Magento\Catalog\Model\Category::ENTITY
        );
        
        $meta = [];
        
        $attributes = $entityType->getAttributeCollection()
            ->addFieldToFilter('is_user_defined', 1)
            ->addFieldToFilter('frontend_input', ['neq' => 'media_image']);
        if ($this->filterFields){
            foreach ($this->filterFields as $name => $condition) {
                $attributes->addFieldToFilter($name, $condition);
            }
        }
        
        /* @var EavAttribute $attribute */
        foreach ($attributes as $attribute) {
            $code = $attribute->getAttributeCode();
            // use getDataUsingMethod, since some getters are defined and apply additional processing of returning value
            foreach ($this->metaProperties as $metaName => $origName) {
                $value = $attribute->getDataUsingMethod($origName);
                $meta[$code][$metaName] = $value;
                if ('frontend_input' === $origName) {
                    $meta[$code]['formElement'] = isset($this->formElement[$value])
                        ? $this->formElement[$value]
                        : $value;
                }
                if ($attribute->usesSource()) {
                    $meta[$code]['options'] = $attribute->getSource()->getAllOptions();
                }
            }

            $rules = $this->eavValidationRules->build($attribute, $meta[$code]);
            if (!empty($rules)) {
                $meta[$code]['validation'] = $rules;
            }

            $meta[$code]['scopeLabel'] = $this->getScopeLabel($attribute);
            $meta[$code]['componentType'] = Field::NAME;
            if ($attribute->getFrontendInput() == 'boolean') {
                $meta[$code]['prefer'] = 'toggle';
                $meta[$code]['valueMap'] = ['true' => '1', 'false' => '0'];
            }
        }

        $result = [];
        foreach ($meta as $key => $item) {
            $result[$key] = $item;
            $result[$key]['sortOrder'] = 0;
        }

        //$result = $this->getDefaultMetaData($result);

        return $result;
    }
    
    public function getChildComponents()
    {
        $fields = $this->getAttributesMeta();

        foreach ($fields as $name => $fieldConfig) {
            $fieldInstance = $this->fieldFactory->create();
            $fieldInstance->setData(
                [
                    'config' => $fieldConfig,
                    'name' => $name
                ]
            );
            $fieldInstance->prepare();
            $this->addComponent($name, $fieldInstance);
        }

        return parent::getChildComponents();
        
    }
}