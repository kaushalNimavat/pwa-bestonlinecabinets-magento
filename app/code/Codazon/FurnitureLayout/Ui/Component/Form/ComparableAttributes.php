<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Ui\Component\Form;

class ComparableAttributes extends \Codazon\FurnitureLayout\Ui\Component\Form\CategoryFieldset
{
    protected $filterFields = [];
}