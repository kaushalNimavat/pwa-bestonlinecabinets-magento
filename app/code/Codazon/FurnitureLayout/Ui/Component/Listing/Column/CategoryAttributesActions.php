<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Ui\Component\Listing\Column;

class CategoryAttributesActions extends \Codazon\FurnitureLayout\Ui\Component\Listing\Column\AbstractActions
{
	/** Url path */
	protected $_editUrl = 'furniturelayout/categoryattributes/edit';
    /**
    * @var string
    */
	protected $_deleteUrl = 'furniturelayout/categoryattributes/delete';
    /**
    * @var string
    */
    protected $_primary = 'attribute_id';
}

