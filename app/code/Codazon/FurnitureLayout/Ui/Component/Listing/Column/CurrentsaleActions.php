<?php
/**
* Copyright © 2019 Codazon. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Codazon\FurnitureLayout\Ui\Component\Listing\Column;

class CurrentsaleActions extends \Codazon\FurnitureLayout\Ui\Component\Listing\Column\AbstractActions
{
	/** Url path */
	protected $_editUrl = 'furniturelayout/currentsale/edit';
    /**
    * @var string
    */
	protected $_deleteUrl = 'furniturelayout/currentsale/delete';
    /**
    * @var string
    */
    protected $_primary = 'entity_id';
}

