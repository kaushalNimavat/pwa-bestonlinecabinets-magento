/**
 * Copyright © Codazon, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'productGallery'
], function($) {
    
    $.widget('codazon.fCategoryGallery', $.mage.productGallery , {
        setBase: function (imageData) {
            var baseImage = this.options.types.cat_base_image,
                sameImages = $.grep(
                    $.map(this.options.types, function (el) {
                        return el;
                    }),
                    function (el) {
                        return el.value === baseImage.value;
                    }
                ),
                isImageOpened = this.findElement(imageData).hasClass('active');

            $.each(sameImages, $.proxy(function (index, image) {
                this.element.trigger('setImageType', {
                    type: image.code,
                    imageData: imageData
                });

                if (isImageOpened) {
                    this.element.find('.item').addClass('selected');
                    this.element.find('[data-role=type-selector]').prop({
                        'checked': true
                    });
                }
            }, this));
        }
    });
    return $.codazon.fCategoryGallery;
});