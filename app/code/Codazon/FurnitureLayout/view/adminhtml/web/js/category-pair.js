define([
    'underscore',
    'Magento_Ui/js/lib/view/utils/async',
    'mage/template',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract',
    'Magento_Ui/js/modal/confirm',
    'Magento_Ui/js/modal/alert',
    'Codazon_FurnitureLayout/js/category-chooser-keep-state',
    'jquery/ui',
    'mage/calendar'
], function (_, $, mageTemplate, rg, Abstract, confirm, alert, categoryChooser) {
    $.widget('codazon.categoryPairBuilder', {
        options: {
            itemTmpl: '#category-pair-tmpl'
        },
        _create: function () {
            this._assignVariables();
            this._bindEvents();
            this._prepareHtml();
        },
        _assignVariables: function () {
            var self = this, conf = this.options;
            this.$map = $('[role="map"]', this.element);
            this.itemTmpl = mageTemplate(conf.itemTmpl);
            this.tempCategoryId = 'cdz_temp_category';
            this.$tempCategory = $('<input type="text" id="' + this.tempCategoryId + '" />').appendTo('body').hide();
            this.$tempCategoryName = $('<div id="' + this.tempCategoryId + '_label">').appendTo('body').hide();
            this.$form = $('[role="data_form"]', this.element).first();
            this.$mapForm = $('[role="map_form"]', this.element).validation();
            window[this.tempCategoryId] = new categoryChooser(
                this.tempCategoryId,
                codazon.catChooserUrl + 'uniq_id/' + this.tempCategoryId,
                {
                    "buttons":  {"open" : "Select Category...", "close": "Close"}
                }
            );
            this.categoryChooser = window[this.tempCategoryId];
            this.$focusedCat = null;
        },
        _prepareHtml: function() {
            var self = this, conf = this.options;
            if (conf.catData.length) {
                $.each(conf.catData, function(i, pair) {
                    self._addRow(pair.id, pair.cat1, pair.cat2);
                });
            } else {
                self._addRow();
            }
        },
        _bindEvents: function () {
            var self = this, conf = this.options;
            this.element.on('click', '[role="add"]', function (e) {
                e.preventDefault();
                self._addRow(null, null, null);
            }).on('click', '[role="remove"]', function (e) {
                e.preventDefault();
                var $btn = $(this), $item = $btn.parents('[role="item"]').first();
                $item.fadeOut(300, 'linear', function() {
                    $item.remove();
                });
            }).on('click', '[role="choose_cat"]', function (e) {
                e.preventDefault();
                self.$focusedCat = $(this).parents('[role="cat"]').first();
                self._chooseCategory();
            }).on('click', '[role="save"]', function (e) {
                e.preventDefault();
                self._saveProccess();
            }).on('click', '[role="remove_all"]', function (e) {
                e.preventDefault();
                self._removeAll();
            });
            this.$tempCategory.on('updateValue', function(e, catId) {
                if ($('[role="cat_id"][value="' + catId + '"]', self.element).length) {
                    self.$focusedCat = false;
                    alert({
                        content: conf.catExistMsg
                    });
                    return false;
                }
                if (self.$focusedCat) {
                    self.$focusedCat.find('[role="cat_id"]').val(catId).attr('value', catId);
                    self.$focusedCat.find('[role="cat_id_label"]').html(catId);
                }
           }).on('updateLabel', function(e, catName) {
               if (self.$focusedCat) {
                   self.$focusedCat.find('[role="cat_name"]').html(catName);
                   self.$focusedCat = false;
               }
           });
        },
        _removeAll: function() {
            var self = this, conf = this.options;
            confirm({
                title: 'Warning',
                content: 'Are you sure?',
                actions: {
                    confirm: function () {
                        self.$map.empty();
                    }
                }
            })
        },
        _chooseCategory: function() {
            this.categoryChooser.choose();
        },
        _addRow: function(id, cat1, cat2) {
            var self = this, conf = this.options;
            if (!id) id = 0;
            if (!cat1) cat1 = {id: '', name: ''};
            if (!cat2) cat2 = {id: '', name: ''};
            $(self.itemTmpl({
                id: id,
                cat1: cat1,
                cat2: cat2,
                widget: self,
            })).appendTo(self.$map);
        },
        uniqid: function() {
            return Math.random().toString().substr(2,6);
        },
        _saveProccess: function() {
            var self = this, conf = this.options;
            var data = this._getJsonData();
            if (this.$mapForm.validation('isValid')) {
                $('[name="cat_data"]', this.$form).val(JSON.stringify(data));
                $('body').data('loader').show();
                this.$form.submit();
            }
        },
        _getJsonData: function() {
            var data = [];
            $('[role="item"]', this.element).each(function(id, el) {
                var $item = $(this);
                var itemData = {};
                $('[data-name]', $item).each(function() {
                    var $input = $(this), name = $input.data('name'), value = $input.val();
                    itemData[name] = value;
                });
                data.push(itemData);
            });
            return data;
        }
    });
    return $.codazon.categoryPairBuilder;
});