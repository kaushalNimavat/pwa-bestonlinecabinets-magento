define([
    'underscore',
    'Magento_Ui/js/lib/view/utils/async',
    'mage/template',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract',
    'Magento_Ui/js/modal/confirm',
    'Magento_Ui/js/modal/alert',
    'Codazon_FurnitureLayout/js/category-chooser',
    'jquery/ui',
    'mage/calendar'
], function (_, $, mageTemplate, rg, Abstract, confirm, alert, categoryChooser) {
   if (typeof window.czdDatepicker == 'undefined') {
        window.cdzGetUid = function() {
           return Math.random().toString().substr(2,6);
        }
        window.czdDatepicker = function(input) {
           var $input = $(input);
           if (!$input.data('datepicker')) {
               $input.data('datepicker', true);
               $.mage.calendar({
                   showsTime: false,
                   showHour: false,
                   showMinute: false
               }, $input);
           }
        };
        window.cdzEditCategoryField = function(label) {
            var $label = $(label);
            var $wrap = $label.parents('[data-role="category_chooser_wrapper"]').first(),
            $valueWrap = $wrap.find('[data-role="value_wrap"]').first();
            $label.hide();
            $valueWrap.show();
        }
        window.cdzOpenCategoryChooser = function(a) {
            var $a = $(a);
            var $wrap = $a.parents('[data-role="category_chooser_wrapper"]').first(),
            $input = $wrap.find('[data-role="category_value"]').first(), $chooser = $wrap.find('[data-role="category_chooser"]').first(), selected = $input.val(),
            $label = $wrap.find('[data-role="category_label"]').first(), $valueWrap = $wrap.find('[data-role="value_wrap"]').first();
            if ($chooser.hasClass('active')) {
                $input.val(selected.replace(/ /g, ''));
                $chooser.removeClass('active').hide();
                return false;
            }
            var ajaxUrl = codazon.catIdsChooserUrl;
            if (!$wrap.attr('id')) {
                $wrap.attr('id', 'category_chooser_wrapper_' + cdzGetUid());
            }
            var wrapId = $wrap.attr('id');
            var data = {};
            data.form_key = $('input[name="form_key"]').first().val();
            if (selected) {
                data.selected = selected.split(',');
            }
            $wrap.get(0).updateElement = $input.get(0);
            $.ajax({
                url: ajaxUrl + 'form/' + wrapId,
                method: 'post',
                data: data,
                showLoader: true,
                success: function(rs) {
                    $chooser.html(rs);
                    $('body').trigger('contentUpdated');
                    $chooser.addClass('active').show();
                }
            });
            return false;
        };
        window.cdzApplyCategoryChooser = function(a) {
            var $a = $(a);
            var $wrap = $a.parents('[data-role="category_chooser_wrapper"]').first(),
            $input = $wrap.find('[data-role="category_value"]').first(), $chooser = $wrap.find('[data-role="category_chooser"]').first(), selected = $input.val(),
            $label = $wrap.find('[data-role="category_label"]').first(), $valueWrap = $wrap.find('[data-role="value_wrap"]').first();
            selected = selected.replace(/ /g, '');
            $input.val(selected);
            $chooser.removeClass('active').hide();
            $label.text(selected ? selected : '...').show();
            $valueWrap.hide();
            return false;
        }
   }
   $.widget('codazon.currentSaleLayout', {
       _create: function() {
           var self = this, conf = this.options;
           this._assignVariables();
           this._prepareHtml();
           this._bindEvents();
       },
       _assignVariables: function() {
            var self = this, conf = this.options;
            this.$input = conf.input;
            this.initData = this.$input.val() ? JSON.parse(this.$input.val()) : {};
            this.groupTmpl = mageTemplate('#cdz-category-group-tmpl');
            this.subCatListTmpl = mageTemplate('#cdz-sub-cat-list-tmpl');
            this.multiTextTmpl = mageTemplate('#cdz-multi-text-tmpl');
            this.$groups = self.element.find('[data-role="groups"]').first();
            
            this.tempCategoryId = 'cdz_temp_category';
            this.$tempCategory = $('<input type="text" id="' + this.tempCategoryId + '" />').appendTo('body').hide();
            this.$tempCategoryName = $('<div id="' + this.tempCategoryId + '_label">').appendTo('body').hide();
            
            window[this.tempCategoryId] = new categoryChooser(
                this.tempCategoryId,
                codazon.catChooserUrl + 'uniq_id/' + this.tempCategoryId,
                {
                    "buttons":  {"open" : "Select Category...", "close":"Close"}
                }
            );
            this.categoryChooser = window[this.tempCategoryId];
            //this.$tempCategory.get(0).advaiceContainer = this.tempCategoryId + "advice-container";
       },
       _prepareHtml: function() {
           var self = this, conf = this.options;
           if (this.initData.groups) {
               self._loadJson(this.initData);
           } else {
               self._addEmtyGroup();
           }
       },
       _addEmtyGroup: function(data) {
           this._addGroup({
               cat_name: 'No select',
               cat_id: 0,
               config_fields: this._getGroupConfigFields(data),
               widget: this
           });
       },
       _loadJson: function(initData) {
           var self = this, conf = this.options;
           $.each(initData.groups, function(i, group) {
               var groupId = group.cat_id;
               if (groupId) {
                   var parentcat = codazon.availableChildren[groupId];
                   var $group = self._addGroup({
                       cat_name: parentcat.cat_name,
                       cat_id: parentcat.cat_id,
                       config_fields: self._getGroupConfigFields(group.options),
                       widget: self
                   });
                   if (parentcat.children) {
                        self._appendSubCatListToGroup($group, parentcat.children);
                   }
               } else {
                   self._addEmtyGroup(group.options);
               }
           });
       },
       _getGroupConfigFields: function(data) {
           var self = this, conf = this.options;
           var fields = JSON.parse(JSON.stringify(codazon.groupOptionFields));
           if (typeof data == 'object') {
               $.each(fields, function(name, field) {
                    if (field.type == 'multi_text') {
                        fields[name].value = {};
                        if (typeof data[name] != 'string') {
                            $.each(data[name], function(store_id, value) {
                                fields[name].value[store_id] = data[name][store_id];
                            });
                        } else {
                            fields[name].value[0] = data[name];
                        }
                    } else {
                        fields[name].value = data[name];
                    }
               });
           }
           return fields;
       },
       _bindEvents: function() {
           var self = this, conf = this.options;
           this.$groups.sortable({
               handle: '.group-header'
           });
           self.element.on('click', '[data-role="add-group"]', function() {
               self._addEmtyGroup();
           });
           self.element.on('click', '[data-role="choose-group-category"]', function() {
                var $button = $(this);
                self.$focusedCat = $button.parents('[data-role="cat-group"]').first();
                self._chooseCategory($button);
           });
           self.$tempCategory.on('updateValue', function(e, catId) {
               if (self.$focusedCat) {
                   self.$focusedCat.attr('data-id', catId);
                   self._loadSubCategories(self.$focusedCat, catId);
               }
           });
           self.$tempCategory.on('updateLabel', function(e, catName) {
               if (self.$focusedCat) {
                   self.$focusedCat.find('[data-role="group-cat-name"]').html(catName);
                   self.$focusedCat = false;
               }
           });
           self.element.on('click', '[data-role="edit-group-options"]', function(e) {
                var $button = $(this), $parent = $button.parents('[data-role="cat-group"]'),
                $options = $parent.find('[data-role="group-config"]').first();
                $options.fadeToggle(200, 'swing', function() {
                    $options.toggleClass('open');
                    $button.toggleClass('open');
                });
           });
           self.element.on('click', '[data-role="toggle-children"]', function(e) {
                var $button = $(this), $parent = $button.parents('[data-role="cat-group"]'),
                $options = $parent.find('[data-role="group-children"]').first();
                $options.fadeToggle(200, 'swing', function() {
                    $options.toggleClass('open');
                    $button.toggleClass('open');
                });
           });
           self.element.on('click', '[data-role="delete-group-category"]', function(e) {
                var $button = $(this), $parent = $button.parents('[data-role="cat-group"]');
                confirm({
                    content: 'Are you sure?',
                    actions: {
                        confirm: function() {
                            $parent.remove();
                        }
                    }
                });
           });
           $('body').on('processStart', function(e) {
                var json = self._toJson();
                self.$input.val(JSON.stringify(json)).trigger('change');
           });
           
           self.element.on('change', '[data-show_in_overview="1"]', function(e) {
                var $input = $(this), value = $input.val(), name = $input.data('name');
                var $parent = $input.parents('[data-role="cat-group"]').first();
                if ($input.parents('[data-type]').first().data('type') == 'select') {
                    value = $input.find('option[value="'+value+'"]').html();
                    $parent.find('[data-overview="' + name + '"]').text(value);
                } else {
                    $parent.find('[data-overview="' + name + '"]').text(value);
                }
                $parent.find('[data-overview="' + name + '"]').parents('[data-value]').first().attr('data-value', value);
                
           });
       },
       _toJson: function() {
            var self = this, conf = this.options;
            var json = {};
            json.groups = [];
            self.element.find('[data-role="cat-group"]').each(function() {
                var $group = $(this)
                var group = {};
                group.cat_id = parseInt($group.attr('data-id'));
                group.options = {};
                group.subcats = [];
                
                $group.find('[data-role="group-config"] [data-fieldname]').each(function() {
                    var $field = $(this);
                    var name = $field.data('fieldname');
                    if ($field.data('type') == 'multi_text') {
                        group.options[name] = {};
                        $field.find('input[data-store_id]').each(function() {
                            var $input = $(this);
                            group.options[name][$input.data('store_id')] = $input.val();
                        });
                    } else {
                        var $input = $field.find('[data-name=' + name + ']');
                        if ($field.data('type') == 'checkbox') {
                            group.options[name] = $input.is(':checked');
                        } else {
                            group.options[name] = $input.val();
                        }
                    }
                });
                $group.find('[data-role="sub-cat-item"]').each(function() {
                    var $subcat = $(this);
                    if ($subcat.find('[data-name="active"]').is(':checked')) {
                        group.subcats.push($subcat.data('catid'));
                    }
                });
                json.groups.push(group);
            });
            return json;
       },
       _loadSubCategories: function($group, catId) {
            var self = this, conf = this.options;
            $.ajax({
                url: codazon.catchildrenUrl,
                data: {cat_id: catId},
                type: 'get',
                showLoader: true,
                success: function(rs) {
                    if (rs.categories) {
                        self._appendSubCatListToGroup($group, rs.categories);
                        $group.find('[data-role="group-children"]').addClass('open').show();
                        $group.find('[data-role="toggle-children"]').addClass('open');
                    }
                }
            });
       },
       _appendSubCatListToGroup($group, categories) {
           var self = this, conf = this.options;
           $(this.subCatListTmpl({
               categories: categories
           })).appendTo($group.find('[data-role="group-children"]').empty());
       },
       _chooseCategory: function($button) {
           var self = this, conf = this.options;
           this.categoryChooser.choose();
       },
       _getMultiTextInput: function(field) {
           var self = this, conf = this.options;
           return self.multiTextTmpl({
               name: field.name,
               value: field.value,
               show_in_overview: field.show_in_overview
           });
       },
       _addGroup: function(data) {
           var self = this, conf = this.options;
           var $group = $(this.groupTmpl(data)).appendTo(this.$groups);
           return $group;
       }
   });
    
   return Abstract.extend({
        defaults: {
            elementId: 0,
            prefixName: '',
            prefixElementName: '',
            elementName: '',
            value: '',
            uploadUrl: ''
        },
        initialize: function () {
            this._super().initOldCode();
            return this;
        },
        initConfig: function () {
            this._super();
            return this;
        },
        initOldCode: function(){
            var self = this;
            $.async('#' + this.uid, function(input) {
                var $input = $(input);
                var $parent = $input.parents('[data-role="curent-sale-layout"]').first();
                var options = {
                    input: $input,
                    elementName: self.inputName
                };
                $.codazon.currentSaleLayout(options, $parent);
            });
        }
   });
});