define([
    'jquery',
    'jquery-ui-modules/widget',
    'mage/translate'
], function ($, jUi, $t) {
    'use strict';
    $.widget('codazon.productListBundle', {
        options: {
            formKeyInputSelector: '.column.main > input[name="form_key"]',
            selectorProduct: '.product-item',
            selectorProductPrice: '[data-role=priceBox]',
            mediaGalleryInitial: [{}],
        },
        _create: function() {
            var self = this, conf = this.options;
            if (conf.isLazy) {
                this._lazyPrepare();
            } else {
                this._configBundleOptions();
            }
        },
        _configBundleOptions: function() {
            var self = this, conf = this.options;
            var $form = this.element.parents('form').first();
            require(['jquery', 'priceBox'], function() {
                var priceBoxes = $('[data-role=priceBox]', $form);
                priceBoxes = priceBoxes.filter(function(index, elem){
                    return !$(elem).find('.price-from').length;
                });
                priceBoxes.priceBox(
                    $form.data('bundlepriceconfig')
                );
                $form.removeAttr('data-bundlepriceconfig');
                var id = $form.attr('id');
                require(['priceBundle', 'Magento_Catalog/js/validate-product'], function() {
                    $.mage.priceBundle({
                        optionConfig: conf.optionConfig,
                        controlContainer: conf.controlContainer
                    }, $form);
                    $.mage.productValidate({}, $form);
                });
            });
        },
        _ajaxLoadOptions: function() {
            var self = this, conf = this.options;
            if (conf.ajaxUrl) {
                self.$optionsToggle.attr('disabled', 'disabled');
                $.ajax({
                    url: conf.ajaxUrl,
                    type: 'get',
                    cache: true,
                    success: function(rs) {
                        if (rs) {
                            self.$optionsContent.html(rs).hide();
                            var nOpt = self.$optionsContent.find('[role="option_item"]').length;
                            var iHiddenOpt = 0;
                            self.$optionsContent.find('[role="option_item"]').each(function() {
                                var $field = $(this);
                                var noChangeOption = ($field.find('[name^="bundle_option["][type=hidden]').length > 0);
                                var noChangeQty = ($field.find('.qty-disabled').length > 0);
                                if (noChangeOption && noChangeQty) {
                                    $field.hide();
                                    iHiddenOpt++;
                                }
                            });
                            self.$optionsToggle.removeAttr('disabled');
                            var $form = self.$optionsContent.find('form');
                            self.element.trigger('ajaxloaded', [$form]);
                            $('body').trigger('contentUpdated');
                            if (iHiddenOpt < nOpt) {
                                self.$optionsToggle.removeClass('hidden');
                                self.$optionsContent.css('display', '');
                            } else {
                                var formData = null;
                                var it = setInterval(function() {
                                    formData = $form.data('scommerceCatalogAddToCart');
                                    formData = formData ? formData : $form.data('codazonCatalogAddToCart');
                                    formData = formData ? formData : $form.data('mageValidation');
                                    if (formData) {
                                        clearInterval(it);
                                        $form.trigger('submit');
                                        $form.trigger('ajax:beginAddtoCart');
                                    }
                                }, 50);
                            }
                        }
                    }
                });
            }
        },
        _lazyPrepare: function() {
            var self = this, conf = this.options;
            this.$optionsToggle = this.element.find('[data-role="options_toggle"]').first();
            this.$optionsContent = this.element.find('[data-role="options_content"]').first();
            this.$optionsToggle.on('click', function() {
                if (!self.$optionsContent.data('loaded')) {
                    self.$optionsContent.slideDown(300);
                    self.$optMsg.remove();
                    self._ajaxLoadOptions();
                    self.$optionsContent.data('loaded', true);
                } else {
                    self.$optionsContent.slideToggle(300);
                }
            });
            this.$optMsg = self.element.find('[data-role="option_msg"]');
            this.$form = self.element.parents(conf.selectorProduct).first().find('form[data-role="tocart-form"]');
            this.$form.each(function() {
                var formData = null, bundleFormData = null;
                var $form = $(this);
                var it = setInterval(function() {
                    formData = $form.data('scommerceCatalogAddToCart');
                    formData = formData ? formData : $form.data('codazonCatalogAddToCart');
                    formData = formData ? formData : $form.data('mageCatalogAddToCart');
                    if (formData) {
                        clearInterval(it);
                        formData.oldSubmitForm = formData.submitForm;
                        formData.submitForm = function() {
                            //self.$optMsg.show();
                            self.$optionsToggle.trigger('click');
                        }
                        self.element.on('ajaxloaded', function(e, $bundleForm) {
                            formData.submitForm = function() {
                                $bundleForm.trigger('submit');
                                if ($bundleForm.valid()) {
                                    formData.disableAddToCartButton($form);
                                }
                            }
                            $bundleForm.find('[name="form_key"]').each(function() {
                                var formKey = $(conf.formKeyInputSelector).first().val();
                                $(this).val(formKey);
                            });
                            $bundleForm.on('ajax:beginAddtoCart', function() {
                                formData.disableAddToCartButton($form);
                            });
                            
                            var it1 = setInterval(function() {
                                bundleFormData = $bundleForm.data('scommerceCatalogAddToCart');
                                bundleFormData = bundleFormData ? bundleFormData : $form.data('codazonCatalogAddToCart');
                                bundleFormData = bundleFormData ? bundleFormData : $form.data('mageCatalogAddToCart');
                                if (bundleFormData) {
                                    clearInterval(it1);
                                    bundleFormData.oldEnableAddToCartButton = bundleFormData.enableAddToCartButton;
                                    bundleFormData.enableAddToCartButton = function() {
                                        bundleFormData.oldEnableAddToCartButton($form);
                                        formData.enableAddToCartButton($form);
                                    }
                                }
                            }, 10);
                            
                            $(document).on('ajax:addToCart', function(e, data) {
                                if (data.form.is($bundleForm)) {
                                    formData.enableAddToCartButton($form);
                                }
                            });
                            var currentQty = $('[name=qty]', $form).val();
                            currentQty = currentQty ? currentQty : 1;
                            $bundleForm.find('[name=qty]').val(currentQty);
                            $('[name=qty]', $form).on('change', function() {
                                var qty = $(this).val();
                                $('[name=qty]', $bundleForm).val(qty).trigger('change');
                            });
                        });
                    }
                }, 500);
            });
        },
    });
    return $.codazon.productListBundle;
});