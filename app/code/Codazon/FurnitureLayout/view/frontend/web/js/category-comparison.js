/**
 * Copyright © 2019 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'underscore',
    'jquery',
    'mage/template',
    'jquery-ui-modules/widget',
    'Magento_Ui/js/modal/modal'
], function(_, $, mageTemplate, ui) {
     $.widget('codazon.categoryComparison', {
         _create: function(){
             var self = this, conf = this.options;
             this._setupAll();
         },
         _setupAll: function() {
            var self = this, conf = this.options;
            $.ajax({
                 url: conf.loadItemsUrl,
                 type: 'post',
                 success: function(rs) {
                    if (rs && (typeof rs.items != 'undefined')) {
                        conf.items = rs.items;
                    } else {
                        conf.items = [];
                    }
                    self._assignVariables();
                    self._prepareHtml();
                    self._bindEvents();
                 }
             });
         },
         _assignVariables: function() {
             var self = this, conf = this.options;
             this.itemsTemplate = mageTemplate(conf.itemsTemplate);
             this.overMsgTemplate = mageTemplate(conf.overMsgTemplate);
             this.compTableTemplate = mageTemplate(conf.compTableTemplate);
             this.$items = self.element.find('[data-role="comparison_items"]');
             this.$viewBtn = self.element.find('[data-role="view_comparison_list"]');
             this.$clearBtn = self.element.find('[data-role="clear_compare_items"]');
             this.paddingBottom = 100 * conf.imageSize.height/conf.imageSize.width;
             this.$msgContainer = $('<div class="cdz-msg-container fixed">').appendTo('body');
             this.$compModal = $('<div class="cat-comp-table-wrap">').appendTo('body');
             this.$overModal = $(this.overMsgTemplate(conf)).appendTo('body');
             this.maxItems = parseInt(conf.maxItems);
             this.$barToggle = self.element.find('[data-role="bar-toggle"]').first();
             this.$barContent = self.element.find('[data-role="bar-content"]').first();
             this.$compModal.modal({
                 modalClass: 'cat-comparision-modal',
                 buttons: [],
                 innerScroll: true,
             });
             this.$overModal.modal({
                 modalClass: 'comp-over-modal',
                 buttons: [],
             });
         },
          _bindEvents: function() {
            var self = this, conf = this.options;
            this._buildCompare();
            this._makeupCompToggle(conf.items);
            if (!$('body').data('bindCatComparison')) {
                $('body').data('bindCatComparison', true);
                $('body').on('contentUpdated', function() {
                    setTimeout(function() {
                        self._buildCompare();
                        self._makeupCompToggle(conf.items);
                    }, 1000);
                });
            }
            this.$barToggle.on('click', function() {
                self.$barContent.slideToggle(300, 'swing', function() {
                    self.element.toggleClass('close');
                    self.$barToggle.toggleClass('close');
                });
            });
         },
         _prepareHtml: function() {
             var self = this, conf = this.options;
             self._buildComparisonList(conf.items);
         },
         _buildComparisonList: function(items){
             var self = this, conf = this.options;
             self.$barContent.show();
             self.$barToggle.removeClass('close');
             self.element.removeClass('close');

             $(this.itemsTemplate({
                 items: items,
                 paddingBottom: this.paddingBottom
             })).appendTo(this.$items.empty());
             if (items.length == 0) {
                 self.element.slideUp(300);
             } else {
                 self.element.slideDown(300);
             }
         },
         _printHtml: function(html) {
            var $content = $('<div>').addClass('fly-print-content').html(html).appendTo('body');
            $('body').addClass('fly-printing');
            setTimeout(function() {
               window.print();
               setTimeout(function() {
                   $('body').removeClass('fly-printing');
                   $content.remove();
               }, 10);
            }, 10);
        },
         _buildCompare: function() {
            var self = this, conf = this.options;
            $('[data-viewcomplist]').each(function() {
                var $button = $(this);
                $button.removeAttr('data-viewcomplist');
                $button.on('click', function(e) {
                    e.preventDefault();
                    self._ajaxViewCategoryComparisonList();
                });
            });
            $('[data-clearcomplist]').each(function() {
                var $button = $(this);
                $button.removeAttr('data-clearcomplist');
                $button.on('click', function(e) {
                    e.preventDefault();
                    self._ajaxClearCategoryComparisonList();
                });
            });
            $('[data-rmcatcompitem]').each(function() {
                var $button = $(this);
                var catId = $button.data('rmcatcompitem');
                $button.removeAttr('data-rmcatcompitem');
                $button.on('click', function(e) {
                    e.preventDefault();
                    self._ajaxRemoveItemFromCategoryComparisonList(catId, $button);
                });
            });
            $('[data-catcomparetoggle]').each(function() {
                var $toggle = $(this);
                var catId = $toggle.data('catcomparetoggle');
                $toggle.removeAttr('data-catcomparetoggle');
                $toggle.attr('data-compcatid', catId);

                $toggle.on('change', function(e) {
                    e.preventDefault();
                    if (self._isAddToCompareAction($toggle)) {
                        self._ajaxAddItemToCategoryComparisonList(catId, $toggle);
                    } else {
                        self._ajaxRemoveItemFromCategoryComparisonList(catId, $toggle);
                    }
                });
            });
        },
        _makeupCompToggle: function(items){
            $('[data-compcatid]').each(function(i, el) {
                var $toggle = $(this);
                var id = $toggle.attr('id');
                var catId = parseInt($toggle.data('compcatid'));
                if (!id) {
                    id = 'compare-toggle-' + catId + '-' + i;
                    $toggle.attr('id', id);
                }
                $label = $toggle.next();
                if ($label.length) {
                    if ($label.prop('tagName').toLowerCase() == 'label') {
                        $label.attr('for', id);
                    }
                }

                $toggle.removeAttr('checked');
                $.each(items, function(i, item) {
                    if (item.category == catId) {
                        $toggle.attr('checked', 'checked');
                        return false;
                    }
                });
            });
        },
        _isAddToCompareAction: function($toggle) {
            return $toggle.is(':checked');
        },
        _displayComparisonTable: function(data) {
            var self = this, conf = this.options;
            this.$compModal.html(this.compTableTemplate(data));
            this.$overModal.modal('closeModal');
            this.$compModal.modal('openModal');
            this.$compModal.find('[data-print]').on('click', function() {
                var $content = $($(this).data('print'));
                if ($content.length) {
                    self._printHtml($content.html());
                }
            });
            this.$compModal.find('[data-exportcomplistpdf]').on('click', function(e) {
                e.preventDefault();
                var postData = data;
                var $form = $('<form method="post">');
                $form.attr('action', conf.exportPDFUrl).hide().appendTo('body');
                $form.append($('<input type="hidden" name="data">').val(JSON.stringify(postData)));
                $form.append($('<input type="hidden" name="file_name">').val(conf.pDFFileName));
                $form.submit();
            });
            this.$compModal.find('[data-opencomplistpdf]').on('click', function(e) {
                e.preventDefault();
                var postData = data;
                var $form = $('<form method="get" target="_blank">');
                $form.attr('action', conf.exportPDFUrl).hide().appendTo('body');
                $form.append($('<input type="hidden" name="file_name">').val(conf.pDFFileName));
                $form.append($('<input type="hidden" name="only_view">').val('1'));
                $form.submit();
            });
        },
        _ajaxViewCategoryComparisonList: function() {
            var self = this, conf = this.options;
            var postData = {};
            postData.isAjax = true;
            $.ajax({
                url: conf.comparisonListUrl,
                type: 'POST',
                data: postData,
                showLoader: true,
                success: function(rs) {
                    self._displayComparisonTable(rs.data);
                }
            });
        },
        _ajaxClearCategoryComparisonList: function() {
            var self = this, conf = this.options;
            var postData = {};
            postData.isAjax = true;
            $.ajax({
                url: conf.clearCompareUrl,
                type: 'POST',
                data: postData,
                showLoader: true,
                success: function(rs) {
                    self._updateCompare(rs);
                }
            });
        },
        _ajaxAddItemToCategoryComparisonList: function(catId, $toggle) {
            var self = this, conf = this.options;
            if (conf.items.length >= this.maxItems) {
                this.$overModal.modal('openModal');
                this._makeupCompToggle(conf.items);
                return false;
            }
            var postData = {};
            postData.category = catId;
            postData.isAjax = true;
            $.ajax({
                url: conf.addToCompareUrl,
                type: 'POST',
                data: postData,
                showLoader: true,
                success: function(rs) {
                    self._updateCompare(rs);
                }
            });
        },
        _ajaxRemoveItemFromCategoryComparisonList: function(catId, $toggle) {
            var self = this, conf = this.options;
            var postData = {};
            postData.category = catId;
            postData.isAjax = true;
            $.ajax({
                url: conf.removeFromCompareUrl,
                type: 'POST',
                data: postData,
                showLoader: true,
                success: function(rs) {
                    self._updateCompare(rs);
                }
            });
        },
        _updateCompare: function(rs) {
            var self = this, conf = this.options;
            self._buildComparisonList(rs.items);
            self._buildCompare();
            self._makeupCompToggle(rs.items);
            if (rs.message) {
                self._displayMessages([{
                    type: (rs.success ? 'success' : 'error'),
                    text: rs.message
                }]);
            }
            conf.items = rs.items;
        },
        _displayMessages: function(messages) {
            var self = this;
            self.$msgContainer.show();
            $.each(messages, function(i, msg) {
                var $message = $('<div class="message cdz-translator">').addClass(msg.type).html('<span>' + msg.text + '</span>').prependTo(self.$msgContainer);
                setTimeout(function() {
                    $message.fadeOut(2000, 'swing', function() {
                        $message.remove();
                        if (!self.$msgContainer.children().length) {
                            self.$msgContainer.hide();
                        }
                    });
                }, 3000);
            });
        }
     });
     return $.codazon.categoryComparison;
});
