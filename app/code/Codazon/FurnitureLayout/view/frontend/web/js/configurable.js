/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * @api
 */
define([
    'jquery',
    'jquery-ui-modules/widget'
], function ($, jUi) {
    'use strict';

    $.widget('codazon.productListConfigurable', {
         options: {
            superSelector: '.super-attribute-select',
            selectSimpleProduct: '[name="selected_configurable_option"]',
            priceHolderSelector: '.price-box',
            spConfig: {},
            state: {},
            priceFormat: {},
            optionTemplate: '<%- data.label %>' +
            '<% if (typeof data.finalPrice.value !== "undefined") { %>' +
            ' <%- data.finalPrice.formatted %>' +
            '<% } %>',
            mediaGallerySelector: '[data-gallery-role=gallery-placeholder]',
            mediaGalleryInitial: null,
            slyOldPriceSelector: '.sly-old-price',
            normalPriceLabelSelector: '.normal-price .price-label',
            gallerySwitchStrategy: 'replace',
            tierPriceTemplateSelector: '#tier-prices-template',
            tierPriceBlockSelector: '[data-role="tier-price-block"]',
            tierPriceTemplate: '',
            selectorProduct: '.product-item',
            selectorProductPrice: '[data-role=priceBox]',
            mediaGalleryInitial: [{}],
        },
        _create: function() {
            var self = this, conf = this.options;
            if (conf.isLazy) {
                this._lazyPrepare();
            } else {
                require([
                    'underscore',
                    'mage/template',
                    'mage/translate',
                    'priceUtils',
                    'validation',
                    'priceBox',
                    'jquery/jquery.parsequery'
                ], function( _, mageTemplate, $t, priceUtils, validation) {
                    self._prepareHtml();
                    self._assignVariables();
                    self._bindEvents();
                });
            }
        },
        _lazyPrepare: function() {
            var self = this, conf = this.options;
            this.$optionsToggle = this.element.find('[data-role="options_toggle"]').first();
            this.$optionsContent = this.element.find('[data-role="options_content"]').first();
            this.$optionsToggle.on('click', function() {
                if (!self.$optionsContent.data('loaded')) {
                    self.$optionsContent.slideDown(300);
                    self.$optMsg.remove();
                    self._ajaxLoadOptions();
                    self.$optionsContent.data('loaded', true);
                } else {
                    self.$optionsContent.slideToggle(300);
                }
            });
            this.$optMsg = self.element.find('[data-role="option_msg"]');
            this.$form = self.element.parents(conf.selectorProduct).first().find('form[data-role="tocart-form"]');
            
            this.$form.each(function() {
                var formData = null;
                var $form = $(this);
                var it = setInterval(function() {
                    formData = $form.data('codazonCatalogAddToCart');
                    if (!formData) {
                        formData = $form.data('mageCatalogAddToCart');
                    }
                    if (formData) {
                        clearInterval(it);
                        formData.oldSubmitForm = formData.submitForm;
                        formData.submitForm = function() {
                            self.$optMsg.show();
                        }
                        self.element.on('ajaxloaded', function() {
                            formData.submitForm = formData.oldSubmitForm;
                        });
                    }
                }, 500);
            });
        },
        _ajaxLoadOptions: function() {
            var self = this, conf = this.options;
            if (conf.ajaxUrl) {
                self.$optionsToggle.attr('disabled', 'disabled');
                $.ajax({
                    url: conf.ajaxUrl,
                    type: 'get',
                    cache: true,
                    success: function(rs) {
                        if (rs) {
                            self.$optionsContent.html(rs);
                            self.element.trigger('ajaxloaded');
                            self.$optionsToggle.removeAttr('disabled');
                            $('body').trigger('contentUpdated');
                        }
                    }
                });
            }
        },
        _prepareHtml: function() {
            var self = this, conf = this.options;
            if (this.element.find('[data-configurable]').length) {
                var $confblock = this.element.find('[data-configurable]').first();
                conf.spConfig = $confblock.data('configurable');
                $confblock.removeAttr('data-configurable');
            }
            if (this.element.find('[data-customoption]').length) {
                var $optblock = this.element.find('[data-customoption]').first();
                conf.optionConfig = $optblock.data('customoption');
                $optblock.removeAttr('data-customoption');
            }
            this._fillSelect();
            this.$form = self.element.parents(conf.selectorProduct).first().find('form[data-role="tocart-form"]');
            this.$form.each(function() {
                var $form = $(this);
                if (conf.spConfig) {
                    $.each(conf.spConfig.attributes, function(id, attr) {
                        if ($form.find('[name="super_attribute[' + id + ']"]').length == 0) {
                            $form.append('<input type="hidden" name="super_attribute[' + id + ']">');
                        }
                    });
                }
                if (conf.optionConfig) {
                    var suffix = Math.round(Math.random()*10000);
                    var $optionWrap = self.element.find('[role="custom_options"]').first();
                    $optionWrap.clone().hide().appendTo($form);
                    $optionWrap.find('[name^="options["]').each(function() {
                        var $input = $(this);
                        $input.addClass('js-option').removeAttr('id').removeClass('product-custom-option');
                    });
                    $form.find('[name^="options["]').each(function() {
                        var $input = $(this);
                        var id = $input.attr('id') + '_' + suffix;
                        $input.attr('id', id);
                    });
                    
                    require(['priceOptions'], function(priceOptions) {
                        priceOptions({
                            optionConfig: conf.optionConfig,
                            controlContainer: ".field",
                            priceHolderSelector: self.element.parents(conf.selectorProduct).find('[data-role=priceBox]')
                        }, $form);
                    });
                }
                var formData = $form.data('scommerceCatalogAddToCart');
                formData = formData ? formData : $form.data('codazonCatalogAddToCart');
                formData = formData ? formData : $form.data('mageCatalogAddToCart');
                if (formData) {
                    formData.oldSubmitForm = formData.submitForm;
                    formData.submitForm = function() {
                        if (self.$fakeForm) {
                            self.$fakeForm.parents('[data-role="options_content"]').first().show();
                            if (self.$fakeForm.valid()) {
                                formData.oldSubmitForm($form);
                            }
                        } else {
                            formData.oldSubmitForm($form);
                        }
                    }
                }
            });
            this.$fakeForm = null;
            if (this.element.parents('form').length == 0) {
                this.$fakeForm = $('<form>').insertBefore(this.element);
                this.element.appendTo(this.$fakeForm);
                this.$fakeForm.validation();
            }
        },
        _assignVariables: function() {
            var self = this, conf = this.options;
            this.$main = this.element.parents(conf.selectorProduct).first();
            var mainImage = self.$main.find('.product-image-photo').first().attr('src');
            if (!mainImage) {
                mainImage = self.$main.find('.product-image-photo').first().data('lazysrc');
            }
            conf.mediaGalleryInitial = [{
                'img': mainImage
            }];
        },
        
        _bindEvents: function() {
            var self = this, conf = this.options;
            self.element.find('.js-attr-select').each(function() {
                var $select = $(this);
                $select.on('change', function() {
                    self._updateAll();
                });
            });
            self.element.find('.js-option').each(function() {
                var $select = $(this);
                $select.on('change', function() {
                    self._updateCustomOptions();
                });
            });
        },
        _updateCustomOptions: function() {
            var self = this, conf = this.options;
            self.$form.each(function() {
                var $form = $(this);
                self.element.find('.js-option').each(function() {
                    var $input = $(this);
                    var name = $input.attr('name');
                    if (($input.attr('type') == 'radio') || ($input.attr('type') == 'checkbox')) {
                        var value = $input.val();
                        var $hidden = $form.find('[name="' + name + '"][value="' + value + '"]');
                        if ($input.is(':checked')) {
                            $hidden.prop('checked', true);
                        } else {
                            $hidden.prop('checked', false);
                        }
                        $hidden.trigger('change');
                    } else {
                        var $hidden = $form.find('[name="' + name + '"]');
                        $hidden.val($input.val());
                        $hidden.trigger('change');
                    }
                });
            });
        },
        _updateAll: function() {
            var self = this, conf = this.options;
            var currentIndex = self._getCurrentIndex();
            self._updatePrice(currentIndex);
            self._updateFormData(currentIndex);
            self._updateBaseImage(currentIndex);
        },
        _updateFormData: function(currentIndex) {
            var self = this, conf = this.options;
            self.element.find('.js-attr-select').each(function() {
                var $select = $(this), value = $select.val();
                var id = $select.data('attrid');
                self.$form.each(function() {
                    var $form = $(this);
                    var $input = $form.find('[name="super_attribute[' + id + ']"]');
                    $input.val(value);
                });
            });
        },
        _updatePrice: function(currentIndex) {
            var self = this, conf = this.options;
            var result, $product = self.element.parents(conf.selectorProduct).first(),
            $productPrice = $product.find(conf.selectorProductPrice);
            result = conf.spConfig.optionPrices[currentIndex];
            $productPrice.trigger('updatePrice', {
                    'prices': self._getPrices(result, $productPrice.priceBox('option').prices)
                }
            );
        },
        _updateBaseImage: function(currentIndex) {
            var self = this, conf = this.options;
            var images = conf.spConfig.images[currentIndex];
            if (!images) {
                images = conf.mediaGalleryInitial;
            }
            var justAnImage = images[0];
            if (justAnImage && justAnImage.img) {
                var img = new Image();
                self.$main.find('.product-image-photo').addClass('swatch-option-loading');
                $(img).on('load', function() {
                    self.$main.find('.product-image-photo').removeClass('swatch-option-loading').attr('src', justAnImage.img);
                });
                img.src = justAnImage.img;
            }
        },
        _getPrices: function (newPrices, displayPrices) {
            var self = this, conf = this.options;

            if (!newPrices) {
                newPrices = conf.spConfig.prices;
            }

            $.each(displayPrices, function (code, price) {
                if (newPrices[code]) {
                    displayPrices[code].amount = newPrices[code].amount - displayPrices[code].amount;
                }
            });
            
            return displayPrices;
        },
        _getCurrentIndex: function() {
            var self = this, conf = this.options;
            var selectedIndex = {};
            var currentIndex = null;
            self.element.find('.js-attr-select').each(function() {
                var $select = $(this);
                if ($select.val()) {
                    selectedIndex[$select.data('attrid')] = parseInt($select.val());
                } else {
                    selectedIndex[$select.data('attrid')] = null;
                }
            });
            
            $.each(conf.spConfig.index, function(productId, index) {
                var isCurrent = true;
                $.each(selectedIndex, function(attrId, optionId) {
                    isCurrent = isCurrent && (optionId == index[attrId]);
                });
                if (isCurrent) {
                    currentIndex = productId;
                    return false;
                }
            });
            return currentIndex;
        },
        _fillSelect: function() {
            var self = this, conf = this.options;
            var attributes = conf.spConfig.attributes;

            $.each(attributes, function(id, attr) {
                var $select = self.element.find('[name="super_attribute[' + id + ']"]');
                $select.addClass('js-attr-select').data('attrid', id);
                
                var options = self._getAttributeOptions(id);
                $.each(options, function(oi, optData) {
                   var opt = new Option(optData.label, optData.id);
                   $select.append(opt);
                });
            });
        },
        _getAttributeOptions: function (attributeId) {
            if (this.options.spConfig.attributes[attributeId]) {
                return this.options.spConfig.attributes[attributeId].options;
            }
        },
    });

    return $.codazon.productListConfigurable;
});
