define([
    'underscore',
    'jquery',
    'mage/template',
    'Magento_Catalog/js/price-utils',
    'jquery-ui-modules/widget'
], function(_, $, mageTemplate, utils) {
    $.widget('codazon.frontCurrentSaleLayout', {
        options: {
            groupTmpl: '#cdz-current-sale-group-tmpl',
            itemTmpl: '#cdz-current-sale-subcat-tmpl',
            filterTmpl: '#cdz-current-sale-filter-tmpl',
            groupList: '[data-role="group-list"]',
            groupFilter: '[data-role="group-filter"]',
            defaultStore: 0,
            store: 0
        },
        _create: function() {
            var self = this, conf = this.options;
            self.element.find('[data-role="loader"]').remove();
            this._assignVariables();
            this._prepareHtml();
            this._bindEvent();
            this._findLazyImages();
            this._loadLazyImages();
        },
        _assignVariables: function() {
            var self = this, conf = this.options;
            this.groupTmpl = mageTemplate(conf.groupTmpl);
            this.itemTmpl = mageTemplate(conf.itemTmpl);
            this.filterTmpl = mageTemplate(conf.filterTmpl);
            this.$groupList = this.element.find(conf.groupList);
            this.$groupFilter = this.element.find(conf.groupFilter);
            this.store = conf.store;
            this.defaultStore =  conf.defaultStore;
            this.uid = $.now();
            this.price_format = conf.data.price_format;
            this.price_format.requiredPrecision = 0;
            this.paddingBottom = 100*(conf.data.image_size.height-2)/conf.data.image_size.width;
            this.lazyImages = [];
        },
        _findLazyImages: function() {
            var self = this, conf = this.options;
            this.element.find('[data-src]').each(function() {
                var $img = $(this);
                var src = $img.data('src');
                $img.removeAttr('data-src');
                $img.data('src', src);
                self.lazyImages.push($img);
            });
        },
        _loadLazyImages: function() {
            var self = this, conf = this.options;
            if (!this.lazyInterval) {
                this.lazyInterval = setInterval(function() {
                    $.each(self.lazyImages, function(index, $img) {
                        if ($img) {
                            if (self._checkVisible($img.parent())) {
                                $img.attr('src', $img.data('src')).removeClass('swatch-option-loading').removeAttr('srcset');
                                self.lazyImages.splice(index, 1);
                                if (self.lazyImages.length == 0) {
                                    clearInterval(self.lazyInterval);
                                    self.lazyInterval = false;
                                }
                            }
                        }
                    });
                }, 100);
            }
        },
        _checkVisible: function($element){
            var cond1 = ($element.get(0).offsetWidth > 0) && ($element.get(0).offsetHeight > 0);
            var cond2 = ($element.is(':visible'));
            var winTop = $(window).scrollTop(),
            winBot = winTop + window.innerHeight,
            elTop = $element.offset().top,
            elHeight = $element.outerHeight(true),
            elBot = elTop + elHeight;
            
            var delta = 100;
            
            var cond3 = (elTop <= winTop) && (elBot >= winTop);
            var cond4 = (elTop >= winTop) && (elTop <= winBot);
            var cond5 = (elTop >= winTop) && (elBot <= winBot);
            var cond6 = (elTop <= winBot) && (elBot >= winBot);
            
            return cond1 && cond2 && (cond3 || cond4 || cond5 || cond6);
        },
        _addGroup: function(group) {
            var self = this, conf = this.options;
            return $(this.groupTmpl({
                group: group,
                widget: this
            })).appendTo(this.$groupList);
        },
        _getStoreValue: function(values) {
            return values[this.store] ? values[this.store] : values[this.defaultStore]
        },
        _getSubCatItemHtml: function(subcatId, options) {
            var self = this, conf = this.options;
            var subcat = conf.data.categories[subcatId];
            if (subcat) {
                return this.itemTmpl({
                    subcat: subcat,
                    widget: this,
                    options: options,
                    padding_bottom: self.paddingBottom
                });
            }
            return '';
        },
        _getItemLabel: function(options) {
            return this._getStoreValue(options.item_label);
        },
        _addFilterOptionsHtml: function() {
            var self = this, conf = this.options;
            $(this.filterTmpl({
                filter_options: conf.data.filter_options,
                uid: self.uid
            })).appendTo(this.$groupFilter);
        },
        _prepareHtml: function() {
            var self = this, conf = this.options;
            this._addFilterOptionsHtml();
            $.each(conf.data.groups, function(i, group) {
                self._addGroup(group);
            });
        },
        _bindEvent: function() {
            var self = this, conf = this.options;
            this.element.find('[data-role="group-filter-otion"]').on('change', function(e) {
                var displayGroup = [];
                self.element.find('[data-role="group-filter-otion"]').each(function() {
                    if (this.checked) {
                        displayGroup.push(parseInt(this.value));
                    }
                });
                self.element.find('[data-role="crs-group"]').each(function() {
                    var $group = $(this);
                    if (displayGroup.indexOf($group.data('discount_type')) > -1) {
                        $group.fadeIn(300);
                    } else {
                        $group.fadeOut(300);
                    }
                });
            });
        },
        _formatPrice: function(price) {
            return utils.formatPrice(price, this.price_format);
        }
    });
    return $.codazon.frontCurrentSaleLayout;
});