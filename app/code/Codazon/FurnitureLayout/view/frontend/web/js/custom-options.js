define([
    'jquery',
    'priceOptions',
    'jquery-ui-modules/widget',
], function ($, priceOptions) {
   $.widget('codazon.productListCustomOptions', {
       options: {
           selectorProduct: '.product-item',
           selectorProductPrice: '[data-role=priceBox]',
           optionSelector: '.product-custom-option'
       },
        _create: function () {
            var self = this, conf = this.options;
            this._assignVariables();
            this._prepareHtml();
            this._bindEvents();
        },
        _assignVariables: function() {
            var self = this, conf = this.options;
            this.$form = self.element.parents(conf.selectorProduct).first().find('form[data-role="tocart-form"]');
            this.$fakeForm = null;
        },
        _prepareHtml: function() {
            var self = this, conf = this.options;
            if (this.element.parents('form').length == 0) {
                this.$fakeForm = $('<form>').insertBefore(this.element);
                this.element.appendTo(this.$fakeForm);
                this.$fakeForm.validation();
            }
            this.$form.each(function() {
                var $form = $(this);
                self.element.find(conf.optionSelector).each(function() {
                    var $option = $(this), name = $option.attr('name');
                    if ($form.find('[name="' + name + '"]').length == 0) {
                        var $input = $('<input type="hidden">').appendTo($form);
                        $input.attr('name', name);
                        if ($option.hasClass('required')) {
                            $input.attr('data-requiredfield', 1);
                        }
                    }
                });
            });
        },
        _bindEvents() {
            
        }
   });
   return $.codazon.productListCustomOptions;
});