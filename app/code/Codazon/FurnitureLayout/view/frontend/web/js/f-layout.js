define([
    'jquery',
    'jquery-ui-modules/widget',
    'mage/template',
    'mage/translate',
    'underscore',
    'validation'
], function($, jqueyUi, mageTemplate, $t, _) {
    $.widget('codazon.fCategoryLayout', {
        options: {
            formKeyInputSelector: '.column.main > input[name="form_key"]'
        },
        _create: function() {
            var self = this, conf = this.options;
            this._assignVariables();
            this._prepareHtml();
            this._bindEvents();
            this._customBlockCart();
        },
        _customBlockCart: function() {
            var self = this, conf = this.options;
            this.$blockCart = this.element.find('[data-block="minicart"]');
            if (this.$blockCart.length) {
                var it = setInterval(function() {
                    var cartSidebar = self.$blockCart.data('mageSidebar');
                    if (cartSidebar) {
                        clearInterval(it);
                        self._modifyCartSidebar(self.$blockCart, cartSidebar);
                    }
                }, 1000);
            }
        },
        _modifyCartSidebar: function($blockCart, cartSidebar) {
            cartSidebar._hideItemButton = function(elem) {
                var itemId = elem.data('cart-item');
                $blockCart.find('.update-cart-item[data-cart-item=' + itemId + ']').hide('fade', 300);
            };
            cartSidebar._showItemButton = function(elem) {
                var itemId = elem.data('cart-item'),
                    itemQty = elem.data('item-qty');

                if (this._isValidQty(itemQty, elem.val())) {
                    $blockCart.find('.update-cart-item[data-cart-item=' + itemId + ']').show('fade', 300);
                } else if (elem.val() == 0) {
                    this._hideItemButton(elem);
                } else {
                    this._hideItemButton(elem);
                }
            };
            cartSidebar._updateItemQty = function (elem) {
                var itemId = elem.data('cart-item');
                this._ajax(this.options.url.update, {
                    'item_id': itemId,
                    'item_qty': $blockCart.find('.cart-item-qty[data-cart-item=' + itemId + ']').val()
                }, elem, this._updateItemQtyAfter);
            };
        },
        _assignVariables: function() {
            var self = this, conf = this.options;
            this.catTabsTmpl = mageTemplate(conf.catTabsTmpl);
            this.$catTabsContainer = this.element.find('[data-role="cat-tabs-container"]').first();
            this.$productContainer = $('#fly-product-list-container');
            this.lazyImages = [];
            
            this.$scrollBox = self.element.find('[data-role="scroll-box"]');
            this.$notif = self.element.find('[data-role="f-cat-notif"]');
            this.$closeNotif = this.$notif.find('[data-role="close-notif"]');
            
            //this.$msgContainer = $('<div class="fly-message-container" />').appendTo('body').hide();
            this.$msgContainer = $('<div class="cdz-msg-container fixed">').appendTo('body');
            self.productSections = {};
        },
        _prepareHtml: function() {
            var self = this, conf = this.options;
            this._categoryTabs();
            self._enableButtons();
        },
        _categoryTabs: function() {
            var self = this, conf = this.options;
            var data = {
                'categories':   conf.categories
            }
            this.$catTabs = $(this.catTabsTmpl(data));
            this.$catTabsContainer.append(this.$catTabs);
        },
        _enableButtons: function() {
            if ($('[role="predisabled"]').length) {
                var t = setInterval(function() {
                    if ($('[role="predisabled"]').first().parents('form').first().data('scommerceCatalogAddToCart') || $('[role="predisabled"]').first().parents('form').first().data('mageCatalogAddToCart')) {
                        clearInterval(t);
                        $('[role="predisabled"]').removeAttr('disabled').removeAttr('role');
                    }
                }, 50);
            }
        },
        _loadProducts: function($link) {
            var self = this, conf = this.options;
            var catId = $link.data('id');
            var updated_url = conf.submitUrl + '?' + conf.catQuery + '=' + catId;
            if (typeof self.productSections[catId] === 'undefined') {
                if (self.$productContainer.find('#sub-cat-tab-' + catId).length) {
                    self.productSections[catId] = self.$productContainer.find('#sub-cat-tab-' + catId);
                    self.$productContainer.find('[data-role="sub-cat-tab"]').hide();
                    self.productSections[catId].show();
                    window.history.pushState(updated_url, document.title, updated_url);
                    self._enableButtons();
                } else {
                    $.ajax({
                        url: conf.submitUrl,
                        method: 'get',
                        cache: true,
                        data: {[conf.catQuery]: catId, 'ajax_cat_tab': 1},
                        showLoader: true,
                        success: function(rs) {
                            if (rs.category_products) {
                                self.productSections[catId] = $(rs.category_products);
                                self.productSections[catId].appendTo(self.$productContainer);
                                self.$productContainer.find('[data-role="sub-cat-tab"]').hide();
                                setTimeout(function() {
                                    self.productSections[catId].find('[name="form_key"]').each(function() {
                                        var formKey = $(conf.formKeyInputSelector).first().val();
                                        var $field = $(this).val(formKey);
                                    });
                                    self.productSections[catId].show();
                                    $('body').trigger('contentUpdated');
                                }, 200);
                                window.history.pushState(updated_url, document.title, updated_url);
                            } else {
                                $('body').trigger('contentUpdated');
                            }
                            self._findLazyImages();
                            self._loadLazyImages();
                            self._enableButtons();
                        }
                    });
                }
            } else {
                self.$productContainer.find('[data-role="sub-cat-tab"]').hide();
                self.productSections[catId].show();
                window.history.pushState(updated_url, document.title, updated_url);
            }
            
            
        },
        _bindEvents: function() {
            var self = this, conf = this.options;
            this.$catTabs.on('click', '.tab-link', function(e) {
                e.preventDefault();
                var $link = $(this);
                var $tabContent = $('#' + $link.data('target'));
                $link.parents('.tab-title').addClass('active').siblings().removeClass('active');
                $tabContent.addClass('active').siblings().removeClass('active');
                self._loadProducts($link);
            });
            this.$catTabs.on('click', '[data-role="scroll-item"]', function(e) {
                e.preventDefault();
                var $link = $(this);
                var catId = $link.data('id');
                var section = '#sub-cat-sec-' + catId;
                var $section = $(section);
                if ($section.length) {
                    var srollTop = $section.offset().top - 100;
                    $('html,body').animate({scrollTop: srollTop});
                }
            });
            this.$closeNotif.on('click', function() {
                self.$notif.fadeOut(300);
            });
            if (!this.isInit) {
                this.$catTabs.find('.tab-link[data-id=' + conf.currentCat + ']').click();
                this.isInit = true;
            }
            this._findLazyImages();
            this._loadLazyImages();
            this._updateMessageAfterAddToCart();
            this._printAction();
            setTimeout(function() {
                self._stickyBar();
            }, 5000);
        },
        _updateMessageAfterAddToCart: function() {
            var self = this, conf = this.options;
            $(document).on('ajax:addToCart', function(e, data) {
                if (data.response.backUrl) {
                    data.response.backUrl = false;
                }
                $.get(conf.updateMsgUrl, {sections: 'messages'}, function(rs) {
                    self.$msgContainer.html('');
                    var messages = rs.messages;
                    if (messages) {
                        if (messages.messages) {
                            self._displayMessages(messages.messages);
                        }
                    }
                });
            });
        },
        _displayMessages: function(messages) {
            var self = this;
            self.$msgContainer.show();
            $.each(messages, function(i, msg) {
                var $message = $('<div class="message cdz-translator">').addClass(msg.type).html('<span>' + msg.text + '</span>').prependTo(self.$msgContainer);
                setTimeout(function() {
                    $message.fadeOut(2000, 'swing', function() {
                        $message.remove();
                        if (!self.$msgContainer.children().length) {
                            self.$msgContainer.hide();
                        }
                    });
                }, 3000);
            });
        },
        _printAction: function() {
            var self = this, conf = this.options;
            self.element.on('click', '[data-print]', function() {
                var $a = $(this);
                var $content = $($a.data('print'));
                if ($content.length) {
                    self._printHtml($content.html());
                }
            });
        },
        _printHtml: function(html) {
            var $content = $('<div>').addClass('fly-print-content').html(html).appendTo('body');
            $('body').addClass('fly-printing');
            setTimeout(function() {
               window.print();
               setTimeout(function() {
                   $('body').removeClass('fly-printing');
                   $content.remove();
               }, 10);
            }, 10);
        },
        _stickyBar: function() {
            var self = this, conf = this.options;
            if (this.$scrollBox.length) {
                require(['Codazon_FurnitureLayout/js/jquery.sticky-kit'], function() {
                    self.$scrollBox.stick_in_parent({
                        offset_top: 50,
                    });
                });
            }
        },
        _checkVisible: function($element){
            var cond1 = ($element.get(0).offsetWidth > 0) && ($element.get(0).offsetHeight > 0);
            var cond2 = ($element.is(':visible'));
            var winTop = $(window).scrollTop(),
            winBot = winTop + window.innerHeight,
            elTop = $element.offset().top,
            elHeight = $element.outerHeight(true),
            elBot = elTop + elHeight;
            
            var delta = 100;
            
            var cond3 = (elTop <= winTop) && (elBot >= winTop);
            var cond4 = (elTop >= winTop) && (elTop <= winBot);
            var cond5 = (elTop >= winTop) && (elBot <= winBot);
            var cond6 = (elTop <= winBot) && (elBot >= winBot);
            
            return cond1 && cond2 && (cond3 || cond4 || cond5 || cond6);
        },
        _findLazyImages: function() {
            var self = this, conf = this.options;
            this.element.find('[data-src]').each(function() {
                var $img = $(this);
                var src = $img.data('src');
                $img.removeAttr('data-src');
                $img.data('src', src);
                self.lazyImages.push($img);
            });
        },
        _loadLazyImages: function() {
            var self = this, conf = this.options;
            if (!this.lazyInterval) {
                this.lazyInterval = setInterval(function() {
                    $.each(self.lazyImages, function(index, $img) {
                        if ($img) {
                            if (self._checkVisible($img.parent())) {
                                $img.attr('src', $img.data('src')).removeClass('swatch-option-loading').removeAttr('srcset');
                                self.lazyImages.splice(index, 1);
                                if (self.lazyImages.length == 0) {
                                    clearInterval(self.lazyInterval);
                                    self.lazyInterval = false;
                                }
                            }
                        }
                    });
                }, 100);
            }
        }
    });
    return $.codazon.fCategoryLayout;
});