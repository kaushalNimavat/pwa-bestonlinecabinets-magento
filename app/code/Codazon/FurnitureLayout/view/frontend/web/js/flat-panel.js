define([
    'jquery',
    'jquery-ui-modules/widget',
    'mage/template',
    'Codazon_FurnitureLayout/magicscroll/magicscroll',
    'Codazon_FurnitureLayout/magiczoomplus/magiczoomplus'
], function($, jqueryUi, mageTemplate) {
    $.widget('codazon.fCategoryGallery', {
        _create: function() {
            this._assignVariables();
            this._prepareHtml();
            this._bindEvents();
        },
        _assignVariables: function() {
            var self = this, conf = this.options;
            this.paddingBottom = 100 * conf.height / conf.width;
            this.flatPanelData = {};
            this.flatPanelData.flatPanels = conf.flatPanels;
            this.flatPanelData.paddingBottom = this.paddingBottom;
            this.flatPanelData.mediaUrl =  conf.mediaUrl;
            this.flatPanelTmpl = mageTemplate(conf.flatPanelTmpl);
            this.galleryTmpl = mageTemplate(conf.galleryTmpl);
            this.$toolTip = $('<div class="cdz-flex-tooltip">').css({position: 'absolute', width: 150}).hide().appendTo('body');
        },
        _prepareHtml: function() {
            var self = this, conf = this.options;
            this.element.find('[data-role="loader"]').remove();
            this.$flatPanels = $(this.flatPanelTmpl(this.flatPanelData));
            this.element.html(this.$flatPanels);
            $.each(this.$flatPanels, function(i, flatPanel) {
                MagicScroll.refresh('flat-style-list-' + i);
            });
            this.$sampleGallery = this.element.find('[data-role="style-sample-gallery"]');
            this.element.find('[data-role="style-list"]').each(function() {
                var $styleList = $(this);
                MagicScroll.refresh('mb-style-images');
                $styleList.find('[data-sample]').each(function() {
                    var $sample = $(this);
                    $sample.on('click', function(e) {
                        e.stopPropagation();
                        if (!self.$currentSample.is($sample)) {
                            self.$currentSample = $sample;
                            self._sampleGallery($sample, 0);
                        }
                    });
                });
            });
            this.$currentSample = this.element.find('[data-sample]').first();
            self._sampleGallery(this.$currentSample, 1);
        },
        _sampleGallery: function($sample, init) {
            var self = this, conf = this.options;
            var sample = $sample.data('sample');
            self.element.find('[data-sample].active').removeClass('active');
            $sample.addClass('active');
            if (sample.length) {
                var galleryData = {};
                galleryData.images = [];
                galleryData.paddingBottom = self.paddingBottom;
                galleryData.label = $sample.data('name');
                galleryData.mediaUrl = conf.mediaUrl;
                $.each(sample, function(i, image) {
                    image.label = conf.sampleTitlePattern.replace('%1', galleryData.label).replace('%2', i + 1);
                    galleryData.images.push(image);
                });
                galleryData.main = galleryData.images[0];
                $gallery = $(self.galleryTmpl(galleryData));
                self.$sampleGallery.html($gallery);
                MagicScroll.refresh('style-thumb-images');
                MagicScroll.refresh('mb-style-images');
                if (!init) {
                    MagicZoom.refresh('style-base-image');
                }
            }
        },
        _bindEvents: function() {
            var self = this, conf = this.options;
            var t = false;
            self.element.find('[data-role=info-tooltip-wrap]').on('mouseover', function() {
                if (window.innerWidth >= 768) {
                    var $parent = $(this), $info = $parent.find('[data-role=info-tooltip]').first();
                    self.$toolTip.html($info.html());
                    self.$toolTip.show();
                    if (t) clearTimeout(t);
                    t = setTimeout(function() {
                        var pTop = $parent.offset().top;
                        var pLeft = $parent.offset().left;
                        var pWidth = $parent.outerWidth(), pHeight = $parent.outerHeight();
                        var iWidth = self.$toolTip.outerWidth(), iHeight = self.$toolTip.outerHeight();
                        var iTop = pTop - iHeight - 20;
                        var iLeft = pLeft + (pWidth - iWidth)/2;
                        self.$toolTip.css({
                            left: iLeft,
                            top: iTop,
                            zIndex: 1000
                        });
                    }, 100);
                }
            }).on('mouseout', function() {
                self.$toolTip.css({
                    zIndex: -1,
                    display: 'none'
                });
            });
        }
    });
    return $.codazon.fCategoryGallery;
});