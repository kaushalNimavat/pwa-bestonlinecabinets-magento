define([
    'jquery',
    'jquery-ui-modules/widget',
    'mage/template'
], function($, jqueryUi, mageTemplate) {
    $.widget('codazon.magicGalleryPopup', {
        options: {
            paddingBottom: 66.625,
            galleryTmpl: '' +
            '<div class="category-gallery">' +
                '<% if (main) { %>' +
                '<div class="base-image-wrap abs-frame" data-role="base-image-wrap" style="padding-bottom: <%- paddingBottom %>%">' +
                    '<div class="abs-frame-inner">' +
                        '<a href="<%- main.base %>" title="<%- main.label %>" class="MagicZoom fly-main-image" id="cat-base-image-<%- id %>" data-options="lazyZoom: true; zoomMode:off">' +
                            '<img class="gallery-main-img" src="<%- main.large %>" alt="<%- main.label %>">' +
                        '</a>' +
                    '</div>' +
                '</div>' +
                '<% } %>' +
                '<div class="MagicScroll bxslider11 cat-thumb-ul" data-options="pagination: true; lazyLoad: true, zoomDistance: 400px; loop:off; arrows: inside;">' +
                    '<% _.each(images, function(image, index) { %>' +
                    '<a class="cat-thumb" data-zoom-id="cat-base-image-<%- id %>" title="<%- image.label %>" data-role="thumb" href="<%- image.base %>" data-image="<%- image.large %>" >' +
                        '<img src="<%- image.small %>">' +
                    '</a>' +
                    '<% }); %>' +
                '</div>' +
            '</div>'
        },
        _create: function() {
            var self = this, conf = this.options;
            this.galleryTmpl = mageTemplate(conf.galleryTmpl);
            this._prepareHtml();
            this._assignVariables();
            this._bindEvents();
        },
        _assignVariables: function() {
            var self = this, conf = this.options;
        },
        _prepareHtml: function() {
            var self = this, conf = this.options, id = this.element.attr('id');
            this.$images = this.element.find('img');
            if (this.$images.length) {
                this.element.removeAttr('id');
                if (!id) {
                    id = 'magic-popup-' + Math.random().toString().substr(2,6);
                }
                this.$popup = $('<div data-cdzpopup="1" data-parentclass="magic-images-popup" class="no-max-height no-nice-scroll">').attr('id', id).appendTo('body');
                this.id = id;
                $(this._galleryHtml(self.$images, id)).appendTo(this.$popup);
                $('body').trigger('cdzBuildPopup');
                require(['Codazon_FurnitureLayout/magicscroll/magicscroll', 'Codazon_FurnitureLayout/magiczoomplus/magiczoomplus']);
            }
        },
        _galleryHtml: function($images, id) {
            var self = this, conf = this.options;
            var images = [], data = {};
            $images.each(function(id, img) {
                var $img = $(this), src = $img.attr('data-src');
                images.push({
                    base: $img.data('fullscreen-image'),
                    large: src ? src : $img.attr('src'),
                    small: src ? src : $img.attr('src'),
                    label: $img.attr('alt')
                });
            });
            data.id = id;
            data.main = images[0];
            data.images = images;
            data.paddingBottom = conf.paddingBottom;
            return this.galleryTmpl(data);
        },
        _bindEvents: function() {
            
        },
    });
    return $.codazon.magicGalleryPopup;
});