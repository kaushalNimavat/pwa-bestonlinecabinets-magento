define([
    'jquery',
    'jquery-ui-modules/widget',
    'mage/template'
], function($, jqueryUi, mageTemplate) {
    $.widget('codazon.fCategoryGallery', {
        _create: function() {
            this._assignVariables();
            this._prepareHtml();
            this._bindEvents();
        },
        _assignVariables: function() {
            var self = this, conf = this.options;
            this.galleryData = {};
            this.galleryData.panorama = null;
            this.galleryData.images = [];
            this.galleryData.uid = conf.uid ? conf.uid : '';
            this.galleryData.paddingBottom = 100 * conf.height / conf.width;
            $.each(conf.images, function(i, image) {
                if (image.is_panorama) {
                    self.galleryData.panorama = image;
                } else {
                    self.galleryData.images.push(image);
                }
            });
            this.galleryData.main = this.galleryData.images[0];
            this.galleryTmpl = mageTemplate(conf.galleryTmpl);
        },
        _prepareHtml: function() {
            var self = this, conf = this.options;
            this.$gallery = $(this.galleryTmpl(this.galleryData));
            this.element.html(this.$gallery);
            require(['Codazon_FurnitureLayout/magicscroll/magicscroll', 'Codazon_FurnitureLayout/magiczoomplus/magiczoomplus']);
            if (this.galleryData.panorama) {
                require(['Codazon_FurnitureLayout/js/libpannellum'], function() {
                    require(['Codazon_FurnitureLayout/js/pannellum'], function() {
                        pannellum.viewer('category-panorama' + self.galleryData.uid, {
                            "type": "equirectangular",
                            "panorama": self.galleryData.panorama.base
                        });
                    });
                });
            }
            this.$baseImageWrap = $('[data-role="base-image-wrap"]', this.element);
            this.$panoramaWrap = $('[data-role="panorama-wrap"]', this.element);
        },
        _bindEvents: function() {
            var self = this, conf = this.options;
            self.element.on('click', '[data-role="thumb"]', function(e) {
                e.preventDefault();
                var $link = $(this);
                if ($link.data('viewpanorama')) {
                    self.$panoramaWrap.show();
                    self.$baseImageWrap.hide();
                } else {
                    self.$panoramaWrap.hide();
                    self.$baseImageWrap.show();
                }
            });
        }
    });
    return $.codazon.fCategoryGallery;
});