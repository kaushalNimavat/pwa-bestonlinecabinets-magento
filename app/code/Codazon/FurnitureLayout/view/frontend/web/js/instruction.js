/**
 * Copyright © 2019 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'underscore',
    'jquery',
    'mage/template',
    'Magento_Catalog/js/price-utils',
    'jquery-ui-modules/widget',
    'owlslider'
], function(_, $, mageTemplate, utils) {
    $.widget('codazon.assemblyInstruction', {
        _create: function() {
            var self = this, conf = this.options;
            this._assignVariables();
            this._prepareHtml();
            this._bindEvents();
        },
        _assignVariables: function() {
            var self = this, conf = this.options;
            this.colTmpl = mageTemplate(conf.colTmpl);
            this.docTmpl = mageTemplate(conf.docTmpl);
            this.colData = {
                collections: conf.collections,
                mediaUrl: conf.mediaUrl
            };
            this.loadedDoc = {};
            this.$frameVideo = $('#cdz-fly-video-frame');
        },
        _prepareHtml: function() {
            var self = this, conf = this.options;
            self.element.html(this.colTmpl(this.colData));
            self.element.find('[role="style_slider"]').addClass('owl-carousel').owlCarousel({
                responsive: {
                    0: {items: 3, margin: 10},
                    320: {items: 3, margin: 10},
                    480: {items: 5, margin: 10},
                    768: {items: 6, margin: 20},
                    1024: {items: 8},
                    1366: {items: 10}
                },
                margin: 20,
                nav: true,
                dots: false,
                navElement: 'div',
                mouseDrag: false
            });
            this.$docWrap = self.element.find('[role="doc-wrap"]').first();
            this.$docLoader = $('[role="loader"]', this.$docWrap).hide();
            if (!this.$frameVideo.length) {
                this.$frameVideo = $('<div class="cdz-fly-video-frame" id="cdz-fly-video-frame">').hide().appendTo($('body'));                
                this.$frameVideo.modal({
                    autoOpen: false,
                    clickableOverlay: false,
                    innerScroll: true,
                    modalClass: 'cdz-fly-video-frame',
                    buttons: [],
                    closed: function() {
                        self.$frameVideo.html('');
                    }
                });
            }
        },
        _bindEvents: function() {
            var self = this, conf = this.options;
            self.element.on('click.loaddoc', '[data-loaddoc]', function(e, needScroll) {
                if (typeof needScroll == 'undefined') {
                    needScroll = true;
                }
                var $link = $(this), styleId = $link.data('loaddoc'), styleName = $link.data('stylename');
                var $doc = $('.doc-style-' + styleId, self.element);
                $('[data-loaddoc]', self.element).removeClass('active');
                $link.addClass('active');
                if ($('.doc-style-' + styleId, self.element).length) {
                    $doc.fadeIn(300).siblings().hide();
                    self._sameHeight();
                    if (needScroll) {
                        self._scrollToDocs($doc);
                    }
                } else {
                    self._loadDoc(styleId, styleName, needScroll);
                }
            });
            setTimeout(function() {
                $('[data-loaddoc]', self.element).first().trigger('click.loaddoc', [false]);
            }, 50);
        },
        _scrollToDocs: function($doc) {
            var ot = $doc.offset().top - 80;
            $('html,body').animate({'scrollTop': ot}, 300);
        },
        _loadDoc: function(styleId, styleName, needScroll) {
            var self = this, conf = this.options;
            self.$docLoader.show();
            self.element.find('.instruction-item').hide();
            if (this.ajaxLoading) {
                this.ajaxLoading.abort();
            }
            this.ajaxLoading = $.ajax({
                url: conf.ajaxUrl,
                data: {style: styleId},
                type: 'get',
                cache: true,
                showLoader: (needScroll ? true : false),
                success: function(rs) {
                    self.$docLoader.hide();
                    var data = {};
                    if (rs.itemsCount) {
                        var pdf = [], video = [];
                        _.each(rs.items, function(item) {
                            if (item.type == 1) {
                                pdf.push(item);
                            } else if (item.type == 2) {
                                video.push(item);
                            }
                        });
                        data.pdf = pdf;
                        data.video = video;
                        data.message = false;
                    } else {
                        data.pdf = [];
                        data.video = [];
                        data.message = rs.message;
                    }
                    data.styleName = styleName;
                    data.mediaUrl = conf.mediaUrl;
                    data.fileUrl = conf.fileUrl;
                    data.widget = self;
                    data.styleId = styleId;
                    var $doc = $(self.docTmpl(data));
                    self.$docWrap.append($doc);
                    $doc.hide().fadeIn(300).siblings().hide();
                    self._attachVideoData($doc);
                    self._sameHeight();
                    if (needScroll) {
                        self._scrollToDocs($doc);
                    }
                }
            })
        },
        _sameHeight: function() {
            if (typeof window.themecore != 'undefined') {
                window.themecore.makeSameHeight();
            }
        },
        _attachVideoData: function($doc) {
            var self = this, conf = this.options;
            var paddingBottom = '75%';
            $('[data-video]', $doc).each(function() {
                var $video = $(this), url = $video.data('video'), phdSrc, embedUrl;
                var $placeholder = $('[role="video-thumbnail"]', $video);
                var videoData = self._getVideoData(url);
                if (videoData.type == 'youtube') {
                    embedUrl = '//www.youtube.com/embed/' + videoData.id + '?autoplay=1';
                } else {
                    embedUrl = '//player.vimeo.com/video/' + videoData.id + '?autoplay=1';
                }
                if (videoData.type === 'youtube') {
                    phdSrc = "//img.youtube.com/vi/" + videoData.id + "/hqdefault.jpg";
                    $placeholder.attr('src', phdSrc);
                } else if (videoData.type === 'vimeo') {
                    $.ajax({
                        type: 'GET',
                        url: '//vimeo.com/api/v2/video/' + videoData.id + '.json',
                        jsonp: 'callback',
                        dataType: 'jsonp',
                        success: function (data) {
                            phdSrc = data[0].thumbnail_large;
                            $placeholder.attr('src', phdSrc);
                        }
                    });
                } else {
                    $video.hide();
                }
                
                $video.on('click', '[role="play-video"]', function() {
                    if (!$('body').hasClass('_has-modal')) {
                        $('body').addClass('_has-modal');
                    }
                    var frame = '<iframe frameborder="0" allowfullscreen="1" class="abs-frame-inner" src="' + embedUrl + '"></iframe>';
                    var $frameInner = $('<div class="frame-inner abs-frame">');
                    $frameInner.html(frame);
                    $frameInner.css({paddingBottom: paddingBottom});
                    self.$frameVideo.html($frameInner);
                    self.$frameVideo.modal('openModal');
                });
            });
        },
        _getVideoData: function (url) {
            if (url) {
                id = url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
                if (id[3].indexOf('youtu') > -1) {
                    type = 'youtube';
                } else if (id[3].indexOf('vimeo') > -1) {
                    type = 'vimeo';
                } else {
                    throw new Error('Video URL not supported.');
                }
                id = id[6];
            } else {
                throw new Error('Missing video URL.');
            }
            return {
                type: type,
                id: id
            };
        },
        filterVideoUrl: function(videoUrl) {
            var self = this, conf = this.options;
            var videoData = this._getVideoData(videoUrl);
            if (videoData.type == 'youtube') {
                return '//www.youtube.com/embed/' + videoData.id + '?autoplay=1';
            } else {
                return '//player.vimeo.com/video/' + videoData.id + '?autoplay=1';
            }
        },
        getVideoThumb: function(videoUrl) {
            
        },
    });
    return $.codazon.assemblyInstruction;
});