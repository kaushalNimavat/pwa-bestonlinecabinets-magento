/**
 * Copyright © 2019 Codazon. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'underscore',
    'jquery',
    'mage/template',
    'Magento_Catalog/js/price-utils',
    'jquery-ui-modules/widget'
], function(_, $, mageTemplate, utils) {
    $.widget('codazon.categoryLandingPage', {
        options: {
            categoryIds: []
        },
        _create: function() {
            var self = this, conf = this.options;
            this._setupAll();
        },
        _setupAll: function() {
            this._assignVariables();
            this._prepareHtml();
            this._bindEvents();
        },
        _assignVariables: function() {
            var self = this, conf = this.options;
            this.$boxItems = self.element.find('[role="box-item"]');
            this.$toolbar = self.element.find('.toolbar.toolbar-products').first();
        },
        _prepareHtml: function() {
            var self = this, conf = this.options;
        },
        _bindEvents: function() {
            var self = this, conf = this.options;
            this._makupStyle();
            this._buildFilter();
            this._builSort();
            this._buildClear();
        },
        _buildFilter: function() {
            var self = this, conf = this.options;
            this.$toolbar.find('[data-role="filter"]').on('change', function() {
                var $option = $(this);
                var $list = $option.parents('[data-role="options-list"]').first();
                var code = $option.data('code');
                var values = [];
                $list.find('input[data-code="' + code + '"]:checked').each(function() {
                    var $opt = $(this);
                    values.push($opt.data('value'));
                });
                self._ajaxLoad(self._changeUrl(code, values.join(','), ''));
            });
        },
        _builSort: function() {
            var self = this, conf = this.options;
            this.$toolbar.find('[data-role="sort"]').on('change', function() {
                var $option = $(this);
                var $list = $option.parents('[data-role="options-list"]').first();
                var code = $option.data('code');
                var value = $option.data('value');
                var ajaxUrl = self._changeUrl(conf.orderParam, code, '', conf.ajaxUrl);
                ajaxUrl = self._changeUrl(conf.directionParam, value, conf.directionDefault, ajaxUrl);
                self._ajaxLoad(ajaxUrl);
            });
        },
        _buildClear: function() {
            var self = this, conf = this.options;
            this.$toolbar.find('[data-role="clear"]').on('click', function() {
                var ajaxUrl = conf.ajaxUrl.split('?');
                ajaxUrl = ajaxUrl[0];
                self._ajaxLoad(ajaxUrl);
            });
        },
        _ajaxLoad: function(ajaxUrl) {
            var self = this, conf = this.options;
            conf.ajaxUrl = ajaxUrl;
            $.ajax({
                url: ajaxUrl,
                type: 'POST',
                data: conf.ajaxParams,
                showLoader: true,
                success: function(rs) {
                    self.element.html(rs.categoryList);
                    self._setupAll();
                    var currentUrl = conf.currentUrl.split('?');
                    currentUrl = currentUrl[0];
                    if (rs.paramData) {
                        var paramData = $.param(rs.paramData);
                        currentUrl = currentUrl + (paramData.length ? '?' + paramData : '').replace(/%2C/g,',');
                    }
                    window.history.pushState(currentUrl, document.title, currentUrl);
                    $('body').trigger('contentUpdated');
                }
            });
        },
        _changeUrl: function (paramName, paramValue, defaultValue, url) {
            var self = this, conf = this.options;
            if (!url) {
                url = conf.ajaxUrl;
            }
            var decode = window.decodeURIComponent,
                urlPaths = url.split('?'),
                baseUrl = urlPaths[0],
                urlParams = urlPaths[1] ? urlPaths[1].split('&') : [],
                paramData = {},
                parameters, i;
            for (i = 0; i < urlParams.length; i++) {
                parameters = urlParams[i].split('=');
                paramData[decode(parameters[0])] = parameters[1] !== undefined ?
                decode(parameters[1].replace(/\+/g, '%20')) :
                '';
            }
            paramData[paramName] = paramValue;

            if (paramValue == defaultValue) {
                delete paramData[paramName];
            }
            paramData = $.param(paramData);
            var ajaxUrl = baseUrl + (paramData.length ? '?' + paramData : '');
            return ajaxUrl;
        },
        _makupStyle: function() {
            var self = this, conf = this.options;
            this.$boxItems.each(function() {
                var $boxItem = $(this);
                var $trigger = $boxItem.find('[role="box-trigger"]');
                var $dropdown = $boxItem.find('[role="box-options"]');
                $trigger.on('click', function(e) {
                    $boxItem.toggleClass('open');
                    $dropdown.slideToggle(200);
                });
                $('body').on('click', function(e) {
                    var $target = $(e.target);
                    if (!($boxItem.is($target) || $boxItem.has($target).length)) {
                        $dropdown.slideUp(200);
                        $boxItem.removeClass('open');
                    }
                });
            });
        }
    });
    return $.codazon.categoryLandingPage;
});