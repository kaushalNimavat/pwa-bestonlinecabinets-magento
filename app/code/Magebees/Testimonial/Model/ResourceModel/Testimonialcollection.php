<?php
namespace Magebees\Testimonial\Model\ResourceModel;

class Testimonialcollection extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magebees_customer_testimonials', 'testimonial_id');
    }

    /**
     * @param int $storeId
     * @param array $testimonialIds
     * @param int $fromId
     * @param int $limit
     * @return array
     */
    public function loadTestimonials($storeId = 0, array $testimonialIds = [], $fromId = 0, $limit = 1000): array
    {
        $select = $this->getConnection()->select()->from(['testimonial' => $this->getTable('magebees_customer_testimonials')]);
        if (!empty($bannerIds)) {
            $select->where('testimonial.testimonial_id IN (?)', $testimonialIds);
        }

        $select->where('testimonial.testimonial_id > ?', $fromId)
            ->limit($limit)
            ->order('testimonial.testimonial_id');

        return $this->getConnection()->fetchAll($select);
    }
}
