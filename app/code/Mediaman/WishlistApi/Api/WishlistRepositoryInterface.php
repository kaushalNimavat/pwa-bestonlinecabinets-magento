<?php
/**
 * @copyright: Copyright © 2017 mediaman GmbH. All rights reserved.
 * @see LICENSE.txt
 */

namespace Mediaman\WishlistApi\Api;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface WishlistRepositoryInterface
 * @package Mediaman\WishlistApi\Api
 * @api
 */
interface WishlistRepositoryInterface
{

    /**
     * Get the current customers wishlist
     *
     * @return \Mediaman\WishlistApi\Api\WishlistInterface
     * @throws NoSuchEntityException
     */
    public function getCurrent(int $customerId): WishlistInterface;

    /**
     * Add an item from the customers wishlist
     *
     * @param string $sku
     * @param int $customerId
     * @return bool
     */
    public function addItem(string $sku, int $customerId): bool;

    /**
     * @param Data\WishlistItemInterface $wishlistItem
     * @return mixed
     */
    public function addItemWithQty(\Mediaman\WishlistApi\Api\Data\WishlistItemInterface $wishlistItem);

    /**
     * Remove an item from the customers wishlist
     *
     * @param int $itemId
     * @param int $customerId
     * @return boolean
     * @throws NoSuchEntityException
     */
    public function removeItem(int $itemId, int $customerId): bool;

    /**
     * Clear all wishlist items
     *
     * @param int $customerId
     * @return boolean
     * @throws NoSuchEntityException
     */
    public function clearItems(int $customerId): bool;
}
