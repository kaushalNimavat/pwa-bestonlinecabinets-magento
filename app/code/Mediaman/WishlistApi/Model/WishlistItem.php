<?php

namespace Mediaman\WishlistApi\Model;

use Magento\Framework\Model\AbstractModel;
use Mediaman\WishlistApi\Api\Data\WishlistItemInterface;

class WishlistItem extends AbstractModel implements WishlistItemInterface
{
    const QTY = 'qty';
    const CUSTOMER_ID = 'customer_id';
    const SKU = 'sku';

    /**
     * @return array|mixed|null
     */
    public function getQty()
    {
        return $this->getData(self::QTY);
    }

    /**
     * @param $qty
     * @return WishlistItem|mixed
     */
    public function setQty($qty)
    {
        return $this->setData(self::QTY, $qty);
    }

    /**
     * @return array|mixed|null
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * @param $customerId
     * @return WishlistItem|mixed
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * @return array|mixed|null
     */
    public function getSku()
    {
        return $this->getData(self::SKU);
    }

    /**
     * @param $sku
     * @return WishlistItem|mixed
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }


}
