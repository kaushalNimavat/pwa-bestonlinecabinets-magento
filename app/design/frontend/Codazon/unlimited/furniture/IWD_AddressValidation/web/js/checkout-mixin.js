define([
    'jquery',
    'Magento_Ui/js/modal/alert',
    'mage/translate',
    'Magento_Checkout/js/model/quote'
], function($, modal, $t, quote) {
    'use strict';
    var iwdAddressValidationMixin = {
        _create: function() {
            this.init(this.options);
        },

        init: function(options) {
            this._super(options);
            this.initPaypalButton();
        },

        initPaypalButton: function() {
            var self = this;
            quote.paymentMethod.subscribe(function() {
                if (quote.paymentMethod().method === "braintree_paypal") {
                    self.readAddressQuote();
                    self.validateAddress();
                }
            }, null, 'change');
        },

        onClickNextButton: function() {
            var self = this;
            $(document).on('click touchstart', this.options.nextStepButtonId, (function(e) {
                if(self.validation === 'changed') {
                    e.preventDefault();
                    self.readAddressQuote();
                    self.validateAddress();
                    return;
                }

                if(self.validation == true) {
                    $(self.options.nextStepButtonId).attr('disabled', null);
                    return;
                }

                if(self.validation == false) {
                    e.preventDefault();
                    $(self.options.nextStepButtonId).attr('disabled', 'disabled');
                    self.readAddressQuote();
                    self.validateAddress();
                } else {
                    $(self.options.nextStepButtonId).attr('disabled', null);
                }
            }));
        },

        disableNextButton: function(message) {
            if (this.options.allowInvalidAddress === '0') {
                $(this.options.nextStepButtonId).attr('disabled', 'disabled');
                $(this.options.paypalButton).hide();
            }
            $('.iwd-address-validation-error-message').remove();
            $(this.options.paypalButton).parent()
                .append('<div style="clear:both"></div><div generated="true" class="iwd-address-validation-error-message mage-error">' +
                    $t(message) +
                    '</div>');
            $(this.options.nextStepButtonId).parent()
                .append('<div style="clear:both"></div><div generated="true" class="iwd-address-validation-error-message mage-error">' +
                    $t(message) +
                    '</div>');
        },

        enableBlocks: function(){
            $('.iwd-loader-for-av').hide();
            $(this.options.paypalButton).show();
        },

        showModal:function (response) {
            var self = this;

            $('.iwd-address-validation-popup').removeClass('_show').addClass('_hide');

            modal({
                title: $t(self.options.content.header),
                content: response.modal_content,
                modalClass: "iwd-address-validation-popup",
                buttons:[
                    {
                        text: $t('Continue'),
                        class: '',
                        click: function() {
                            self.updateAddress(response);
                            this.closeModal();
                        }
                    }
                ]
            })
        },
    };

    return function (widget) {
        $.widget('mage.iwdAddressValidationCheckout', widget, iwdAddressValidationMixin);
        return $.mage.iwdAddressValidationCheckout;
    };
});
