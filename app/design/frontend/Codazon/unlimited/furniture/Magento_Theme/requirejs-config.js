var config = {
    config: {
        mixins: {
            'Magento_Ui/js/lib/validation/validator': {
                'Magento_Theme/js/lib/ui-telephone-validation': true
            },
            'mage/validation': {
                'Magento_Theme/js/mage-telephone-validation': true
            }
        }
    }
}
