define([
    'jquery'
], function($) {
    return function(validator) {
        validator.addRule(
            'checkout-telephone-validation',
            function (value) {
                return /^[0-9]{10}$/.test(value);
            },
                $.mage.__('Please enter exactly ten digits.')
            );
        return validator;
    }
});
