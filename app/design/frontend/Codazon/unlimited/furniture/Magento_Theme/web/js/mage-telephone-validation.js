define(['jquery'],
    function($) {
    'use strict';
    return function() {
        $.validator.addMethod(
            'customer-telephone-validation',
            function(value, element) {
                return /^[0-9]{10}$/.test(value);
            },
            $.mage.__('Please enter exactly ten digits.')
        )
    }
});
